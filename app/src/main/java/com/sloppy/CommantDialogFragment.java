package com.sloppy;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sloppy.aapbdLib.AlertMessage;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.NetInfo;
import com.sloppy.aapbdLib.PersistData;
import com.sloppy.aapbdLib.PersistentUser;
import com.sloppy.adapter.BrandListAdapter;
import com.sloppy.model.ProCommentAddResponse;
import com.sloppy.model.ProCommentListResponse;
import com.sloppy.model.ProCommentListResultInfo;
import com.sloppy.model.ProCommentListUserDetailsInfo;
import com.sloppy.model.ProductListByBrandResponse;
import com.sloppy.model.TaggableUsers;
import com.sloppy.utils.AAPBDHttpClient;
import com.sloppy.utils.AllURL;
import com.sloppy.utils.AppConstant;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.concurrent.Executors;

/**
 * Created by hp on 7/28/2016.
 */
public class CommantDialogFragment extends DialogFragment {

    private Context con;
    private View view;
    private String tagged_user_id,tagUserName;
    private Boolean tagPos;
    private TextView tvDataNotFound;
    private ListView listViewProComment;
    private EditText etWriteComment;
    private Dialog dialogTag;
    private ProCommentListResponse proCommentListResponse;
    private ProCommentAddResponse proCommentAddResponse;
    private ProCommentListAdapter proCommentListAdapter;
    private DialogInterface.OnDismissListener onDismissListener;
    private TagUseListAdapter tagAdapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialog_comments, container, true);
        con = getActivity();
        initUi();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!PersistentUser.isLogged(con)) {
            callProductCommentListAPI(con, AppConstant.productIDfeedRow, "demo");
        } else {
            callProductCommentListAPI(con, AppConstant.productIDfeedRow, "all");
        }
    }

    private void initUi() {
        //--------Call 6. Single product details of a product id.----------------
        tvDataNotFound = (TextView) view.findViewById(R.id.tvDataNotFound);
        TextView tvComment = (TextView) view.findViewById(R.id.tvComment);
        TextView tvTag = (TextView) view.findViewById(R.id.tvTag);

        ImageView imgCommentCross = (ImageView) view.findViewById(R.id.imgCommentCross);
        TextView tvSendComment = (TextView) view.findViewById(R.id.tvSendComment);
        listViewProComment = (ListView) view.findViewById(R.id.listViewProComment);
        etWriteComment = (EditText) view.findViewById(R.id.etWriteComment);
        /**
         * ---------------Call Product Comment List Adapter--------------------
         */
        AppConstant.setFont(con, tvComment, "regular");
        AppConstant.setFont(con, tvDataNotFound, "regular");
        AppConstant.setFont(con, tvSendComment, "bold");
        AppConstant.setFont(con, etWriteComment, "regular");

        imgCommentCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });
        tvSendComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(etWriteComment.getText().toString())) {
                    AppConstant.alertDialoag(con,"Alert",getString(R.string.write_comment),"Close");
                } else {

                    /**
                     * -------------------Call 11. Product Comments Add API------------------
                     */
                    callAddProductCommentAPI(AppConstant.productIDfeedRow, etWriteComment.getText().toString(),tagged_user_id);
                }
            }
        });

        tvTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tagDialog();
//                TagDialogFragment tagDialog= new TagDialogFragment();
//                tagDialog.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar);
//                tagDialog.show(getActivity().getFragmentManager(),"offerdialog");
            }
        });
    }

    /**
     * -------------------12. Product Comments List  API------------------
     */
    public void callProductCommentListAPI(final Context con, final String product_id, final String userType) {
        /**
         * ---------------check internet first------------
         */
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }
         /**
          * ----------------Show Busy Dialog -----------------------------------------
          */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
        /**
         * =========================Start Thread====================================================
         */
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            String msg = "";
            String response = "";

            @Override
            public void run() {
                // You can performed your task here.

                try {
                    Log.e("ProCommentList URL", AllURL.getProCommentList(product_id, userType));
                    //-------------Hit Server---------------------
                    response = AAPBDHttpClient.get(AllURL.getProCommentList(product_id, userType)).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();
                    Log.e("ProCommentList Response", ">>" + response);

                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        /**
                         * -------------Here we do UI related tasks inside run() method of runOnUiThread()-------------------
                         */

                        //-------Stop Busy Dialog-----
                        if (busy != null) {
                            busy.dismis();
                        }
                        Gson gson = new Gson();
                        proCommentListResponse = gson.fromJson(response, ProCommentListResponse.class);
                        /**
                         * ---------main Ui related work--------------
                         */
                        if (proCommentListResponse.getStatus().equalsIgnoreCase("1")) {
                            Log.e("CommentList size", ">>" + proCommentListResponse.getResult().size());
                            if (proCommentListResponse.getResult().size() > 0) {
                                tvDataNotFound.setVisibility(View.GONE);
                                listViewProComment.setVisibility(View.VISIBLE);
                                proCommentListAdapter = new ProCommentListAdapter(con);
                                listViewProComment.setAdapter(proCommentListAdapter);
                                AppConstant.commentCount=String.valueOf(proCommentListResponse.getResult().size());
                                proCommentListAdapter.notifyDataSetChanged();
                            } else {
                                listViewProComment.setVisibility(View.GONE);
                                tvDataNotFound.setVisibility(View.VISIBLE);
                                proCommentListAdapter = new ProCommentListAdapter(con);
                                listViewProComment.setAdapter(proCommentListAdapter);
                                proCommentListAdapter.notifyDataSetChanged();
                            }

                        } else {
                            msg = proCommentListResponse.getMsg();
                            AlertMessage.showMessage(con, "ProCommentList", msg);
                        }
                    }
                });
            }
        });
    }

    private void tagDialog() {

         dialogTag = new Dialog(con);
        dialogTag.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogTag.setContentView(R.layout.tag_dialog_fragment);
        dialogTag.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        dialogTag.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        ListView lvTagableUser= (ListView) dialogTag.findViewById(R.id.lvTagableUser);
         tagAdapter = new TagUseListAdapter(con);
        lvTagableUser.setAdapter(tagAdapter);
        tagAdapter.notifyDataSetChanged();

        dialogTag.show();
    }



    /**
     * -------------------11. Product Comments Add API------------------
     */
    private void callAddProductCommentAPI(final String product_id, final String comment_text,final String tagged_user_id) {
  /*
   * ---------------check internet first------------
   */
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }

  /*
   * ----------------Show Busy Dialog -----------------------------------------
   */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
  /*
   *
   *----------------------Start Thread-------------------------------------------
   *
   */
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            String msg = "";
            String response = "";

            @Override
            public void run() {
                // You can performed your task here.
                try {

                    Log.w("AddProCommList URL", AllURL.getAddProCommentList(product_id, comment_text,tagged_user_id));
                    //-------------Hit Server---------------------
                    response = AAPBDHttpClient.get(AllURL.getAddProCommentList(product_id, comment_text,tagged_user_id)).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();

                    //----------------Persist response with the help of Gson----------------
                    Log.w("AddProCommList Response", ">>" + response);
                    Gson gson = new Gson();
                    proCommentAddResponse = gson.fromJson(response, ProCommentAddResponse.class);

                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        /**
                         * -------------Here we do UI related tasks inside run() method of runOnUiThread()-------------------
                         */

                        //-------Stop Busy Dialog-----
                        if (busy != null) {
                            busy.dismis();
                        }
                        /**
                         * ---------main Ui related work--------------
                         */
                        if (proCommentAddResponse.getStatus().equalsIgnoreCase("0")) {
                            msg = proCommentAddResponse.getMsg();
                            AlertMessage.showMessage(con, "AddProCommList", msg);
                        } else {
                            msg = proCommentAddResponse.getMsg();
                            Toast.makeText(con,msg,Toast.LENGTH_SHORT).show();
                            /**
                             * -------Success message Show---------------------
                             */
//                            Toast.makeText(con, "AddProCommList " + proCommentAddResponse.getMsg(), Toast.LENGTH_LONG).show();
//                            etWriteComment.setText("");

                            /**
                             * --------Set data locally in the List View---------------------
                             */
                            //-------Create an object same as array list---------------

                            ProCommentListResultInfo newProComment = new ProCommentListResultInfo();
                            //---------set data to creat new row of array list
                            newProComment.setId(proCommentAddResponse.getResult().getId());
                            if (TextUtils.isEmpty(tagged_user_id)) {
                                newProComment.setComments_text(proCommentAddResponse.getResult().getComments_text());
                            }else{
                                newProComment.setComments_text(proCommentAddResponse.getResult().getComments_text()+" @" +tagUserName);
                            }
                            newProComment.setCreated_at(proCommentAddResponse.getResult().getCreated_at());
                            //-------new object create for new array list belongs to the main list
                            ProCommentListUserDetailsInfo newuserdetails = new ProCommentListUserDetailsInfo();
                            newuserdetails.setName(proCommentAddResponse.getResult().getUserdetails().get(0).getName());
                            newuserdetails.setProfile_image(proCommentAddResponse.getResult().getUserdetails().get(0).getProfile_image());
                            newProComment.getUserdetails().add(newuserdetails);
                            //-------add row data in the List----------
                            tvDataNotFound.setVisibility(View.GONE);
                            listViewProComment.setVisibility(View.VISIBLE);
                            proCommentListResponse.getResult().add(newProComment);
                            AppConstant.commentCount=String.valueOf(proCommentListResponse.getResult().size());

                            //--------refresh adapter-------------
                            proCommentListAdapter.notifyDataSetChanged();
                            etWriteComment.setText("");
                        }
                    }
                });
            }
        });
    }


    public void setOnDismissListener(DialogInterface.OnDismissListener onDismissListener) {

        this.onDismissListener = onDismissListener;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (onDismissListener != null) {
            onDismissListener.onDismiss(dialog);
        }
    }

    /**
     * -----------------------Product comment List Adapter-----------------------
     */
    public class ProCommentListAdapter extends ArrayAdapter<ProCommentListResultInfo> {
        Context context;

        ProCommentListAdapter(Context context) {
            super(context, R.layout.row_comments_feed, proCommentListResponse.getResult());
            this.context = context;
        }

        @Override
        public View getView(final int position, View commentView, ViewGroup parent) {
            ImageView imgComMessages;
            TextView tvComUName, tvMessagese, tvMessageCreatedTime;
            /**
             * ------------------Set the row layout-----------------------
             */
            if (commentView == null) {
                final LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                commentView = vi.inflate(R.layout.row_comments_feed, null);
            }

            if (position < proCommentListResponse.getResult().size()) {

                final ProCommentListResultInfo query = proCommentListResponse.getResult().get(position);
                /**
                 * ---------------------Initialization of row field------------------------------
                 */
                imgComMessages = (ImageView) commentView.findViewById(R.id.imgComMessages);
//                tvComUName = (TextView) commentView.findViewById(R.id.tvComUName);
                tvMessagese = (TextView) commentView.findViewById(R.id.tvMessagese);
                tvMessageCreatedTime = (TextView) commentView.findViewById(R.id.tvMessageCreatedTime);

                try {
                    /**
                     * -----------------Set data on field of Comment row------------------
                     */
                    Picasso.with(context).load(query.getUserdetails().get(0).getProfile_image())
                            .placeholder(R.drawable.person).error(R.drawable.person).into(imgComMessages);
//                    tvComUName.setText(query.getUserdetails().get(0).getName());
                    if (query.getTagged_user().size()>0){
                        tvMessagese.setText(query.getComments_text()+" @"+query.getTagged_user().get(0).getUsername());
                    }else{
                        tvMessagese.setText(query.getComments_text());
                    }

                    tvMessageCreatedTime.setText(query.getCreated_at());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            return commentView;

        }

    }

    /**
     * -----------------------Product comment List Adapter-----------------------
     */
    public class TagUseListAdapter extends ArrayAdapter<TaggableUsers> {
        Context context;

        TagUseListAdapter(Context context) {
            super(context, R.layout.row_tag_list, AppConstant.tagUserList);
            this.context = context;
        }

        @Override
        public View getView(final int position, View commentView, ViewGroup parent) {
            final ImageView imgTagUser,tagTik;
            TextView tvTagUser;

            /**
             * ------------------Set the row layout-----------------------
             */
            if (commentView == null) {
                final LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                commentView = vi.inflate(R.layout.row_tag_list, null);
            }

            if (position < AppConstant.tagUserList.size()) {

                final TaggableUsers query = AppConstant.tagUserList.get(position);
                /**
                 * ---------------------Initialization of row field------------------------------
                 */
                imgTagUser = (ImageView) commentView.findViewById(R.id.imgTagUser);
                tagTik = (ImageView) commentView.findViewById(R.id.tagTik);
                tvTagUser = (TextView) commentView.findViewById(R.id.tvTagUser);


//                try {
                    /**
                     * -----------------Set data on field of Comment row------------------
                     */
                    if (query.isFlag()){
                        tagTik.setVisibility(View.VISIBLE);
                    } else {
                        tagTik.setVisibility(View.GONE);
                    }
                    Picasso.with(context).load(query.getProfile_image())
                            .placeholder(R.drawable.person).error(R.drawable.person).into(imgTagUser);
                    tvTagUser.setText(query.getUsername());

//                } catch (Exception e) {
//                    e.printStackTrace();
//                }

                commentView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        for (int i=0; i<AppConstant.tagUserList.size(); i++){
                            if (i==position){
                                tagged_user_id=query.getId();
                                tagUserName=query.getUsername();
                                AppConstant.tagUserList.get(position).setFlag(true);
                                tagAdapter.notifyDataSetChanged();
                            }else{
                                AppConstant.tagUserList.get(position).setFlag(false);
                                tagAdapter.notifyDataSetChanged();
                            }
                        }

                        dialogTag.dismiss();
                    }
                });

            }

            return commentView;

        }

    }
}
