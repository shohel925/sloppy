package com.sloppy;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.paypal.android.MEP.CheckoutButton;
import com.paypal.android.MEP.PayPal;
import com.paypal.android.MEP.PayPalActivity;
import com.paypal.android.MEP.PayPalAdvancedPayment;
import com.paypal.android.MEP.PayPalPayment;
import com.paypal.android.MEP.PayPalReceiverDetails;
import com.sloppy.aapbdLib.AlertMessage;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.NetInfo;
import com.sloppy.aapbdLib.PersistData;
import com.sloppy.adapter.ProductListByTypeAdapter;
import com.sloppy.model.BuyNowResponse;
import com.sloppy.model.LikeUnlikeResponse;
import com.sloppy.model.ProductAndUserDetailsInfo;
import com.sloppy.model.ProductListByTypeResponse;
import com.sloppy.utils.AAPBDHttpClient;
import com.sloppy.utils.AllURL;
import com.sloppy.utils.AppConstant;
import com.squareup.picasso.Picasso;

import java.math.BigDecimal;
import java.util.concurrent.Executors;

/**
 * Created by hp on 5/4/2016.
 */
public class BuyNowFragment  extends BaseFragment{
    private Context con;
    private View view;
    private ImageView imgBacbuy,imgLikeBuy;
    private TextView tvSize1,tvLikeCountBuy;
    private RelativeLayout rlMeetInPer,rlShipping;
    private ProductAndUserDetailsInfo feedRowObject;
    private LinearLayout linearLayoutPayNow;
    private String payKey="";
    private float totalPriceBuy=0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        con=getActivity();
        view=inflater.inflate(R.layout.fragment_buy_now, container, false);

        initUI();
        return view;
    }

    private void initUI() {
        /**
         * --------------------Initialization-----------------
         */
        feedRowObject=AppConstant.feedRowObject;
        linearLayoutPayNow=(LinearLayout)view.findViewById(R.id.linearLayoutPayNow);
        LinearLayout lLCancelBuy = (LinearLayout) view.findViewById(R.id.lLCancelBuy);
        ImageView imgProductBuy = (ImageView) view.findViewById(R.id.imgProductBuy);
         imgLikeBuy = (ImageView) view.findViewById(R.id.imgLikeBuy);
        ImageView imgCommentBuy = (ImageView) view.findViewById(R.id.imgCommentBuy);
        ImageView imgShareBuy = (ImageView) view.findViewById(R.id.imgShareBuy);
        ImageView imgMenuBuy = (ImageView) view.findViewById(R.id.imgMenuBuy);
        imgBacbuy = (ImageView) view.findViewById(R.id.imgBacbuy);
        TextView tvProductNameBuy = (TextView) view.findViewById(R.id.tvProductNameBuy);
        TextView tvBrandNameBuy = (TextView) view.findViewById(R.id.tvBrandNameBuy);
        TextView tvOriginalPriceBuy = (TextView) view.findViewById(R.id.tvOriginalPriceBuy);
        TextView tvListingPriceBuy = (TextView) view.findViewById(R.id.tvListingPriceBuy);
        tvLikeCountBuy = (TextView) view.findViewById(R.id.tvLikeCountBuy);
        final TextView tvCommentCountBuy = (TextView) view.findViewById(R.id.tvCommentCountBuy);
        TextView tvTotalPriBuy = (TextView) view.findViewById(R.id.tvTotalPriBuy);
        TextView tvShippingCost = (TextView) view.findViewById(R.id.tvShippingCost);
        TextView tvPriceBuy = (TextView) view.findViewById(R.id.tvPriceBuy);
        tvSize1 = (TextView) view.findViewById(R.id.tvSize1);
        rlMeetInPer=(RelativeLayout) view.findViewById(R.id.rlMeetInPer);
        rlShipping=(RelativeLayout) view.findViewById(R.id.rlShipping);
        /**
         * -----------------Set data--------------------
         */
        if (feedRowObject.getSize_info()!=null){
            tvSize1.setText(feedRowObject.getSize_info().getSize());
        } else {
            tvSize1.setText(feedRowObject.getSize());
        }
        if (feedRowObject.getDelivery_type().equalsIgnoreCase("shipping")) {
            rlMeetInPer.setVisibility(View.GONE);
            rlShipping.setVisibility(View.VISIBLE);
        }else if (feedRowObject.getDelivery_type().equalsIgnoreCase("men")) {
            rlShipping.setVisibility(View.GONE);
            rlMeetInPer.setVisibility(View.VISIBLE);
        }
        Handler handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                double appownerAmonut=(100*15)/100;
                Log.e("appownerAmonut",""+appownerAmonut);
            }
        },1000);

        PayPal ppObj = PayPal.initWithAppID(getActivity(), "APP-80W284485P519543T", PayPal.ENV_SANDBOX);
        ppObj.setShippingEnabled(true);
        CheckoutButton launchPayPalButton = ppObj.getCheckoutButton(getActivity(), PayPal.BUTTON_194x37, CheckoutButton.TEXT_PAY);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.CENTER_HORIZONTAL);
        launchPayPalButton.setLayoutParams(params);
        launchPayPalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float appownerAmonut=(totalPriceBuy*15)/100;
                Log.e("appownerAmonut",""+appownerAmonut);
                float sellerAmount=totalPriceBuy-appownerAmonut;
                PayPalReceiverDetails receiver1=new PayPalReceiverDetails();
                BigDecimal obj_0 = new BigDecimal(appownerAmonut);
                receiver1.setSubtotal(obj_0);
                receiver1.setRecipient("diudeveloper-buyer@gmail.com");
                receiver1.setMerchantName(getString(R.string.app_name));
                PayPalReceiverDetails receiver2=new PayPalReceiverDetails();
                BigDecimal obj_01 = new BigDecimal(sellerAmount);
                receiver2.setSubtotal(obj_01);
                receiver2.setRecipient("diudeveloper-facilitator@gmail.com");
                receiver2.setMerchantName(getString(R.string.app_name));

                PayPalAdvancedPayment advPayment = new PayPalAdvancedPayment();
                advPayment.setCurrencyType("USD");
                advPayment.getReceivers().add(receiver1);
                advPayment.getReceivers().add(receiver2);
                Intent paypalIntent = PayPal.getInstance().checkout(advPayment, getActivity());
                startActivityForResult(paypalIntent, 1);
            }
        });
        linearLayoutPayNow.addView(launchPayPalButton);
        tvProductNameBuy.setText(feedRowObject.getName());
        tvBrandNameBuy.setText(feedRowObject.getBrand_info().get(0).getName());
        tvOriginalPriceBuy.setText(AppConstant.getCurrencySymbol(feedRowObject.getSeller_info().get(0).getCurrency()) + feedRowObject.getOriginal_price());
        tvOriginalPriceBuy.setPaintFlags(tvOriginalPriceBuy.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        tvListingPriceBuy.setText(AppConstant.getCurrencySymbol(feedRowObject.getSeller_info().get(0).getCurrency()) + feedRowObject.getListing_price());
        tvLikeCountBuy.setText(feedRowObject.getLike_count());
        tvCommentCountBuy.setText(feedRowObject.getComments_count());
        String price = String.format("%.02f", Float.parseFloat(feedRowObject.getListing_price()));
        tvPriceBuy.setText(AppConstant.getCurrencySymbol(feedRowObject.getSeller_info().get(0).getCurrency()) + price);

        if (!TextUtils.isEmpty(feedRowObject.getDelivery_charge())) {
            String deleveryCharge = String.format("%.02f", Float.parseFloat(feedRowObject.getDelivery_charge()));
            tvShippingCost.setText(AppConstant.getCurrencySymbol(feedRowObject.getSeller_info().get(0).getCurrency()) +deleveryCharge );
            totalPriceBuy = Float.parseFloat(feedRowObject.getListing_price()) + Float.parseFloat(feedRowObject.getDelivery_charge());
            String totalPrice = String.format("%.02f",totalPriceBuy );
            tvTotalPriBuy.setText(AppConstant.getCurrencySymbol(feedRowObject.getSeller_info().get(0).getCurrency()) + totalPrice);
        } else {
            totalPriceBuy = Float.parseFloat(feedRowObject.getListing_price());
            tvShippingCost.setText(AppConstant.getCurrencySymbol(feedRowObject.getSeller_info().get(0).getCurrency()) + "0.00");
            tvTotalPriBuy.setText(AppConstant.getCurrencySymbol(feedRowObject.getSeller_info().get(0).getCurrency()) + feedRowObject.getListing_price());
        }
        if (feedRowObject.getIs_liked().equalsIgnoreCase("0")) {
            imgLikeBuy.setImageResource(R.drawable.love_unliked);
        } else {
            imgLikeBuy.setImageResource(R.drawable.love_liked);
        }

        imgLikeBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callLikeUnlikeAPI(feedRowObject.getId());
            }
        });

        tvBrandNameBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppConstant.brandID=feedRowObject.getBrand_info().get(0).getId();
                AppConstant.brandName=feedRowObject.getBrand_info().get(0).getName();
                myCommunicator.addContentFragment(new BrandFragment(), true);
            }
        });

        imgMenuBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MenuFragmentDialog dialogMenu= new MenuFragmentDialog();
                dialogMenu.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar);
                dialogMenu.show(getActivity().getFragmentManager(),"");
            }
        });
        Picasso.with(con).load(feedRowObject.getPhoto_one())
                .placeholder(R.drawable.person).error(R.drawable.person).into(imgProductBuy);

        imgBacbuy.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        imgShareBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        lLCancelBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        imgCommentBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AppConstant.productIDfeedRow = feedRowObject.getId();
                AppConstant.tagUserList=feedRowObject.getTaggable_users();
                CommantDialogFragment dialogComment = new CommantDialogFragment();
                dialogComment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        tvCommentCountBuy.setText(AppConstant.commentCount);
                    }
                });
                dialogComment.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar);
                dialogComment.show(getActivity().getFragmentManager(), "");
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode==1){
            switch (resultCode) {
                case Activity.RESULT_OK:
                    // The payment succeeded
                    payKey = data.getStringExtra(PayPalActivity.EXTRA_PAY_KEY);
                    buyNowAPI(AllURL.addProductSellURL(feedRowObject.getId(),""+totalPriceBuy));
                    Log.e("Result Ok: Paykey is:: ",""+payKey);
                    // Tell the user their payment succeeded
                    break;
                case Activity.RESULT_CANCELED:
                    Log.e("Result Cancelled!!!! ",""+payKey);
//                getActivity().finish();
                    // The payment was canceled
                    // Tell the user their payment was canceled
                    break;
                case PayPalActivity.RESULT_FAILURE:
                    // The payment failed -- we get the error from the EXTRA_ERROR_ID
                    // and EXTRA_ERROR_MESSAGE
                    String errorID = data.getStringExtra(PayPalActivity.EXTRA_ERROR_ID);
                    String errorMessage = data.getStringExtra(PayPalActivity.EXTRA_ERROR_MESSAGE);
//                    Log.e("Result Failure:: Error Id!!!! ",""+errorID);
                    System.out.println("Result Failure:: Error Id!!!!"+errorID);
                    System.out.println("Result Failure:: Error Message!!!!"+errorMessage);
//                confirmSubscription.finish();
                    // Tell the user their payment was failed.
                    break;
            }
        }
    }

    /**
     * -------------------10. Like/Unlike Product API------------------
     */
    public void callLikeUnlikeAPI(final String product_id) {
  /*
   * ---------------check internet first------------
   */
        if (!NetInfo.isOnline(con)) {
            AppConstant.alertDialoag(con, con.getString(R.string.status), con.getString(R.string.checkInternet), con.getString(R.string.ok));
            return;
        }

  /*
   * ----------------Show Busy Dialog -----------------------------------------
   */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
        /**
         *----------------------Start Thread-------------------------------------------
         */
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            String msg = "";
            String response = "";
            LikeUnlikeResponse likeUnlikeResponse;

            @Override
            public void run() {
                //--------------- We can performed your task here.-----------------

                try {
                    Log.i("Like/Unlike URL", AllURL.getLikeUnlike(product_id));
                    //-------------Hit Server---------------------
                    response = AAPBDHttpClient.get(AllURL.getLikeUnlike(product_id)).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();

                    //----------------Persist response with the help of Gson----------------
                    Log.i("Like/Unlike Response", ">>" + response);
                    Gson gson = new Gson();
                    likeUnlikeResponse = gson.fromJson(response, LikeUnlikeResponse.class);

                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        /**
                         * -------------Here we do UI related tasks inside run() method of runOnUiThread()-------------------
                         */

                        //-------Stop Busy Dialog-----
                        if (busy != null) {
                            busy.dismis();
                        }
                        /**
                         * ---------main Ui related work--------------
                         */
                        if (likeUnlikeResponse != null) {
                            Toast.makeText(con,likeUnlikeResponse.getMsg(),Toast.LENGTH_LONG).show();
                            if (likeUnlikeResponse.getMsg().equalsIgnoreCase("liked")) {
                                imgLikeBuy.setImageResource(R.drawable.love_liked);
                                tvLikeCountBuy.setText(likeUnlikeResponse.getLikes_count());
                            } else if (likeUnlikeResponse.getMsg().equalsIgnoreCase("unliked")) {
                                imgLikeBuy.setImageResource(R.drawable.love_unliked);
                                tvLikeCountBuy.setText(likeUnlikeResponse.getLikes_count());
                            }
                        }
                    }
                });
            }
        });
    }

    public void buyNowAPI(final String url) {
      /*
       * ---------------check internet first------------
       */
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }
         /*
          * ----------------Show Busy Dialog -----------------------------------------
          */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
      /*
       *----------------------Start Thread-------------------------------------------
       */
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            String msg = "";
            String response = "";

            @Override
            public void run() {

                // You can performed your task here.
                try {
                    //-------------Hit Server---------------------
                    response = AAPBDHttpClient.get(url).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();
                    Log.e("List Response", ">>" + response);
                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        TextView tvDataNotFound= (TextView) view.findViewById(R.id.tvDataNotFound);
                        /**
                         * -------------Here we do UI related tasks inside run() method of runOnUiThread()-------------------
                         */
                        //-------Stop Busy Dialog-----
                        if (busy != null) {
                            busy.dismis();
                        }
                        //----------------Persist response with the help of Gson----------------
                        Gson gson = new Gson();
                      BuyNowResponse  buyNowResponse = gson.fromJson(response, BuyNowResponse.class);
                        /**
                         * ---------main Ui related work--------------
                         */
                        if (buyNowResponse.getStatus().equalsIgnoreCase("1")) {
                            Toast.makeText(con,"Product Buy successful",Toast.LENGTH_SHORT).show();
                            getActivity().onBackPressed();

                        } else {
                            msg = buyNowResponse.getMsg();
                            AlertMessage.showMessage(con, "ProCommentList", msg);
                        }

                    }
                });

            }

        });

    }
}
