package com.sloppy.techlovejump.gcm;

/* http://techlovejump.in/2013/11/android-push-notification-using-google-cloud-messaging-gcm-php-google-play-service-library/
 * techlovejump.in
 * tutorial link
 *
 *  */

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.google.android.gms.gcm.GoogleCloudMessaging;


public class GcmIntentService extends IntentService {
	Context context;
	public static final int NOTIFICATION_ID = 1;
	private NotificationManager mNotificationManager;
	NotificationCompat.Builder builder;
	public static final String TAG = "GCM Demo";

	public GcmIntentService() {
		super("EnBTaxiUserGcmIntentService");
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		
		context = this;
		
		final Bundle extras = intent.getExtras();
		final String msg = intent.getStringExtra("message");
		final GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		final String messageType = gcm.getMessageType(intent);

		if (!extras.isEmpty()) {

			if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
				// sendNotification("Send error: " + extras.toString());
			}
			else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
				//sendNotification("Deleted messages on server: " + extras.toString());
				// If it's a regular GCM message, do some work.
			}
			else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {

//				Gson g = new Gson();
//				PushData pushData = g.fromJson(msg, PushData.class);
//				
//				if((pushData.getMessageType().equalsIgnoreCase("order_accept"))||(pushData.getMessageType().equalsIgnoreCase("driver_arrived"))||(pushData.getMessageType().equalsIgnoreCase("trip_begin"))||(pushData.getMessageType().equalsIgnoreCase("trip_end"))){
//					if (AppConstant.isTracking) {
//						Intent trackingIntent = new Intent();
//						trackingIntent.setAction(AppConstant.ENB_TAXI_USER_TRACING_BROADCAST_ACTION);
//						trackingIntent.addCategory(Intent.CATEGORY_DEFAULT);
//						trackingIntent.putExtra(AppConstant.GCM_TRACKING_INTENT, pushData.getMessageType());
//						sendBroadcast(trackingIntent);
//					}
//					else {
//						buildLocalNotification(pushData.getMessageType());
//					}
//				}else if(pushData.getMessageType().equalsIgnoreCase("sendMessage")){
//					JsonObject jo = pushData.getMessage();
//					PushChatDM pcm = g.fromJson(jo, PushChatDM.class);
//					//Load chat history
//					if(ChatList.getChatList().size() == 0){
//						loadChatHistory(pcm.getUser_id(),pcm.getChat_sender(),pcm.getChat_type());
//					}
//					else{
//						ChatMessage cm = new ChatMessage();
//						cm.setChat_room_id(pcm.getChat_room_id());
//						cm.setChat_sender(pcm.getChat_sender());
//						cm.setChat_text(pcm.getChat_text());
//						cm.setCreatedAt(pcm.getTimeStamp());
//						cm.setChat_type(pcm.getChat_type());
//						
//						cm.setUser_id(PersistData.getStringData(context, AppConstant.userid));
//						
//						ChatList.addChatMessage(cm);
//					}
//
//					if (AppConstant.isChatRunning) {
//						Intent chatIntent = new Intent();
//						chatIntent.setAction(AppConstant.ENB_TAXI_USER_BROADCAST_ACTION);
//						chatIntent.addCategory(Intent.CATEGORY_DEFAULT);
//						chatIntent.putExtra(AppConstant.GCM_MESSAGE_INTENT, jo.toString());
//						sendBroadcast(chatIntent);
//					}
//					else {
//						buildLocalNotification(pcm);
//					}
//				}
				
				

			}
		}
		WakefulBroadcastReceiver.completeWakefulIntent(intent);
	}

//	private void loadChatHistory(String user_id, String partnerUserId, String chat_type) {
//		// TODO Auto-generated method stub
//		try{
//			String url = AllURL.getChatHistorySingle(user_id,partnerUserId, chat_type);
//			
//			HttpClient httpclient = new DefaultHttpClient();
//			HttpResponse response = httpclient.execute(new HttpGet(url));
//			StatusLine statusLine = response.getStatusLine();
//			if(statusLine.getStatusCode() == HttpStatus.SC_OK){
//				ByteArrayOutputStream out = new ByteArrayOutputStream();
//				response.getEntity().writeTo(out);
//				String responseString = out.toString();
//				Gson g = new Gson();
//				ChatHistoryDM chd = g.fromJson(responseString, ChatHistoryDM.class);
//				if(chd.isStatus()){
//					ChatList.setChatList(chd.getDetails());
//				}
//				out.close();
//				//..more logic
//			} else{
//				//Closes the connection.
//				response.getEntity().getContent().close();
////	        throw new IOException(statusLine.getReasonPhrase());
//			}
//		}
//		catch(Exception ex){
//			ex.printStackTrace();
//		}
//	}

//	private void sendNotification(String msg) {
//		mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//		final Intent myintent = new Intent(this, MainActivity.class);
//		myintent.putExtra("message", msg);
//		final PendingIntent contentIntent = PendingIntent.getActivity(this, 0, myintent, PendingIntent.FLAG_UPDATE_CURRENT);
//
//		final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this).setSmallIcon(R.drawable.ic_launcher)
//				.setContentTitle("GCM Notification").setStyle(new NotificationCompat.BigTextStyle().bigText(msg)).setContentText(msg);
//
//		mBuilder.setContentIntent(contentIntent);
//		mNotificationManager.notify(GcmIntentService.NOTIFICATION_ID, mBuilder.build());
//	}

	// Build local notification for chat
//	private void buildLocalNotification(PushChatDM pcm) {
//		// if message is for new chat send notification
//		
//		String showMsg = "Driver: " + pcm.getMessage();
//		mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
//		Intent intent = new Intent(this, ChatActivity.class);
//		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
//		
//		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this).setSmallIcon(R.drawable.ic_launcher).setContentTitle("New Message in " + context.getApplicationInfo().loadLabel(context.getPackageManager()))
//				.setStyle(new NotificationCompat.BigTextStyle().bigText(showMsg)).setOngoing(false).setContentText(showMsg);
//		
//		mBuilder.setContentIntent(contentIntent);
//		Notification notification = mBuilder.build();
////					notification.flags = Notification.DEFAULT_LIGHTS | Notification.FLAG_AUTO_CANCEL;
////					notification.ledARGB=0xffffffff; //color, in this case, white
////					notification.ledOnMS=1000; //light on in milliseconds
////					notification.ledOffMS=4000; //light off in milliseconds
//		
////					notification.defaults |= Notification.DEFAULT_SOUND;
////					notification.defaults |= Notification.DEFAULT_VIBRATE;
//		
//		long[] vibrate = {0,100,200,300};
//		notification.vibrate = vibrate;
//		notification.defaults |= Notification.DEFAULT_LIGHTS;
//		notification.defaults |= Notification.DEFAULT_SOUND; 
//		notification.flags |= Notification.FLAG_AUTO_CANCEL;
//		
//		mNotificationManager.notify(NOTIFICATION_ID, notification);
//		
//	}
	
//	private void buildLocalNotification(String pcm) {
//		// if message is for new chat send notification
//		
//		String showMsg = "Driver: " + pcm;
//		mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
//		Intent intent = new Intent(this, ChatActivity.class);
//		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
//		
//		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this).setSmallIcon(R.drawable.ic_launcher).setContentTitle("New Message in " + context.getApplicationInfo().loadLabel(context.getPackageManager()))
//				.setStyle(new NotificationCompat.BigTextStyle().bigText(showMsg)).setOngoing(false).setContentText(showMsg);
//		
//		mBuilder.setContentIntent(contentIntent);
//		Notification notification = mBuilder.build();
////					notification.flags = Notification.DEFAULT_LIGHTS | Notification.FLAG_AUTO_CANCEL;
////					notification.ledARGB=0xffffffff; //color, in this case, white
////					notification.ledOnMS=1000; //light on in milliseconds
////					notification.ledOffMS=4000; //light off in milliseconds
//		
////					notification.defaults |= Notification.DEFAULT_SOUND;
////					notification.defaults |= Notification.DEFAULT_VIBRATE;
//		
//		long[] vibrate = {0,100,200,300};
//		notification.vibrate = vibrate;
//		notification.defaults |= Notification.DEFAULT_LIGHTS;
//		notification.defaults |= Notification.DEFAULT_SOUND; 
//		notification.flags |= Notification.FLAG_AUTO_CANCEL;
//		
//		mNotificationManager.notify(NOTIFICATION_ID, notification);
//		
//	}

}