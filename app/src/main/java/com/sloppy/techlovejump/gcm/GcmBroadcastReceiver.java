package com.sloppy.techlovejump.gcm;

/* http://techlovejump.in/2013/11/android-push-notification-using-google-cloud-messaging-gcm-php-google-play-service-library/
 * techlovejump.in
 * tutorial link
 *
 *  */

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.KeyguardManager;
import android.app.KeyguardManager.KeyguardLock;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.text.TextUtils;
import android.util.Log;

import com.sloppy.LoginActivity;
import com.sloppy.R;


public class GcmBroadcastReceiver extends WakefulBroadcastReceiver {

	PowerManager pm;
	WakeLock wl;
	KeyguardManager km;
	KeyguardLock kl;
	private Context con;

	private NotificationManager mNotificationManager;
	NotificationCompat.Builder builder;
	String msgType;

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub

		con = context;

		String pushDataInfo = intent.getStringExtra("message");
		Log.e("pushDataInfo", ":"+pushDataInfo);
		
		sendNotification(pushDataInfo);

	}


	// Put the GCM message into a notification and post it.
	Intent intent;
	private void sendNotification(String msg) {

		mNotificationManager = (NotificationManager) con.getSystemService(Context.NOTIFICATION_SERVICE);
		final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(con);
		final Resources res = con.getResources();
		final int icon = R.drawable.ic_launcher;
		mBuilder.setSmallIcon(icon);
		mBuilder.setContentTitle(res.getString(R.string.app_name));

		mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(msg));
		mBuilder.setContentText(msg);
		//mBuilder.setContentIntent(contentIntent);
		mBuilder.setAutoCancel(true);
		mBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
		mBuilder.setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 });

		mNotificationManager.notify(0, mBuilder.build());
	}

//	private void restartApplication(Context context) {
//		// TODO Auto-generated method stub
//
//		// String countryId =
//		// SharedPreferencesHelper.getSelectedCountry(context);
//		final Intent mStartActivity = new Intent(context, MainActivity.class);
//		mStartActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//		final int mPendingIntentId = 123456;
//		final PendingIntent mPendingIntent = PendingIntent.getActivity(context,
//				mPendingIntentId, mStartActivity,
//				PendingIntent.FLAG_CANCEL_CURRENT);
//		final AlarmManager mgr = (AlarmManager) context
//				.getSystemService(Context.ALARM_SERVICE);
//		mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100,
//				mPendingIntent);
//		System.exit(0);
//	}

//	private void msgActivity(Context context) {
//		// TODO Auto-generated method stub
//
//		// String countryId =
//		// SharedPreferencesHelper.getSelectedCountry(context);
//		final Intent mStartActivity = new Intent(context, MainActivity.class);
//		mStartActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//		final int mPendingIntentId = 123456;
//		final PendingIntent mPendingIntent = PendingIntent.getActivity(context,
//				mPendingIntentId, mStartActivity,
//				PendingIntent.FLAG_CANCEL_CURRENT);
//		final AlarmManager mgr = (AlarmManager) context
//				.getSystemService(Context.ALARM_SERVICE);
//		mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 1,
//				mPendingIntent);
//		// System.exit(0);
//	}
	
    static void updateMyActivity(Context context, String message) {

        Intent intent = new Intent("unique_name");

        //put whatever data you want to send, if any
        intent.putExtra("message", message);

        //send broadcast
        context.sendBroadcast(intent);
    }

}
