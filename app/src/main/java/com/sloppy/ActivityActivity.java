package com.sloppy;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Window;


public class ActivityActivity extends FragmentActivity implements CommunicatorFragmentInterface {
	Context con;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_activity);
		addContentFragment(new ActivityFragment(), false);
		con = this;
	}

	/**
	 *---------------------Method for set fragment------------------------
     */
	@Override
	public void setContentFragment(Fragment fragment, boolean addToBackStack) {
		if (fragment == null) {
			return;
		}
		final FragmentManager fragmentManager = getSupportFragmentManager();
		Fragment currentFragment = fragmentManager.findFragmentById(R.id.activityLayout);

		if (currentFragment != null && fragment.getClass().isAssignableFrom(currentFragment.getClass())) {
			return;
		}

		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.replace(R.id.activityLayout, fragment, fragment.getClass().getName());
		if (addToBackStack) {
			fragmentTransaction.addToBackStack(fragment.getClass().getName());
		}
		fragmentTransaction.commit();
		fragmentManager.executePendingTransactions();
	}

	@Override
	public void addContentFragment(Fragment fragment, boolean addToBackStack) {
		if (fragment == null) {
			return;
		}
		final FragmentManager fragmentManager = getSupportFragmentManager();
		Fragment currentFragment = fragmentManager.findFragmentById(R.id.activityLayout);

		if (currentFragment != null && fragment.getClass().isAssignableFrom(currentFragment.getClass())) {
			return;
		}

		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.add(R.id.activityLayout, fragment, fragment.getClass().getName());
		if (addToBackStack) {
			fragmentTransaction.addToBackStack(fragment.getClass().getName());
		}
		fragmentTransaction.commit();
		fragmentManager.executePendingTransactions();

	}

	@Override
	public void onBackPressed() {

		if (getFragmentManager().getBackStackEntryCount() > 0) {
			getFragmentManager().popBackStack();
		} else {
			super.onBackPressed();
		}

	}

}
