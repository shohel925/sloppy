package com.sloppy;

import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sloppy.aapbdLib.AlertMessage;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.NetInfo;
import com.sloppy.aapbdLib.PersistData;
import com.sloppy.model.MyShopResponse;
import com.sloppy.model.ProductAndUserDetailsInfo;
import com.sloppy.utils.AAPBDHttpClient;
import com.sloppy.utils.AllURL;
import com.sloppy.utils.AppConstant;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.concurrent.Executors;


/**
 * Created by hp on 3/13/2016.
 */
public class PurchaedItemsFragment extends BaseFragment{
    Context con;
    View view;
    private RecyclerView rvPurItems;
    private MyShopResponse myShopResponse;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {
        con=getActivity();
        view=inflater.inflate(R.layout.fragment_perchaed_itemms, container, false);
        initUI();
        return view;
    }

    private void initUI() {
        rvPurItems=(RecyclerView) view.findViewById(R.id.rvPurItems);
        rvPurItems.setLayoutManager(new LinearLayoutManager(con));
        callPurchaedItemsAPI();
        ImageView imgPurItemsBack=(ImageView) view.findViewById(R.id.imgPurItemsBack);
        imgPurItemsBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
    }



/*
*   -------------------My Shop API call-------------------------------
 */


    protected void callPurchaedItemsAPI() {

  /*
   * check internet first
   */
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));

            return;
        }

  /*
   * start progress bar
   */

        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
  /*
   * start Thread
   *
   *
   */
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            String msg = "";
            String response="";

            @Override
            public void run() {
                // You can performed your task here.

                try {
/**
 * --------API-32----Get User Product List  by Type------My Shop-----------------
 */
                    response = AAPBDHttpClient.get(AllURL.getMyShopData("purchased_items", "20")).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();
                    Gson gson = new Gson();
                    myShopResponse = gson.fromJson(response, MyShopResponse.class);


                } catch (Exception e1) {

                    e1.printStackTrace();

                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

      /*
       * stop progress bar
       */

                        if (busy != null) {
                            busy.dismis();
                        }


                        if (myShopResponse.getStatus().equalsIgnoreCase("0")) {
                            msg = myShopResponse.getMsg();
                            AlertMessage.showMessage(con, "Sloppy Alert", msg);
                        } else {
                            Log.e("PurchaedItemsResponse", ">>" + response);
                            Toast.makeText(con, "PurchaedItems " + myShopResponse.getMsg(), Toast.LENGTH_LONG).show();
                            if(myShopResponse.getResult().size() > 0){
                                rvPurItems.setAdapter(new MyRecyclerAdapter(con,myShopResponse.getResult()));
                            } else {
                                TextView tvDataNotFound= (TextView) view.findViewById(R.id.tvDataNotFound);
                                tvDataNotFound.setVisibility(View.VISIBLE);
                            }


                        }

                    }
                });

            }

        });

    }

    public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.CustomViewHolder> {
        private List<ProductAndUserDetailsInfo> myresponseList;
        private Context mContext;
        public MyRecyclerAdapter(Context context,List<ProductAndUserDetailsInfo> myresponseList){
            this.mContext=context;
            this.myresponseList=myresponseList;
        }
        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row__my_shop,null);
            CustomViewHolder viewHolder=new CustomViewHolder(view);
            return viewHolder;
        }
        public class CustomViewHolder extends RecyclerView.ViewHolder {
            private ImageView imgMyShop;
            private TextView tvMyBrand,tvMyShopDescrip,tvOriginalPrice,tvMylistingPrice,tvLike,tvChate;

            public CustomViewHolder(View view) {
                super(view);
                this.imgMyShop = (ImageView) view.findViewById(R.id.imgMyShop);
                this.tvMyBrand = (TextView) view.findViewById(R.id.tvMyBrand);
                this.tvMyShopDescrip = (TextView) view.findViewById(R.id.tvMyShopDescrip);
                this.tvOriginalPrice = (TextView) view.findViewById(R.id.tvOriginalPrice);
                this.tvMylistingPrice = (TextView) view.findViewById(R.id.tvMylistingPrice);
                this.tvLike = (TextView) view.findViewById(R.id.tvLike);
                this.tvChate = (TextView) view.findViewById(R.id.tvChate);

            }
        }
        @Override
        public void onBindViewHolder(CustomViewHolder holder, final int position) {
//            MyShopResultInfo myShoprespons= myresponseList.get(position);
//            holder.tvMyShopDescrip.setText(myShoprespons.getName());
//            holder.tvMylistingPrice.setText("$"+myShoprespons.getListing_price());
//            holder.tvOriginalPrice.setText("$" + myShoprespons.getOriginal_price());
//            holder.tvOriginalPrice.setPaintFlags(holder.tvOriginalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//            holder.tvMyBrand.setText(myShoprespons.getBrand_info().get(0).getName());
//            holder.tvLike.setText(myShoprespons.getLike_count());
//            holder.tvChate.setText(myShoprespons.getComments_count());
//            // holder.tvOriginalPrice.setText(Html.fromHtml(myShoprespons.getResult().get(position).getOriginal_price()));
//            Picasso.with(mContext)
//                    .load(myShoprespons.getPhoto_one())
//                    .placeholder(R.drawable.ic_launcher)
//                    .error(R.drawable.ic_launcher)
//                    .into(holder.imgMyShop);
//            holder.imgMyShop.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    MyShopResultInfo myShoprespons= myresponseList.get(position);
//                    // myConnectorInterface.setContentFragment(new );
//                }
//            });

        }

        @Override
        public int getItemCount() {
            return (null != myresponseList ? myresponseList.size() : 0);
        }


    }
}
