package com.sloppy;

import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sloppy.adapter.RecyclerProductListAdapter;
import com.sloppy.adapter.RecyclerUserListAdapter;
import com.sloppy.utils.AppConstant;

/**
 * Created by hp on 7/24/2016.
 */
public class SearchUserListFragment extends BaseFragment {

    private Context con;
    private View view;
    private RecyclerView rvCommonList;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        con = getActivity();
        view = inflater.inflate(R.layout.fragment_common_list, container, false);
        initUI();
        return view;
    }

    private void initUI() {
        TextView tvDataNotFound= (TextView) view.findViewById(R.id.tvDataNotFound);
        TextView tvTitelCommon = (TextView) view.findViewById(R.id.tvTitelCommon);
        //===============Initialization=================
        rvCommonList = (RecyclerView) view.findViewById(R.id.rvCommonList);
        tvTitelCommon.setText(AppConstant.productListTitel);
        //=========On Click==============================

        ImageView imgBackCommonList = (ImageView) view.findViewById(R.id.imgBackCommonList);
        ImageView imgMenuCommon = (ImageView) view.findViewById(R.id.imgMenuCommon);
        imgBackCommonList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        imgMenuCommon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MenuFragmentDialog dialogMenu= new MenuFragmentDialog();
                dialogMenu.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar);
                dialogMenu.show(getActivity().getFragmentManager(),"");
            }
        });

        if (AppConstant.userList.size()> 0) {
            tvDataNotFound.setVisibility(View.GONE);
            rvCommonList.setVisibility(View.VISIBLE);
            /**
             * =================Set Adapter===========================================================
             */
            rvCommonList.setLayoutManager(new LinearLayoutManager(con));
            rvCommonList.setAdapter(new RecyclerUserListAdapter(con, AppConstant.userList));

        } else {
            rvCommonList.setVisibility(View.GONE);
            tvDataNotFound.setVisibility(View.VISIBLE);
        }

    }
}
