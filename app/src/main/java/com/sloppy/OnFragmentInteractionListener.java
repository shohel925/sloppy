package com.sloppy;

import android.app.Fragment;



public interface OnFragmentInteractionListener {

//    public void setContentFragment(Fragment fragment, boolean addToBackStack);

	void setContentFragment(android.support.v4.app.Fragment fragment,
							boolean addToBackStack);
}
