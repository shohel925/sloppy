package com.sloppy.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sloppy.BrandFragment;
import com.sloppy.CommunicatorFragmentInterface;
import com.sloppy.OtherUserDetailsFragment;
import com.sloppy.R;
import com.sloppy.SingleProductFragment;
import com.sloppy.model.ProductAndUserDetailsInfo;
import com.sloppy.model.SearchUserList;
import com.sloppy.utils.AppConstant;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by hp on 7/21/2016.
 */
public class RecyclerUserListAdapter extends RecyclerView.Adapter<RecyclerUserListAdapter.MyViewHolder> {
    private Context con;
    private List<SearchUserList> userInfo;
    public CommunicatorFragmentInterface myCommunicator;

    public RecyclerUserListAdapter(Context con, List<SearchUserList> itemList) {
        this.con =con;
        this.userInfo = itemList;
        try {
            myCommunicator = (CommunicatorFragmentInterface) con;
        } catch (ClassCastException e) {
            throw new ClassCastException(con.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_user_list, parent, false);
        return new MyViewHolder(itemView);
    }

    public  class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imgUser;
        TextView tvNameUser;
        public MyViewHolder(View view) {
            super(view);
            this.imgUser = (ImageView) view.findViewById(R.id.imgUser);
            this.tvNameUser = (TextView) view.findViewById(R.id.tvNameUser);
        }
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final SearchUserList query = userInfo.get(position);
        try {
            holder.tvNameUser.setText(query.getFirst_name()+" "+query.getLast_name());
            Picasso.with(con)
                    .load(query.getProfile_image())
                    .placeholder(R.drawable.ic_launcher)
                    .error(R.drawable.ic_launcher)
                    .into(holder.imgUser);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppConstant.otherUserId=query.getId();
                    myCommunicator.addContentFragment(new OtherUserDetailsFragment(), true);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return userInfo.size();
    }
}
