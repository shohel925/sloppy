package com.sloppy.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sloppy.CommunicatorFragmentInterface;
import com.sloppy.OtherUserDetailsFragment;
import com.sloppy.R;
import com.sloppy.SingleProductFragment;
import com.sloppy.aapbdLib.AlertMessage;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.NetInfo;
import com.sloppy.aapbdLib.PersistData;
import com.sloppy.model.NotificationInfo;
import com.sloppy.model.ProductAndUserDetailsInfo;
import com.sloppy.model.SingleProductDetailsResponse;
import com.sloppy.utils.AAPBDHttpClient;
import com.sloppy.utils.AllURL;
import com.sloppy.utils.AppConstant;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;

/**
 * Created by hp on 8/9/2016.
 */
public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {
    private Activity con;
    private List<NotificationInfo> productInfo;
    public CommunicatorFragmentInterface myCommunicator;
    private SingleProductDetailsResponse sinProResponse;

    public NotificationAdapter(Activity con, List<NotificationInfo> itemList) {
        this.con = con;
        this.productInfo = itemList;
        try {
            myCommunicator = (CommunicatorFragmentInterface) con;
        } catch (ClassCastException e) {
            throw new ClassCastException(con.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_notification, parent, false);
        return new MyViewHolder(itemView);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imgUserNtf, imgProductNot;
        TextView tvDescriptionNot, tvTimeNtf;

        public MyViewHolder(View view) {
            super(view);
            this.imgUserNtf = (ImageView) view.findViewById(R.id.imgUserNtf);
            this.imgProductNot = (ImageView) view.findViewById(R.id.imgProductNot);

            this.tvDescriptionNot = (TextView) view.findViewById(R.id.tvDescriptionNot);
            this.tvTimeNtf = (TextView) view.findViewById(R.id.tvTimeNtf);

        }
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final NotificationInfo query = productInfo.get(position);
        try {
            /**
             * -----------------Set data on field of Comment row------------------
             */

            if (query.getAction_type().equalsIgnoreCase("feedback")) {
                holder.tvDescriptionNot.setText(con.getString(R.string.left_new_feedback));
            } else if (query.getAction_type().equalsIgnoreCase("like")) {
                holder.tvDescriptionNot.setText(con.getString(R.string.liked_your));
            } else if (query.getAction_type().equalsIgnoreCase("follow")) {
                holder.tvDescriptionNot.setText(con.getString(R.string.nowFollowing));
            } else if (query.getAction_type().equalsIgnoreCase("comments")) {
                holder.tvDescriptionNot.setText(con.getString(R.string.left_new_comment));
            } else if (query.getAction_type().equalsIgnoreCase("product_sell")) {
                holder.tvDescriptionNot.setText(con.getString(R.string.purchaed_a));
            }else if (query.getAction_type().equalsIgnoreCase("offer_new")) {
                holder.tvDescriptionNot.setText(con.getString(R.string.made_a));
            }
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String createdDate = query.getCreated_at();
            Date createdate = null;
            try {
                createdate = sdf.parse(createdDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            holder.tvTimeNtf.setText(getDisplayableTime(createdate.getTime()));



            Picasso.with(con)
                    .load(query.getUser_info().get(0).getProfile_image())
                    .placeholder(R.drawable.person)
                    .error(R.drawable.person)
                    .into(holder.imgUserNtf);
            Picasso.with(con)
                    .load(query.getImage_url())
                    .placeholder(R.drawable.product_bg)
                    .error(R.drawable.product_bg)
                    .into(holder.imgProductNot);


            holder.imgUserNtf.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppConstant.otherUserId = query.getUser_id();
                    myCommunicator.setContentFragment(new OtherUserDetailsFragment(), true);
                }
            });

            holder.imgProductNot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callSingProAPI(con, query.getProduct_id(), con.getString(R.string.page_items));
//                    AppConstant.productIDfeedRow=query.getProduct_info().getId();
//                    AppConstant.feedRowObject=query.getProduct_info();
//                    myCommunicator.setContentFragment(new SingleProductFragment(), true);

                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void callSingProAPI(final Activity con, final String id, final String user_type) {
      /*
       * ---------------check internet first------------
       */
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, con.getString(R.string.status), con.getString(R.string.checkInternet));
            return;
        }
         /*
          * ----------------Show Busy Dialog -----------------------------------------
          */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
      /*
       *----------------------Start Thread-------------------------------------------
       */
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            String msg = "";
            String response = "";

            @Override
            public void run() {

                // You can performed your task here.
                try {
                    Log.e("SinPro URL", AllURL.getSingleProDetaURL(id, user_type));
                    //-------------Hit Server---------------------
                    response = AAPBDHttpClient.get(AllURL.getSingleProDetaURL(id, user_type)).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();
                    Log.e("SingPro Response", ">>" + response);
                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                con.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        /**
                         * -------------Here we do UI related tasks inside run() method of runOnUiThread()-------------------
                         */
                        //-------Stop Busy Dialog-----
                        if (busy != null) {
                            busy.dismis();
                        }
                        //----------------Persist response with the help of Gson----------------
                        Gson gson = new Gson();
                        sinProResponse = gson.fromJson(response, SingleProductDetailsResponse.class);
                        /**
                         * ---------main Ui related work--------------
                         */
                        if (sinProResponse.getStatus().equalsIgnoreCase("1")) {

                            AppConstant.productIDfeedRow = sinProResponse.getDetails().getId();
                            AppConstant.feedRowObject = sinProResponse.getDetails();
                            myCommunicator.addContentFragment(new SingleProductFragment(), true);

//                            Toast.makeText(con, "Single Pro " + sinProResponse.getMsg(), Toast.LENGTH_LONG).show();
                        } else {
                            msg = sinProResponse.getMsg();
                            AlertMessage.showMessage(con, "ProCommentList", msg);
                        }

                    }
                });

            }

        });

    }

    public static String getDisplayableTime(long delta) {
        long difference = 0;
        Long mDate = java.lang.System.currentTimeMillis();

        if (mDate > delta) {
            difference = mDate - delta;
            final long seconds = difference / 1000;
            final long minutes = seconds / 60;
            final long hours = minutes / 60;
            final long days = hours / 24;
            final long weeks = days / 7;
            final long months = days / 31;
            final long years = days / 365;

            if (seconds < 0) {
                return "not yet";
            } else if (seconds < 60) {
                return seconds == 1 ? "one second ago" : seconds + " seconds ago";
            } else if (seconds < 120) {
                return "a minute ago";
            } else if (seconds < 2700) // 45 * 60
            {
                return minutes + " minutes ago";
            } else if (seconds < 5400) // 90 * 60
            {
                return "an hour ago";
            } else if (seconds < 86400) // 24 * 60 * 60
            {
                return hours + " hours ago";
            } else if (seconds < 172800) // 48 * 60 * 60
            {
                return "yesterday";
            } else if (seconds < 604800) // 7 * 24 * 60 * 60
            {
                return days + " days ago";
            } else if (seconds < 2592000) // 30 * 24 * 60 * 60
            {
                return weeks <= 1 ? "1 week ago" : weeks + " weeks ago";
            } else if (seconds < 31104000) // 12 * 30 * 24 * 60 * 60
            {

                return months <= 1 ? "1 month ago" : months + " months ago";
            } else {

                return years <= 1 ? "one year ago" : years + " years ago";
            }
        }
        return null;
    }


    @Override
    public int getItemCount() {
        return productInfo.size();
    }
}
