package com.sloppy.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sloppy.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hp on 8/4/2016.
 */
public class SingleProPagerAdepter extends PagerAdapter {

    private LayoutInflater inflater;
    public Context con;
    List<String> imageRSC = new ArrayList<String>();


    // constructor
    public SingleProPagerAdepter(Context con, List<String> imgsSrc) {
        this.con = con;
        this.imageRSC = imgsSrc;
    }

    @Override
    public int getCount() {
        return this.imageRSC.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (RelativeLayout) object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView detailsImageView;
        TextView slideTitle, slideDes;
        position = position % imageRSC.size();
        inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View viewLayout = inflater.inflate(R.layout.pagerlayout,
                container, false);

        detailsImageView = (ImageView) viewLayout.findViewById(R.id.detailsImageView);

//		slideTitle = (TextView) viewLayout.findViewById(R.id.slideTitle);
//		slideDes = (TextView) viewLayout.findViewById(R.id.slideDes);


//				slideTitle.setTypeface(tf);
//				slideDes.setTypeface(tf2);

        try {

            if (imageRSC.size() == 0) {
                Picasso.with(con).load(R.drawable.ic_launcher).into(detailsImageView);
            } else {
                Picasso.with(con).load(imageRSC.get(position)).into(detailsImageView);
            }


//			detailsImageView.setImageResource(imageRSC[position]);
//			slideTitle.setText(titleArray[position]);
//			slideDes.setText(desArray[position]);
//			Picasso.with(con)
//					.load(imageRSC[position])
//					.error(R.drawable.ic_launcher).into(detailsImageView);
        } catch (final Exception e) {
            e.printStackTrace();
        }

        ((ViewPager) container).addView(viewLayout);

        return viewLayout;
    }

    @Override
    public String toString() {
        return "AllViewImage [inflater=" + inflater + ", con=" + con
                + ", imageRSC=" + imageRSC + "]";
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);

    }
}
