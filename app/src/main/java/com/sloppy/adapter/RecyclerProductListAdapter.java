package com.sloppy.adapter;

import android.Manifest;
import android.app.Activity;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sloppy.BrandFragment;
import com.sloppy.CommantDialogFragment;
import com.sloppy.CommunicatorFragmentInterface;
import com.sloppy.OtherUserDetailsFragment;
import com.sloppy.R;
import com.sloppy.SingleProductFragment;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.NetInfo;
import com.sloppy.aapbdLib.PersistData;
import com.sloppy.aapbdLib.PersistentUser;
import com.sloppy.model.LikeUnlikeResponse;
import com.sloppy.model.ProductAndUserDetailsInfo;
import com.sloppy.model.ProductList;
import com.sloppy.utils.AAPBDHttpClient;
import com.sloppy.utils.AllURL;
import com.sloppy.utils.AppConstant;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import java.util.concurrent.Executors;

/**
 * Created by hp on 7/18/2016.
 */
public class RecyclerProductListAdapter extends RecyclerView.Adapter<RecyclerProductListAdapter.MyViewHolder> {
    private Activity con;
    private List<ProductAndUserDetailsInfo> productInfo;
    public CommunicatorFragmentInterface myCommunicator;
    private String pictureUrl = "";

    public RecyclerProductListAdapter(Activity con, List<ProductAndUserDetailsInfo> itemList) {
        this.con = con;
        this.productInfo = itemList;
        try {
            myCommunicator = (CommunicatorFragmentInterface) con;
        } catch (ClassCastException e) {
            throw new ClassCastException(con.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_common_list, parent, false);
        return new MyViewHolder(itemView);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imgProBrand, sellerImage,imgComentCom, imgShareBrand, imgLoveBnd;
        TextView tvProNameBrand, tvBrandNameBrand, tvLikeCountBrand, imgComentCountBrand,
                sellerName, tvOriPriceBrand, tvlstPriceBrand;

        public MyViewHolder(View view) {
            super(view);
            this.imgLoveBnd = (ImageView) view.findViewById(R.id.imgLoveBnd);
            this.imgComentCom = (ImageView) view.findViewById(R.id.imgComentCom);
            this.imgProBrand = (ImageView) view.findViewById(R.id.imgProBrand);
            this.sellerImage = (ImageView) view.findViewById(R.id.sellerImage);
            this.imgShareBrand = (ImageView) view.findViewById(R.id.imgShareBrand);
            this.tvProNameBrand = (TextView) view.findViewById(R.id.tvProNameBrand);
            this.tvBrandNameBrand = (TextView) view.findViewById(R.id.tvBrandNameBrand);
            this.tvLikeCountBrand = (TextView) view.findViewById(R.id.tvLikeCountBrand);
            this.tvOriPriceBrand = (TextView) view.findViewById(R.id.tvOriPriceBrand);
            this.tvlstPriceBrand = (TextView) view.findViewById(R.id.tvlstPriceBrand);
            this.imgComentCountBrand = (TextView) view.findViewById(R.id.imgComentCountBrand);
            this.sellerName = (TextView) view.findViewById(R.id.sellerName);


        }
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ProductAndUserDetailsInfo query = productInfo.get(position);
        try {
            /**
             * -----------------Set data on field of Comment row------------------
             */
            holder.sellerImage.setVisibility(View.GONE);
            holder.sellerName.setVisibility(View.GONE);
            holder.tvProNameBrand.setText(query.getName());
            holder.tvlstPriceBrand.setText(AppConstant.getCurrencySymbol(query.getSeller_info().get(0).getCurrency()) + query.getListing_price());
            holder.tvOriPriceBrand.setText(AppConstant.getCurrencySymbol(query.getSeller_info().get(0).getCurrency()) + query.getOriginal_price());
            holder.tvOriPriceBrand.setPaintFlags(holder.tvOriPriceBrand.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.tvBrandNameBrand.setText(query.getBrand_info().get(0).getName());
            holder.tvLikeCountBrand.setText(query.getLike_count());
            holder.imgComentCountBrand.setText(query.getComments_count());
            if (query.getIs_liked().equalsIgnoreCase("1")) {
                holder.imgLoveBnd.setImageResource(R.drawable.love_liked);
            } else if (query.getIs_liked().equalsIgnoreCase("0")) {
                holder.imgLoveBnd.setImageResource(R.drawable.love_unliked);
            }
//            holder.sellerName.setText(query.getSeller_info().get(0).getName());
            Picasso.with(con)
                    .load(query.getPhoto_one())
                    .placeholder(R.drawable.product_bg)
                    .error(R.drawable.product_bg)
                    .into(holder.imgProBrand);
//            Picasso.with(con)
//                    .load(query.getSeller_info().get(0).getProfile_image())
//                    .placeholder(R.drawable.ic_launcher)
//                    .error(R.drawable.ic_launcher)
//                    .into(holder.sellerImage);
            holder.imgLoveBnd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callLikeUnlikeAPI(query.getId(), position);
                }
            });

            holder.imgComentCom.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppConstant.productIDfeedRow = query.getId();
                    AppConstant.tagUserList = query.getTaggable_users();
                    FragmentManager manager = ((Activity) con).getFragmentManager();
                    CommantDialogFragment dialogMenu = new CommantDialogFragment();
                    dialogMenu.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            holder.imgComentCountBrand.setText(AppConstant.commentCount);
                        }
                    });
                    dialogMenu.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar);
                    dialogMenu.show(manager, "");
                }
            });

            holder.tvBrandNameBrand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppConstant.brandID = query.getBrand_info().get(0).getId();
                    AppConstant.brandName = query.getBrand_info().get(0).getName();
                    Log.e("BrandID", AppConstant.brandID);
                    Log.e("BrandName", AppConstant.brandName);
                    myCommunicator.setContentFragment(new BrandFragment(), true);

                }
            });

            holder.imgShareBrand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (PersistentUser.isLogged(con)) {
                        pictureUrl=productInfo.get(position).getPhoto_one();
                        Picasso.with(con).load(pictureUrl).memoryPolicy(MemoryPolicy.NO_CACHE).into(target);
                    } else {
                        AppConstant.loginDialoag(con);
                    }

                }
            });
            holder.sellerName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppConstant.otherUserId = query.getUser_id();
                    myCommunicator.setContentFragment(new OtherUserDetailsFragment(), true);
                }
            });

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppConstant.productIDfeedRow = query.getId();
                    AppConstant.feedRowObject = query;
                    myCommunicator.addContentFragment(new SingleProductFragment(), true);
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    BusyDialog busyDialog;

    private Target target = new Target() {


        @Override
        public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
            new Thread(new Runnable() {

                @Override
                public void run() {

                    File file = new File(Environment.getExternalStorageDirectory().getPath() + "/picture.jpg");
                    try {
                        file.createNewFile();
                        FileOutputStream ostream = new FileOutputStream(file);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, ostream);
                        busyDialog.dismis();
                        String path = MediaStore.Images.Media.insertImage(con.getContentResolver(), bitmap, "Title", null);
                        AppConstant.defaultShare(con, Uri.parse(path));
                        ostream.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }).start();
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
            busyDialog.dismis();
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
            if (placeHolderDrawable != null) {
            }
            busyDialog = new BusyDialog(con, false, "");
            busyDialog.show();
        }
    };


    @Override
    public int getItemCount() {
        return productInfo.size();
    }

    /**
     * -------------------10. Like/Unlike Product API------------------
     */
    public void callLikeUnlikeAPI(final String product_id, final int position) {
  /*
   * ---------------check internet first------------
   */
        if (!NetInfo.isOnline(con)) {
            AppConstant.alertDialoag(con, con.getString(R.string.status), con.getString(R.string.checkInternet), con.getString(R.string.ok));
            return;
        }

  /*
   * ----------------Show Busy Dialog -----------------------------------------
   */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
        /**
         *----------------------Start Thread-------------------------------------------
         */
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            String msg = "";
            String response = "";
            LikeUnlikeResponse likeUnlikeResponse;

            @Override
            public void run() {
                //--------------- We can performed your task here.-----------------

                try {
                    Log.e("Like/Unlike URL", AllURL.getLikeUnlike(product_id));
                    //-------------Hit Server---------------------
                    response = AAPBDHttpClient.get(AllURL.getLikeUnlike(product_id)).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();

                    //----------------Persist response with the help of Gson----------------
                    Log.e("Like/Unlike Response", ">>" + response);
                    Gson gson = new Gson();
                    likeUnlikeResponse = gson.fromJson(response, LikeUnlikeResponse.class);

                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                con.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        /**
                         * -------------Here we do UI related tasks inside run() method of runOnUiThread()-------------------
                         */

                        //-------Stop Busy Dialog-----
                        if (busy != null) {
                            busy.dismis();
                        }
                        /**
                         * ---------main Ui related work--------------
                         */
                        if (likeUnlikeResponse != null) {
                            Toast.makeText(con, likeUnlikeResponse.getMsg(), Toast.LENGTH_LONG).show();
                            if (likeUnlikeResponse.getMsg().equalsIgnoreCase("liked")) {
                                productInfo.get(position).setLike_count(likeUnlikeResponse.getLikes_count());
                                productInfo.get(position).setIs_liked("1");
                                RecyclerProductListAdapter.this.notifyDataSetChanged();

                            } else if (likeUnlikeResponse.getMsg().equalsIgnoreCase("unliked")) {
                                productInfo.get(position).setLike_count(likeUnlikeResponse.getLikes_count());
                                productInfo.get(position).setIs_liked("0");
                                RecyclerProductListAdapter.this.notifyDataSetChanged();
                            }

                        }

                    }
                });

            }

        });

    }
}
