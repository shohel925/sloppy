package com.sloppy.adapter;

import android.Manifest;
import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.app.FragmentManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sloppy.CommantDialogFragment;
import com.sloppy.CommunicatorFragmentInterface;
import com.sloppy.OtherUserDetailsFragment;
import com.sloppy.R;
import com.sloppy.SingleProductFragment;
import com.sloppy.aapbdLib.AlertMessage;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.NetInfo;
import com.sloppy.aapbdLib.PersistData;
import com.sloppy.aapbdLib.PersistentUser;
import com.sloppy.model.LikeUnlikeResponse;
import com.sloppy.model.ProductAndUserDetailsInfo;
import com.sloppy.utils.AAPBDHttpClient;
import com.sloppy.utils.AllURL;
import com.sloppy.utils.AppConstant;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import java.util.concurrent.Executors;

/**
 * Created by hp on 5/3/2016.
 */
public class BrandListAdapter extends RecyclerView.Adapter<BrandListAdapter.CustomViewHolder> {
    private List<ProductAndUserDetailsInfo> myresponseList;
    private Activity con;
    public CommunicatorFragmentInterface myCommunicator;
    private String pictureUrl="";

    public BrandListAdapter(Activity context, List<ProductAndUserDetailsInfo> myresponseList) {
        this.con = context;
        this.myresponseList = myresponseList;
        try {
            myCommunicator = (CommunicatorFragmentInterface) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_product_list_by_brand, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public int getItemCount() {
        return (null != myresponseList ? myresponseList.size() : 0);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgProBrand, sellerImage, imgShareBrand, imgLoveBnd, imgComentbdl;
        private TextView tvProNameBrand, tvBrandNameBrand, tvLikeCountBrand, imgComentCountBrand,
                sellerName, tvOriPriceBrand, tvlstPriceBrand;
        private LinearLayout viewBrand;

        public CustomViewHolder(View view) {
            super(view);
            this.viewBrand = (LinearLayout) view.findViewById(R.id.viewBrand);
            this.imgLoveBnd = (ImageView) view.findViewById(R.id.imgLoveBnd);
            this.imgComentbdl = (ImageView) view.findViewById(R.id.imgComentbdl);
            this.imgProBrand = (ImageView) view.findViewById(R.id.imgProBrand);
            this.sellerImage = (ImageView) view.findViewById(R.id.sellerImage);
            this.imgShareBrand = (ImageView) view.findViewById(R.id.imgShareBrand);
            this.tvProNameBrand = (TextView) view.findViewById(R.id.tvProNameBrand);
            this.tvBrandNameBrand = (TextView) view.findViewById(R.id.tvBrandNameBrand);
            this.tvLikeCountBrand = (TextView) view.findViewById(R.id.tvLikeCountBrand);
            this.tvOriPriceBrand = (TextView) view.findViewById(R.id.tvOriPriceBrand);
            this.tvlstPriceBrand = (TextView) view.findViewById(R.id.tvlstPriceBrand);
            this.imgComentCountBrand = (TextView) view.findViewById(R.id.imgComentCountBrand);
            this.sellerName = (TextView) view.findViewById(R.id.sellerName);
        }
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, final int position) {
        final ProductAndUserDetailsInfo brandList = myresponseList.get(position);
        holder.tvProNameBrand.setText(brandList.getName());
        holder.tvlstPriceBrand.setText(AppConstant.getCurrencySymbol(brandList.getSeller_info().get(0).getCurrency()) + brandList.getListing_price());
        holder.tvOriPriceBrand.setText(AppConstant.getCurrencySymbol(brandList.getSeller_info().get(0).getCurrency()) + brandList.getOriginal_price());
        holder.tvOriPriceBrand.setPaintFlags(holder.tvOriPriceBrand.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.tvBrandNameBrand.setText(brandList.getBrand_info().get(0).getName());
        holder.tvLikeCountBrand.setText(brandList.getLike_count());
        holder.imgComentCountBrand.setText(brandList.getComments_count());
        if (brandList.getIs_liked().equalsIgnoreCase("1")) {
            holder.imgLoveBnd.setImageResource(R.drawable.love_liked);
        } else if (brandList.getIs_liked().equalsIgnoreCase("0")) {
            holder.imgLoveBnd.setImageResource(R.drawable.love_unliked);
        }
        holder.sellerName.setText(brandList.getSeller_info().get(0).getUsername());
        Picasso.with(con)
                .load(brandList.getPhoto_one())
                .placeholder(R.drawable.product_bg)
                .error(R.drawable.product_bg)
                .into(holder.imgProBrand);
        Picasso.with(con)
                .load(brandList.getSeller_info().get(0).getProfile_image())
                .placeholder(R.drawable.person)
                .error(R.drawable.person)
                .into(holder.sellerImage);
        /**
         * ==================ON Click===============================================================
         */
        holder.imgLoveBnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callLikeUnlikeAPI(brandList.getId(),position);
            }
        });


        holder.imgLoveBnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callLikeUnlikeAPI(brandList.getId(),position);
            }
        });
        holder.imgComentbdl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.productIDfeedRow = brandList.getId();
                AppConstant.tagUserList=brandList.getTaggable_users();
                FragmentManager manager = ((Activity) con).getFragmentManager();
                CommantDialogFragment dialogComment = new CommantDialogFragment();
                dialogComment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        holder.imgComentCountBrand.setText(AppConstant.commentCount);
                    }
                });
                dialogComment.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar);
                dialogComment.show( manager, "");
            }
        });

        holder.imgShareBrand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PersistentUser.isLogged(con)) {
                    pictureUrl=brandList.getPhoto_one();
                    if (ActivityCompat.checkSelfPermission(con, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions((Activity)con,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 11);
                    }else{
                        Picasso.with(con).load(pictureUrl).memoryPolicy(MemoryPolicy.NO_CACHE).into(target);
                    }

                } else {
                    AppConstant.loginDialoag(con);
                }
            }
        });

        holder.sellerName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.otherUserId = brandList.getUser_id();
                myCommunicator.addContentFragment(new OtherUserDetailsFragment(), true);
            }
        });
        holder.sellerImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.otherUserId = brandList.getUser_id();
                myCommunicator.addContentFragment(new OtherUserDetailsFragment(), true);
            }
        });

        holder.viewBrand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("View Brand", "Click");
                AppConstant.productIDfeedRow = brandList.getId();
                AppConstant.feedRowObject = myresponseList.get(position);
                myCommunicator.addContentFragment(new SingleProductFragment(), true);
            }
        });
    }

    BusyDialog busyDialog;

    private Target target = new Target() {


        @Override
        public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
            new Thread(new Runnable() {

                @Override
                public void run() {

                    File file = new File(Environment.getExternalStorageDirectory().getPath() + "/picture.jpg");
                    try {
                        file.createNewFile();
                        FileOutputStream ostream = new FileOutputStream(file);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, ostream);
                        busyDialog.dismis();
                        String path = MediaStore.Images.Media.insertImage(con.getContentResolver(), bitmap, "Title", null);
                        AppConstant.defaultShare(con, Uri.parse(path));
                        ostream.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }).start();
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
            busyDialog.dismis();
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
            if (placeHolderDrawable != null) {}
            busyDialog=new BusyDialog(con,false,"");
            busyDialog.show();
        }
    };

    /**
     * -------------------10. Like/Unlike Product API------------------
     */
    public void callLikeUnlikeAPI(final String product_id,  final int position) {
  /*
   * ---------------check internet first------------
   */
        if (!NetInfo.isOnline(con)) {
            AppConstant.alertDialoag(con, con.getString(R.string.status), con.getString(R.string.checkInternet), con.getString(R.string.ok));
            return;
        }

  /*
   * ----------------Show Busy Dialog -----------------------------------------
   */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
        /**
         *----------------------Start Thread-------------------------------------------
         */
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            String msg = "";
            String response = "";
            LikeUnlikeResponse likeUnlikeResponse;

            @Override
            public void run() {
                //--------------- We can performed your task here.-----------------

                try {
                    Log.i("Like/Unlike URL", AllURL.getLikeUnlike(product_id));
                    //-------------Hit Server---------------------
                    response = AAPBDHttpClient.get(AllURL.getLikeUnlike(product_id)).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();

                    //----------------Persist response with the help of Gson----------------
                    Log.i("Like/Unlike Response", ">>" + response);
                    Gson gson = new Gson();
                    likeUnlikeResponse = gson.fromJson(response, LikeUnlikeResponse.class);

                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                con.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        /**
                         * -------------Here we do UI related tasks inside run() method of runOnUiThread()-------------------
                         */

                        //-------Stop Busy Dialog-----
                        if (busy != null) {
                            busy.dismis();
                        }
                        /**
                         * ---------main Ui related work--------------
                         */
                        if (likeUnlikeResponse != null) {
                            Toast.makeText(con,likeUnlikeResponse.getMsg(),Toast.LENGTH_LONG).show();
                            if (likeUnlikeResponse.getMsg().equalsIgnoreCase("liked")) {
                                myresponseList.get(position).setLike_count(likeUnlikeResponse.getLikes_count());
                                myresponseList.get(position).setIs_liked("1");
                                BrandListAdapter.this.notifyDataSetChanged();

                            } else if (likeUnlikeResponse.getMsg().equalsIgnoreCase("unliked")) {
                                myresponseList.get(position).setLike_count(likeUnlikeResponse.getLikes_count());
                                myresponseList.get(position).setIs_liked("0");
                                BrandListAdapter.this.notifyDataSetChanged();
                            }

                        }


                    }
                });

            }

        });

    }
}
