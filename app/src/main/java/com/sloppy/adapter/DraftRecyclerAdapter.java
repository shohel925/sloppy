package com.sloppy.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sloppy.CommunicatorFragmentInterface;
import com.sloppy.DatabaseHandler;
import com.sloppy.R;
//import com.sloppy.SellFragment;
import com.sloppy.SellFragment;
import com.sloppy.model.ProductAndUserDetailsInfo;
import com.sloppy.utils.AppConstant;

import java.io.File;
import java.util.List;

public class DraftRecyclerAdapter extends RecyclerView.Adapter<DraftRecyclerAdapter.MyViewHolder> {
    private Context con;
    DatabaseHandler dbholder;
    private List<ProductAndUserDetailsInfo> productInfo;
    public CommunicatorFragmentInterface myCommunicator;

    public DraftRecyclerAdapter(Context con, List<ProductAndUserDetailsInfo> itemList) {
        this.con =con;
        this.productInfo = itemList;
        try {
            myCommunicator = (CommunicatorFragmentInterface) con;
        } catch (ClassCastException e) {
            throw new ClassCastException(con.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }


    public  class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgProductDraft;
        private TextView tvNameDrf, tvBrandDraft, tvOriPriceDraft, tvlistingPriceDraft;
        private LinearLayout llEdit, llMyShop, llRemoveDraft;

        public MyViewHolder(View view) {
            super(view);
            this.llMyShop = (LinearLayout) view.findViewById(R.id.llMyShop);
            this.llEdit = (LinearLayout) view.findViewById(R.id.llEdit);
            this.imgProductDraft = (ImageView) view.findViewById(R.id.imgProductDraft);
            this.tvNameDrf = (TextView) view.findViewById(R.id.tvNameDrf);
            this.tvBrandDraft = (TextView) view.findViewById(R.id.tvBrandDraft);
            this.tvOriPriceDraft = (TextView) view.findViewById(R.id.tvOriPriceDraft);
            this.tvlistingPriceDraft = (TextView) view.findViewById(R.id.tvlistingPriceDraft);
            this.llRemoveDraft = (LinearLayout) view.findViewById(R.id.llRemoveDraft);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_draft, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ProductAndUserDetailsInfo query = productInfo.get(position);
        holder.tvNameDrf.setText(query.getName());
        holder.tvBrandDraft.setText(query.getBrand_info().get(0).getName());
        Log.e("Draft id",query.getId());
        Log.e("Draft mane",query.getName());
        //            holder.tvMylistingPrice.setText(AppConstant.getCurrencySymbol(query.getSeller_info()
//                    .get(0).getCurrency()) + query.getListing_price());

//            holder.tvOriginalPrice.setText(AppConstant.getCurrencySymbol(query.getSeller_info().get(0).getCurrency()) + query.getOriginal_price());
        if (TextUtils.isEmpty(query.getOriginal_price())) {
            holder.tvOriPriceDraft.setText(AppConstant.getCurrencySymbol(AppConstant.currencyProfile)+"0.00");
        } else {
            holder.tvOriPriceDraft.setText(AppConstant.getCurrencySymbol(AppConstant.currencyProfile)+query.getOriginal_price());
        }
        holder.tvOriPriceDraft.setPaintFlags(holder.tvOriPriceDraft.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        if (TextUtils.isEmpty(query.getOriginal_price())) {
            holder.tvlistingPriceDraft.setText(AppConstant.getCurrencySymbol(AppConstant.currencyProfile)+"0.00");
        } else {
            holder.tvlistingPriceDraft.setText(AppConstant.getCurrencySymbol(AppConstant.currencyProfile)+query.getListing_price());
        }

        holder.imgProductDraft.setImageURI(Uri.fromFile(new File(query.getPhoto_one())));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.editable = "editable";
                AppConstant.editabledraft="true";
                AppConstant.feedRowObject = query;
                myCommunicator.setContentFragment(new SellFragment(), true);

            }
        });
        holder.llRemoveDraft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dltDraftDialoag(con, "Confirm", "Are you sure to remove from\n Draft?", "Yes", "No", query.getId(),position);
            }
        });

    }

    /**
     *============================Remove dialog================================================
     */
    public void dltDraftDialoag(final Context con, final String titel, final String description,
                                final String left, final String right, final String id,final int position) {
        final Dialog dialog2 = new Dialog(con);
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.setContentView(R.layout.dialog_login);
        dialog2.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        /**
         * =====================Initialization==================================================
         */
        TextView tvTitel2 = (TextView) dialog2.findViewById(R.id.tvTitel2);
        TextView tvDescription2 = (TextView) dialog2.findViewById(R.id.tvDescription2);
        TextView tvLeftCommund = (TextView) dialog2.findViewById(R.id.tvLeftCommund);
        TextView tvRightCommund = (TextView) dialog2.findViewById(R.id.tvRightCommund);
        /**
         * =====================Data Set========================================================
         */
        tvTitel2.setText(titel);
        tvDescription2.setText(description);
        tvLeftCommund.setText(left);
        tvRightCommund.setText(right);
        //==================Font set==========================
        Typeface helveticaNeuRegular = Typeface.createFromAsset(con.getAssets(), "font/helveticaNeuRegular.ttf");
        Typeface helveticaNeueBold = Typeface.createFromAsset(con.getAssets(), "font/helveticaNeueBold.ttf");
        tvTitel2.setTypeface(helveticaNeueBold);
        tvLeftCommund.setTypeface(helveticaNeueBold);
        tvRightCommund.setTypeface(helveticaNeuRegular);
        tvDescription2.setTypeface(helveticaNeuRegular);
        tvRightCommund.setTypeface(helveticaNeuRegular);
        //==================On click============================================================
        tvLeftCommund.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbholder = new DatabaseHandler(con);
                Integer deleteRow= dbholder.deteteProductInfo(id);
                if (deleteRow > 0){
                    Toast.makeText(con,"Data Deleted",Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(con,"Data not Deleted",Toast.LENGTH_LONG).show();
                }
                productInfo.remove(position);
                DraftRecyclerAdapter.this.notifyDataSetChanged();
                dialog2.dismiss();
            }
        });

        tvRightCommund.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog2.dismiss();
            }
        });
        dialog2.show();
    }

    @Override
    public int getItemCount() {
        return productInfo.size();
    }
}