package com.sloppy.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.sloppy.BrandFragment;
import com.sloppy.CommantDialogFragment;
import com.sloppy.CommunicatorFragmentInterface;
import com.sloppy.MainActivity;
import com.sloppy.MyTabActivity;
import com.sloppy.R;
import com.sloppy.SellFragment;
import com.sloppy.SingleProductFragment;
import com.sloppy.aapbdLib.AlertMessage;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.NetInfo;
import com.sloppy.aapbdLib.PersistData;
import com.sloppy.aapbdLib.PersistentUser;
import com.sloppy.aapbdLib.StartActivity;
import com.sloppy.model.DeleteProductResponse;
import com.sloppy.model.LikeUnlikeResponse;
import com.sloppy.model.LoginResponse;
import com.sloppy.model.ProductAndUserDetailsInfo;
import com.sloppy.model.RepostToFeedResponse;
import com.sloppy.utils.AAPBDHttpClient;
import com.sloppy.utils.AllURL;
import com.sloppy.utils.AppConstant;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.concurrent.Executors;

import cz.msebera.android.httpclient.Header;

/**
 * Created by hp on 8/6/2016.
 */
public class MyShopAdapter extends RecyclerView.Adapter<MyShopAdapter.CustomViewHolder> {
    private Activity con;
    private int rowPosition;
    private View view;
    private List<ProductAndUserDetailsInfo> productInfo;
    private CommunicatorFragmentInterface myCommunicator;
    private DeleteProductResponse delResponse;

    public MyShopAdapter(Activity con, List<ProductAndUserDetailsInfo> itemList) {
        this.con = con;
        this.productInfo = itemList;
        try {
            myCommunicator = (CommunicatorFragmentInterface) con;
        } catch (ClassCastException e) {
            throw new ClassCastException(con.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row__my_shop, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgMyShop,imgLove,imgCommentShop;
        private TextView tvMyBrand, tvMyShopDescrip, tvOriginalPrice, tvMylistingPrice, tvLike, tvChate,tvRepostTo;
        private LinearLayout llEdit, llMyShop, llRemove;

        public CustomViewHolder(View view) {
            super(view);
            this.llMyShop = (LinearLayout) view.findViewById(R.id.llMyShop);
            this.llRemove = (LinearLayout) view.findViewById(R.id.llRemove);
            this.llEdit = (LinearLayout) view.findViewById(R.id.llEdit);
            this.imgMyShop = (ImageView) view.findViewById(R.id.imgMyShop);
            this.imgCommentShop = (ImageView) view.findViewById(R.id.imgCommentShop);
            this.imgLove = (ImageView) view.findViewById(R.id.imgLove);
            this.tvMyBrand = (TextView) view.findViewById(R.id.tvMyBrand);
            this.tvMyShopDescrip = (TextView) view.findViewById(R.id.tvMyShopDescrip);
            this.tvRepostTo = (TextView) view.findViewById(R.id.tvRepostTo);
            this.tvOriginalPrice = (TextView) view.findViewById(R.id.tvOriginalPrice);
            this.tvMylistingPrice = (TextView) view.findViewById(R.id.tvMylistingPrice);
            this.tvLike = (TextView) view.findViewById(R.id.tvLike);
            this.tvChate = (TextView) view.findViewById(R.id.tvChate);

        }
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, final int position) {
        final ProductAndUserDetailsInfo myShoprespons = productInfo.get(position);
        holder.tvMyShopDescrip.setText(myShoprespons.getName());
        holder.tvMylistingPrice.setText(AppConstant.getCurrencySymbol(myShoprespons.getSeller_info().get(0).getCurrency()) + myShoprespons.getListing_price());
        holder.tvOriginalPrice.setText(AppConstant.getCurrencySymbol(myShoprespons.getSeller_info().get(0).getCurrency()) + myShoprespons.getOriginal_price());
        holder.tvOriginalPrice.setPaintFlags(holder.tvOriginalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.tvMyBrand.setText(myShoprespons.getBrand_info().get(0).getName());
        holder.tvLike.setText(myShoprespons.getLike_count());
        holder.tvChate.setText(myShoprespons.getComments_count());
        Picasso.with(con)
                .load(myShoprespons.getPhoto_one())
                .placeholder(R.drawable.product_bg)
                .error(R.drawable.product_bg)
                .into(holder.imgMyShop);
        holder.imgMyShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        if (myShoprespons.getIs_liked().equalsIgnoreCase("1")) {
            holder.imgLove.setImageResource(R.drawable.love_liked);
        } else if (myShoprespons.getIs_liked().equalsIgnoreCase("0")) {
            holder.imgLove.setImageResource(R.drawable.love_unliked);
        }

        holder.imgLove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callLikeUnlikeAPI(myShoprespons.getId(),position);
            }
        });

        holder.imgCommentShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.productIDfeedRow = myShoprespons.getId();
                AppConstant.tagUserList=myShoprespons.getTaggable_users();
                FragmentManager manager = ((Activity) con).getFragmentManager();
                CommantDialogFragment dialogMenu = new CommantDialogFragment();
                dialogMenu.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                    Log.e("Comment call","call");
                        holder.tvChate.setText(AppConstant.commentCount);
                    }
                });
                dialogMenu.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar);
                dialogMenu.show( manager, "");
            }
        });

        holder.tvRepostTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                repostDialoag(myShoprespons.getId());
            }
        });
        holder.tvMyBrand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.brandID = myShoprespons.getBrand_info().get(0).getId();
                AppConstant.brandName = myShoprespons.getBrand_info().get(0).getName();
                Log.e("BrandID", AppConstant.brandID);
                Log.e("BrandName", AppConstant.brandName);
                myCommunicator.setContentFragment(new BrandFragment(), true);
            }
        });
        holder.llEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.editable = "editable";
                final ProductAndUserDetailsInfo myShoprespons = productInfo.get(position);
                AppConstant.editabledraft="";
                AppConstant.feedRowObject = myShoprespons;
                myCommunicator.setContentFragment(new SellFragment(), true);
            }
        });
        holder.llMyShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.productIDfeedRow = myShoprespons.getId();
                AppConstant.feedRowObject = myShoprespons;
                myCommunicator.setContentFragment(new SingleProductFragment(), true);
            }
        });
        holder.llRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rowPosition = position;
                Log.e("rowPosition", String.valueOf(rowPosition));
                deleteProductDialoag(myShoprespons.getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != productInfo ? productInfo.size() : 0);
    }

    /**
     * ====================Delet Product dialog===========================================
     */
    public void deleteProductDialoag(final String product_id) {

        final Dialog bundleRemovedialog = new Dialog(con);
        bundleRemovedialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        bundleRemovedialog.setContentView(R.layout.dialog_remove);
        bundleRemovedialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView tvbundleCancel = (TextView) bundleRemovedialog.findViewById(R.id.tvbundleCancel);
        TextView tvBundleOk = (TextView) bundleRemovedialog.findViewById(R.id.tvBundleOk);
        TextView tvConfirm = (TextView) bundleRemovedialog.findViewById(R.id.tvConfirm);
        TextView tvDescrtion = (TextView) bundleRemovedialog.findViewById(R.id.tvDescrtion);
        tvConfirm.setText("Confirm");

        tvDescrtion.setText(con.getResources().getString(R.string.are_you_sure_shop));

        tvBundleOk.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                callDeleteProductAPI(product_id);
                bundleRemovedialog.dismiss();
            }
        });

        tvbundleCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundleRemovedialog.dismiss();
            }
        });

        bundleRemovedialog.show();
    }
    /**
     * ====================Delet Product dialog===========================================
     */
    public void repostDialoag(final String product_id) {

        final Dialog bundleRemovedialog = new Dialog(con);
        bundleRemovedialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        bundleRemovedialog.setContentView(R.layout.dialog_remove);
        bundleRemovedialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView tvbundleCancel = (TextView) bundleRemovedialog.findViewById(R.id.tvbundleCancel);
        TextView tvBundleOk = (TextView) bundleRemovedialog.findViewById(R.id.tvBundleOk);
        TextView tvConfirm = (TextView) bundleRemovedialog.findViewById(R.id.tvConfirm);
        TextView tvDescrtion = (TextView) bundleRemovedialog.findViewById(R.id.tvDescrtion);
        tvConfirm.setText("Confirm");
        tvDescrtion.setText("Are you sure to Repost to feed this\n item?");

        tvBundleOk.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                callRepostToAPI(AllURL.repostToFeedURL(),product_id);
                bundleRemovedialog.dismiss();
            }
        });

        tvbundleCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundleRemovedialog.dismiss();
            }
        });

        bundleRemovedialog.show();
    }
    /**
     * -------------------37. Delete Product------------------
     */
    private void callDeleteProductAPI(final String product_id) {
        /**
         * ---------------check internet first------------
         */
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, con.getString(R.string.status), con.getString(R.string.checkInternet));
            return;
        }
        /**
         * ----------------Show Busy Dialog -----------------------------------------
         */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
        /**
         *----------------------Start Thread-------------------------------------------
         */
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            String msg = "";
            String response = "";

            @Override
            public void run() {

                try {
                    Log.e("DeletePro URL", AllURL.getDelProduct(product_id));
                    //-------------Hit Server---------------------
                    response = AAPBDHttpClient.get(AllURL.getDelProduct(product_id)).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();

                    //----------------Persist response with the help of Gson----------------
                    Log.e("DeletePro Response", ">>" + response);
                    Gson gson = new Gson();
                    delResponse = gson.fromJson(response, DeleteProductResponse.class);

                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                con.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //-------Stop Busy Dialog-----
                        if (busy != null) {
                            busy.dismis();
                        }

                        /**
                         * ---------Main Ui related work--------------
                         */
                        if (delResponse.getStatus().equalsIgnoreCase("1")) {
                            Toast.makeText(con, "DeletePro Remove " + delResponse.getMsg(), Toast.LENGTH_LONG).show();
                            Log.e("rowPosition", String.valueOf(rowPosition));
                            productInfo.remove(rowPosition);
                            MyShopAdapter.this.notifyDataSetChanged();

                            if (productInfo.size() == 0) {
                                TextView tvDataNotFound = (TextView) view.findViewById(R.id.tvDataNotFound);
                                tvDataNotFound.setVisibility(View.VISIBLE);
                            }
                        } else {
                            msg = delResponse.getMsg();
                            AlertMessage.showMessage(con, "Bunlde", msg);
                        }

                    }
                });

            }

        });

    }

    /**
     * -------------------10. Like/Unlike Product API------------------
     */
    public void callLikeUnlikeAPI(final String product_id,  final int position) {
  /*
   * ---------------check internet first------------
   */
        if (!NetInfo.isOnline(con)) {
            AppConstant.alertDialoag(con, con.getString(R.string.status), con.getString(R.string.checkInternet), con.getString(R.string.ok));
            return;
        }

  /*
   * ----------------Show Busy Dialog -----------------------------------------
   */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
        /**
         *----------------------Start Thread-------------------------------------------
         */
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            String msg = "";
            String response = "";
            LikeUnlikeResponse likeUnlikeResponse;

            @Override
            public void run() {
                //--------------- We can performed your task here.-----------------

                try {
                    Log.i("Like/Unlike URL", AllURL.getLikeUnlike(product_id));
                    //-------------Hit Server---------------------
                    response = AAPBDHttpClient.get(AllURL.getLikeUnlike(product_id)).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();

                    //----------------Persist response with the help of Gson----------------
                    Log.i("Like/Unlike Response", ">>" + response);
                    Gson gson = new Gson();
                    likeUnlikeResponse = gson.fromJson(response, LikeUnlikeResponse.class);

                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                con.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        /**
                         * -------------Here we do UI related tasks inside run() method of runOnUiThread()-------------------
                         */

                        //-------Stop Busy Dialog-----
                        if (busy != null) {
                            busy.dismis();
                        }
                        /**
                         * ---------main Ui related work--------------
                         */
                        if (likeUnlikeResponse != null) {

                            if (likeUnlikeResponse.getMsg().equalsIgnoreCase("liked")) {
                                productInfo.get(position).setLike_count(likeUnlikeResponse.getLikes_count());
                                productInfo.get(position).setIs_liked("1");
                                MyShopAdapter.this.notifyDataSetChanged();

                            } else if (likeUnlikeResponse.getMsg().equalsIgnoreCase("unliked")) {
                                productInfo.get(position).setLike_count(likeUnlikeResponse.getLikes_count());
                                productInfo.get(position).setIs_liked("0");
                                MyShopAdapter.this.notifyDataSetChanged();
                            }

                        }

                    }
                });

            }

        });

    }

    // -------------Post Method For normal Using Login--------------------
    protected void callRepostToAPI(final String url, final String product_id) {
        /**
         * --------------- Check Internet------------
         */
        if (!NetInfo.isOnline(con)) {
            AppConstant.alertDialoag(con, con.getString(R.string.status), con.getString(R.string.checkInternet),"Close");
            return;
        }

        /**
         * ------Show Busy Dialog------------------
         */
        final BusyDialog busyNow = new BusyDialog(con, true, false);
        busyNow.show();
        /**
         * ---------Create object of  RequestParams to send value with URL---------------
         */
        final RequestParams param = new RequestParams();

        try {
            param.put("product_id", product_id);

        } catch (final Exception e1) {
            e1.printStackTrace();
        }
        /**
         * ---------Create object of  AsyncHttpClient class to heat server ---------------
         */
        final AsyncHttpClient client = new AsyncHttpClient();
        Log.i("Login URL ", ">>" + url);
        client.addHeader("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token));
        client.post(url, param, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers,
                                  byte[] response) {
                //-----------Off Busy Dialog ----------------
                if (busyNow != null) {
                    busyNow.dismis();
                }
                //-----------------Print Response--------------------
                Log.i("RepostResponse ", ">>" + new String(response));

                //------------Data persist using Gson------------------
                Gson g = new Gson();
               RepostToFeedResponse ropostResponse = g.fromJson(new String(response), RepostToFeedResponse.class);

                if (ropostResponse.getStatus().equalsIgnoreCase("1")) {

					Toast.makeText(con, ropostResponse.getMsg(), Toast.LENGTH_LONG).show();


                } else {
                    AppConstant.alertDialoag(con,"Status",ropostResponse.getMsg(),"Ok");
                    return;
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)

//				Log.e("LoginerrorResponse", new String(errorResponse));

                if (busyNow != null) {
                    busyNow.dismis();
                }
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried

            }
        });

    }

}
