package com.sloppy.adapter;

import android.Manifest;
import android.app.Activity;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sloppy.CommantDialogFragment;
import com.sloppy.CommunicatorFragmentInterface;
import com.sloppy.OtherUserDetailsFragment;
import com.sloppy.R;
import com.sloppy.SingleProductFragment;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.NetInfo;
import com.sloppy.aapbdLib.PersistData;
import com.sloppy.aapbdLib.PersistentUser;
import com.sloppy.model.LikeUnlikeResponse;
import com.sloppy.model.ProCommentListResultInfo;
import com.sloppy.model.ProductAndUserDetailsInfo;
import com.sloppy.utils.AAPBDHttpClient;
import com.sloppy.utils.AllURL;
import com.sloppy.utils.AppConstant;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import java.util.concurrent.Executors;

/**
 * Created by hp on 8/28/2016.
 */
public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.CustomViewHolder> {
    private List<ProCommentListResultInfo> myresponseList;
    private Activity con;
    public CommunicatorFragmentInterface myCommunicator;
    private String pictureUrl="";

    public CommentAdapter(Activity context, List<ProCommentListResultInfo> myresponseList) {
        this.con = context;
        this.myresponseList = myresponseList;
        try {
            myCommunicator = (CommunicatorFragmentInterface) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_comment_singleproduct, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public int getItemCount() {
        return (null != myresponseList ? myresponseList.size() : 0);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgCommentedPer;
        private TextView singleProComme,commentPerName,commentTime;


        public CustomViewHolder(View view) {
            super(view);

             this.imgCommentedPer = (ImageView) view.findViewById(R.id.imgCommentedPer);
             this.singleProComme = (TextView) view.findViewById(R.id.singleProComme);
             this.commentPerName = (TextView) view.findViewById(R.id.commentPerName);
            this.commentTime = (TextView) view.findViewById(R.id.commentTime);


        }
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, final int position) {
        final ProCommentListResultInfo query = myresponseList.get(position);
        try {
            /**
             * -----------------Set data on field of Comment row------------------
             */
            Picasso.with(con).load(query.getUserdetails().get(0).getProfile_image())
                    .placeholder(R.drawable.person).error(R.drawable.person).into(holder.imgCommentedPer);
//                    commentPerName.setText(query.getComments_text()+" @"+query.getTagged_user().get(0).getUsername());
            holder.singleProComme.setText(query.getComments_text());

            if (query.getTagged_user().size()>0){
                holder.singleProComme.setText(query.getComments_text()+" @"+query.getTagged_user().get(0).getUsername());
            }else{
                holder.singleProComme.setText(query.getComments_text());
            }
            holder.commentTime.setText(query.getCreated_at());
        } catch (Exception e) {
            e.printStackTrace();
        }

        /**
         * ==================ON Click===============================================================
         */

        holder.commentPerName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.otherUserId = query.getUserdetails().get(0).getUser_id();
                myCommunicator.setContentFragment(new OtherUserDetailsFragment(), true);

            }
        });
        holder.imgCommentedPer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.otherUserId = query.getUserdetails().get(0).getUser_id();
                myCommunicator.setContentFragment(new OtherUserDetailsFragment(), true);

            }
        });

//        holder.imgComentbdl.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                AppConstant.productIDfeedRow = brandList.getId();
//                AppConstant.tagUserList=brandList.getTaggable_users();
//                FragmentManager manager = ((Activity) con).getFragmentManager();
//                CommantDialogFragment dialogComment = new CommantDialogFragment();
//                dialogComment.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                    @Override
//                    public void onDismiss(DialogInterface dialog) {
//                        holder.imgComentCountBrand.setText(AppConstant.commentCount);
//                    }
//                });
//                dialogComment.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar);
//                dialogComment.show( manager, "");
//            }
//        });




    }


}
