package com.sloppy.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sloppy.BrandFragment;
import com.sloppy.CommunicatorFragmentInterface;
import com.sloppy.OtherUserDetailsFragment;
import com.sloppy.R;
import com.sloppy.SingleProductFragment;
import com.sloppy.aapbdLib.AlertMessage;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.NetInfo;
import com.sloppy.aapbdLib.PersistData;
import com.sloppy.model.DeleteProductResponse;
import com.sloppy.model.OfferAcceptResponse;
import com.sloppy.model.OfferResultInfo;
import com.sloppy.model.ProductAndUserDetailsInfo;
import com.sloppy.model.ProductList;
import com.sloppy.utils.AAPBDHttpClient;
import com.sloppy.utils.AllURL;
import com.sloppy.utils.AppConstant;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.concurrent.Executors;

/**
 * Created by hp on 8/9/2016.
 */
public class OfferListAdapter extends RecyclerView.Adapter<OfferListAdapter.MyViewHolder> {
    private Activity con;
    private List<OfferResultInfo> productInfo;
    public CommunicatorFragmentInterface myCommunicator;

    public OfferListAdapter(Activity con, List<OfferResultInfo> itemList) {
        this.con =con;
        this.productInfo = itemList;
        try {
            myCommunicator = (CommunicatorFragmentInterface) con;
        } catch (ClassCastException e) {
            throw new ClassCastException(con.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_offer, parent, false);
        return new MyViewHolder(itemView);
    }


    public  class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imgProOffer,imgTikOfr,imgCrossOfr;
        TextView tvProNameOffer,tvBrOffer,tvOriPriceOffer,tvlstPriceOffer,tvOfferPrice;
        public MyViewHolder(View view) {
            super(view);
            this.imgProOffer = (ImageView) view.findViewById(R.id.imgProOffer);
            this.imgTikOfr = (ImageView) view.findViewById(R.id.imgTikOfr);
            this.imgCrossOfr = (ImageView) view.findViewById(R.id.imgCrossOfr);
//            this.imgShareBrand = (ImageView) view.findViewById(R.id.imgShareBrand);
            this.tvProNameOffer = (TextView) view.findViewById(R.id.tvProNameOffer);
            this.tvBrOffer = (TextView) view.findViewById(R.id.tvBrOffer);
            this.tvOriPriceOffer = (TextView) view.findViewById(R.id.tvOriPriceOffer);
            this.tvlstPriceOffer = (TextView) view.findViewById(R.id.tvlstPriceOffer);
            this.tvOfferPrice = (TextView) view.findViewById(R.id.tvOfferPrice);

        }
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ProductAndUserDetailsInfo query = productInfo.get(position).getProduct_info();
        try {
            /**
             * -----------------Set data on field of Comment row------------------
             */
            holder.tvProNameOffer.setText(query.getName());
            holder.tvlstPriceOffer.setText(AppConstant.getCurrencySymbol(query.getSeller_info().get(0).getCurrency())+query.getListing_price());
            holder.tvOriPriceOffer.setText(AppConstant.getCurrencySymbol(query.getSeller_info().get(0).getCurrency()) + query.getOriginal_price());
            holder.tvOriPriceOffer.setPaintFlags(holder.tvOriPriceOffer.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.tvBrOffer.setText(query.getBrand_info().get(0).getName());
            holder.tvOfferPrice.setText(AppConstant.getCurrencySymbol(query.getSeller_info().get(0).getCurrency())+productInfo.get(position).getOffer_price());

            Picasso.with(con)
                    .load(query.getPhoto_one())
                    .placeholder(R.drawable.product_bg)
                    .error(R.drawable.product_bg)
                    .into(holder.imgProOffer);

            holder.imgTikOfr.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  callOfferAcceptAPI(productInfo.get(position).getOffer_id());
                }
            });

            holder.imgCrossOfr.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callOfferDeclineAPI(productInfo.get(position).getOffer_id());
                }
            });

            holder.tvBrOffer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppConstant.brandID=query.getBrand_info().get(0).getId();
                    AppConstant.brandName=query.getBrand_info().get(0).getName();
                    Log.e("BrandID",AppConstant.brandID);
                    Log.e("BrandName",AppConstant.brandName);
                    myCommunicator.setContentFragment(new BrandFragment(), true);

                }
            });


//            holder.itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    AppConstant.productIDfeedRow=query.getProduct_info().getId();
//                    AppConstant.feedRowObject=query.getProduct_info();
//                    myCommunicator.setContentFragment(new SingleProductFragment(), true);
//
//                }
//            });


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return productInfo.size();
    }

    /**
     * -------------------28.1. Offer Accept-----------------
     */
    private void callOfferAcceptAPI(final String offer_id) {
        /**
         * ---------------check internet first------------
         */
        if (!NetInfo.isOnline(con)) {
            AppConstant.alertDialoag(con, con.getResources().getString(R.string.alert), con.getResources().getString(R.string.checkInternet), con.getResources().getString(R.string.ok));
            return;
        }
        /**
         * ----------------Show Busy Dialog -----------------------------------------
         */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
        /**
         *----------------------Start Thread-------------------------------------------
         */
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            String msg = "";
            String response = "";
            OfferAcceptResponse acceptResponse;

            @Override
            public void run() {

                try {
                    Log.e("DeletePro URL", AllURL.getOfferAccept(offer_id));
                    //-------------Hit Server---------------------
                    response = AAPBDHttpClient.get(AllURL.getOfferAccept(offer_id)).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();

                    //----------------Persist response with the help of Gson----------------
                    Log.e("OfferAccept Response", ">>" + response);
                    Gson gson = new Gson();
                    acceptResponse = gson.fromJson(response, OfferAcceptResponse.class);

                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                con.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //-------Stop Busy Dialog-----
                        if (busy != null) {
                            busy.dismis();
                        }

                        /**
                         * ---------Main Ui related work--------------
                         */
                        if (acceptResponse.getStatus().equalsIgnoreCase("1")) {
                            Toast.makeText(con,  acceptResponse.getMsg(), Toast.LENGTH_LONG).show();

                        } else {
                            msg = acceptResponse.getMsg();
                            AlertMessage.showMessage(con, "Bunlde", msg);
                        }

                    }
                });

            }

        });

    }


    /**
     * -------------------28.1. Offer Accept-----------------
     */
    private void callOfferDeclineAPI(final String offer_id) {
        /**
         * ---------------check internet first------------
         */
        if (!NetInfo.isOnline(con)) {
            AppConstant.alertDialoag(con, con.getResources().getString(R.string.alert), con.getResources().getString(R.string.checkInternet), con.getResources().getString(R.string.ok));
            return;
        }
        /**
         * ----------------Show Busy Dialog -----------------------------------------
         */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
        /**
         *----------------------Start Thread-------------------------------------------
         */
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            String msg = "";
            String response = "";
            OfferAcceptResponse acceptResponse;

            @Override
            public void run() {

                try {
                    Log.e("DeletePro URL", AllURL.getOfferDecline(offer_id));
                    //-------------Hit Server---------------------
                    response = AAPBDHttpClient.get(AllURL.getOfferDecline(offer_id)).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();

                    //----------------Persist response with the help of Gson----------------
                    Log.e("OfferAccept Response", ">>" + response);
                    Gson gson = new Gson();
                    acceptResponse = gson.fromJson(response, OfferAcceptResponse.class);

                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                con.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //-------Stop Busy Dialog-----
                        if (busy != null) {
                            busy.dismis();
                        }

                        /**
                         * ---------Main Ui related work--------------
                         */
                        if (acceptResponse.getStatus().equalsIgnoreCase("1")) {
                            Toast.makeText(con,  acceptResponse.getMsg(), Toast.LENGTH_LONG).show();

                        } else {
                            msg = acceptResponse.getMsg();
                            AlertMessage.showMessage(con, "Bunlde", msg);
                        }

                    }
                });

            }

        });

    }
}
