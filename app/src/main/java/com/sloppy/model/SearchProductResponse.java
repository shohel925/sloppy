package com.sloppy.model;

/**
 * Created by hp on 7/21/2016.
 */
public class SearchProductResponse {

    private String status="";
    private String msg="";
    private SearchProductResult result= new SearchProductResult();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public SearchProductResult getResult() {
        return result;
    }

    public void setResult(SearchProductResult result) {
        this.result = result;
    }
}
