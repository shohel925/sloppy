package com.sloppy.model;

/**
 * Created by hp on 8/15/2016.
 */
public class OfferAcceptResponse {
    private  String code;
    private  String status;
    private  String msg;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
