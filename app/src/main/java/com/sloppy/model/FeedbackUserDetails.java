package com.sloppy.model;

/**
 * Created by hp on 4/4/2016.
 */
public class FeedbackUserDetails {

    private String user_id="";
    private String name="";
    private String profile_image="";

    /**
     * ----------------Getter and Setter----------------
     */
    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }
}
