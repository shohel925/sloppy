package com.sloppy.model;

import java.util.ArrayList;
import java.util.List;

public class CategoryInfo {
	private String id="id";
	private String name="name";
	private String description="description";
	private String image_man="image_man";
	private String image_woman="image_woman";
	private String root="root";
	private String type="type";
	private String created_at="created_at";
	private String updated_at="updated_at";
	private SubcategoryInfo subcategory =new SubcategoryInfo();
	
	
	
	//-----------setter and getter method---------------------------
	
	
	public String getId() {
		return id;
	}
	public String getImage_man() {
		return image_man;
	}
	public void setImage_man(String image_man) {
		this.image_man = image_man;
	}
	public String getImage_woman() {
		return image_woman;
	}
	public void setImage_woman(String image_woman) {
		this.image_woman = image_woman;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	public String getRoot() {
		return root;
	}
	public void setRoot(String root) {
		this.root = root;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}


	public SubcategoryInfo getSubcategory() {
		return subcategory;
	}

	public void setSubcategory(SubcategoryInfo subcategory) {
		this.subcategory = subcategory;
	}
}
