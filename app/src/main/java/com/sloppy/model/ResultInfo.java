package com.sloppy.model;

public class ResultInfo {
	
	private String id="id";
	private String user_id="user_id";
	private String name="name";
	private String description="description";
	private String photo_one="photo_one";
	private String photo_two="photo_two";
	private String photo_three="photo_three";
	private String photo_four="photo_four";
	private String category_id="category_id";
	private String sub_category_id="sub_category_id";
	private String brand_id="brand_id";
	private String size="size";
	private String new_tag="new_tag";
	private String suitable_for="suitable_for";
	private String original_price="original_price";
	private String listing_price="listing_price";
	private String delivery_type="delivery_type";
	private String delivery_charge="delivery_charge";
	private String status="status";
	private String created_at="created_at";
	private String updated_at="updated_at";
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPhoto_one() {
		return photo_one;
	}
	public void setPhoto_one(String photo_one) {
		this.photo_one = photo_one;
	}
	public String getPhoto_two() {
		return photo_two;
	}
	public void setPhoto_two(String photo_two) {
		this.photo_two = photo_two;
	}
	public String getPhoto_three() {
		return photo_three;
	}
	public void setPhoto_three(String photo_three) {
		this.photo_three = photo_three;
	}
	public String getPhoto_four() {
		return photo_four;
	}
	public void setPhoto_four(String photo_four) {
		this.photo_four = photo_four;
	}
	public String getCategory_id() {
		return category_id;
	}
	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}
	public String getSub_category_id() {
		return sub_category_id;
	}
	public void setSub_category_id(String sub_category_id) {
		this.sub_category_id = sub_category_id;
	}
	public String getBrand_id() {
		return brand_id;
	}
	public void setBrand_id(String brand_id) {
		this.brand_id = brand_id;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getNew_tag() {
		return new_tag;
	}
	public void setNew_tag(String new_tag) {
		this.new_tag = new_tag;
	}
	public String getSuitable_for() {
		return suitable_for;
	}
	public void setSuitable_for(String suitable_for) {
		this.suitable_for = suitable_for;
	}
	public String getOriginal_price() {
		return original_price;
	}
	public void setOriginal_price(String original_price) {
		this.original_price = original_price;
	}
	public String getListing_price() {
		return listing_price;
	}
	public void setListing_price(String listing_price) {
		this.listing_price = listing_price;
	}
	public String getDelivery_type() {
		return delivery_type;
	}
	public void setDelivery_type(String delivery_type) {
		this.delivery_type = delivery_type;
	}
	public String getDelivery_charge() {
		return delivery_charge;
	}
	public void setDelivery_charge(String delivery_charge) {
		this.delivery_charge = delivery_charge;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}
	
	


}
