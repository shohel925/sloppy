package com.sloppy.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hp on 8/9/2016.
 */
public class OfferResponse {

    private String status="";
    private String msg="";
    private List<OfferResultInfo> result= new ArrayList<>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<OfferResultInfo> getResult() {
        return result;
    }

    public void setResult(List<OfferResultInfo> result) {
        this.result = result;
    }
}
