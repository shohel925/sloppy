package com.sloppy.model;

import java.util.ArrayList;
import java.util.List;



import android.R.string;

public class FollowingFollowerResponse {
	
	private String code="code";
	private String status="";
	private String msg="";
	private List<FollowingFollowerResultInfo> result =new ArrayList<FollowingFollowerResultInfo>();
	
	
	//private String code="code";
	//---------------------Getter-----------
	public String getCode() {
		return code;
	}
	public List<FollowingFollowerResultInfo> getResult() {
		return result;
	}
	public void setResult(List<FollowingFollowerResultInfo> result) {
		this.result = result;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
}
