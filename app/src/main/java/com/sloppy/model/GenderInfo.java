package com.sloppy.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 8/9/2016.
 */
public class GenderInfo {
    private String sub_category_id="";
    private String name="";
    private String image_man="";
    private String image_woman="";
    private String type="";
    private String all_count="";
    private String man_count="";
    private String woman_count="";

    private List<SizesInfo> sizes =new ArrayList<SizesInfo>();


    public String getSub_category_id() {
        return sub_category_id;
    }

    public void setSub_category_id(String sub_category_id) {
        this.sub_category_id = sub_category_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage_man() {
        return image_man;
    }

    public void setImage_man(String image_man) {
        this.image_man = image_man;
    }

    public String getImage_woman() {
        return image_woman;
    }

    public void setImage_woman(String image_woman) {
        this.image_woman = image_woman;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAll_count() {
        return all_count;
    }

    public void setAll_count(String all_count) {
        this.all_count = all_count;
    }

    public String getMan_count() {
        return man_count;
    }

    public void setMan_count(String man_count) {
        this.man_count = man_count;
    }

    public String getWoman_count() {
        return woman_count;
    }

    public void setWoman_count(String woman_count) {
        this.woman_count = woman_count;
    }

    public List<SizesInfo> getSizes() {
        return sizes;
    }

    public void setSizes(List<SizesInfo> sizes) {
        this.sizes = sizes;
    }
}
