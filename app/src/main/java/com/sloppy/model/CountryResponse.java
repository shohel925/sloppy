package com.sloppy.model;

import java.util.ArrayList;

public class CountryResponse {

	ArrayList<CountryInfo> countries;

	public ArrayList<CountryInfo> getCountries() {
		return countries;
	}

	public void setCountries(ArrayList<CountryInfo> countries) {
		this.countries = countries;
	}

}
