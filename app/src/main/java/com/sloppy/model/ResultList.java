package com.sloppy.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hp on 5/17/2016.
 */
public class ResultList {

    private String id="";
    private String first_name="";
    private String last_name="";
    private String paypal_email="";
    private String profile_image="";
    private List<ProductAndUserDetailsInfo> products = new ArrayList<ProductAndUserDetailsInfo>();
    private String like_count="";
    private String bundle_count="";
    private String feedback_count="";
    private String rating_value="";
    private String follower_count="";
    private String following_count="";
    private String is_followed="";
    private String messages="";
    private String shop_items="";
    private String sold_items="";
    private String purchased_items="";
    private String username="";

    /**
     * ===================Getter and Setter=============================
     */
    public String getLike_count() {
        return like_count;
    }

    public void setLike_count(String like_count) {
        this.like_count = like_count;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPaypal_email() {
        return paypal_email;
    }

    public void setPaypal_email(String paypal_email) {
        this.paypal_email = paypal_email;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public List<ProductAndUserDetailsInfo> getProducts() {
        return products;
    }

    public void setProducts(List<ProductAndUserDetailsInfo> products) {
        this.products = products;
    }

    public String getBundle_count() {
        return bundle_count;
    }

    public void setBundle_count(String bundle_count) {
        this.bundle_count = bundle_count;
    }

    public String getFeedback_count() {
        return feedback_count;
    }

    public void setFeedback_count(String feedback_count) {
        this.feedback_count = feedback_count;
    }

    public String getRating_value() {
        return rating_value;
    }

    public void setRating_value(String rating_value) {
        this.rating_value = rating_value;
    }

    public String getFollower_count() {
        return follower_count;
    }

    public void setFollower_count(String follower_count) {
        this.follower_count = follower_count;
    }

    public String getFollowing_count() {
        return following_count;
    }

    public void setFollowing_count(String following_count) {
        this.following_count = following_count;
    }

    public String getIs_followed() {
        return is_followed;
    }

    public void setIs_followed(String is_followed) {
        this.is_followed = is_followed;
    }

    public String getMessages() {
        return messages;
    }

    public void setMessages(String messages) {
        this.messages = messages;
    }

    public String getShop_items() {
        return shop_items;
    }

    public void setShop_items(String shop_items) {
        this.shop_items = shop_items;
    }

    public String getSold_items() {
        return sold_items;
    }

    public void setSold_items(String sold_items) {
        this.sold_items = sold_items;
    }

    public String getPurchased_items() {
        return purchased_items;
    }

    public void setPurchased_items(String purchased_items) {
        this.purchased_items = purchased_items;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
