package com.sloppy.model;

/**
 * Created by hp on 7/21/2016.
 */
public class SearchUserResponse {
    private String status="";
    private String msg="";
    private SearchUserResult result =new SearchUserResult();
    private SearchUserPeginator paginator =new SearchUserPeginator();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public SearchUserResult getResult() {
        return result;
    }

    public void setResult(SearchUserResult result) {
        this.result = result;
    }

    public SearchUserPeginator getPaginator() {
        return paginator;
    }

    public void setPaginator(SearchUserPeginator paginator) {
        this.paginator = paginator;
    }
}
