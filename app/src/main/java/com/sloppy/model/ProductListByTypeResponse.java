package com.sloppy.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hp on 6/23/2016.
 */
public class ProductListByTypeResponse {
    private String code="";
    private String status="";
    private String msg="";
    private List<ProductList> result=new ArrayList<ProductList>();

    /**
     * =================Getter and Setter===========================================
     */
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<ProductList> getResult() {
        return result;
    }

    public void setResult(List<ProductList> result) {
        this.result = result;
    }
}
