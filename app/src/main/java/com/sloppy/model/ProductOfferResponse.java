package com.sloppy.model;

/**
 * Created by hp on 3/24/2016.
 */
public class ProductOfferResponse {

   private String code="";
    private String status="";
    private String msg="";

    /**
     * ------------------Getter and setter--------------------
     */
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
