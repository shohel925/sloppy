package com.sloppy.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hp on 5/24/2016.
 */
public class BrandResultList {

    private String id;
    private String user_id;
    private String product_id;
    private String seller_id;
    private String created_at;
    private String updated_at;
    private List<ProductAndUserDetailsInfo> product_info= new ArrayList<ProductAndUserDetailsInfo>();

    /**
     * ====================Getter and Setter========================================================
     */
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getSeller_id() {
        return seller_id;
    }

    public void setSeller_id(String seller_id) {
        this.seller_id = seller_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public List<ProductAndUserDetailsInfo> getProduct_info() {
        return product_info;
    }

    public void setProduct_info(List<ProductAndUserDetailsInfo> product_info) {
        this.product_info = product_info;
    }
}
