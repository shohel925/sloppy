package com.sloppy.model;

/**
 * Created by hp on 4/4/2016.
 */
public class SubmitFBResultInfo {
    private String id="";
    private String rating_value="";
    private String feedback_count="";
    /**
     * ----------------------Getter and Setter-------------------------
     */
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRating_value() {
        return rating_value;
    }

    public void setRating_value(String rating_value) {
        this.rating_value = rating_value;
    }

    public String getFeedback_count() {
        return feedback_count;
    }

    public void setFeedback_count(String feedback_count) {
        this.feedback_count = feedback_count;
    }
}
