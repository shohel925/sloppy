package com.sloppy.model;

import java.util.ArrayList;
import java.util.List;

public class ProductAndUserDetailsInfo {

	private String id="";
	private String user_id="";
	private String name="";
	private String username="";
	private String description="";
	private String photo_one="";
	private String photo_two="";
	private String photo_three="";
	private String photo_four="";
	private String category_id="";
	private String sub_category_id="";
	private String brand_id="";
	private String size="";
	private String color_code="";
	private String new_tag="";
	private String suitable_for="";
	private String original_price="";
	private String listing_price="";
	private String delivery_type="";
	private String delivery_charge="";
	private String status="";
	private String Is_highlighted="";
	private String heighlighted_sort_order="";
	private String created_at="";
	private String updated_at="";
	private String buyer_id="";
	private String is_archive="";
	private String is_sold="";
	private String total_count="";
	private SizeInfoObject size_info=new SizeInfoObject();
	private String hashtag_info;
	private List <HashtagInfo> hashtag_detail = new ArrayList<HashtagInfo>();
	private List <BrandInfo> brand_info = new ArrayList<BrandInfo>();
	private List <SellerInfo> seller_info = new ArrayList<SellerInfo>();
	private String comments_count;
	private String like_count;
	private String is_liked;
	private String bundle_added;
	private PaginatorInfo paginator = new PaginatorInfo();
	private List <TaggableUsers> taggable_users = new ArrayList<TaggableUsers>();
	private String bundle_id;

	public String getIs_archive() {
		return is_archive;
	}

	public void setIs_archive(String is_archive) {
		this.is_archive = is_archive;
	}

	public List<HashtagInfo> getHashtag_detail() {
		return hashtag_detail;
	}

	public void setHashtag_detail(List<HashtagInfo> hashtag_detail) {
		this.hashtag_detail = hashtag_detail;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 *==================Getter and Setter=====================================
     */


	public String getBuyer_id() {
		return buyer_id;
	}

	public void setBuyer_id(String buyer_id) {
		this.buyer_id = buyer_id;
	}

	public String getIs_sold() {
		return is_sold;
	}

	public void setIs_sold(String is_sold) {
		this.is_sold = is_sold;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPhoto_one() {
		return photo_one;
	}

	public void setPhoto_one(String photo_one) {
		this.photo_one = photo_one;
	}

	public String getPhoto_two() {
		return photo_two;
	}

	public void setPhoto_two(String photo_two) {
		this.photo_two = photo_two;
	}

	public String getPhoto_three() {
		return photo_three;
	}

	public void setPhoto_three(String photo_three) {
		this.photo_three = photo_three;
	}

	public String getPhoto_four() {
		return photo_four;
	}

	public void setPhoto_four(String photo_four) {
		this.photo_four = photo_four;
	}

	public String getCategory_id() {
		return category_id;
	}

	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}

	public String getSub_category_id() {
		return sub_category_id;
	}

	public void setSub_category_id(String sub_category_id) {
		this.sub_category_id = sub_category_id;
	}

	public String getBrand_id() {
		return brand_id;
	}

	public void setBrand_id(String brand_id) {
		this.brand_id = brand_id;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getColor_code() {
		return color_code;
	}

	public void setColor_code(String color_code) {
		this.color_code = color_code;
	}

	public String getNew_tag() {
		return new_tag;
	}

	public void setNew_tag(String new_tag) {
		this.new_tag = new_tag;
	}

	public String getSuitable_for() {
		return suitable_for;
	}

	public void setSuitable_for(String suitable_for) {
		this.suitable_for = suitable_for;
	}

	public String getOriginal_price() {
		return original_price;
	}

	public void setOriginal_price(String original_price) {
		this.original_price = original_price;
	}

	public String getListing_price() {
		return listing_price;
	}

	public void setListing_price(String listing_price) {
		this.listing_price = listing_price;
	}

	public String getDelivery_type() {
		return delivery_type;
	}

	public void setDelivery_type(String delivery_type) {
		this.delivery_type = delivery_type;
	}

	public String getDelivery_charge() {
		return delivery_charge;
	}

	public void setDelivery_charge(String delivery_charge) {
		this.delivery_charge = delivery_charge;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIs_highlighted() {
		return Is_highlighted;
	}

	public void setIs_highlighted(String is_highlighted) {
		Is_highlighted = is_highlighted;
	}

	public String getHeighlighted_sort_order() {
		return heighlighted_sort_order;
	}

	public void setHeighlighted_sort_order(String heighlighted_sort_order) {
		this.heighlighted_sort_order = heighlighted_sort_order;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getTotal_count() {
		return total_count;
	}

	public void setTotal_count(String total_count) {
		this.total_count = total_count;
	}

	public SizeInfoObject getSize_info() {
		return size_info;
	}

	public void setSize_info(SizeInfoObject size_info) {
		this.size_info = size_info;
	}

	public String getHashtag_info() {
		return hashtag_info;
	}

	public void setHashtag_info(String hashtag_info) {
		this.hashtag_info = hashtag_info;
	}

	public List<BrandInfo> getBrand_info() {
		return brand_info;
	}

	public void setBrand_info(List<BrandInfo> brand_info) {
		this.brand_info = brand_info;
	}

	public List<SellerInfo> getSeller_info() {
		return seller_info;
	}

	public void setSeller_info(List<SellerInfo> seller_info) {
		this.seller_info = seller_info;
	}

	public String getComments_count() {
		return comments_count;
	}

	public void setComments_count(String comments_count) {
		this.comments_count = comments_count;
	}

	public String getLike_count() {
		return like_count;
	}

	public void setLike_count(String like_count) {
		this.like_count = like_count;
	}

	public String getIs_liked() {
		return is_liked;
	}

	public void setIs_liked(String is_liked) {
		this.is_liked = is_liked;
	}

	public String getBundle_added() {
		return bundle_added;
	}

	public void setBundle_added(String bundle_added) {
		this.bundle_added = bundle_added;
	}

	public PaginatorInfo getPaginator() {
		return paginator;
	}

	public void setPaginator(PaginatorInfo paginator) {
		this.paginator = paginator;
	}

	public List<TaggableUsers> getTaggable_users() {
		return taggable_users;
	}

	public void setTaggable_users(List<TaggableUsers> taggable_users) {
		this.taggable_users = taggable_users;
	}

	public String getBundle_id() {
		return bundle_id;
	}

	public void setBundle_id(String bundle_id) {
		this.bundle_id = bundle_id;
	}
}
