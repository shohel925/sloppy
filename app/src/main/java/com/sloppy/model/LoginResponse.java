package com.sloppy.model;



public class LoginResponse {
	private String code = "";

	private String status = "";
	private String msg = "";
	ResultsInfo results = new ResultsInfo();
	private String token = "";
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public ResultsInfo getResults() {
		return results;
	}
	public void setResults(ResultsInfo results) {
		this.results = results;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	
	
}
