package com.sloppy.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hp on 8/9/2016.
 */
public class OfferResultInfo {

    private String offer_id="";
    private String product_id="";
    private String user_id="";
    private String offer_price="";
    private String status="";
    private String created_at="";
    private String updated_at="";
   private ProductAndUserDetailsInfo product_info= new ProductAndUserDetailsInfo();

    public String getOffer_id() {
        return offer_id;
    }

    public void setOffer_id(String offer_id) {
        this.offer_id = offer_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getOffer_price() {
        return offer_price;
    }

    public void setOffer_price(String offer_price) {
        this.offer_price = offer_price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public ProductAndUserDetailsInfo getProduct_info() {
        return product_info;
    }

    public void setProduct_info(ProductAndUserDetailsInfo product_info) {
        this.product_info = product_info;
    }
}
