package com.sloppy.model;

/**
 * Created by hp on 8/10/2016.
 */
public class HashtagInfo {
    private String id="";
    private String name="";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
