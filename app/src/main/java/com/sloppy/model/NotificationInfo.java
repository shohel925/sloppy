package com.sloppy.model;

import com.google.firebase.auth.UserInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hp on 8/9/2016.
 */
public class NotificationInfo {
    private String id;
    private String user_id;
    private String product_id;
    private String target_user_id;
    private String action_type;
    private String is_read;
    private String created_at;
    private String updated_at;
    private List<UserDetailInfo> user_info= new ArrayList<>();
    private String image_url;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getTarget_user_id() {
        return target_user_id;
    }

    public void setTarget_user_id(String target_user_id) {
        this.target_user_id = target_user_id;
    }

    public String getAction_type() {
        return action_type;
    }

    public void setAction_type(String action_type) {
        this.action_type = action_type;
    }

    public String getIs_read() {
        return is_read;
    }

    public void setIs_read(String is_read) {
        this.is_read = is_read;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public List<UserDetailInfo> getUser_info() {
        return user_info;
    }

    public void setUser_info(List<UserDetailInfo> user_info) {
        this.user_info = user_info;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }
}
