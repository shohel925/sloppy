package com.sloppy.model;

/**
 * Created by hp on 5/3/2016.
 */
public class BrandSize {

    private  String id="";
    private  String size="";

    /**
     * ----------------------Getter and Setter-------------------------------------
     */
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }
}
