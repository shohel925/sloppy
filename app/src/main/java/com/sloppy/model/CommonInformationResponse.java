package com.sloppy.model;

import java.util.ArrayList;
import java.util.List;

public class CommonInformationResponse {
	private String code="";
	private String status="";
	private String msg="";
	private String  most_popular_count="";
	private String paypal_key="";
	private List<HashtagsInfo> hashtags =new ArrayList<HashtagsInfo>();
	private List<BrandInfo> brand =new ArrayList<BrandInfo>();
	private List<CategoryInfo> category =new ArrayList<CategoryInfo>();

	public List<BrandInfo> getBrand() {
		return brand;
	}
	public void setBrand(List<BrandInfo> brand) {
		this.brand = brand;
	}
	public List<CategoryInfo> getCategory() {
		return category;
	}
	public void setCategory(List<CategoryInfo> category) {
		this.category = category;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public List<HashtagsInfo> getHashtags() {
		return hashtags;
	}
	public void setHashtags(List<HashtagsInfo> hashtags) {
		this.hashtags = hashtags;
	}


	public String getMost_popular_count() {
		return most_popular_count;
	}

	public void setMost_popular_count(String most_popular_count) {
		this.most_popular_count = most_popular_count;
	}

	public String getPaypal_key() {
		return paypal_key;
	}

	public void setPaypal_key(String paypal_key) {
		this.paypal_key = paypal_key;
	}
}
