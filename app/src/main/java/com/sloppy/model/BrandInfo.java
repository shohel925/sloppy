package com.sloppy.model;

/**
 * Created by User on 1/28/2016.
 */
public class BrandInfo {
	
	private String id="id";
    private String name="name";
	private String created_at="created_at";
	private String updated_at="updated_at";

		
	public String getName() {
		return name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}
	public void setName(String name) {
		this.name = name;
	}
    
    
    
    
}
