package com.sloppy.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hp on 6/23/2016.
 */
public class ProductList {
    private String id="";
    private String user_id="";
    private String seller_id="";
    private String product_id="";
    private String created_at="";
    private String updated_at="";
    private ProductAndUserDetailsInfo product_info=new ProductAndUserDetailsInfo();
    private String hashtag_info="";
    private List<BrandInfo> brand_info = new ArrayList<BrandInfo>();
    private String comments_count="";
    private String like_count="";
    private String is_liked="";
    private String bundle_added="";

    /**
     * ===================Getter and Setter=============================
     */
    public String getHashtag_info() {
        return hashtag_info;
    }

    public void setHashtag_info(String hashtag_info) {
        this.hashtag_info = hashtag_info;
    }

    public List<BrandInfo> getBrand_info() {
        return brand_info;
    }

    public void setBrand_info(List<BrandInfo> brand_info) {
        this.brand_info = brand_info;
    }

    public String getComments_count() {
        return comments_count;
    }

    public void setComments_count(String comments_count) {
        this.comments_count = comments_count;
    }

    public String getLike_count() {
        return like_count;
    }

    public void setLike_count(String like_count) {
        this.like_count = like_count;
    }

    public String getIs_liked() {
        return is_liked;
    }

    public void setIs_liked(String is_liked) {
        this.is_liked = is_liked;
    }

    public String getBundle_added() {
        return bundle_added;
    }

    public void setBundle_added(String bundle_added) {
        this.bundle_added = bundle_added;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public ProductAndUserDetailsInfo getProduct_info() {
        return product_info;
    }

    public void setProduct_info(ProductAndUserDetailsInfo product_info) {
        this.product_info = product_info;
    }

    public String getSeller_id() {
        return seller_id;
    }

    public void setSeller_id(String seller_id) {
        this.seller_id = seller_id;
    }
}
