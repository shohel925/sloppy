package com.sloppy.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hp on 5/24/2016.
 */
public class BundleListResponse {

    private String status;
    private String msg;
    private List<ResultList> result=new ArrayList<ResultList>();

    /**
     *============================Getter and Setter=================================================
     */

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<ResultList> getResult() {
        return result;
    }

    public void setResult(List<ResultList> result) {
        this.result = result;
    }

}
