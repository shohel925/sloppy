package com.sloppy.model;

/**
 * Created by hp on 7/18/2016.
 */
public class ProListBySubResponse {
    private String status="";
    private String msg="";
    private SubListResultInfo result= new SubListResultInfo();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public SubListResultInfo getResult() {
        return result;
    }

    public void setResult(SubListResultInfo result) {
        this.result = result;
    }
}
