package com.sloppy.model;

/**
 * Created by hp on 4/12/2016.
 */
public class FollowUnfollowResponse {
    private String status="";
    private String msg="";

    /**
     * -------------Getter and Setter----------------------------------
     */
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
