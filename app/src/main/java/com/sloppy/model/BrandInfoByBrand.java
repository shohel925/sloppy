package com.sloppy.model;

/**
 * Created by hp on 5/3/2016.
 */
public class BrandInfoByBrand {

    private String id="";
    private String name="";
    /**
     * --------------------Getter and Setter-------------------------------
     */
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
