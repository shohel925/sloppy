package com.sloppy.model;

/**
 * Created by hp on 4/11/2016.
 */
public class OtherUserDetailsResponse {
    private String code="";
    private String status="";
    private String msg="";
    private OtherUserDetailsInfo user_details= new OtherUserDetailsInfo();

    /**
     * ------------------Getter and Setter-----------------------------------------
     */
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public OtherUserDetailsInfo getUser_details() {
        return user_details;
    }

    public void setUser_details(OtherUserDetailsInfo user_details) {
        this.user_details = user_details;
    }
}
