package com.sloppy.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hp on 8/9/2016.
 */
public class NotificationResponse {

    private String status;
    private String msg;
    private List<NotificationInfo> notifications= new ArrayList<>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<NotificationInfo> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<NotificationInfo> notifications) {
        this.notifications = notifications;
    }
}
