package com.sloppy.model;

/**
 * Created by hp on 3/3/2016.
 */
public class SocialLoginResponse {
    private String code="";
    private String status="";
    private String msg="";
    private SocileUserInfo userdetails=new SocileUserInfo();
    private String token="";

    //-------Getter and Setter------------

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public SocileUserInfo getUserdetails() {
        return userdetails;
    }

    public void setUserdetails(SocileUserInfo userdetails) {
        this.userdetails = userdetails;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
