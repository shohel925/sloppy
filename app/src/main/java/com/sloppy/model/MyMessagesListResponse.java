package com.sloppy.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hp on 3/13/2016.
 */
public class MyMessagesListResponse {
    private String status="";
    private String msg="";
    private List<MyMessageResultInfo> result=new ArrayList<MyMessageResultInfo>();

    /**
     * ---------------Getter and Setter----------------
     */
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<MyMessageResultInfo> getResult() {
        return result;
    }

    public void setResult(List<MyMessageResultInfo> result) {
        this.result = result;
    }
}
