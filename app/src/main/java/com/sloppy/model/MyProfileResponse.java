package com.sloppy.model;

/**
 * Created by hp on 3/9/2016.
 */
public class MyProfileResponse {
    private String status="";
    private String msg="";
    private MyProfileUserdetailsInfo user_details= new  MyProfileUserdetailsInfo();


//-------------------Getter and Setter---------------------
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public MyProfileUserdetailsInfo getUser_details() {
        return user_details;
    }

    public void setUser_details(MyProfileUserdetailsInfo user_details) {
        this.user_details = user_details;
    }
}
