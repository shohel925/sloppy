package com.sloppy.model;

/**
 * Created by hp on 5/3/2016.
 */
public class ProductListByBrandResponse {

    private String code="";
    private String status="";
    private String msg="";
    private BrandResultInfo result= new BrandResultInfo();

/**
 * ---------------Getter and Setter----------------------
 */
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public BrandResultInfo getResult() {
        return result;
    }

    public void setResult(BrandResultInfo result) {
        this.result = result;
    }
}
