package com.sloppy.model;

/**
 * Created by hp on 3/2/2016.
 */
public class ProfileUpdateResponse {

    private String code="";
    private String status="";
    private String msg="";
    private UserdetailsInfo userdetails = new UserdetailsInfo();
    private String token="";



   //-----------------getter and setter-------
    public String getCode() {return code;}

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
