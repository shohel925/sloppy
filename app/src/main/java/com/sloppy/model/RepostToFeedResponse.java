package com.sloppy.model;

/**
 * Created by hp on 8/10/2016.
 */
public class RepostToFeedResponse {
    private String status;
    private String msg;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
