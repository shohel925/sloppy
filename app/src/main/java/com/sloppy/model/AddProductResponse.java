package com.sloppy.model;

public class AddProductResponse {

	private String code = "code";
	private String status = "status";
	private String msg = "msg";
	private ResultInfo results = new ResultInfo();
	
	
	
	//------------------Getter setter method------------
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public ResultInfo getResults() {
		return results;
	}
	public void setResults(ResultInfo results) {
		this.results = results;
	}
	

}
