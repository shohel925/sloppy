package com.sloppy.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hp on 3/20/2016.
 */
public class ProCommentListResultInfo {
    private String id="";
    private String user_id="";
    private String product_id="";
    private String comments_text="";
    private String tagged_user_id="";
    private String created_at="";
    private String updated_at="";
    private List<ProCommentListUserDetailsInfo> userdetails =new ArrayList<ProCommentListUserDetailsInfo>();
    private List<TaggableUsers> tagged_user =new ArrayList<TaggableUsers>();

    public String getTagged_user_id() {
        return tagged_user_id;
    }

    public void setTagged_user_id(String tagged_user_id) {
        this.tagged_user_id = tagged_user_id;
    }

    public List<TaggableUsers> getTagged_user() {
        return tagged_user;
    }

    public void setTagged_user(List<TaggableUsers> tagged_user) {
        this.tagged_user = tagged_user;
    }

    /**
     * ----------Getter and Setter Method--------------------
     */

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getComments_text() {
        return comments_text;
    }

    public void setComments_text(String comments_text) {
        this.comments_text = comments_text;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public List<ProCommentListUserDetailsInfo> getUserdetails() {
        return userdetails;
    }

    public void setUserdetails(List<ProCommentListUserDetailsInfo> userdetails) {
        this.userdetails = userdetails;
    }
}
