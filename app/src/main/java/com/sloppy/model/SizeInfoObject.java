package com.sloppy.model;

/**
 * Created by hp on 5/17/2016.
 */
public class SizeInfoObject {

    private String id="";
    private String size="";
    /**
     * =================getter and Setter===============================
     */
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }
}
