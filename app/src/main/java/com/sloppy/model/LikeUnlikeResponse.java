package com.sloppy.model;

/**
 * Created by hp on 3/15/2016.
 */
public class LikeUnlikeResponse {
    private String status="";
    private String msg="";
    private String likes_count="";

    /**
     * ---------------Getter and Setter Method----------------------------------
     */
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getLikes_count() {
        return likes_count;
    }

    public void setLikes_count(String likes_count) {
        this.likes_count = likes_count;
    }
}
