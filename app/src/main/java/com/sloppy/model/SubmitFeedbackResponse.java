package com.sloppy.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hp on 4/4/2016.
 */
public class SubmitFeedbackResponse {

    private String code="";
    private String status="";
    private String msg="";
    private List<SubmitFBResultInfo> result = new ArrayList<SubmitFBResultInfo>();


    /**
     *-----------------------Getter and Setter----------------------------
     */
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<SubmitFBResultInfo> getResult() {
        return result;
    }

    public void setResult(List<SubmitFBResultInfo> result) {
        this.result = result;
    }
}
