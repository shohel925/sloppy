package com.sloppy.model;

/**
 * Created by User on 8/9/2016.
 */
public class NotificationCountResponse {

    private int status;
    private String msg="";
    private String notifications="";

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getNotifications() {
        return notifications;
    }

    public void setNotifications(String notifications) {
        this.notifications = notifications;
    }
}
