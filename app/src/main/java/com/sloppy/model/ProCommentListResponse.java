package com.sloppy.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hp on 3/20/2016.
 */
public class ProCommentListResponse {

    private String code="";
    private String status="";
    private String msg="";
    private List<ProCommentListResultInfo> result=new ArrayList<ProCommentListResultInfo>();

    /**
     * ---------------getter and Setter method----------------
     */
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<ProCommentListResultInfo> getResult() {
        return result;
    }

    public void setResult(List<ProCommentListResultInfo> result) {
        this.result = result;
    }
}
