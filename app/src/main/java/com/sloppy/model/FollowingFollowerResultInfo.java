package com.sloppy.model;

public class FollowingFollowerResultInfo {
	private String id = "";
	private String name = "name";
	private String username = "";
	private String profile_image = "profile_image";
	private String currency = "currency";
	private String feedback_count = "";
	private String rating_value = "";
	private String follower_count = "";
	private String following_count = "";
	private String is_followed = "";
	private String messages = "";
	
	
	
	//--------------------Getter and Setter-----------------

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getProfile_image() {
		return profile_image;
	}
	public void setProfile_image(String profile_image) {
		this.profile_image = profile_image;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getFeedback_count() {
		return feedback_count;
	}
	public void setFeedback_count(String feedback_count) {
		this.feedback_count = feedback_count;
	}
	public String getRating_value() {
		return rating_value;
	}
	public void setRating_value(String rating_value) {
		this.rating_value = rating_value;
	}
	public String getFollower_count() {
		return follower_count;
	}
	public void setFollower_count(String follower_count) {
		this.follower_count = follower_count;
	}
	public String getFollowing_count() {
		return following_count;
	}
	public void setFollowing_count(String following_count) {
		this.following_count = following_count;
	}
	public String getIs_followed() {
		return is_followed;
	}
	public void setIs_followed(String is_followed) {
		this.is_followed = is_followed;
	}
	public String getMessages() {
		return messages;
	}
	public void setMessages(String messages) {
		this.messages = messages;
	}
	
	
}
