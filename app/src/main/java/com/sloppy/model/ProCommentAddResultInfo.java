package com.sloppy.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hp on 3/19/2016.
 */
public class ProCommentAddResultInfo {

    private String id="";
    private String user_id="";
    private String product_id="";
    private String comments_text="";
    private String created_at="";
    private String updated_at="";
    private List<ProCommentAddUserDetailsInfo> userdetails= new ArrayList<ProCommentAddUserDetailsInfo>();

/**
 * ---------------------Getter and setter Method------------------------------
 */
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getComments_text() {
        return comments_text;
    }

    public void setComments_text(String comments_text) {
        this.comments_text = comments_text;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public List<ProCommentAddUserDetailsInfo> getUserdetails() {
        return userdetails;
    }

    public void setUserdetails(List<ProCommentAddUserDetailsInfo> userdetails) {
        this.userdetails = userdetails;
    }
}
