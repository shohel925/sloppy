package com.sloppy.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hp on 3/7/2016.
 */
public class MyShopResponse {
    private String code;
    private String status="";
    private String msg="";
    private List<ProductAndUserDetailsInfo> result=new ArrayList<ProductAndUserDetailsInfo>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<ProductAndUserDetailsInfo> getResult() {
        return result;
    }

    public void setResult(List<ProductAndUserDetailsInfo> result) {
        this.result = result;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
