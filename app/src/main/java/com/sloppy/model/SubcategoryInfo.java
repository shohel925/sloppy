package com.sloppy.model;

import java.util.ArrayList;
import java.util.List;

public class SubcategoryInfo {

	List<GenderInfo> woman =new ArrayList<>();
	List<GenderInfo> man =new ArrayList<>();
	List<GenderInfo> both =new ArrayList<>();

	public List<GenderInfo> getWoman() {
		return woman;
	}

	public void setWoman(List<GenderInfo> woman) {
		this.woman = woman;
	}

	public List<GenderInfo> getMan() {
		return man;
	}

	public void setMan(List<GenderInfo> man) {
		this.man = man;
	}

	public List<GenderInfo> getBoth() {
		return both;
	}

	public void setBoth(List<GenderInfo> both) {
		this.both = both;
	}
}
