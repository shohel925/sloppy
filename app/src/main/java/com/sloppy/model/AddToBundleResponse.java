package com.sloppy.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hp on 3/28/2016.
 */
public class AddToBundleResponse {
    private String code="";
    private String status="";
    private String msg="";
    private List<BundleResultInfo> result=new ArrayList<BundleResultInfo>();

    /**
     * ----------------Getter and setter------------------------------
     */
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<BundleResultInfo> getResult() {
        return result;
    }

    public void setResult(List<BundleResultInfo> result) {
        this.result = result;
    }
}
