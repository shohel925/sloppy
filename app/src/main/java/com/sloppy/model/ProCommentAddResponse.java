package com.sloppy.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hp on 3/19/2016.
 */
public class ProCommentAddResponse {

    private String code="";
    private String status="";
    private String msg="";
    private ProCommentAddResultInfo result= new ProCommentAddResultInfo();

    /**
     * -------------------------Getter and setter Method-------------------------
     */
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ProCommentAddResultInfo getResult() {
        return result;
    }

    public void setResult(ProCommentAddResultInfo result) {
        this.result = result;
    }
}
