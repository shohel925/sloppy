package com.sloppy.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hp on 5/9/2016.
 */
public class ActivityListResponse {
    private String code="";
    private String status="";
    private String msg="";
    private List<ActivityResultInfo> result=new ArrayList<ActivityResultInfo>();

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<ActivityResultInfo> getResult() {
        return result;
    }

    public void setResult(List<ActivityResultInfo> result) {
        this.result = result;
    }
}
