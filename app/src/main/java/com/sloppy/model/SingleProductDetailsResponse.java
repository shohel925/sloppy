package com.sloppy.model;

/**
 * Created by hp on 3/22/2016.
 */
public class SingleProductDetailsResponse {

    private String code="";
    private String status="";
    private String msg="";
    private ProductAndUserDetailsInfo details= new ProductAndUserDetailsInfo();

    /**
     * ----------------Getter and Setter--------------------------------------------
     */
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ProductAndUserDetailsInfo getDetails() {
        return details;
    }

    public void setDetails(ProductAndUserDetailsInfo details) {
        this.details = details;
    }
}
