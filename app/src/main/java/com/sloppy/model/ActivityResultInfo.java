package com.sloppy.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hp on 5/9/2016.
 */
public class ActivityResultInfo {

    private String day="";
    private List<ActivitiesInfo> activities=new ArrayList<ActivitiesInfo>();

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public List<ActivitiesInfo> getActivities() {
        return activities;
    }

    public void setActivities(List<ActivitiesInfo> activities) {
        this.activities = activities;
    }
}
