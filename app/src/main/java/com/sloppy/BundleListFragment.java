package com.sloppy;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.sloppy.aapbdLib.AlertMessage;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.NetInfo;
import com.sloppy.aapbdLib.PersistData;
import com.sloppy.model.AddToBundleResponse;
import com.sloppy.model.BundleListResponse;
import com.sloppy.model.ProductAndUserDetailsInfo;
import com.sloppy.model.ResultList;
import com.sloppy.utils.AAPBDHttpClient;
import com.sloppy.utils.AllURL;
import com.sloppy.utils.AppConstant;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.concurrent.Executors;


/**
 * Created by hp on 7/27/2016.
 */
public class BundleListFragment extends BaseFragment {
    Context con;
    View view;
    private ListView lVBundle;
    private BundleListResponse myResponse;
    private AddToBundleResponse bundleResponse;
    private MyArrayListAdapter myArrayListAdapter;
    private int myBundlePosition;


    /**
     * ========================On Create================================
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        con = getActivity();
        view = inflater.inflate(R.layout.fragment_my_bundle, container, false);
        initUI();
        return view;
    }

    private void initUI() {
        /**
         * =======================Call 18.1 Bundle List=============================================
         */

        /**
         * ========================Initialization===================================================
         */
        ImageView imgMyBundleBack = (ImageView) view.findViewById(R.id.imgMyBundleBack);
        lVBundle = (ListView) view.findViewById(R.id.lVBundle);

        myArrayListAdapter = new MyArrayListAdapter(con, AppConstant.productList);
        lVBundle.setAdapter(myArrayListAdapter);
        myArrayListAdapter.notifyDataSetChanged();


        /**
         * ==================On Click===============================================================
         */
        imgMyBundleBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
    }



    private class MyArrayListAdapter extends ArrayAdapter<ProductAndUserDetailsInfo> {
        Context con;
        ImageView  imgBundle, imgLoveBundle;
        TextView tvBundlefrom, tvCountItem, tvProNameBundle, tvBrandNameBund,
                tvOriginalPriceBund, tvlistingPriceBdl, tvTotalAmount, tvDiscountRate, tvNetTotal;
        LinearLayout llEdit, llMyShop, llRemove;
        List<ProductAndUserDetailsInfo> mylist;

        MyArrayListAdapter(Context context, List<ProductAndUserDetailsInfo> list) {
            super(context, R.layout.row_bundle, list);
            this.con = context;
            mylist=list;
        }

        @Override
        public View getView(final int position, View v, ViewGroup viewGroup) {

            if (v == null) {
                final LayoutInflater inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = inflater.inflate(R.layout.row_bundle, null);
            }
            /**
             * ---------------------Initialization of bundle row field------------------------------
             */
            imgBundle = (ImageView) v.findViewById(R.id.imgBundle);
            imgLoveBundle = (ImageView) v.findViewById(R.id.imgLoveBundle);
            LinearLayout lLCancel = (LinearLayout) v.findViewById(R.id.lLCancel);
            tvProNameBundle = (TextView) v.findViewById(R.id.tvProNameBundle);
            tvBrandNameBund = (TextView) v.findViewById(R.id.tvBrandNameBund);
            tvOriginalPriceBund = (TextView) v.findViewById(R.id.tvOriginalPriceBund);
            tvlistingPriceBdl = (TextView) v.findViewById(R.id.tvlistingPriceBdl);
            TextView tvLikeCountBdl = (TextView) v.findViewById(R.id.tvLikeCountBdl);
            TextView imgComentCountBdl = (TextView) v.findViewById(R.id.imgComentCountBdl);

            /**
             * ==========================Set data on the field of Bundle row====================
             */

            //==== Quary To get data from vector/Arrraylist according to there position============
            final ProductAndUserDetailsInfo query = mylist.get(position);

            if (position < mylist.size()) {

                try {
                    ProductAndUserDetailsInfo productInfo = query;

                    Picasso.with(con).load(query.getPhoto_one())
                            .placeholder(R.drawable.person).error(R.drawable.person).into(imgBundle);
                    tvProNameBundle.setText(query.getName());
                    tvBrandNameBund.setText(productInfo.getBrand_info().get(0).getName());
                    tvOriginalPriceBund.setText(AppConstant.getCurrencySymbol(productInfo
                            .getSeller_info().get(0).getCurrency()) + productInfo.getOriginal_price());
                    tvOriginalPriceBund.setPaintFlags(tvOriginalPriceBund.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    tvlistingPriceBdl.setText(AppConstant.getCurrencySymbol(productInfo.getSeller_info()
                            .get(0).getCurrency()) + productInfo.getListing_price());
                    tvLikeCountBdl.setText(productInfo.getLike_count());
                    if (productInfo.getIs_liked().equalsIgnoreCase("1")) {
                        imgLoveBundle.setBackgroundResource(R.drawable.love_liked);
                    } else if (productInfo.getIs_liked().equalsIgnoreCase("1")) {
                        imgLoveBundle.setBackgroundResource(R.drawable.love_unliked);
                    }
                    imgComentCountBdl.setText(productInfo.getComments_count());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                /**
                 * ========================On click=================================================
                 */
                tvBrandNameBund.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AppConstant.brandID=query.getBrand_info().get(0).getId();
                        AppConstant.brandName=query.getBrand_info().get(0).getName();
                        Log.e("BrandID",AppConstant.brandID);
                        Log.e("BrandName",AppConstant.brandName);
                        myCommunicator.addContentFragment(new BrandFragment(), true);

                    }
                });
                lLCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myBundlePosition = position;
                        bundleRemoveDialoag(con, query.getId(), query.getSeller_info().get(0).getUser_id());
                    }
                });

                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AppConstant.productIDfeedRow = query.getId();
                        AppConstant.feedRowObject = query;
                        myCommunicator.addContentFragment(new SingleProductFragment(), true);
                    }
                });


            }
            return v;

        }

    }

    /**
     * ====================Bundle Remove dialog===========================================
     */
    public void bundleRemoveDialoag(final Context con, final String bundleID, final String sellerID) {

        final Dialog bundleRemovedialog = new Dialog(con);
        bundleRemovedialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        bundleRemovedialog.setContentView(R.layout.dialog_bundle_remov);

        bundleRemovedialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        TextView tvbundleCancel = (TextView) bundleRemovedialog
                .findViewById(R.id.tvbundleCancel);
        TextView tvBundleOk = (TextView) bundleRemovedialog.findViewById(R.id.tvBundleOk);

        tvBundleOk.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                bundleRemovedialog.dismiss();
            }
        });

        tvbundleCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBundleRemoveAPI(bundleID, sellerID);
                bundleRemovedialog.dismiss();
            }
        });

        bundleRemovedialog.show();
    }

    /**
     * -------------------18.2 Bundle remove API------------------
     */
    private void callBundleRemoveAPI(final String bundle_id, final String seller_id) {
      /*
       * ---------------check internet first------------
       */
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }
         /*
          * ----------------Show Busy Dialog -----------------------------------------
          */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
      /*
       *----------------------Start Thread-------------------------------------------
       */
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            String msg = "";
            String response = "";

            @Override
            public void run() {
                // You can performed your task here.

                try {
                    Log.e("AddBundle URL", AllURL.getBundleRemove(bundle_id, seller_id));
                    //-------------Hit Server---------------------
                    response = AAPBDHttpClient.get(AllURL.getBundleRemove(bundle_id, seller_id)).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();

                    //----------------Persist response with the help of Gson----------------
                    Log.e("AddBundle Response", ">>" + response);
                    Gson gson = new Gson();
                    bundleResponse = gson.fromJson(response, AddToBundleResponse.class);

                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //-------Stop Busy Dialog-----
                        if (busy != null) {
                            busy.dismis();
                        }

                        /**
                         * ---------main Ui related work--------------
                         */
                        if (bundleResponse.getStatus().equalsIgnoreCase("1")) {
                            Toast.makeText(con, "Bundle Remove " + bundleResponse.getMsg(), Toast.LENGTH_LONG).show();

                            Log.e("bundleRowPosition", String.valueOf(myBundlePosition));
                            myResponse.getResult().remove(myBundlePosition);
                            myArrayListAdapter.notifyDataSetChanged();
                        } else {
                            msg = bundleResponse.getMsg();
                            AlertMessage.showMessage(con, "Bunlde", msg);
                        }

                    }
                });

            }

        });

    }
}
