package com.sloppy;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sloppy.aapbdLib.AlertMessage;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.NetInfo;
import com.sloppy.aapbdLib.PersistData;
import com.sloppy.model.FollowUnfollowResponse;
import com.sloppy.model.FollowingFollowerResponse;
import com.sloppy.model.FollowingFollowerResultInfo;
import com.sloppy.utils.AAPBDHttpClient;
import com.sloppy.utils.AllURL;
import com.sloppy.utils.AppConstant;
import com.squareup.picasso.Picasso;
import java.util.concurrent.Executors;


/**
 * Created by hp on 4/18/2016.
 */
public class FollowersFollowingFragment extends BaseFragment {
    Context con;
    View view;
    private String followerType;
    private String typeFollow;
    private ListView lvFollowerFollowing;
    private LinearLayout llFollowers, llFollowing;
    private TextView tvfollUserNane, tvUsefolllower, tvUserFollowing,tvFollowerFollowing;
    private ImageView imgUserfolllowers, imgUserFollowing;
    //----------Response----------------
    private FollowingFollowerResponse myFollowersResponse;

    //----------Adapter-----------------------------
    private MyFollowingAdapter myFollowingAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        con = getActivity();
        view = inflater.inflate(R.layout.fragment_my_followers, container, false);
        initUI();
        return view;
    }

    private void initUI() {
        /**
         * ----------------Initialization-------------------------
         */

        lvFollowerFollowing = (ListView) view.findViewById(R.id.lvFollowerFollowing);
        llFollowers = (LinearLayout) view.findViewById(R.id.llFollowers);
        llFollowing = (LinearLayout) view.findViewById(R.id.llFollowing);
        tvUsefolllower = (TextView) view.findViewById(R.id.tvUsefolllower);
        tvUserFollowing = (TextView) view.findViewById(R.id.tvUserFollowing);
        imgUserfolllowers = (ImageView) view.findViewById(R.id.imgUserfolllowers);
        imgUserFollowing = (ImageView) view.findViewById(R.id.imgUserFollowing);
        tvUserFollowing = (TextView) view.findViewById(R.id.tvUserFollowing);
        tvfollUserNane = (TextView) view.findViewById(R.id.tvfollUserNane);
        ImageView imgMenuFoll = (ImageView) view.findViewById(R.id.imgMenuFoll);
        /**
         * ------------------Set Data---------------------------
         */

        if (AppConstant.followingFollower.equalsIgnoreCase("following")) {
            tvfollUserNane.setText(getArguments().getString("title") + "'s Following");
            changeUserBg("following");
            callFollowersFollowingAPI("following", AppConstant.otherUserIdFoll);
        } else if (AppConstant.followingFollower.equalsIgnoreCase("follower")) {
            tvfollUserNane.setText(getArguments().getString("title") + "'s Followers");
            changeUserBg("follower");
            callFollowersFollowingAPI("follower", AppConstant.otherUserIdFoll);
        }
        ImageView imgMyFollowersBack = (ImageView) view.findViewById(R.id.imgMyFollowersBack);

        /**
         * --------------------On click----------------------------------------
         */
        imgMyFollowersBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        imgMenuFoll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.showMenuDiadog(getActivity().getFragmentManager());
            }
        });
        llFollowers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tvfollUserNane.setText(getArguments().getString("title") + "'s Followers");
                changeUserBg("follower");
                AppConstant.followingFollower="follower";
                callFollowersFollowingAPI("follower", AppConstant.otherUserIdFoll);
            }
        });
        llFollowing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.followingFollower="following";
                tvfollUserNane.setText(getArguments().getString("title") + "'s Following");
                changeUserBg("following");
                callFollowersFollowingAPI("following", AppConstant.otherUserIdFoll);
            }
        });
    }


    /**
     * ---------------------Change Background-----------------
     */
    private void changeUserBg(String folloFollowing) {
        if (folloFollowing.equalsIgnoreCase("follower")) {
            tvUsefolllower.setTextColor(Color.parseColor("#FB9A7F"));
            imgUserfolllowers.setImageResource(R.drawable.lil_right_arrow_select);
            tvUserFollowing.setTextColor(Color.parseColor("#999999"));
            imgUserFollowing.setImageResource(R.drawable.rrrrr);
        } else if (folloFollowing.equalsIgnoreCase("following")) {
            tvUserFollowing.setTextColor(Color.parseColor("#FB9A7F"));
            imgUserFollowing.setImageResource(R.drawable.rrrrr_s);
            tvUsefolllower.setTextColor(Color.parseColor("#999999"));
            imgUserfolllowers.setImageResource(R.drawable.lil_right_arrow);
        }
    }

/*
*   -------------------API-21. Following/Follower List -----------------------------
 */

    protected void callFollowersFollowingAPI(final String type, final String user_id) {
  /*
   * check internet first
   */
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }

  /*
   * start progress bar
   */

        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
  /*
   * start Thread
   *
   */
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            String msg = "";
            String response = "";

            @Override
            public void run() {

                try {
                    Log.e("20.Follower/folling URL", AllURL.getFolowersFollingList(type, user_id,"all"));
                    response = AAPBDHttpClient.get(AllURL.getFolowersFollingList(type, user_id,"all")).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();
                    Gson gson = new Gson();
                    myFollowersResponse = gson.fromJson(response, FollowingFollowerResponse.class);

                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
      /*
       *----------------- stop progress bar--------------------
       */
                        if (busy != null) {
                            busy.dismis();
                        }
                        if (myFollowersResponse.getStatus().equalsIgnoreCase("1")) {
                            followerType=type;
                            Log.e("MyFolowersResponse", ">>" + response);
                            //-------------------Call adapter----------------------
                            myFollowingAdapter = new MyFollowingAdapter(con);
                            lvFollowerFollowing.setAdapter(myFollowingAdapter);
                            myFollowingAdapter.notifyDataSetChanged();
                        } else {
                            msg = myFollowersResponse.getMsg();
                            AlertMessage.showMessage(con, "My follow ", msg);
                        }
                    }
                });

            }
        });
    }



    //
    private class MyFollowingAdapter extends ArrayAdapter<FollowingFollowerResultInfo> {
        Context context;

        MyFollowingAdapter(Context context) {
            super(context, R.layout.row_followrs, myFollowersResponse.getResult());
            this.context = context;
        }

        @Override
        public View getView(final int position, View view, ViewGroup parent) {

            if (view == null) {
                final LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = vi.inflate(R.layout.row_followrs, null);
            }

            if (position < myFollowersResponse.getResult().size()) {

                //------------Query------------------------------
                final FollowingFollowerResultInfo query = myFollowersResponse.getResult().get(position);
                /**
                 * --------------------Initialization-----------------------------
                 */
                final ImageView imgFollowerFolloing = (ImageView) view.findViewById(R.id.imgFollowerFolloing);

                final TextView tvUserName = (TextView) view.findViewById(R.id.tvUserName);
                tvFollowerFollowing=(TextView) view.findViewById(R.id.tvFollowerFollowing);
                /**
                 * ---------------Set Data--------------------------------
                 */
                if (followerType.equalsIgnoreCase("follower")){
                    if (query.getIs_followed().equalsIgnoreCase("0") ) {
                        tvFollowerFollowing.setText(getResources().getString(R.string.follow));
                    } else {
                        tvFollowerFollowing.setText(getResources().getString(R.string.unfollow));
                    }

                } else if (followerType.equalsIgnoreCase("following")){
                    tvFollowerFollowing.setText(getResources().getString(R.string.unfollow));
                }

                tvFollowerFollowing.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                      callFollowUnfollowAPI(query.getId(),position);
                    }
                });
                tvUserName.setText(query.getUsername());
                Picasso.with(con)
                        .load(query.getProfile_image())
                        .placeholder(R.drawable.person)
                        .error(R.drawable.person)
                        .into(imgFollowerFolloing);
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AppConstant.otherUserId = query.getId();
                        myCommunicator.setContentFragment(new OtherUserDetailsFragment(), true);
                    }
                });
            }
            return view;
        }
    }

    /**
     * -------------------20. Follow/Unfollow a user------------------
     */
    private void callFollowUnfollowAPI(final String user_id,final int position) {
  /*
   * ---------------check internet first------------
   */
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }

         /*
          * ----------------Show Busy Dialog -----------------------------------------
          */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
  /*
   *
   *----------------------Start Thread-------------------------------------------
   *
   */
        Executors.newSingleThreadExecutor().submit(new Runnable() {
            FollowUnfollowResponse followUnfollowResponse;
            String msg = "";
            String response = "";

            @Override
            public void run() {
                // You can performed your task here.

                try {
                    Log.e("20.Follow/Unfollow URL", AllURL.getFollowUnfollowUrl(user_id));
                    //-------------Hit Server---------------------
                    response = AAPBDHttpClient.get(AllURL.getFollowUnfollowUrl(user_id)).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();

                    //----------------Persist response with the help of Gson----------------
                    Log.e("20.Follow/UnfolResponse", ">>" + response);
                    Gson gson = new Gson();
                    followUnfollowResponse = gson.fromJson(response, FollowUnfollowResponse.class);

                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        /**
                         * -------------Here we do UI related tasks inside run() method of runOnUiThread()-------------------
                         */
                        //-------Stop Busy Dialog-----
                        if (busy != null) {
                            busy.dismis();
                        }

                        /**
                         * ---------main Ui related work--------------
                         */
                        if (followUnfollowResponse.getStatus().equalsIgnoreCase("1")) {
                            Toast.makeText(con, followUnfollowResponse.getMsg(), Toast.LENGTH_LONG).show();
                            if (followerType.equalsIgnoreCase("follower")){
                                if (myFollowersResponse.getResult().get(position).getIs_followed().equalsIgnoreCase("0")) {
                                    myFollowersResponse.getResult().get(position).setIs_followed("1");
                                    myFollowingAdapter.notifyDataSetChanged();
                                } else{
                                    myFollowersResponse.getResult().get(position).setIs_followed("0");
                                    myFollowingAdapter.notifyDataSetChanged();
                                }
                            } else if (followerType.equalsIgnoreCase("following")){
                                myFollowersResponse.getResult().remove(position);
                                myFollowingAdapter.notifyDataSetChanged();
                            }

                        } else {

                            msg = followUnfollowResponse.getMsg();
                            AlertMessage.showMessage(con, "Follow/Unfollow", msg);

                        }

                    }
                });

            }

        });

    }
}
