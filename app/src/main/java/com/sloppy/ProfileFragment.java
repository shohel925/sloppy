package com.sloppy;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sloppy.aapbdLib.AlertMessage;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.NetInfo;
import com.sloppy.aapbdLib.PersistData;
import com.sloppy.aapbdLib.PersistentUser;
import com.sloppy.aapbdLib.StartActivity;
import com.sloppy.model.FeedbackResultInfo;
import com.sloppy.model.MyProfileResponse;
import com.sloppy.model.SubmitFeedbackResponse;
import com.sloppy.model.UserFeedbacksResponse;
import com.sloppy.utils.AAPBDHttpClient;
import com.sloppy.utils.AllURL;
import com.sloppy.utils.AppConstant;
import com.squareup.picasso.Picasso;

import java.util.concurrent.Executors;


public class ProfileFragment extends BaseFragment {
    /**
     * ---------------------Variable-------------------
     */
    private DatabaseHandler dbholder;
    private Context con;
    private int ratingCount = 0;
    private TextView tvFeedBackText, tvFBUserName, feedbackTime, tvRatingCount, tvFllowersShow, tvFeedBackPro, tvDraftCount,
            tvMyRatingProf, tvNickname, tvFeedbackCount, tvMRCount, tvMyShopCount, tvMyBundleCount, tvMyLikesCount, tnNoFeedback,
            tvSoldItemCount, tvPurchaedItemCount, tvMessageCount, tvFirstFollowersName, tvSecFollerName, tvFirstFollowingName,
            tvSecFollowingName, tvFlowersCount, tvFlowingCount, tvFollowers, tvFollowing, tvSettingPro, tvEditPro, tvOfferCount,
            tvLogoutPro, tvMyShopPro, tvMyBundle, tvMyLikes, tvSoldItem, tvpurchaed, tvDrafts, tvNotification, tvMessage, tvContuct;
    private EditText etWriteFeedback;
    private ImageView imgFeedbackUser, imageViewRate, ivUser, ivFirstFollower, ivSecFollower, ivFirstFlowing, ivSecFollowing, imageViewFeedBack;
    private View view;
    private RatingBar rb;
    private MyProfileResponse myProfileResponse;
    private RelativeLayout relativeLayoutHasa, rlSecondFollowing, rlSecondFollower, rlOffer;
    private LinearLayout llFollowing, llFollower, logoutLayout, edit_profile;
    protected OnFragmentInteractionListener mListener;
    private TextView tvNotificationCount;
    //--------------Response---------------------------
    private UserFeedbacksResponse feedbacksResponse;
    private SubmitFeedbackResponse submitFeedbackResponse;
    //-------------Adapter-----------------
    FeedbacksListAdapter feedbacksListAdapter;
    private Typeface helveticaNeuRegular, helveticaNeueBold;


    /**
     * ----------------------------On Create Method----------------------
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        con = getActivity();
        if (!PersistentUser.isLogged(con)) {
            // ------- Log in or not  dialog show----------
            AppConstant.loginDialoag(con);

        } else {
            //======N.B: Becareful First inflate view and then call initUI(); ============
            view = inflater.inflate(R.layout.fragment_profile, container, false);
            dbholder = new DatabaseHandler(con);
            initUI(view);
            /**
             * ----------------Call 24. My Profile API----------------------
             */

            callProfileAPI();
        }
        return view;
    }


    /**
     * -----------------------------Initialization and Onclick-------------------------------------------
     */
    private void initUI(View view) {
        tvNotificationCount=(TextView)view.findViewById(R.id.tvNotificationCount);
        imageViewRate = (ImageView) view.findViewById(R.id.imageViewRate);
        logoutLayout = (LinearLayout) view.findViewById(R.id.logoutLayout);
        edit_profile = (LinearLayout) view.findViewById(R.id.edit_profile);
        imageViewFeedBack = (ImageView) view.findViewById(R.id.imageViewFeedBack);
        tvFllowersShow = (TextView) view.findViewById(R.id.tvFllowersShow);
        ivUser = (ImageView) view.findViewById(R.id.ivUser);
        ivFirstFollower = (ImageView) view.findViewById(R.id.ivFirstFollower);
        ivSecFollower = (ImageView) view.findViewById(R.id.ivSecFollower);
        ivFirstFlowing = (ImageView) view.findViewById(R.id.ivFirstFlowing);
        ivSecFollowing = (ImageView) view.findViewById(R.id.ivSecFollowing);
        tvFeedBackPro = (TextView) view.findViewById(R.id.tvFeedBackPro);
        tvMyRatingProf = (TextView) view.findViewById(R.id.tvMyRatingProf);
        tvNickname = (TextView) view.findViewById(R.id.tvNickname);
        tvFeedbackCount = (TextView) view.findViewById(R.id.tvFeedbackCount);
        tvMRCount = (TextView) view.findViewById(R.id.tvMRCount);
        tvMyShopCount = (TextView) view.findViewById(R.id.tvMyShopCount);
        tvMyBundleCount = (TextView) view.findViewById(R.id.tvMyBundleCount);
        tvMyLikesCount = (TextView) view.findViewById(R.id.tvMyLikesCount);
        tvSoldItemCount = (TextView) view.findViewById(R.id.tvSoldItemCount);
        tvPurchaedItemCount = (TextView) view.findViewById(R.id.tvPurchaedItemCount);
        tvOfferCount = (TextView) view.findViewById(R.id.tvOfferCount);
        tvDraftCount = (TextView) view.findViewById(R.id.tvDraftCount);
//        tvMessageCount = (TextView) view.findViewById(R.id.tvMessageCount);
        tvFirstFollowersName = (TextView) view.findViewById(R.id.tvFirstFollowersName);
        tvSecFollerName = (TextView) view.findViewById(R.id.tvSecFollerName);
        tvFirstFollowingName = (TextView) view.findViewById(R.id.tvFirstFollowingName);
        tvSecFollowingName = (TextView) view.findViewById(R.id.tvSecFollowingName);
        tvFlowersCount = (TextView) view.findViewById(R.id.tvFlowersCount);
        tvFlowingCount = (TextView) view.findViewById(R.id.tvFlowingCount);
        tvFollowers = (TextView) view.findViewById(R.id.tvFollowers);
        tvFollowing = (TextView) view.findViewById(R.id.tvFollowing);
        tvSettingPro = (TextView) view.findViewById(R.id.tvSettingPro);
        tvEditPro = (TextView) view.findViewById(R.id.tvEditPro);
        tvLogoutPro = (TextView) view.findViewById(R.id.tvLogoutPro);
        tvMyShopPro = (TextView) view.findViewById(R.id.tvMyShopPro);
        tvMyBundle = (TextView) view.findViewById(R.id.tvMyBundle);
        tvMyLikes = (TextView) view.findViewById(R.id.tvMyLikes);
        tvSoldItem = (TextView) view.findViewById(R.id.tvSoldItem);
        tvpurchaed = (TextView) view.findViewById(R.id.tvpurchaed);
        tvDrafts = (TextView) view.findViewById(R.id.tvDrafts);
        tvNotification = (TextView) view.findViewById(R.id.tvNotification);
        tvMessage = (TextView) view.findViewById(R.id.tvMessage);
        tvContuct = (TextView) view.findViewById(R.id.tvNotification);
        rlSecondFollowing = (RelativeLayout) view.findViewById(R.id.rlSecondFollowing);
        llFollowing = (LinearLayout) view.findViewById(R.id.llFollowing);
        llFollower = (LinearLayout) view.findViewById(R.id.llFollower);
        rlSecondFollower = (RelativeLayout) view.findViewById(R.id.rlSecondFollower);
        rlOffer = (RelativeLayout) view.findViewById(R.id.rlOffer);

        /**
         * ------------------Font set--------------------------------------------------
         */
        helveticaNeuRegular = Typeface.createFromAsset(getActivity().getAssets(), "font/helveticaNeuRegular.ttf");
        helveticaNeueBold = Typeface.createFromAsset(getActivity().getAssets(), "font/helveticaNeueBold.ttf");
        tvNickname.setTypeface(helveticaNeuRegular);
        tvFeedbackCount.setTypeface(helveticaNeuRegular);
        tvFollowers.setTypeface(helveticaNeueBold);
        tvFollowing.setTypeface(helveticaNeueBold);
        tvMRCount.setTypeface(helveticaNeuRegular);
        tvFeedBackPro.setTypeface(helveticaNeuRegular);
        tvMyRatingProf.setTypeface(helveticaNeuRegular);
        tvMyShopCount.setTypeface(helveticaNeuRegular);
        tvMyBundleCount.setTypeface(helveticaNeuRegular);
        tvMyLikesCount.setTypeface(helveticaNeuRegular);
        tvSoldItemCount.setTypeface(helveticaNeuRegular);
        tvPurchaedItemCount.setTypeface(helveticaNeuRegular);
//        tvMessageCount.setTypeface(helveticaNeuRegular);
        tvFirstFollowersName.setTypeface(helveticaNeuRegular);
        tvSecFollerName.setTypeface(helveticaNeuRegular);
        tvFirstFollowingName.setTypeface(helveticaNeuRegular);
        tvSecFollowingName.setTypeface(helveticaNeuRegular);
        tvFlowersCount.setTypeface(helveticaNeuRegular);
        tvFlowingCount.setTypeface(helveticaNeuRegular);
        tvSettingPro.setTypeface(helveticaNeuRegular);
        tvEditPro.setTypeface(helveticaNeuRegular);
        tvLogoutPro.setTypeface(helveticaNeuRegular);
        tvMyBundle.setTypeface(helveticaNeuRegular);
        tvMyLikes.setTypeface(helveticaNeuRegular);
        tvSoldItem.setTypeface(helveticaNeuRegular);
        tvpurchaed.setTypeface(helveticaNeuRegular);
        tvDrafts.setTypeface(helveticaNeuRegular);
        tvNotification.setTypeface(helveticaNeuRegular);
        tvMessage.setTypeface(helveticaNeuRegular);
        tvContuct.setTypeface(helveticaNeuRegular);
        /**
         * ====================Set data======================================
         */
        tvDraftCount.setText(String.valueOf(dbholder.getDraftDataSize(PersistData.getStringData(con, AppConstant.user_id))));

        /**
         * ---------------------On Click--------------------------------------------
         */
        logoutLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // ------- for dialog show----------
                logoutDialog();
            }

        });

        imageViewFeedBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                callFeedbackAPI(myProfileResponse.getUser_details().getId());
            }
        });
        imageViewRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                callFeedbackAPI(myProfileResponse.getUser_details().getId());
            }
        });

        edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myCommunicator.setContentFragment(new EditProfileFragment(), true);
            }
        });
        LinearLayout settings_layout = (LinearLayout) view.findViewById(R.id.settings_layout);
        settings_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myCommunicator.setContentFragment(new SettingFragment(), true);
            }
        });

        RelativeLayout reLayoutMyShop = (RelativeLayout) view.findViewById(R.id.reLayoutMyShop);
        reLayoutMyShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myCommunicator.setContentFragment(new MyShopFragment(), true);
            }
        });
        RelativeLayout reLayoutMyBundle = (RelativeLayout) view.findViewById(R.id.reLayoutMyBundle);
        reLayoutMyBundle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myCommunicator.setContentFragment(new MyBundleFragment(), true);
            }
        });
        RelativeLayout rlMyLikes = (RelativeLayout) view.findViewById(R.id.rlMyLikes);
        rlMyLikes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.productListTitel = "My Likes";
                AppConstant.productListType = "likes";
                myCommunicator.setContentFragment(new MylikeFragment(), true);
            }
        });
        RelativeLayout rLaySoldItem = (RelativeLayout) view.findViewById(R.id.rLaySoldItem);
        rLaySoldItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.productListTitel = "Sold Item";
                AppConstant.productListType = "sold_items";
                myCommunicator.setContentFragment(new CommonListFragment(), true);
            }
        });
        RelativeLayout rlPurchaedItems = (RelativeLayout) view.findViewById(R.id.rlPurchaedItems);
        rlPurchaedItems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.productListTitel = "Purchaed Item";
                AppConstant.productListType = "purchased_items";
                myCommunicator.setContentFragment(new CommonListFragment(), true);
            }
        });

        RelativeLayout rlDrafts = (RelativeLayout) view.findViewById(R.id.rlDrafts);
        rlDrafts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myCommunicator.setContentFragment(new DraftsFragment(), true);
            }
        });
        RelativeLayout rlNotifications = (RelativeLayout) view.findViewById(R.id.rlNotifications);
        rlNotifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myCommunicator.setContentFragment(new NotificationFragment(), true);
            }
        });

//        RelativeLayout rlMessages = (RelativeLayout) view.findViewById(R.id.rlMessages);
        rlOffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myCommunicator.setContentFragment(new OffersFragment(), true);
            }
        });

        RelativeLayout rlContuct = (RelativeLayout) view.findViewById(R.id.rlContuct);
        rlContuct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL, new String[]{"hello@sloppy2ndz.com"});
                i.putExtra(Intent.EXTRA_SUBJECT, "Sloppy 2ndz query");
//                i.putExtra(Intent.EXTRA_TEXT   , "I have a question!");
                try {
                    startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(con, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                }
            }
        });


        tvFllowersShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.followingFollower = "follower";
                AppConstant.otherUserIdFoll = "";
                FollowersFollowingFragment fragment = new FollowersFollowingFragment();
                Bundle bundle = new Bundle();
                bundle.putString("title", myProfileResponse.getUser_details().getUsername());
                fragment.setArguments(bundle);
                myCommunicator.setContentFragment(fragment, true);
            }
        });

        TextView tvShowFolling = (TextView) view.findViewById(R.id.tvShowFolling);
        tvShowFolling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.followingFollower = "following";
                AppConstant.otherUserIdFoll = "";
                FollowersFollowingFragment fragment = new FollowersFollowingFragment();
                Bundle bundle = new Bundle();
                bundle.putString("title", myProfileResponse.getUser_details().getUsername());
                fragment.setArguments(bundle);
                myCommunicator.setContentFragment(fragment, true);
            }
        });

    }

    /**
     * -----------------Logout Dialog-------------
     */
    private void logoutDialog() {
        final Dialog dialogLogout = new Dialog(con);
        dialogLogout.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLogout.setContentView(R.layout.dialog_logout);
        dialogLogout.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialogLogout.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView tvCanLogout = (TextView) dialogLogout.findViewById(R.id.tvCanLogout);
        TextView tvOk = (TextView) dialogLogout.findViewById(R.id.tvOk);
        TextView tvWarningLO = (TextView) dialogLogout.findViewById(R.id.tvWarningLO);
        TextView areYou = (TextView) dialogLogout.findViewById(R.id.areYou);

        //--------------------Font Set---------------------------------
        Typeface helveticaNeuRegular = Typeface.createFromAsset(getActivity().getAssets(), "font/helveticaNeuRegular.ttf");
        Typeface helveticaNeueBold = Typeface.createFromAsset(getActivity().getAssets(), "font/helveticaNeueBold.ttf");

        tvCanLogout.setTypeface(helveticaNeueBold);
        tvWarningLO.setTypeface(helveticaNeueBold);
        tvOk.setTypeface(helveticaNeuRegular);
        areYou.setTypeface(helveticaNeuRegular);

        // --------- Get picture from Gallery-----------------
        tvCanLogout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                dialogLogout.dismiss();
            }
        });

        tvOk.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                PersistentUser.logOut(con);
                PersistentUser.resetAllData(con);
                PersistData.setStringData(con, AppConstant.token, "");
                StartActivity.toActivity(con, MainActivity.class);
                PersistData.setStringData(con, AppConstant.socialId, "");

                dialogLogout.dismiss();
                // ------ -To finish an activity---------
                if (MyTabActivity.getMyTabActivity() != null) {
                    MyTabActivity.getMyTabActivity().finish();
                }
            }
        });

        dialogLogout.show();

    }

    /**
     * -------------API-24. My Profile API-------------------
     */
    protected void callProfileAPI() {
  /*
   * -----------check internet first-------------
   */
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }
  /*
   * -------start progress bar-----------
   */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
  /*
   *-------------------- start Thread-----------------
   *
   */
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            @Override
            public void run() {
                // -----------You can performed your task here.------------
                String msg;
                try {
                    long unixTime = System.currentTimeMillis() / 1000L;
                    String date = String.valueOf(unixTime);
                    Log.w("Date Unix", ">>" + date);
                    //-------------Call API-------------------------
                    Log.e("24.MyProfileURL", ">>" + AllURL.getProfileData(date));
                    String response = AAPBDHttpClient.get(AllURL.getProfileData(date)).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();
                    //------------Purse data using Gson------------------
                    Gson gson = new Gson();
                    myProfileResponse = gson.fromJson(response, MyProfileResponse.class);
                    Log.e("24MyProfileRespononse", ">>" + response);
                    PersistData.setStringData(con, AppConstant.myProfileResponceAC, new String(response));
                    AppConstant.currencyProfile = myProfileResponse.getUser_details().getCurrency();
                    PersistData.setStringData(con, AppConstant.firstName, myProfileResponse.getUser_details().getFirst_name());
                    PersistData.setStringData(con, AppConstant.lastName, myProfileResponse.getUser_details().getLast_name());
                    PersistData.setStringData(con, AppConstant.userImage, myProfileResponse.getUser_details().getProfile_image());

                } catch (Exception e1) {

                    e1.printStackTrace();

                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

      /*
       * ----------stop progress bar---------------------------
       */
                        if (busy != null) {
                            busy.dismis();
                        }

                        if (myProfileResponse.getStatus().equalsIgnoreCase("1")) {

                            /**
                             * ----------------Set data-------------------------------------------------------------
                             */
//                            try {
                            Picasso.with(con).load(myProfileResponse.getUser_details().getProfile_image())
                                    .placeholder(R.drawable.person)
                                    .error(R.drawable.person)
                                    .into(ivUser);

                            if (!TextUtils.isEmpty(myProfileResponse.getUser_details().getUsername())) {
                                tvNickname.setText(myProfileResponse.getUser_details().getUsername());
                            } else {
                                tvNickname.setText(myProfileResponse.getUser_details().getInstagram_username());
                            }

                            if (myProfileResponse.getUser_details().getFeedback_count() != null) {
                                tvFeedbackCount.setText(myProfileResponse.getUser_details().getFeedback_count());
                            } else {
                                tvFeedbackCount.setText("0");
                            }

                            if (myProfileResponse.getUser_details().getLike_count() != null) {
                                tvMyLikesCount.setText(myProfileResponse.getUser_details().getLike_count());
                            } else {
                                tvMyLikesCount.setText("0");
                            }

                            if (myProfileResponse.getUser_details().getBundle_count() != null) {
                                tvMyBundleCount.setText(myProfileResponse.getUser_details().getBundle_count());
                            } else {
                                tvMyBundleCount.setText("0");
                            }
                            if (MyTabActivity.getMyTabActivity()!=null){
                                tvNotificationCount.setText(MyTabActivity.getMyTabActivity().myResponse.getNotifications());
                            }
                            if (myProfileResponse.getUser_details().getRating_value() != null) {
                                String test = String.format("%.01f", Float.parseFloat(myProfileResponse.getUser_details().getRating_value()));
                                tvMRCount.setText(test);
                            } else {
                                tvMRCount.setText("0.0");
                            }
                            tvMyShopCount.setText(myProfileResponse.getUser_details().getShop_items());
                            tvSoldItemCount.setText(myProfileResponse.getUser_details().getSold_items());
                            tvPurchaedItemCount.setText(myProfileResponse.getUser_details().getPurchased_items());
                            tvOfferCount.setText(myProfileResponse.getUser_details().getOffers());

                            if (myProfileResponse.getUser_details().getFollowing_users().size() > 0) {
                                llFollowing.setVisibility(View.VISIBLE);
                                rlSecondFollowing.setVisibility(View.INVISIBLE);
                                Log.e("Flowing Count", myProfileResponse.getUser_details().getFollowing_count());
                                tvFlowingCount.setText(myProfileResponse.getUser_details().getFollowing_count());
                                Picasso.with(con)
                                        .load(myProfileResponse.getUser_details().getFollowing_users().get(0).getProfile_image())
                                        .placeholder(R.drawable.person)
                                        .error(R.drawable.person)
                                        .into(ivFirstFlowing);
                                tvFirstFollowingName.setText(myProfileResponse.getUser_details().getFollowing_users().get(0).getUsername());
                                llFollowing.setOnClickListener(new OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        AppConstant.otherUserId = myProfileResponse.getUser_details().getFollowing_users().get(0).getId();
                                        myCommunicator.setContentFragment(new OtherUserDetailsFragment(), true);
                                    }
                                });
                                if (myProfileResponse.getUser_details().getFollowing_users().size() > 1) {
                                    rlSecondFollowing.setVisibility(View.VISIBLE);
                                    tvSecFollowingName.setText(myProfileResponse.getUser_details().getFollowing_users().get(1).getUsername());
                                    Picasso.with(con)
                                            .load(myProfileResponse.getUser_details().getFollowing_users().get(1).getProfile_image())
                                            .placeholder(R.drawable.person)
                                            .error(R.drawable.person)
                                            .into(ivSecFollowing);

                                    rlSecondFollowing.setOnClickListener(new OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            AppConstant.otherUserId = myProfileResponse.getUser_details().getFollowing_users().get(1).getId();
                                            myCommunicator.setContentFragment(new OtherUserDetailsFragment(), true);
                                        }
                                    });
                                } else {
                                    rlSecondFollowing.setVisibility(View.GONE);
                                }

                            } else {
                                llFollowing.setVisibility(View.GONE);
                            }

                            if (myProfileResponse.getUser_details().getFollower_users().size() > 0) {
                                Log.e("Follower Count", String.valueOf(myProfileResponse.getUser_details().getFollower_users().size()));
                                llFollower.setVisibility(View.VISIBLE);
                                rlSecondFollower.setVisibility(View.INVISIBLE);
                                Log.e("Follower Count", myProfileResponse.getUser_details().getFollower_count());
                                tvFlowersCount.setText(myProfileResponse.getUser_details().getFollower_count());
                                Picasso.with(con)
                                        .load(myProfileResponse.getUser_details().getFollower_users().get(0).getProfile_image())
                                        .placeholder(R.drawable.person)
                                        .error(R.drawable.person)
                                        .into(ivFirstFollower);
                                tvFirstFollowersName.setText(myProfileResponse.getUser_details().getFollower_users().get(0).getUsername());
                                llFollower.setOnClickListener(new OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        AppConstant.otherUserId = myProfileResponse.getUser_details().getFollower_users().get(0).getId();
                                        myCommunicator.setContentFragment(new OtherUserDetailsFragment(), true);
                                    }
                                });

                                if (myProfileResponse.getUser_details().getFollower_users().size() > 1) {
                                    rlSecondFollower.setVisibility(View.VISIBLE);
                                    tvSecFollerName.setText(myProfileResponse.getUser_details().getFollower_users().get(1).getUsername());
                                    Picasso.with(con)
                                            .load(myProfileResponse.getUser_details().getFollower_users().get(1).getProfile_image())
                                            .placeholder(R.drawable.person)
                                            .error(R.drawable.person)
                                            .into(ivSecFollower);
                                    rlSecondFollower.setOnClickListener(new OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            AppConstant.otherUserId = myProfileResponse.getUser_details().getFollower_users().get(1).getId();
                                            myCommunicator.setContentFragment(new OtherUserDetailsFragment(), true);
                                        }
                                    });
                                } else {
                                    rlSecondFollower.setVisibility(View.GONE);
                                }
                            } else {
                                llFollower.setVisibility(View.GONE);
                            }

//                            } catch (Exception e) {
////								Log.e("My Profile exception : ", e.getMessage());
//                            }


                        } else {
                            String msg = myProfileResponse.getMsg();
                            AppConstant.alertDialoag(con, getResources().getString(R.string.alert), msg, getResources().getString(R.string.ok));
                        }

                    }
                });

            }

        });

    }

    /**
     * --------------------Feed Back dialog------------------------------------------
     */
    private void feedBacksDialog() {

        final Dialog dialogFeedbacks = new Dialog(con);
        dialogFeedbacks.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogFeedbacks.setContentView(R.layout.dialog_feedbacks);
        dialogFeedbacks.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogFeedbacks.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        /**
         * ---------------Initialization----------------------
         */
        tnNoFeedback = (TextView) dialogFeedbacks.findViewById(R.id.tnNoFeedback);
        TextView tvPostFeedbck = (TextView) dialogFeedbacks.findViewById(R.id.tvPostFeedbck);
        tvRatingCount = (TextView) dialogFeedbacks.findViewById(R.id.tvRatingCount);

        ImageView imgFeedbackCross = (ImageView) dialogFeedbacks.findViewById(R.id.imgFeedbackCross);
        View vLine = (View) dialogFeedbacks.findViewById(R.id.vLine);
        ListView listViewFeedback = (ListView) dialogFeedbacks.findViewById(R.id.listViewFeedback);

        etWriteFeedback = (EditText) dialogFeedbacks.findViewById(R.id.etWriteFeedback);
        etWriteFeedback.setVisibility(View.GONE);
        vLine.setVisibility(View.GONE);
        tvPostFeedbck.setVisibility(View.GONE);
        /**
         * ---------------Set Adapter----------------
         */
        if (feedbacksResponse.getResult().size() < 1) {
            listViewFeedback.setVisibility(View.GONE);
            tnNoFeedback.setVisibility(View.VISIBLE);
            tnNoFeedback.setTypeface(helveticaNeueBold);
        } else {
            tnNoFeedback.setVisibility(View.GONE);
            listViewFeedback.setVisibility(View.VISIBLE);
            feedbacksListAdapter = new FeedbacksListAdapter(con);
            listViewFeedback.setAdapter(feedbacksListAdapter);
            feedbacksListAdapter.notifyDataSetChanged();

        }

        if (!TextUtils.isEmpty(myProfileResponse.getUser_details().getRating_value())) {
            String test = String.format("%.01f", Float.parseFloat(myProfileResponse.getUser_details().getRating_value()));
            tvRatingCount.setText(test);
        }


        /**
         * --------------On Click------------------------------
         */

        imgFeedbackCross.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                dialogFeedbacks.dismiss();
            }
        });

        rb = (RatingBar) dialogFeedbacks.findViewById(R.id.ratingBar1);
        rb.setEnabled(false);
        if(!TextUtils.isEmpty(myProfileResponse.getUser_details().getRating_value())){
            rb.setRating(Float.parseFloat(myProfileResponse.getUser_details().getRating_value()));
        }

        dialogFeedbacks.show();
    }

    public class FeedbacksListAdapter extends ArrayAdapter<FeedbackResultInfo> {
        Context context;

        FeedbacksListAdapter(Context context) {
            super(context, R.layout.row_feedback_list, feedbacksResponse.getResult());
            this.context = context;
        }

        @Override
        public View getView(final int feedBackListPosition, View commentView, ViewGroup parent) {
            /**
             * ------------------Set the row layout-----------------------
             */
            if (commentView == null) {
                final LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                commentView = vi.inflate(R.layout.row_feedback_list, null);

            }
            Log.e("Product List Size", Integer.toString(feedbacksResponse.getResult().size()));
            if (feedBackListPosition < feedbacksResponse.getResult().size()) {

                // TO get data from vector/Arrraylist according to there position
                final FeedbackResultInfo feedBackQuery = feedbacksResponse.getResult().get(feedBackListPosition);

                /**
                 * ---------------------Initialization of Feed Back row field------------------------------
                 */
                imgFeedbackUser = (ImageView) commentView.findViewById(R.id.imgFeedbackUser);
                tvFeedBackText = (TextView) commentView.findViewById(R.id.tvFeedBackText);
                tvFBUserName = (TextView) commentView.findViewById(R.id.tvFBUserName);
                feedbackTime = (TextView) commentView.findViewById(R.id.feedbackTime);

                try {
                    /**
                     * -----------------Set data on field of Comment row------------------
                     */
                    Picasso.with(context).load(feedBackQuery.getUser_details().getProfile_image())
                            .placeholder(R.drawable.person).error(R.drawable.person).into(imgFeedbackUser);
                    tvFBUserName.setText(feedBackQuery.getUser_details().getName());
                    tvFeedBackText.setText(feedBackQuery.getComment());
                    feedbackTime.setText(feedBackQuery.getCreated_at());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            return commentView;

        }

    }

    /**
     * -------------------16. User Feedbacks List API------------------
     */
    private void callFeedbackAPI(final String user_id) {
  /*
   * ---------------check internet first------------
   */
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }

  /*
   * ----------------Show Busy Dialog -----------------------------------------
   */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
  /*
   *
   *----------------------Start Thread-------------------------------------------
   *
   */
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            String msg = "";
            String response = "";

            @Override
            public void run() {
                // You can performed your task here.

                try {

                    Log.w("UserFeedbackList URL", AllURL.getFeedbacks(user_id));
                    //-------------Hit Server---------------------
                    response = AAPBDHttpClient.get(AllURL.getFeedbacks(user_id)).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();

                    //----------------Persist response with the help of Gson----------------
                    Log.w("UserFeedbackList Resp", ">>" + response);
                    Gson gson = new Gson();
                    feedbacksResponse = gson.fromJson(response, UserFeedbacksResponse.class);

                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        /**
                         * -------------Here we do UI related tasks inside run() method of runOnUiThread()-------------------
                         */

                        //-------Stop Busy Dialog-----
                        if (busy != null) {
                            busy.dismis();
                        }
                        /**
                         * ---------main Ui related work--------------
                         */
                        if (feedbacksResponse.getStatus().equalsIgnoreCase("1")) {
                            /**
                             * -------Success message Show---------------------
                             */
//                            Toast.makeText(con, "UserFeedbackList " + feedbacksResponse.getMsg(), Toast.LENGTH_LONG).show();
                            /**
                             * ----------------Feedback Dialog Show---------------------
                             */
                            feedBacksDialog();

                        } else {
                            msg = feedbacksResponse.getMsg();
                            AlertMessage.showMessage(con, "UserFeedbackList", msg);
                        }
                    }
                });
            }
        });
    }
}
