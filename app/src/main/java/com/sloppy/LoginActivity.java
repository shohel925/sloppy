package com.sloppy;

import android.*;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.sloppy.aapbdLib.AlertMessage;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.GPSTracker;
import com.sloppy.aapbdLib.NetInfo;
import com.sloppy.aapbdLib.PersistData;
import com.sloppy.aapbdLib.PersistentUser;
import com.sloppy.aapbdLib.StartActivity;
import com.sloppy.model.LoginResponse;
import com.sloppy.utils.AllURL;
import com.sloppy.utils.AppConstant;
import com.squareup.picasso.Picasso;

import cz.msebera.android.httpclient.Header;


public class LoginActivity extends Activity {

	// --------------Variable---------------
	Context con;
	private TextView tvLogin;
	private EditText etUserOrEmail, etPasword;
	private String  password, identifier;
	static final String TAG = "GCMDemo";
	GoogleCloudMessaging gcm;
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	private String SENDER_ID = "809492412066";
	GPSTracker gps;
	//------------Class--------
	private LoginResponse logInResponse;

	/**
	 * ------------------------On Create--------------------------------
	 */

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_login);
		con = this;
		gps = new GPSTracker(con);
		initUI();
	}

	/**
	 * -----------------Initialization and On click------------------------------------
	 */
	private void initUI() {

//		if (ActivityCompat.checkSelfPermission(con, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//			ActivityCompat.requestPermissions(this, new String[]{
//					android.Manifest.permission.ACCESS_FINE_LOCATION},11);
//			Toast.makeText(con, "yy granted", Toast.LENGTH_SHORT).show();
//
//		}else {
//			Toast.makeText(con, "granted", Toast.LENGTH_SHORT).show();
//		}

		//------------------Text View-------------------------
		TextView tvTermAndSer = (TextView) findViewById(R.id.tvTermAndSer);
		LinearLayout llTermOf = (LinearLayout) findViewById(R.id.llTermOf);
		TextView tvPrivacy = (TextView) findViewById(R.id.tvPrivacy);
		TextView tvLogin1=(TextView) findViewById(R.id.tvLogin1);
		TextView tvSignUpLogin=(TextView) findViewById(R.id.tvSignUpLogin);
		tvLogin = (TextView) findViewById(R.id.tvLogin);
		TextView tvForgotPasword = (TextView) findViewById(R.id.tvForgotPasword);
		//-----------------Image View-------------------------
		ImageView imgBackLogin = (ImageView) findViewById(R.id.imgBackLogin);
		ImageView imgMenuLogin = (ImageView) findViewById(R.id.imgMenuLogin);

		//-----------------Edit Text--------------------------
		etUserOrEmail = (EditText) findViewById(R.id.etUserOrEmail);
		etPasword = (EditText) findViewById(R.id.etPasword);

		//----------------To Show Sign Up Button------------------
		LinearLayout linLayoutSignUp=(LinearLayout) findViewById(R.id.linLayoutSignUp);
		LinearLayout llInstagram=(LinearLayout) findViewById(R.id.llInstagram);
		if (!AppConstant.islogin) {
			llTermOf.setVisibility(View.VISIBLE);
			linLayoutSignUp.setVisibility(View.VISIBLE);
			llInstagram.setVisibility(View.VISIBLE);
		}else{
			llTermOf.setVisibility(View.GONE);
			linLayoutSignUp.setVisibility(View.GONE);
			llInstagram.setVisibility(View.GONE);
		}

		/**
		 * ----------------Font Set--------------------------
		 */
		Typeface helveticaNeuRegular = Typeface.createFromAsset(getAssets(), "font/helveticaNeuRegular.ttf");
		Typeface helveticaNeueBold = Typeface.createFromAsset(getAssets(), "font/helveticaNeueBold.ttf");
		tvLogin1.setTypeface(helveticaNeuRegular);
		tvLogin.setTypeface(helveticaNeuRegular);
		tvTermAndSer.setTypeface(helveticaNeuRegular);
		tvForgotPasword.setTypeface(helveticaNeuRegular);
		etUserOrEmail.setTypeface(helveticaNeuRegular);
		tvPrivacy.setTypeface(helveticaNeuRegular);
		etPasword.setTypeface(helveticaNeuRegular);
		tvSignUpLogin.setTypeface(helveticaNeuRegular);
		SpannableString content = new SpannableString(getResources().getString(R.string.terms_of));
		content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
		tvTermAndSer.setText(content);
		SpannableString privecyPolicy = new SpannableString(getResources().getString(R.string.privecy_policy));
		privecyPolicy.setSpan(new UnderlineSpan(), 0, privecyPolicy.length(), 0);
		tvPrivacy.setText(privecyPolicy);

		/**
		 * -----------------------On Click------------------------------
		 */
		tvTermAndSer.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AppConstant.menuType="term";
				FragmentManager manager= getFragmentManager();
				TermsOfServiceDialogFragment dialogTerm= new TermsOfServiceDialogFragment();
				dialogTerm.setStyle(android.app.DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar);
				dialogTerm.show(manager,"");
			}
		});

		tvPrivacy.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AppConstant.menuType="privacy";
				FragmentManager manager= getFragmentManager();
				TermsOfServiceDialogFragment dialogTerm= new TermsOfServiceDialogFragment();
				dialogTerm.setStyle(android.app.DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar);
				dialogTerm.show(manager,"");
			}
		});

		tvForgotPasword.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				StartActivity.toActivity(con, ForgotPasswordActivity.class);
			}
		});

		linLayoutSignUp.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
			final	Intent intent=new Intent(con, SignUpActivity.class);
			startActivity(intent);
			}
		});
		imgMenuLogin.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				FragmentManager fra= getFragmentManager();
				AppConstant.showMenuDiadog(fra);
			}
		});
		
/*
 * -------------------------Normal User Login---------------------------------------------
 */
		tvLogin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				// --------Check user id and password-----------
				if (TextUtils.isEmpty(etUserOrEmail.getText().toString())) {
					AppConstant.alertDialoag(con,"Alert",getString(R.string.corrent_info),"OK");
//					AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.please_enter_usr_pass));
				} else if (TextUtils.isEmpty(etPasword.getText().toString())) {
					AppConstant.alertDialoag(con,"Alert",getString(R.string.corrent_info),"OK");
//					AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.please_ent_password));
				} else {

					//--------- Set data of same type of API--------------
					identifier = etUserOrEmail.getText().toString();
					password = etPasword.getText().toString();

					/*
						-------------------API-2. Normal User Login call-------------------
					 */
					if (PersistentUser.isLogged(con)) {
						PersistentUser.logOut(con);
					}
					callNormalUserLoginAPI(AllURL.normalUserLoinURL());
				}
			}
		});
		imgBackLogin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
					finish();
			}
		});

	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);



		if (requestCode == 11) {
			if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				//Picasso.with(con).load(pictureUrl).into(target);
			} else {
				// Your app will not have this permission. Turn off all functions
				// that require this permission or it will force close like your
				// original question

				if ( ActivityCompat.checkSelfPermission(con, android.Manifest.permission.ACCESS_FINE_LOCATION)
						!= PackageManager.PERMISSION_GRANTED) {
					ActivityCompat.requestPermissions(LoginActivity.this,
							new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 11);
				}
			}
		}



	}

	// -------------Post Method For normal Using Login--------------------
	protected void callNormalUserLoginAPI(final String url) {
		/**
		 * --------------- Check Internet------------
		 */
		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
			return;
		}

		/**
		 * ------Show Busy Dialog------------------
		 */
		final BusyDialog busyNow = new BusyDialog(con, true, false);
		busyNow.show();
		/**
		 * ---------Create object of  RequestParams to send value with URL---------------
		 */
		final RequestParams param = new RequestParams();

		try {
			param.put("identifier", identifier);
			param.put("password", password);
			param.put("device_type", "android");
			param.put("push_id", PersistData.getStringData(con, AppConstant.GCMID));
		} catch (final Exception e1) {
			e1.printStackTrace();
		}
		/**
		 * ---------Create object of  AsyncHttpClient class to heat server ---------------
		 */
		final AsyncHttpClient client = new AsyncHttpClient();
		Log.i("Login URL ", ">>" + url);
		client.post(url, param, new AsyncHttpResponseHandler() {

			@Override
			public void onStart() {
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers,
								  byte[] response) {
				//-----------Off Busy Dialog ----------------
				if (busyNow != null) {
					busyNow.dismis();
				}
				//-----------------Print Response--------------------
				Log.i("LogInresposne ", ">>" + new String(response));

				//------------Data persist using Gson------------------
				Gson g = new Gson();
				logInResponse = g.fromJson(new String(response), LoginResponse.class);

				Log.e("Loginstatus", "=" + logInResponse.getStatus());

				if (logInResponse.getStatus().equalsIgnoreCase("1")) {

					PersistentUser.setLogin(con);

//					Toast.makeText(con, logInResponse.getMsg(), Toast.LENGTH_LONG).show();


					PersistData.setStringData(con, AppConstant.user_id,
							logInResponse.getResults().getId());
					PersistData.setStringData(con, AppConstant.first_name,
							logInResponse.getResults().getFirst_name());
					PersistData.setStringData(con, AppConstant.last_name,
							logInResponse.getResults().getLast_name());
					PersistData.setStringData(con, AppConstant.email,
							logInResponse.getResults().getEmail());
					PersistData.setStringData(con, AppConstant.username,
							logInResponse.getResults().getUsername());
					PersistData.setStringData(con, AppConstant.loginResponse, new String(response));
					PersistData.setStringData(con, AppConstant.role,
							logInResponse.getResults().getRole());
					PersistData.setStringData(con, AppConstant.registrationtype,
							logInResponse.getResults().getRegistrationtype());
					PersistData.setStringData(con, AppConstant.profile_image,
							logInResponse.getResults().getProfile_image());
					PersistData.setStringData(con, AppConstant.device_type,
							logInResponse.getResults().getDevice_type());
					PersistData.setStringData(con, AppConstant.push_id,
							logInResponse.getResults().getPush_id());
					PersistData.setStringData(con, AppConstant.token,
							logInResponse.getToken());

					Log.e("token", "=" + PersistData.getStringData(con, AppConstant.token));

					//---------Go Tab Activity-----------------------
					MainActivity.instance.finish();
					if (MyTabActivity.getMyTabActivity()!=null){
						MyTabActivity.getMyTabActivity().finish();
					}
					StartActivity.toActivity(con, MyTabActivity.class);
					finish();

				} else {
					AppConstant.alertDialoag(con,"Status",logInResponse.getMsg(),"Ok");
//					AlertMessage.showMessage(con, "Status", logInResponse.getMsg() + "");
					return;
				}

			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
								  byte[] errorResponse, Throwable e) {
				// called when response HTTP status is "4XX" (eg. 401, 403, 404)

//				Log.e("LoginerrorResponse", new String(errorResponse));

				if (busyNow != null) {
					busyNow.dismis();
				}
			}

			@Override
			public void onRetry(int retryNo) {
				// called when request is retried

			}
		});

	}

//	// ---------For GCM ID-----------------
//
//	private boolean checkPlayServices() {
//		final int resultCode = GooglePlayServicesUtil
//				.isGooglePlayServicesAvailable(getApplicationContext());
//		if (resultCode != ConnectionResult.SUCCESS) {
//			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
//				GooglePlayServicesUtil.getErrorDialog(resultCode, this,
//						LoginActivity.PLAY_SERVICES_RESOLUTION_REQUEST).show();
//			} else {
//				Log.i(LoginActivity.TAG, "This device is not supported.");
//				finish();
//			}
//			return false;
//		}
//		return true;
//	}
//
//	class RegisterBackground extends AsyncTask<String, String, String> {
//
//		@Override
//		protected String doInBackground(String... arg0) {
//
//			String msg = "";
//			try {
//				if (gcm == null) {
//					gcm = GoogleCloudMessaging.getInstance(con);
//				}
//				String regid = gcm.register(SENDER_ID);
//				PersistData.setStringData(con, AppConstant.GCMID, regid);
//
//				msg = "Dvice registered, registration ID=" + regid;
//				Log.e("Google Registration ID", "---------" + msg);
//				// Persist the regID - no need to
//			} catch (final IOException ex) {
//				msg = "Error :" + ex.getMessage();
//			}
//			return msg;
//		}
//
//		@Override
//		protected void onPostExecute(String msg) {
//
//		}
//	}


}
