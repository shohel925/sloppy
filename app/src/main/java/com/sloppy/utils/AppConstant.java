package com.sloppy.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.sloppy.CommantDialogFragment;
import com.sloppy.LoginActivity;
import com.sloppy.MenuFragmentDialog;
import com.sloppy.MyTabActivity;
import com.sloppy.R;
import com.sloppy.TermsOfServiceDialogFragment;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.StartActivity;
import com.sloppy.model.CategoryInfo;
import com.sloppy.model.CommonInformationResponse;
import com.sloppy.model.MessageListInfo;
import com.sloppy.model.MyMessageResultInfo;
import com.sloppy.model.ProductAndUserDetailsInfo;
import com.sloppy.model.OtherUserDetailsResponse;
import com.sloppy.model.SearchUserList;
import com.sloppy.model.SubcategoryInfo;
import com.sloppy.model.TaggableUsers;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.app.FragmentManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

public class AppConstant {
	public static MenuFragmentDialog dialogMenu;
	static public boolean islogin;
	public static int feedRowPosition=0;
	public static String productListType="";
	public static String productListTitel="";
	public static int tabPos=0;
	static public String disPersent="";
	static public String minItem="";
	static public String editable="";
	static public String editabledraft="";
	public static String menuType = "";

	//-------Capture Image-----
//	public static String path = "";
	public static String path1 = "path1";
	public static String path2 = "path2";
	public static String path3 = "path3";
	public static String path4 = "path4";
//	public static Bitmap imagebit = null;
	
	//-------activity_login-----
	public static String userId = "userId";
	public static String GCMID = "GCMID";
	public static String user_id = "user_id";
	public static String first_name = "first_name";
	public static String last_name = "last_name";
	public static String email = "email";
	public static String username = "username";
	public static String gender = "gender";
	public static String role = "role";
	public static String registrationtype = "registrationtype";
	public static String country = "country";
	public static String categoryName = "categoryName";
	public static String categoryID = "categoryID";
	public static String subCategoryID = "subCategoryID";
	public static String profile_image = "profile_image";
	public static String device_type = "device_type";
	public static String push_id = "push_id";
	public static String token = "token";

	//-----------------Feed activity------------------------
	public static String otherUserId = "";
	public static String brandName="brandName";
	public static String brandID="brandID";
	public static String main="yes";
	public static String hashtagName="";
	public static String hashtagId="";
	public static String productIDfeedRow="productIDfeedRow";

	public static ProductAndUserDetailsInfo feedRowObject=new ProductAndUserDetailsInfo();
    public static CommonInformationResponse commonResponse = new CommonInformationResponse();
	public static OtherUserDetailsResponse otherUserDetailsResponse = new OtherUserDetailsResponse();
	public static MyMessageResultInfo chatList=new MyMessageResultInfo();
	public static List<ProductAndUserDetailsInfo> productList= new ArrayList<>();
	public static List<SearchUserList> userList= new ArrayList<>();
	public static List<TaggableUsers> tagUserList= new ArrayList<>();

	public static String myProfileResponceAC="myProfileResponceAC";
	public static String loginResponse="loginResponse";
	public static String feedsResponse="feedsResponse";
	public static String currencyProfile="currencyProfile";
	public static String firstName="firstName";
	public static String lastName="lastName";
	public static String userImage="userImage";
	public static String followingFollower="followingFollower";
	public static String otherUserIdFoll="";
	public static String proListType="";
	public static String currency="USD";
	public static String categoryType="";
	public static String commentCount="";


	public static void OpenKeyBoard(Context mContext){
		InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
	}

	public static void keyboardHide(Context context,View vv){
		InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(vv.getWindowToken(), 0);
	}

	public static void setFont( Context con, TextView textView, String type){
		if (type.equalsIgnoreCase("bold")){
			Typeface	helveticaNeueBold = Typeface.createFromAsset(con.getAssets(), "font/helveticaNeueBold.ttf");
			textView.setTypeface(helveticaNeueBold);
		}else if(type.equalsIgnoreCase("regular")){
			Typeface	helveticaNeuRegular = Typeface.createFromAsset(con.getAssets(), "font/helveticaNeuRegular.ttf");
			textView.setTypeface(helveticaNeuRegular);
		}
	}
	public static void showMenuDiadog( FragmentManager manager){
		dialogMenu = new MenuFragmentDialog();
		dialogMenu.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar);
		dialogMenu.show(manager , "");
	}
	static public String getCategoryName( String id){
		String name=null;
		for (int i = 0; i < AppConstant.commonResponse.getCategory().size(); i++) {
			if (AppConstant.commonResponse.getCategory().get(i).getId().equalsIgnoreCase(id)){
				name=AppConstant.commonResponse.getCategory().get(i).getName();
				break;
			}
		}
		return name;
	}
	static public String getTypeFromCategoryId( String id){
		String name="";
		for (int i = 0; i < AppConstant.commonResponse.getCategory().size(); i++) {
			if (AppConstant.commonResponse.getCategory().get(i).getId().equalsIgnoreCase(id)){
				name=AppConstant.commonResponse.getCategory().get(i).getType();
				break;
			}
		}
		return name;
	}
	static public int getCategoryIdByName( String name){
		int id = 0;
		for (int i = 0; i < AppConstant.commonResponse.getCategory().size(); i++) {
			if (AppConstant.commonResponse.getCategory().get(i).getName().equalsIgnoreCase(name)){
				id=Integer.parseInt(AppConstant.commonResponse.getCategory().get(i).getId());
				break;
			}
		}
		return id;
	}
	static public int getBrandIDByName(String brandName){
		int id = 0;
		for (int i = 0; i < AppConstant.commonResponse.getBrand().size(); i++) {
			if (AppConstant.commonResponse.getBrand().get(i).getName().equalsIgnoreCase(brandName)){
				id=Integer.parseInt(AppConstant.commonResponse.getBrand().get(i).getId());
				break;
			}
		}
		return id;
	}

//	static public int getSubCategoryIdByName( String catName, String subcatName){
//		int subId = 0;
//		for (int i = 0; i < AppConstant.commonResponse.getCategory().size(); i++) {
//			if (AppConstant.commonResponse.getCategory().get(i).getName().equalsIgnoreCase(catName)){
//				for (int j = 0; j < AppConstant.commonResponse.getCategory().get(i).getSubcategory().size(); j++) {
//					if (AppConstant.commonResponse.getCategory().get(i).getSubcategory().get(j).getName().equalsIgnoreCase(subcatName)){
//						subId=Integer.parseInt(AppConstant.commonResponse.getCategory().get(i).getSubcategory().get(j).getSub_category_id());
//					}
//				}
//
//			}
//		}
//		return subId;
//	}
	static public int getSubCategoryIdByName( String suitable,String catName,String subcatName){
		int subId = 0;
		for (int i = 0; i < AppConstant.commonResponse.getCategory().size(); i++) {
			if (AppConstant.commonResponse.getCategory().get(i).getName().equalsIgnoreCase(catName)){
				if (suitable.equalsIgnoreCase("Woman")) {
					for (int j = 0; j < AppConstant.commonResponse.getCategory().get(i).getSubcategory().getWoman().size(); j++) {
						if (AppConstant.commonResponse.getCategory().get(i).getSubcategory().getWoman().get(j).getName().equalsIgnoreCase(subcatName)) {
							subId = Integer.parseInt(AppConstant.commonResponse.getCategory().get(i).getSubcategory().getWoman().get(j).getSub_category_id());
							break;
						}
					}
				}else if (suitable.equalsIgnoreCase("Man")){

					for (int j = 0; j < AppConstant.commonResponse.getCategory().get(i).getSubcategory().getMan().size(); j++) {
						if (AppConstant.commonResponse.getCategory().get(i).getSubcategory().getMan().get(j).getName().equalsIgnoreCase(subcatName)){
							subId=Integer.parseInt(AppConstant.commonResponse.getCategory().get(i).getSubcategory().getMan().get(j).getSub_category_id());
							break;
						}
					}
				}else {
					for (int j = 0; j < AppConstant.commonResponse.getCategory().get(i).getSubcategory().getBoth().size(); j++) {
						if (AppConstant.commonResponse.getCategory().get(i).getSubcategory().getBoth().get(j).getName().equalsIgnoreCase(subcatName)){
							subId=Integer.parseInt(AppConstant.commonResponse.getCategory().get(i).getSubcategory().getBoth().get(j).getSub_category_id());
							break;
						}
					}
				}
				break;
			}
		}
		return subId;
	}


	static public String getSubCategoryName( String categoryId,String subCateId){
		String subCatName="";
		for (int i = 0; i < AppConstant.commonResponse.getCategory().size(); i++) {
			if (AppConstant.commonResponse.getCategory().get(i).getId().equalsIgnoreCase(categoryId)){
				for (int j = 0; j < AppConstant.commonResponse.getCategory().get(i).getSubcategory().getWoman().size(); j++) {
					if (AppConstant.commonResponse.getCategory().get(i).getSubcategory().getWoman().get(j).getSub_category_id().equalsIgnoreCase(subCateId)){
						subCatName=AppConstant.commonResponse.getCategory().get(i).getSubcategory().getWoman().get(j).getName();
						break;
					}
				}
				if (TextUtils.isEmpty(subCatName)){
					for (int j = 0; j < AppConstant.commonResponse.getCategory().get(i).getSubcategory().getMan().size(); j++) {
						if (AppConstant.commonResponse.getCategory().get(i).getSubcategory().getMan().get(j).getSub_category_id().equalsIgnoreCase(subCateId)){
							subCatName=AppConstant.commonResponse.getCategory().get(i).getSubcategory().getMan().get(j).getName();
							break;
						}
					}
				}
				if (TextUtils.isEmpty(subCatName)){
					for (int j = 0; j < AppConstant.commonResponse.getCategory().get(i).getSubcategory().getBoth().size(); j++) {
						if (AppConstant.commonResponse.getCategory().get(i).getSubcategory().getBoth().get(j).getSub_category_id().equalsIgnoreCase(subCateId)){
							subCatName=AppConstant.commonResponse.getCategory().get(i).getSubcategory().getBoth().get(j).getName();
							break;
						}
					}
				}
				break;
			}
		}
		return subCatName;
	}

	static public String getCurrencySymbol(String currency){

		String symbol = null;
		switch (currency.toLowerCase()) {
			case "usd":
				symbol="$";
				break;
			case "rub":
				symbol="\u20BD";
//				symbol="₽";
				break;
			case "gbp":
				symbol="£";
				break;
			case "eur":
				symbol="€";
				break;
		}
		return symbol;
	}


	/**
	 * ----------------------Dialog Share------------------------------------------------------
	 */
	public static void showCommentDialogFragment(FragmentManager manager) {
		CommantDialogFragment dialogMenu = new CommantDialogFragment();
		dialogMenu.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar);
		dialogMenu.show( manager, "");
	}

	public static void shareDialog(final Context con) {

		final Dialog dialogShare = new Dialog(con);
		dialogShare.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialogShare.setContentView(R.layout.dialog_feed_share);
		dialogShare.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialogShare.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

		ImageView imgShareCross = (ImageView) dialogShare.findViewById(R.id.imgShareCross);

		imgShareCross.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				dialogShare.dismiss();
			}
		});

		dialogShare.show();
	}


	public static void alertDialoag(final Context con,final String titel,final String description,final String comand) {

		final Dialog dialogAlert = new Dialog(con);
		dialogAlert.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialogAlert.setContentView(R.layout.dialog_alert);
		dialogAlert.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		dialogAlert.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

		TextView tvTitel = (TextView) dialogAlert.findViewById(R.id.tvTitel);
		TextView tvDescription = (TextView) dialogAlert.findViewById(R.id.tvDescription);
		TextView tvComand = (TextView) dialogAlert.findViewById(R.id.tvComand);

		Typeface helveticaNeuRegular = Typeface.createFromAsset(con.getAssets(), "font/helveticaNeuRegular.ttf");
		Typeface helveticaNeueBold = Typeface.createFromAsset(con.getAssets(), "font/helveticaNeueBold.ttf");
		tvTitel.setTypeface(helveticaNeueBold);
		tvDescription.setTypeface(helveticaNeuRegular);
		tvComand.setTypeface(helveticaNeuRegular);

		tvTitel.setText(titel);
		tvDescription.setText(description);
		tvComand.setText(comand);

		tvComand.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogAlert.dismiss();
			}
		});

		dialogAlert.show();
	}

	public static void loginDialoag(final Context con) {
		final Dialog dialogLogin = new Dialog(con);
		dialogLogin.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialogLogin.setContentView(R.layout.dialog_login);
		dialogLogin.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
		dialogLogin.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

		TextView tvTitel2 = (TextView) dialogLogin.findViewById(R.id.tvTitel2);
		TextView tvDescription2 = (TextView) dialogLogin.findViewById(R.id.tvDescription2);
		TextView tvLeftCommund = (TextView) dialogLogin.findViewById(R.id.tvLeftCommund);
		TextView tvRightCommund = (TextView) dialogLogin.findViewById(R.id.tvRightCommund);

			//==================Font set==========================
		Typeface helveticaNeuRegular = Typeface.createFromAsset(con.getAssets(), "font/helveticaNeuRegular.ttf");
		Typeface helveticaNeueBold = Typeface.createFromAsset(con.getAssets(), "font/helveticaNeueBold.ttf");
		tvTitel2.setTypeface(helveticaNeueBold);
		tvLeftCommund.setTypeface(helveticaNeueBold);
		tvRightCommund.setTypeface(helveticaNeuRegular);
		tvDescription2.setTypeface(helveticaNeuRegular);

		tvRightCommund.setTypeface(helveticaNeuRegular);
		tvLeftCommund.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				MyTabActivity.getMyTabActivity().getTabHost().setCurrentTab(AppConstant.tabPos);
				dialogLogin.dismiss();
			}
		});

		tvRightCommund.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				AppConstant.islogin = false;
				MyTabActivity.getMyTabActivity().getTabHost().setCurrentTab(AppConstant.tabPos);
				StartActivity.toActivity(con, LoginActivity.class);
				dialogLogin.dismiss();
			}
		});

		dialogLogin.show();
	}
	public static String socialId="socialId";
	public static String insagramUserName="insagramUserName";
	public static int CAMERA_RUNTIME_PERMISSION=2,WRITEEXTERNAL_PERMISSION_RUNTIME=3;
	public static boolean isGallery=false;
	public static void defaultShare(Context context,Uri uri){
		String finalStr="Hey! This is a cool app. You can purchase the item from this app."+" https://play.google.com/store/apps/details?id=com.sloppy ";
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("image/jpeg");
		intent.putExtra(Intent.EXTRA_TEXT, finalStr);
		intent.putExtra(Intent.EXTRA_STREAM, uri);
		context.startActivity(Intent.createChooser(intent, "Share Image"));
	}
	public static BusyDialog busyDialog;
	//tarpget to save
	public static Target getTarget(final Context context,final String url){
		busyDialog= new BusyDialog(context,false,"Saving...");
		Target target = new Target(){

			@Override
			public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
				new Thread(new Runnable() {

					@Override
					public void run() {

						File file = new File(Environment.getExternalStorageDirectory().getPath() + "/" + url);
						try {
							file.createNewFile();
							FileOutputStream ostream = new FileOutputStream(file);
							bitmap.compress(Bitmap.CompressFormat.JPEG, 80, ostream);
							String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, "Title", null);
							defaultShare(context,Uri.parse(path));
							busyDialog.dismis();
							ostream.flush();
							ostream.close();
						} catch (IOException e) {
							Log.e("IOException", e.getLocalizedMessage());
						}
					}
				}).start();

			}

			@Override
			public void onBitmapFailed(Drawable errorDrawable) {
				busyDialog.dismis();
			}

			@Override
			public void onPrepareLoad(Drawable placeHolderDrawable) {
				busyDialog.show();
			}
		};
		return target;
	}
	public static CategoryInfo categoryInfo;
	public static boolean isFollowing=false;
	public static String sellerCheck="sellerCheck";
	public static String sellerPercent="sellerPercent";
	public static String sellerItem="sellerItem";
	public static String discountEnabled="discountEnabled";
}
