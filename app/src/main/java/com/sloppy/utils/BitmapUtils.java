package com.sloppy.utils;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Vector;


public class BitmapUtils {

	public static Bitmap getRescaledBitmap(final Bitmap bit, final int x,
										   final int y, final boolean flag) {

		return Bitmap.createScaledBitmap(bit, x, y, true);

	}

	public static Vector<Bitmap> getBitmapFromAsset(Context con, String path)
			throws IOException {
		final AssetManager am = con.getAssets();
		final String[] files = am.list(path);
		final Vector<Bitmap> temp = new Vector<Bitmap>();

		for (final String s : files) {
			try {
				temp.addElement(BitmapFactory.decodeStream(am.open(path + "/"
						+ s)));
			} catch (final Exception e) {

			}
		}

		return temp;

	}
	public static int getImageOrientation(Context context, String imagePath) {
		int orientation = getOrientationFromExif(imagePath);
		if(orientation <= 0) {
			orientation = getOrientationFromMediaStore(context, imagePath);
		}

		return orientation;
	}
	public static Bitmap rotateBitmap(File f, int orientation) {
		String photopath = f.getPath().toString();
		Bitmap bitmap = BitmapFactory.decodeFile(photopath);
		Matrix matrix = new Matrix();
		switch (orientation) {
			case ExifInterface.ORIENTATION_NORMAL:
				return bitmap;
			case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
				matrix.setScale(-1, 1);
				break;
			case ExifInterface.ORIENTATION_ROTATE_180:
				matrix.setRotate(180);
				break;
			case ExifInterface.ORIENTATION_FLIP_VERTICAL:
				matrix.setRotate(180);
				matrix.postScale(-1, 1);
				break;
			case ExifInterface.ORIENTATION_TRANSPOSE:
				matrix.setRotate(90);
				matrix.postScale(-1, 1);
				break;
			case ExifInterface.ORIENTATION_ROTATE_90:
				matrix.setRotate(90);
				break;
			case ExifInterface.ORIENTATION_TRANSVERSE:
				matrix.setRotate(-90);
				matrix.postScale(-1, 1);
				break;
			case ExifInterface.ORIENTATION_ROTATE_270:
				matrix.setRotate(-90);
				break;
			default:
				return bitmap;
		}
		try {
			Bitmap bmRotated = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);






				FileOutputStream fOut;
				try {
					fOut = new FileOutputStream(f);
					bmRotated.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
					fOut.flush();
					fOut.close();

				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		bitmap.recycle();
			return bmRotated;
		}
		catch (OutOfMemoryError e) {
			e.printStackTrace();
			return null;
		}
	}
	private static int getOrientationFromExif(String imagePath) {
		int orientation = -1;
		try {
			ExifInterface exif = new ExifInterface(imagePath);
			int exifOrientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_NORMAL);

			switch (exifOrientation) {
				case ExifInterface.ORIENTATION_ROTATE_270:
					orientation = 270;

					break;
				case ExifInterface.ORIENTATION_ROTATE_180:
					orientation = 180;

					break;
				case ExifInterface.ORIENTATION_ROTATE_90:
					orientation = 90;

					break;

				case ExifInterface.ORIENTATION_NORMAL:
					orientation = 0;

					break;
				case ExifInterface.ORIENTATION_TRANSVERSE:
					orientation = -90;
					break;
				case ExifInterface.ORIENTATION_TRANSPOSE:
					orientation = 90;
					break;
				case ExifInterface.ORIENTATION_FLIP_VERTICAL:
					orientation = 180;
					break;

				default:
					break;




			}
		} catch (IOException e) {
			Log.e("Check", "Unable to get image exif orientation", e);
		}

		return orientation;
	}
	public static void rotateAndSaveImg(Context context, File f, Uri selectedImage){
		String photopath = f.getPath().toString();
		Bitmap bmp = BitmapFactory.decodeFile(photopath);
		bmp = imageOreintationValidator(bmp,photopath,context,selectedImage);


		FileOutputStream fOut;
		try {
			fOut = new FileOutputStream(f);
			bmp.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
			fOut.flush();
			fOut.close();

		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static int getOrientation(Context context, Uri photoUri) {
    /* it's on the external media. */
		Cursor cursor = context.getContentResolver().query(photoUri,
				new String[]{MediaStore.Images.ImageColumns.ORIENTATION}, null, null, null);

		if(cursor == null) return -1;
		if (!cursor.moveToFirst()) {

			cursor.close();
			return -1;
		}
		return cursor.getInt(0);
	}
	public static Uri getOutputMediaFileUri(int type){

		return Uri.fromFile(getOutputMediaFile(type));
	}
	private static File getOutputMediaFile(int type){

		// Check that the SDCard is mounted
		File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
				Environment.DIRECTORY_PICTURES), "MyCameraVideo");


		// Create the storage directory(MyCameraVideo) if it does not exist
		if (! mediaStorageDir.exists()){

			if (! mediaStorageDir.mkdirs()){


				Log.d("MyCameraVideo", "Failed to create directory MyCameraVideo.");
				return null;
			}
		}


		// Create a media file name

		// For unique file name appending current timeStamp with file name
		java.util.Date date= new java.util.Date();
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
				.format(date.getTime());

		File mediaFile;

		if(type == 2) {

			// For unique video file name appending current timeStamp with file name
			mediaFile = new File(mediaStorageDir.getPath() + File.separator +
					"VID_"+ timeStamp + ".mp4");

		} else {
			return null;
		}

		return mediaFile;
	}
	private static Bitmap imageOreintationValidator(Bitmap bitmap, String path, Context context, Uri selectedImage) {

		ExifInterface ei;
		try {
			ei = new ExifInterface(path);
			int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_NORMAL);
			if(orientation == 0) {
				orientation = getOrientation(context, selectedImage);
			}
			switch (orientation) {
				case ExifInterface.ORIENTATION_ROTATE_90:
					bitmap = rotateImage(bitmap, 90);
					break;
				case 90:
					bitmap = rotateImage(bitmap, 90);
					break;
				case ExifInterface.ORIENTATION_ROTATE_180:
					bitmap = rotateImage(bitmap, 180);
					break;
				case 180:
					bitmap = rotateImage(bitmap, 180);
					break;
				case ExifInterface.ORIENTATION_ROTATE_270:
					bitmap = rotateImage(bitmap, 270);
					break;
				case 270:
					bitmap = rotateImage(bitmap, 270);
					break;
				case ExifInterface.ORIENTATION_TRANSVERSE:
					bitmap = rotateImage(bitmap, -90);
					break;
				case ExifInterface.ORIENTATION_FLIP_VERTICAL:
					bitmap = rotateImage(bitmap,180);
					break;
				case ExifInterface.ORIENTATION_TRANSPOSE:
					bitmap = rotateImage(bitmap,90);
					break;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return bitmap;
	}
	private static Bitmap rotateImage(Bitmap source, float angle) {

		Bitmap bitmap = null;
		Matrix matrix = new Matrix();
		matrix.postRotate(angle);
		try {
			bitmap = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
					matrix, true);
		} catch (OutOfMemoryError err) {
			err.printStackTrace();

		}
		return bitmap;
	}
	private static int getOrientationFromMediaStore(Context context, String imagePath) {
		Uri imageUri = getImageContentUri(context, imagePath);
		if(imageUri == null) {
			return -1;
		}

		String[] projection = {MediaStore.Images.ImageColumns.ORIENTATION};
		Cursor cursor = context.getContentResolver().query(imageUri, projection, null, null, null);

		int orientation = -1;
		if (cursor != null && cursor.moveToFirst()) {
			orientation = cursor.getInt(0);
			cursor.close();
		}

		return orientation;
	}

	private static Uri getImageContentUri(Context context, String imagePath) {
		String[] projection = new String[] {MediaStore.Images.Media._ID};
		String selection = MediaStore.Images.Media.DATA + "=? ";
		String[] selectionArgs = new String[] {imagePath};
		Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection,
				selection, selectionArgs, null);

		if (cursor != null && cursor.moveToFirst()) {
			int imageId = cursor.getInt(0);
			cursor.close();

			return Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, Integer.toString(imageId));
		}

		if (new File(imagePath).exists()) {
			ContentValues values = new ContentValues();
			values.put(MediaStore.Images.Media.DATA, imagePath);

			return context.getContentResolver().insert(
					MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
		}

		return null;
	}
	public static Bitmap getResizedBitmapByDeviceWidth(Bitmap src,
													   int deviceWidth) {

		
		final int origWidth = src.getWidth();
		final int origHeight = src.getHeight();

		
		float scale = (float)  origHeight/ origWidth;


		final int destHeight = Math.round(deviceWidth * scale);


		
		Bitmap bit=  Bitmap.createScaledBitmap(src, deviceWidth, destHeight, false);
		


		try
		{
//			print.message("AAPBDLIB","image width and height" + origWidth +""+ origHeight);
//
//			print.message("AAPBDLIB","destination width and height" + deviceWidth +""+ destHeight);
//			print.message("AAPBDLIB","new image width and height" + bit.getWidth() +""+ bit.getHeight());

			

		}catch(Exception e)
		{
			
		}
		
		return bit;
		

	}

	public static Bitmap getResizedBitmapUsingMatrix(final Bitmap BitmapOrg,
													 int x, int y, final boolean flag) {

		final int origHeight = BitmapOrg.getHeight();
		final int origWidth = BitmapOrg.getWidth();
		float scaleWidth;
		float scaleHeight;
		if (origWidth >= origHeight) {
			scaleWidth = (float) x / origWidth;
			scaleHeight = scaleWidth;
		} else {
			scaleHeight = (float) y / origHeight;
			scaleWidth = scaleHeight;
		}
		x = Math.round(origWidth * scaleWidth);
		y = Math.round(origHeight * scaleWidth);

		// create a matrix for the manipulation
		final Matrix matrix = new Matrix();
		// resize the Bitmap
		matrix.postScale(scaleWidth, scaleHeight);
		// if you want to rotate the Bitmap
		// matrix.postRotate(45);

		return Bitmap.createBitmap(BitmapOrg, 0, 0, x, y, matrix, flag);

	}

	public static byte[] getByteArrayFromBitmap(final Bitmap bitmap, int quality) {

		final ByteArrayOutputStream out = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, quality, out);
		return out.toByteArray();
	}

	public static Bitmap getBitmapFromByteArray(final byte data[], int maxSize) {
		final int MAX_SIZE = maxSize;
		if (data.length > 0) {
			try {
				final BitmapFactory.Options options = new BitmapFactory.Options();
				options.inJustDecodeBounds = true;
				Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0,
						data.length, options);
				final int fullWidth = options.outWidth;
				final int fullHeight = options.outHeight;
				int w = 0;
				int h = 0;
				if (fullWidth > fullHeight) {
					w = MAX_SIZE;
					h = w * fullHeight / fullWidth;
				} else {
					h = MAX_SIZE;
					w = h * fullWidth / fullHeight;
				}
				options.inJustDecodeBounds = false;
				options.inSampleSize = fullWidth / w;
				bitmap = BitmapFactory.decodeByteArray(data, 0, data.length,
						options);
				return bitmap;
			} catch (final Exception e) {
			}
		}
		return null;
	}

	
	
	public static Bitmap getResizedBitmap(final Bitmap bit, int maxSize) {
		final int MAX_SIZE = maxSize;




		if (bit!=null) {
			
//			print.message("Input Bitmap","is not NULL");
			try {
				
				final int fullWidth = bit.getWidth();
				final int fullHeight = bit.getHeight();
//				print.message("Input Bitmap size ", fullWidth+" "+fullHeight);


				int w = 0;
				int h = 0;
				if (fullWidth > fullHeight) {
					w = MAX_SIZE;
					h = w * fullHeight / fullWidth;
				} else {
					h = MAX_SIZE;
					w = h * fullWidth / fullHeight;
				}
				
								
//				print.message("Input Bitmap size  after Scale ", w+" "+h);

				
				
			    BitmapFactory.Options outBitmap = new BitmapFactory.Options();
			    outBitmap.inJustDecodeBounds = false; // the decoder will return a bitmap
			    outBitmap.inSampleSize = calculateSampleSize(fullWidth, fullHeight, w, h);

			    byte[] data1=getByteArrayFromBitmap(bit, 80);
			 
			    		Bitmap resizedBitmap   = BitmapFactory.decodeByteArray(data1,0, data1.length, outBitmap);
			    		data1=null;
				
			//	return getRescaledBitmap(bit,w,h,false);
			    
		
			return resizedBitmap;
			} catch (final Exception e) {
				
//				print.message("Input Bitmap Resize error",e.toString());

				
			}
		}
		else
		{
//			print.message("Input Bitmap","NULL");

		}
		return null;
	}

	
	
	
	public static int calculateSampleSize(int width, int height, int targetWidth, int targetHeight) {
	    int inSampleSize = 1;

	    if (height > targetHeight || width > targetWidth) {

	        // Calculate ratios of height and width to requested height and
	        // width
	        final int heightRatio = Math.round((float) height
	                / (float) targetHeight);
	        final int widthRatio = Math.round((float) width / (float) targetWidth);

	        // Choose the smallest ratio as inSampleSize value, this will
	        // guarantee
	        // a final image with both dimensions larger than or equal to the
	        // requested height and width.
	        inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
	    }
	    return inSampleSize;
	}
	


	// decodes image and scales it to reduce memory consumption
	public static Bitmap decodeFile(File f, int size) {
		try {
			// Decode image size
			final BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(f), null, o);

			// The new size we want to scale to
			final int REQUIRED_SIZE = size;

			// Find the correct scale value. It should be the power of 2.
			int scale = 1;
			while (o.outWidth / scale / 2 >= REQUIRED_SIZE
					&& o.outHeight / scale / 2 >= REQUIRED_SIZE)
				scale *= 2;

			// Decode with inSampleSize
			final BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
		} catch (final FileNotFoundException e) {
		}
		return null;
	}

	public static InputStream getInputStreamFromBitmap(Bitmap bitmap) {
		final ByteArrayOutputStream out = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
		final byte[] bitmapdata = out.toByteArray();
		final ByteArrayInputStream bs = new ByteArrayInputStream(bitmapdata);
		return bs;
	}

}
