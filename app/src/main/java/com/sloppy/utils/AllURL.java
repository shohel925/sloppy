package com.sloppy.utils;

import java.util.Vector;

import android.annotation.SuppressLint;
import android.text.TextUtils;

import com.sloppy.aapbdLib.KeyValue;
import com.sloppy.aapbdLib.UrlUtils;


@SuppressLint("DefaultLocale")
public class AllURL {

	private static String getcommonURLWithParamAndAction(String action,
			Vector<KeyValue> params) {
		String HTTP = "http://sloppy2ndz.aapbd.xyz/api/v1/";

		if (params == null || params.size() == 0) {
			return HTTP + action;
		} else {
			String pString = "";

			for (final KeyValue obj : params) {

				pString += obj.getKey().trim() + "=" + obj.getValue().trim()
						+ "&";
			}

			if (pString.endsWith("&")) {
				pString = pString.subSequence(0, pString.length() - 1)
						.toString();
			}

			return HTTP + action + "?" + UrlUtils.encode(pString);
		}
	}

	
//-----------Create URL for post method----------
	public static String normalUserLoinURL() {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		return getcommonURLWithParamAndAction("login", temp);
	}
	public static String discountUpdateURL() {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		return getcommonURLWithParamAndAction("edit-discount", temp);
	}
	public static String addProductURL() {
		final Vector<KeyValue> temp = new Vector<KeyValue>();	
		return getcommonURLWithParamAndAction("add-product", temp);
	}
	public static String repostToFeedURL() {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		return getcommonURLWithParamAndAction("repost-feed", temp);
	}
	public static String postUpdateProductURL() {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		return getcommonURLWithParamAndAction("edit-product", temp);
	}
	public static String addProductSellURL(String productId,String price) {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		temp.addElement(new KeyValue("product_id",productId));
		temp.addElement(new KeyValue("sell_price",price));
		return getcommonURLWithParamAndAction("add-product-sell", temp);
	}

	public static String profileUpdateURL() {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		return getcommonURLWithParamAndAction("edit-profile", temp);
	}
	public static String socialSignUpURL() {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		return getcommonURLWithParamAndAction("login/social", temp);
	}
	
//-----------------------URL for get method--------------
public static String getActivityListURL(String type,String page_items) {
	final Vector<KeyValue> temp = new Vector<KeyValue>();
	temp.addElement(new KeyValue("type",type));
	temp.addElement(new KeyValue("page_items",page_items));
	return getcommonURLWithParamAndAction("activity-list", temp);
}
	public static String getSendMessage(String user_id,String msg_text) {
	final Vector<KeyValue> temp = new Vector<KeyValue>();
	temp.addElement(new KeyValue("user_id",user_id));
	temp.addElement(new KeyValue("msg_text",msg_text));
	return getcommonURLWithParamAndAction("send-message", temp);
	}

	public static String getNotification(String page_items) {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		temp.addElement(new KeyValue("page_items",page_items));
		return getcommonURLWithParamAndAction("notifications", temp);
	}

	public static String getOfferList(String page_items) {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		temp.addElement(new KeyValue("page_items",page_items));
		return getcommonURLWithParamAndAction("offer-list", temp);
	}
	public static String followUnfollowUrl(String type,String user_id) {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		temp.addElement(new KeyValue("type",type));
		temp.addElement(new KeyValue("user_id",user_id));
		return getcommonURLWithParamAndAction("follow-unfollow-list", temp);
	}

	public static String getFollowUnfollowUrl(String user_id) {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		temp.addElement(new KeyValue("user_id",user_id));
		return getcommonURLWithParamAndAction("follow-unfollow", temp);
	}

	public static String commonInformationUrl(String user_type) {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		temp.addElement(new KeyValue("user_type",user_type));
		return getcommonURLWithParamAndAction("common-data", temp);
	}

	public static String forgotPassword(String query_key) {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		temp.addElement(new KeyValue("query_key",query_key));
		return getcommonURLWithParamAndAction("forgot-password", temp);
	}

	/**
	 * -----------6. Single product details of a product id.
	 */
	public static String getSingleProDetaURL(String product_id, String user_type) {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		temp.addElement(new KeyValue("product_id",product_id));
		temp.addElement(new KeyValue("user_type",user_type));
		return getcommonURLWithParamAndAction("product-details", temp);
	}
	public static String signUp() {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		return getcommonURLWithParamAndAction("register", temp);
	}

	public static String getFeeddata(String page_items) {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		temp.addElement(new KeyValue("page_items",page_items));
		return getcommonURLWithParamAndAction("home-feed", temp);
	}

	public static String getHashTagList(String hashId,String userType) {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		temp.addElement(new KeyValue("hash_id",hashId));
		temp.addElement(new KeyValue("user_type",userType));
		return getcommonURLWithParamAndAction("hash-tag-products", temp);
	}

	public static String getFeeddataDemo() {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		return getcommonURLWithParamAndAction("demo-user", temp);
	}

	public static String getFolowersFollingList(String type,String user_id,String user_type) {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		temp.addElement(new KeyValue("type",type));
		temp.addElement(new KeyValue("user_id",user_id));
		temp.addElement(new KeyValue("user_type",user_type));
		return getcommonURLWithParamAndAction("follow-unfollow-list", temp);
	}
	public static String getbrandList(String brand_id,String user_type) {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		temp.addElement(new KeyValue("brand_id",brand_id));
		temp.addElement(new KeyValue("user_type",user_type));
		return getcommonURLWithParamAndAction("product-list-by-brand", temp);
	}

	public static String getSubCatProList(String category_id,String suitable_for,String page_items, String user_type,String sub_category_id) {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		temp.addElement(new KeyValue("category_id",category_id));
		temp.addElement(new KeyValue("suitable_for",suitable_for));
		temp.addElement(new KeyValue("page_items",page_items));
		temp.addElement(new KeyValue("user_type",user_type));
		temp.addElement(new KeyValue("sub_category_id",sub_category_id));
		return getcommonURLWithParamAndAction("product-list", temp);
	}


	public static String getProfileData(String date) {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		temp.addElement(new KeyValue("date",date));
		return getcommonURLWithParamAndAction("my-profile", temp);
	}
	public static String getMyShopData(String type, String page_items) {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		temp.addElement(new KeyValue("type",type));
		temp.addElement(new KeyValue("page_items",page_items));
		return getcommonURLWithParamAndAction("product-list-by-type", temp);
	}

	public static String getSoldData(String type, String page_items) {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		temp.addElement(new KeyValue("type",type));
		temp.addElement(new KeyValue("page_items",page_items));
		return getcommonURLWithParamAndAction("product-list-by-type", temp);
	}

	public static String getPurChaedData(String type, String page_items) {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		temp.addElement(new KeyValue("type",type));
		temp.addElement(new KeyValue("page_items",page_items));
		return getcommonURLWithParamAndAction("product-list-by-type", temp);
	}

	public static String getSearchProduct(String search_text, String search_type,String category_id,
										  String suitable_for,String page_items,String user_type,String subcategory_id) {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		temp.addElement(new KeyValue("search_text",search_text));
		temp.addElement(new KeyValue("search_type",search_type));
		temp.addElement(new KeyValue("category_id",category_id));
		temp.addElement(new KeyValue("suitable_for",suitable_for));
		temp.addElement(new KeyValue("page_items",page_items));
		temp.addElement(new KeyValue("user_type",user_type));
		temp.addElement(new KeyValue("subcategory_id",subcategory_id));

		return getcommonURLWithParamAndAction("search", temp);
	}

	public static String getMessagesData(String date) {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		temp.addElement(new KeyValue("date",date));
		return getcommonURLWithParamAndAction("my-message", temp);
	}

	public static String getBundleList() {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		return getcommonURLWithParamAndAction("bundle-list", temp);
	}
	public static String sentReport() {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		return getcommonURLWithParamAndAction("send-flag", temp);
	}

	public static String getNotificationCount() {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		return getcommonURLWithParamAndAction("notification-count", temp);
	}
	public static String getLikeUnlike(String product_id) {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		temp.addElement(new KeyValue("product_id",product_id));
		return getcommonURLWithParamAndAction("product-like", temp);
	}
	public static String getAddBundle(String product_id) {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		temp.addElement(new KeyValue("product_id",product_id));
		return getcommonURLWithParamAndAction("add-bundle", temp);
	}

	public static String getUserDetailURL(String user_id, String user_type) {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		temp.addElement(new KeyValue("user_id",user_id));
		temp.addElement(new KeyValue("user_type",user_type));
		return getcommonURLWithParamAndAction("user-profile", temp);
	}

	public static String getProCommentList(String product_id, String user_type) {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		temp.addElement(new KeyValue("product_id",product_id));
		temp.addElement(new KeyValue("user_type",user_type));
		return getcommonURLWithParamAndAction("comments", temp);
	}

	public static String getSearchUserList(String search_text, String user_type) {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		temp.addElement(new KeyValue("search_text",search_text));
		temp.addElement(new KeyValue("user_type",user_type));
		return getcommonURLWithParamAndAction("search-user", temp);
	}

	public static String getAddProCommentList(String product_id, String comment_text,String tagged_user_id) {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		temp.addElement(new KeyValue("product_id",product_id));
		temp.addElement(new KeyValue("comment_text",comment_text));
		if (!TextUtils.isEmpty(tagged_user_id)) {
			temp.addElement(new KeyValue("tagged_user_id", tagged_user_id));
		}
		return getcommonURLWithParamAndAction("add-comment", temp);
	}
	public static String getProductOffer(String product_id, String office_price) {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		temp.addElement(new KeyValue("product_id",product_id));
		temp.addElement(new KeyValue("offer_price",office_price));
		return getcommonURLWithParamAndAction("send-offer", temp);
	}
	public static String getBundleRemove(String bundle_id, String seller_id) {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		temp.addElement(new KeyValue("bundle_id",bundle_id));
		temp.addElement(new KeyValue("seller_id",seller_id));
		return getcommonURLWithParamAndAction("bundle-remove", temp);
	}
	public static String notificationRead(String type, String date) {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		temp.addElement(new KeyValue("type",type));
		temp.addElement(new KeyValue("date",date));
		return getcommonURLWithParamAndAction("make-read", temp);
	}

	public static String getFeedbacks(String user_id) {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		temp.addElement(new KeyValue("user_id",user_id));
		return getcommonURLWithParamAndAction("feedbacks", temp);
	}
	public static String getDelProduct(String product_id) {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		temp.addElement(new KeyValue("product_id",product_id));
		return getcommonURLWithParamAndAction("delete-product", temp);
	}

	public static String getOfferAccept(String offer_id) {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		temp.addElement(new KeyValue("offer_id",offer_id));
		return getcommonURLWithParamAndAction("offer-accept", temp);
	}

	public static String getOfferDecline(String offer_id) {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		temp.addElement(new KeyValue("offer_id",offer_id));
		return getcommonURLWithParamAndAction("offer-decline", temp);
	}


	public static String getSubmitFeedback(String user_id, String feedback_text,int rating_value) {
		final Vector<KeyValue> temp = new Vector<KeyValue>();
		temp.addElement(new KeyValue("user_id",user_id));
		temp.addElement(new KeyValue("feedback_text",feedback_text));
		temp.addElement(new KeyValue("rating_value",""+rating_value));
		return getcommonURLWithParamAndAction("submit-feedback", temp);
	}
}