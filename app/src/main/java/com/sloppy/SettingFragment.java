package com.sloppy;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.sloppy.BaseFragment;
import com.sloppy.BillingInformationFragment;
import com.sloppy.CommunicatorFragmentInterface;
import com.sloppy.MenuFragmentDialog;
import com.sloppy.OnFragmentInteractionListener;
import com.sloppy.R;
import com.sloppy.SellerDiscountFragment;
import com.sloppy.aapbdLib.AlertMessage;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.NetInfo;
import com.sloppy.aapbdLib.PersistData;
import com.sloppy.aapbdLib.PersistentUser;
import com.sloppy.model.ProfileUpdateResponse;
import com.sloppy.utils.AllURL;
import com.sloppy.utils.AppConstant;

import cz.msebera.android.httpclient.Header;

/**
 * Created by hp on 3/6/2016.
 */
public class SettingFragment extends BaseFragment {
    private View viewCurrency;
    private Context con;
    private Typeface helveticaNeuRegular, helveticaNeueBold;

    private ProfileUpdateResponse profileUpdateResponse;
    private TextView tvEuro,tvEUR,tvUnitedState,tvUSD,tvBritish,tvGBD,tvRussian,tvRUB,textViewSetBilling,tvSellingCurrency,
            tvSelCurrency,tvTurn,tvBuiltVersion,tvAbout,tvBuild,tvSettin,textViewSellerDiscount,selling,textViewManagePaypal,textViewSharing,tvfaceboo,tvTwitter,tvInstagra;
    private RelativeLayout rLEuro,rlUSD,rlBritishPount,rlRUB;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        con=getActivity();
        viewCurrency= inflater.inflate(R.layout.fragment_setting, container,false);
        initUI();
        return  viewCurrency;
    }

    private void initUI() {
        /**
         * ---------------------Initialization--------------------------------------------------
         */
        ImageView imgSettinMenu= (ImageView) viewCurrency.findViewById(R.id.imgSettinMenu);
        tvSettin= (TextView) viewCurrency.findViewById(R.id.tvSettin);
        selling= (TextView) viewCurrency.findViewById(R.id.selling);
        tvSellingCurrency= (TextView) viewCurrency.findViewById(R.id.tvSellingCurrency);
        textViewSellerDiscount= (TextView) viewCurrency.findViewById(R.id.textViewSellerDiscount);
        tvSelCurrency= (TextView) viewCurrency.findViewById(R.id.tvSelCurrency);
        tvAbout= (TextView) viewCurrency.findViewById(R.id.tvAbout);
        tvBuild= (TextView) viewCurrency.findViewById(R.id.tvBuild);
        tvBuiltVersion= (TextView) viewCurrency.findViewById(R.id.tvBuiltVersion);


//        textViewSetBilling= (TextView) viewCurrency.findViewById(R.id.textViewSetBilling);
//
//
//        textViewManagePaypal= (TextView) viewCurrency.findViewById(R.id.textViewManagePaypal);
//
//
//        textViewSharing= (TextView) viewCurrency.findViewById(R.id.textViewSharing);
//        tvfaceboo= (TextView) viewCurrency.findViewById(R.id.tvfaceboo);
//        tvTwitter= (TextView) viewCurrency.findViewById(R.id.tvTwitter);
//        tvInstagra= (TextView) viewCurrency.findViewById(R.id.tvInstagra);
//        tvTurn= (TextView) viewCurrency.findViewById(R.id.tvTurn);
//
//

        ImageView imgSettingBack=(ImageView) viewCurrency.findViewById(R.id.imgSettingBack);
        RelativeLayout rlCurrency=(RelativeLayout) viewCurrency.findViewById(R.id.rlCurrency);
        /**
         * ------------------Set dat---------------------------------------------------
         */

        /**
         * -------------------------------Font set----------------------------------------
         */
        helveticaNeuRegular = Typeface.createFromAsset(getActivity().getAssets(), "font/helveticaNeuRegular.ttf");
        helveticaNeueBold = Typeface.createFromAsset(getActivity().getAssets(), "font/helveticaNeueBold.ttf");
        tvSettin.setTypeface(helveticaNeuRegular);
        selling.setTypeface(helveticaNeueBold);
//        textViewSharing.setTypeface(helveticaNeueBold);
        tvAbout.setTypeface(helveticaNeueBold);

//        textViewManagePaypal.setTypeface(helveticaNeuRegular);
//        textViewSetBilling.setTypeface(helveticaNeuRegular);
        textViewSellerDiscount.setTypeface(helveticaNeuRegular);
        tvSellingCurrency.setTypeface(helveticaNeuRegular);
        tvSelCurrency.setTypeface(helveticaNeuRegular);
//        tvfaceboo.setTypeface(helveticaNeuRegular);
//        tvTwitter.setTypeface(helveticaNeuRegular);
//        tvInstagra.setTypeface(helveticaNeuRegular);
//        tvTurn.setTypeface(helveticaNeuRegular);
        tvBuild.setTypeface(helveticaNeuRegular);
        tvBuiltVersion.setTypeface(helveticaNeuRegular);


        setCurrency(AppConstant.currencyProfile);
//        StringBuilder sb = new StringBuilder(PersistData.getStringData(con,AppConstant.currencyProfile));
//        for (int index = 0; index < sb.length(); index++) {
//            char c = sb.charAt(index);
//            if (Character.isLowerCase(c)) {
//                sb.setCharAt(index, Character.toUpperCase(c));
//            } else {
//                sb.setCharAt(index, Character.toLowerCase(c));
//            }
//        }
//        tvSelCurrency.setText(sb.toString());


        /**
         * -------------------------On Click------------------------------------------------
         */
        imgSettinMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MenuFragmentDialog dialogMenu= new MenuFragmentDialog();
                dialogMenu.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar);
                dialogMenu.show(getActivity().getFragmentManager(),"");
            }
        });
//        textViewSetBilling.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                myCommunicator.setContentFragment(new BillingInformationFragment(), true);
//            }
//        });
        textViewSellerDiscount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myCommunicator.setContentFragment(new SellerDiscountFragment(), true);
            }
        });
        rlCurrency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               currencyDialog();
            }
        });
        imgSettingBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

    }

    /**
     * ----------------------Currency dialog----------------------------------------------------
     */
    private void currencyDialog(){

        final Dialog dialogCurrency = new Dialog(con);
        //----------To Remove Title Bar-----------------
        dialogCurrency.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //------------TO Connect UI with dialog---------------
        dialogCurrency.setContentView(R.layout.dialog_currency);
        //------------To make background transparent-----------------
        dialogCurrency.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        //-----------To cover all screen-----------------------------
        dialogCurrency.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        /**
         * ---------------Initialization----------------------
         */
        TextView tvSelectCu= (TextView) dialogCurrency.findViewById(R.id.tvSelectCu);
         rLEuro= (RelativeLayout) dialogCurrency.findViewById(R.id.rLEuro);
         rlUSD= (RelativeLayout) dialogCurrency.findViewById(R.id.rlUSD);
         rlBritishPount= (RelativeLayout) dialogCurrency.findViewById(R.id.rlBritishPount);
         rlRUB= (RelativeLayout) dialogCurrency.findViewById(R.id.rlRUB);
         tvEuro=(TextView) dialogCurrency.findViewById(R.id.tvEuro);
         tvEUR=(TextView) dialogCurrency.findViewById(R.id.tvEUR);
         tvUnitedState=(TextView) dialogCurrency.findViewById(R.id.tvUnitedState);
         tvUSD=(TextView) dialogCurrency.findViewById(R.id.tvUSD);
         tvBritish=(TextView) dialogCurrency.findViewById(R.id.tvBritish);
         tvGBD=(TextView) dialogCurrency.findViewById(R.id.tvGBD);
         tvRussian=(TextView) dialogCurrency.findViewById(R.id.tvRussian);
         tvRUB=(TextView) dialogCurrency.findViewById(R.id.tvRUB);

        ImageView imgCurrencyCross= (ImageView) dialogCurrency.findViewById(R.id.imgCurrencyCross);

        /**
         * -----------------Set data-----------------------------------------
         */
        setCurrencyBackground(AppConstant.currencyProfile);

        /**
         * --------------On Click------------------------------
         */
        rLEuro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profileUpdateAPI(AllURL.profileUpdateURL(),"eur");
                setCurrencyBackground("eur");
            }
        });
        rlUSD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCurrencyBackground("usd");
                profileUpdateAPI(AllURL.profileUpdateURL(), "usd");
            }
        });
        rlBritishPount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCurrencyBackground("gbp");
                profileUpdateAPI(AllURL.profileUpdateURL(), "gbp");
            }
        });
        rlRUB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCurrencyBackground("rub");
                profileUpdateAPI(AllURL.profileUpdateURL(), "rub");
            }
        });

        imgCurrencyCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialogCurrency.dismiss();
            }
        });

        /**
         * ---------------------To show Currency dialog-----------------
         */
            dialogCurrency.show();
    }

    /**
     * ------------------Set currency in setting fragment-------------------------
     */
    private void setCurrency(String currency){
        switch (currency) {
            case "eur":
                tvSelCurrency.setText("EUR");
               break;
            case "usd":
                tvSelCurrency.setText("USD");
                break;
            case "gbp":
                tvSelCurrency.setText("GBD");
                break;
            case "rub":
                tvSelCurrency.setText("RUB");
                break;
        }
    }

    /**
     * ------------------------Set Currency background----------------------------------------
     */
    private void setCurrencyBackground(String currency){
        switch (currency) {
            case "eur":
                rLEuro.setBackgroundResource(R.drawable.currency_red);
                rlUSD.setBackgroundResource(R.drawable.currency_white);
                rlBritishPount.setBackgroundResource(R.drawable.currency_white);
                rlRUB.setBackgroundResource(R.drawable.currency_white);
                tvEuro.setTextColor(Color.parseColor("#FFFFFF"));
                tvEUR.setTextColor(Color.parseColor("#FFFFFF"));
                tvUnitedState.setTextColor(Color.parseColor("#000000"));
                tvUSD.setTextColor(Color.parseColor("#999999"));
                tvBritish.setTextColor(Color.parseColor("#000000"));
                tvGBD.setTextColor(Color.parseColor("#999999"));
                tvRussian.setTextColor(Color.parseColor("#000000"));
                tvRUB.setTextColor(Color.parseColor("#999999"));
                break;
            case "usd":
                rLEuro.setBackgroundResource(R.drawable.currency_white);
                rlUSD.setBackgroundResource(R.drawable.currency_red);
                rlBritishPount.setBackgroundResource(R.drawable.currency_white);
                rlRUB.setBackgroundResource(R.drawable.currency_white);
                tvEuro.setTextColor(Color.parseColor("#000000"));
                tvEUR.setTextColor(Color.parseColor("#999999"));
                tvUnitedState.setTextColor(Color.parseColor("#FFFFFF"));
                tvUSD.setTextColor(Color.parseColor("#FFFFFF"));
                tvBritish.setTextColor(Color.parseColor("#000000"));
                tvGBD.setTextColor(Color.parseColor("#999999"));
                tvRussian.setTextColor(Color.parseColor("#000000"));
                tvRUB.setTextColor(Color.parseColor("#999999"));
                break;
            case "gbp":
                rLEuro.setBackgroundResource(R.drawable.currency_white);
                rlUSD.setBackgroundResource(R.drawable.currency_white);
                rlBritishPount.setBackgroundResource(R.drawable.currency_red);
                rlRUB.setBackgroundResource(R.drawable.currency_white);
                tvEuro.setTextColor(Color.parseColor("#000000"));
                tvEUR.setTextColor(Color.parseColor("#999999"));
                tvUnitedState.setTextColor(Color.parseColor("#000000"));
                tvUSD.setTextColor(Color.parseColor("#999999"));
                tvBritish.setTextColor(Color.parseColor("#FFFFFF"));
                tvGBD.setTextColor(Color.parseColor("#FFFFFF"));
                tvRussian.setTextColor(Color.parseColor("#000000"));
                tvRUB.setTextColor(Color.parseColor("#999999"));
                break;
            case "rub":
                rLEuro.setBackgroundResource(R.drawable.currency_white);
                rlUSD.setBackgroundResource(R.drawable.currency_white);
                rlBritishPount.setBackgroundResource(R.drawable.currency_white);
                rlRUB.setBackgroundResource(R.drawable.currency_red);
                tvEuro.setTextColor(Color.parseColor("#000000"));
                tvEUR.setTextColor(Color.parseColor("#999999"));
                tvUnitedState.setTextColor(Color.parseColor("#000000"));
                tvUSD.setTextColor(Color.parseColor("#999999"));
                tvBritish.setTextColor(Color.parseColor("#000000"));
                tvGBD.setTextColor(Color.parseColor("#999999"));
                tvRussian.setTextColor(Color.parseColor("#FFFFFF"));
                tvRUB.setTextColor(Color.parseColor("#FFFFFF"));
                break;
        }
    }

    /**
     *--------------Call API for currency setting(4. Profile Update)--------------------------------------------------
     */
    private void profileUpdateAPI(final String url, final String currency) {
        //--- for net check-----
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }

        final BusyDialog busyNow = new BusyDialog(con, true, false);
        busyNow.show();

        final AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token));
        Log.e("token", PersistData.getStringData(con, AppConstant.token));

        final RequestParams param = new RequestParams();

        try {
            Log.w("currency", currency);
            param.put("currency", currency);
        } catch (final Exception e1) {
            e1.printStackTrace();
        }

        client.post(url, param, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {}
            @Override
            public void onSuccess(int statusCode, Header[] headers,
                                  byte[] response) {
                if (busyNow != null) {
                    busyNow.dismis();
                }
                Log.e("resposne ", ">>" + new String(response));
                Gson g = new Gson();
                profileUpdateResponse = g.fromJson(new String(response), ProfileUpdateResponse.class);

                if (profileUpdateResponse.getStatus().equalsIgnoreCase("1")) {
                    AppConstant.currencyProfile=currency;
                    PersistentUser.setLogin(con);
                    Toast.makeText(con,"Currency"+ profileUpdateResponse.getMsg() + "", Toast.LENGTH_LONG).show();
                    setCurrency(currency);
                } else {
                    AlertMessage.showMessage(con, "Status", profileUpdateResponse.getMsg() + "");
                    return;
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  byte[] errorResponse, Throwable e) {
                Log.e("errorResponse", new String(errorResponse));
                if (busyNow != null) {
                    busyNow.dismis();
                }
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }
}
