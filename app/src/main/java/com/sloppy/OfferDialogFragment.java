package com.sloppy;

import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.sloppy.aapbdLib.AlertMessage;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.NetInfo;
import com.sloppy.aapbdLib.PersistData;
import com.sloppy.aapbdLib.PersistentUser;
import com.sloppy.model.LikeUnlikeResponse;
import com.sloppy.model.ProCommentListResponse;
import com.sloppy.model.ProductAndUserDetailsInfo;
import com.sloppy.model.ProductOfferResponse;
import com.sloppy.utils.AAPBDHttpClient;
import com.sloppy.utils.AllURL;
import com.sloppy.utils.AppConstant;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.util.concurrent.Executors;

/**
 * Created by hp on 8/3/2016.
 */
public class OfferDialogFragment extends DialogFragment {
    private Context con;
    private View view;
    private ImageView imgLoveOffer;
    private ProductAndUserDetailsInfo feedRowObject;
    private String pictureUrl = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialog_offer, container, false);
        con = getActivity();
        initUi();
        return view;
    }

    private void initUi() {
        feedRowObject = AppConstant.feedRowObject;
        ImageView imgOfferCross = (ImageView) view.findViewById(R.id.imgOfferCross);
        ImageView imgOfferProduct = (ImageView) view.findViewById(R.id.imgOfferProduct);
        ImageView imgOfferProSeller = (ImageView) view.findViewById(R.id.imgOfferProSeller);
         imgLoveOffer = (ImageView) view.findViewById(R.id.imgLoveOffer);
        ImageView imgShareOffer = (ImageView) view.findViewById(R.id.imgShareOffer);
        ImageView imgComentOffer = (ImageView) view.findViewById(R.id.imgComentOffer);

        TextView tvOffer = (TextView) view.findViewById(R.id.tvOffer);
        TextView tvOfferPrdtName = (TextView) view.findViewById(R.id.tvOfferPrdtName);
        TextView tvOfferBrand = (TextView) view.findViewById(R.id.tvOfferBrand);
        TextView tvOfferOriginalPrice = (TextView) view.findViewById(R.id.tvOfferOriginalPrice);
        TextView tvOfferlistingPrice = (TextView) view.findViewById(R.id.tvOfferlistingPrice);
        TextView tvLikeCountOffer = (TextView) view.findViewById(R.id.tvLikeCountOffer);
        final TextView tvCommentCountOffer = (TextView) view.findViewById(R.id.tvCommentCountOffer);
        TextView tvSellerName = (TextView) view.findViewById(R.id.tvSellerName);
        TextView tvSubmitOffer = (TextView) view.findViewById(R.id.tvSubmitOffer);
        final EditText tvYourOffer = (EditText) view.findViewById(R.id.tvYourOffer);
        AppConstant.setFont(con, tvOffer, "regular");
        AppConstant.setFont(con, tvOfferPrdtName, "regular");
        AppConstant.setFont(con, tvOfferBrand, "regular");
        AppConstant.setFont(con, tvOfferOriginalPrice, "regular");
        AppConstant.setFont(con, tvOfferlistingPrice, "regular");
        AppConstant.setFont(con, tvLikeCountOffer, "regular");
        AppConstant.setFont(con, tvCommentCountOffer, "regular");
        AppConstant.setFont(con, tvSellerName, "regular");
        AppConstant.setFont(con, tvSubmitOffer, "regular");

        /**
         * -----------------Set data--------------------
         */
        tvYourOffer.setHint(getResources().getString(R.string.your_offer) + " " + AppConstant.getCurrencySymbol(feedRowObject.getSeller_info().get(0).getCurrency()));
        tvOfferPrdtName.setText(feedRowObject.getName());
        tvOfferBrand.setText(feedRowObject.getBrand_info().get(0).getName());
        tvOfferOriginalPrice.setText(AppConstant.getCurrencySymbol(feedRowObject.getSeller_info().get(0).getCurrency()) + feedRowObject.getOriginal_price());
        tvOfferOriginalPrice.setPaintFlags(tvOfferOriginalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        tvOfferlistingPrice.setText(AppConstant.getCurrencySymbol(feedRowObject.getSeller_info().get(0).getCurrency()) + feedRowObject.getListing_price());
        tvLikeCountOffer.setText(feedRowObject.getLike_count());
        tvCommentCountOffer.setText(feedRowObject.getComments_count());
        tvSellerName.setText(feedRowObject.getSeller_info().get(0).getUsername());
        if (feedRowObject.getIs_liked().equalsIgnoreCase("0")) {
            imgLoveOffer.setImageResource(R.drawable.love_unliked);
        } else {
            imgLoveOffer.setImageResource(R.drawable.love_liked);
        }

        imgLoveOffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callLikeUnlikeAPI(feedRowObject.getId());
            }
        });



        Picasso.with(con)
                .load(feedRowObject.getSeller_info().get(0).getProfile_image())
                .placeholder(R.drawable.person)
                .error(R.drawable.person)
                .into(imgOfferProSeller);

        Picasso.with(con).load(feedRowObject.getPhoto_one())
                .placeholder(R.drawable.person)
                .error(R.drawable.person).into(imgOfferProduct);

        /**
         * ------------------On Click----------------
         */
        imgOfferCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (view != null) {
                    getDialog().dismiss();
                }
            }
        });
        imgShareOffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PersistentUser.isLogged(con)) {
                    pictureUrl=feedRowObject.getPhoto_one();
                    Picasso.with(con).load(pictureUrl).memoryPolicy(MemoryPolicy.NO_CACHE).into(target);
                } else {
                    AppConstant.loginDialoag(con);
                }
            }
        });
        imgComentOffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.productIDfeedRow = feedRowObject.getId();
                AppConstant.tagUserList=feedRowObject.getTaggable_users();
                CommantDialogFragment dialogComment = new CommantDialogFragment();
                dialogComment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        tvCommentCountOffer.setText(AppConstant.commentCount);
                    }
                });
                dialogComment.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar);
                dialogComment.show(getActivity().getFragmentManager(), "");
            }
        });
        tvSubmitOffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(tvYourOffer.getText().toString())) {
                    AlertMessage.showMessage(con, getString(R.string.status), "Provide your offer first.");
                } else {
                    callProductOfferAPI(feedRowObject.getId(), tvYourOffer.getText().toString());
                }
            }
        });
    }

    BusyDialog busyDialog;

    private Target target = new Target() {


        @Override
        public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
            new Thread(new Runnable() {

                @Override
                public void run() {

                    File file = new File(Environment.getExternalStorageDirectory().getPath() + "/picture.jpg");
                    try {
                        file.createNewFile();
                        FileOutputStream ostream = new FileOutputStream(file);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, ostream);
                        busyDialog.dismis();
                        String path = MediaStore.Images.Media.insertImage(con.getContentResolver(), bitmap, "Title", null);
                        AppConstant.defaultShare(con, Uri.parse(path));
                        ostream.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }).start();
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
            busyDialog.dismis();
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
            if (placeHolderDrawable != null) {
            }
            busyDialog = new BusyDialog(con, false, "");
            busyDialog.show();
        }
    };

    /**
     * -------------------10. Like/Unlike Product API------------------
     */
    public void callLikeUnlikeAPI(final String product_id) {
  /*
   * ---------------check internet first------------
   */
        if (!NetInfo.isOnline(con)) {
            AppConstant.alertDialoag(con, con.getString(R.string.status), con.getString(R.string.checkInternet), con.getString(R.string.ok));
            return;
        }

  /*
   * ----------------Show Busy Dialog -----------------------------------------
   */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
        /**
         *----------------------Start Thread-------------------------------------------
         */
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            String msg = "";
            String response = "";
            LikeUnlikeResponse likeUnlikeResponse;

            @Override
            public void run() {
                //--------------- We can performed your task here.-----------------

                try {
                    Log.i("Like/Unlike URL", AllURL.getLikeUnlike(product_id));
                    //-------------Hit Server---------------------
                    response = AAPBDHttpClient.get(AllURL.getLikeUnlike(product_id)).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();

                    //----------------Persist response with the help of Gson----------------
                    Log.i("Like/Unlike Response", ">>" + response);
                    Gson gson = new Gson();
                    likeUnlikeResponse = gson.fromJson(response, LikeUnlikeResponse.class);

                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        /**
                         * -------------Here we do UI related tasks inside run() method of runOnUiThread()-------------------
                         */

                        //-------Stop Busy Dialog-----
                        if (busy != null) {
                            busy.dismis();
                        }
                        /**
                         * ---------main Ui related work--------------
                         */
                        if (likeUnlikeResponse != null) {
                            Toast.makeText(con,likeUnlikeResponse.getMsg(),Toast.LENGTH_LONG).show();
                            if (likeUnlikeResponse.getMsg().equalsIgnoreCase("liked")) {
                                imgLoveOffer.setImageResource(R.drawable.love_liked);
                            } else if (likeUnlikeResponse.getMsg().equalsIgnoreCase("unliked")) {
                                imgLoveOffer.setImageResource(R.drawable.love_unliked);
                            }

                        }


                    }
                });

            }

        });

    }

    /**
     * -------------------28. Product Offer  API------------------
     */
    private void callProductOfferAPI(final String product_id, final String office_price) {
  /*
   * ---------------check internet first------------
   */
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }

  /*
   * ----------------Show Busy Dialog -----------------------------------------
   */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
  /*
   *
   *----------------------Start Thread-------------------------------------------
   *
   */
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            String msg = "";
            String response = "";
            ProductOfferResponse productOfferResponse;

            @Override
            public void run() {
                // You can performed your task here.

                try {
                    Log.w("28.Product Offer URL", AllURL.getProductOffer(product_id, office_price));
                    //-------------Hit Server---------------------
                    response = AAPBDHttpClient.get(AllURL.getProductOffer(product_id, office_price)).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();

                    //----------------Persist response with the help of Gson----------------
                    Log.w("28.ProOffer Response", ">>" + response);
                    Gson gson = new Gson();
                    productOfferResponse = gson.fromJson(response, ProductOfferResponse.class);

                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        /**
                         * -------------Here we do UI related tasks inside run() method of runOnUiThread()-------------------
                         */

                        //-------Stop Busy Dialog-----
                        if (busy != null) {
                            busy.dismis();
                        }

                        if (productOfferResponse.getStatus().equalsIgnoreCase("1")) {

                            Toast.makeText(con, "Product Offer " + productOfferResponse.getMsg(), Toast.LENGTH_LONG).show();
                            getDialog().dismiss();
                        } else if (productOfferResponse.getStatus().equalsIgnoreCase("0")) {
                            Toast.makeText(con, "Product " + productOfferResponse.getMsg(), Toast.LENGTH_LONG).show();

                        }

                    }
                });

            }

        });

    }

}
