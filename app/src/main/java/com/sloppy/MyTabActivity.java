package com.sloppy;

import android.app.Activity;
import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sloppy.aapbdLib.AlertMessage;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.NetInfo;
import com.sloppy.aapbdLib.PersistData;
import com.sloppy.aapbdLib.PersistentUser;
import com.sloppy.model.BundleListResponse;
import com.sloppy.model.NotificationCountResponse;
import com.sloppy.utils.AAPBDHttpClient;
import com.sloppy.utils.AllURL;
import com.sloppy.utils.AppConstant;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;

/*
 * Tab activity class
 */

public class MyTabActivity extends TabActivity {
    public static boolean sLoggedIn = false;
    private boolean mBounded;
    private Context con;
    private Activity nActivity;
    private TextView notificationCount;
    private static final String TAG = "MainTabActivity";
    public NotificationCountResponse  myResponse;
    static MyTabActivity mMyTabActivity;
    //------------- return mTabHost;--------------
    public static MyTabActivity getMyTabActivity() {
        return mMyTabActivity;
    }

    /**
     *--------------------On Create-----------------------------------------
     */
    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.tab);
        nActivity = this;
        mMyTabActivity = this;
        con = this;
        notificationCount=(TextView)findViewById(R.id.notificationCount);
        setTabs(0);
        doBindService();

        final Handler handler = new Handler();
        Timer timer = new Timer();
        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        getNotification();
                    }
                });
            }
        };
        timer.schedule(doAsynchronousTask, 0, 30000);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        doUnbindService();
    }

    // Method doBindService

    void doBindService() {
        try {
            // bindService(new Intent(this, MyService.class), mConnection,
            // Context.BIND_AUTO_CREATE);
        } catch (Exception e) {
            Log.e("Error ", "At binding ");
        }
    }

    // Method doUnbindService

    void doUnbindService() {
        // if (mBounded) {

        try {
            // unbindService(mConnection);
        } catch (Exception e) {

        }
    }

    protected void getNotification() {

  /*
   * check internet first
   */
        if (!NetInfo.isOnline(con)) {
//            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }
        Executors.newSingleThreadExecutor().submit(new Runnable() {
            String msg = "";
            String response = "";

            @Override
            public void run() {
                try {
                    response = AAPBDHttpClient.get(AllURL.getNotificationCount()).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();
                    Log.e("count response", response);
                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //=======Persist Response Using Gson========================================
                        Gson gson = new Gson();
                        myResponse = gson.fromJson(response, NotificationCountResponse.class);
                        if (myResponse!=null){
                            if (myResponse.getStatus()==1) {
                                if (myResponse.getNotifications().equalsIgnoreCase("0")){
                                    notificationCount.setVisibility(View.GONE);
                                }else {
                                    notificationCount.setVisibility(View.VISIBLE);
                                    notificationCount.setText(myResponse.getNotifications());
                                }

                            } else {
                                msg = myResponse.getMsg();
//                                AlertMessage.showMessage(con, "Sloppy Alert", msg);
                            }
                        }
                    }
                });

            }

        });

    }


    private void setTabs(final int activeTab) {

        Intent intent;

        // MyView is a inheritance class which inherit LinearLayout
        MyView view = null;
        TabHost.TabSpec spec;
        /*
		 * 
		 *---------------- first tab---------------------
		 */
        //---- using Intent class we select the activity class to use the tab-----
        intent = new Intent().setClass(this, FeedActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // using MyView we add image and image label
        view = new MyView(this, R.drawable.iconss_03, R.drawable.icon_03);

        // we use TabHost.TabSpec as container to add Intent and view
        spec = getTabHost().newTabSpec("").setIndicator(view).setContent(intent);
        // we use getTabHost().addTab to add one tab element
        getTabHost().addTab(spec);
/*
        Intent intentAndroid = new Intent().setClass(this, AndroidActivity.class);
        TabSpec tabSpecAndroid = tabHost
                .newTabSpec("Android")
                .setIndicator("", ressources.getDrawable(R.drawable.icon_android_config))
                .setContent(intentAndroid);*/

		/*
		 * 
		 *---------------------- Second tab-----------------------
		 */
        intent = new Intent().setClass(this, SearchActivity.class).addFlags(
                Intent.FLAG_ACTIVITY_CLEAR_TOP);
        view = new MyView(this, R.drawable.iconss_05, R.drawable.icon_05);
        view.setFocusable(true);
        getTabHost().addTab(
                getTabHost().newTabSpec("")
                        .setIndicator(view).setContent(intent));
		/*
		 * 
		 *------------------- third tab--------------------
		 */
        /*if (PersistentUser.isLogged(con)){

        }else{
            AppConstant.loginDialoag(con);
        }*/
        intent = new Intent().setClass(this, SellActivity.class).addFlags(
                Intent.FLAG_ACTIVITY_CLEAR_TOP);
        view = new MyView(this, R.drawable.iconss_07, R.drawable.icon_07);
        view.setFocusable(true);
        getTabHost().addTab(getTabHost().newTabSpec("").setIndicator(view).setContent(intent));

		/*
		 *------------------------ Fourth Tab--------------------
		 */

     /*   if (PersistentUser.isLogged(con)){

        }else{
            AppConstant.loginDialoag(con);
        }*/
        intent = new Intent().setClass(this, ActivityActivity.class).addFlags(
                Intent.FLAG_ACTIVITY_CLEAR_TOP);

        view = new MyView(this, R.drawable.iconss_09, R.drawable.icon_09);
        view.setFocusable(true);
        getTabHost().addTab(getTabHost().newTabSpec("").setIndicator(view).setContent(intent));

		/*
		 *---------------------- five Tab---------------------------
		 */

        intent = new Intent().setClass(this, ProfileActivity.class).addFlags(
                Intent.FLAG_ACTIVITY_CLEAR_TOP);

        view = new MyView(this, R.drawable.iconss_11, R.drawable.icon_11);
        view.setFocusable(true);
        getTabHost().addTab(getTabHost().newTabSpec("").setIndicator(view).setContent(intent));

        getTabHost().getTabWidget().getChildAt(0).getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
        getTabHost().getTabWidget().getChildAt(1).getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
        getTabHost().getTabWidget().getChildAt(2).getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
        getTabHost().getTabWidget().getChildAt(3).getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
        getTabHost().getTabWidget().getChildAt(4).getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
        getTabHost().setCurrentTab(activeTab);
    }

    private class MyView extends LinearLayout {
        ImageView iv;

        public MyView(final Context c, final int drawable,
                      final int drawableselec) {
            super(c);
            iv = new ImageView(c);

            final StateListDrawable listDrawable = new StateListDrawable();

            listDrawable.addState(SELECTED_STATE_SET, getResources()
                    .getDrawable(drawableselec));
            listDrawable.addState(ENABLED_STATE_SET, getResources()
                    .getDrawable(drawable));
            iv.setImageDrawable(listDrawable);
            iv.setBackgroundColor(Color.parseColor("#FFFFFF"));

            iv.setPadding(0, 10, 0, 10);
            iv.setLayoutParams(new LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    (float) 1.0));

            addView(iv);

            setBackgroundDrawable(getResources().getDrawable(
                    R.color.transparent));
        }
    }

}