package com.sloppy;


import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.sloppy.utils.AppConstant;

/**
 * Created by hp on 7/13/2016.
 */
public class MenuFragmentDialog extends DialogFragment {
    Context con;
    private View view;
    private TextView tvSloppy2nd, tvTermsOf, tvDoneMenu, tvFrequintly, tvContact,tvPrivacyPo;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialog_menu, container, false);
        con = getActivity();
        initUi();
        return view;
    }

    private void initUi() {
        //======================Initialization======================================================
        tvSloppy2nd = (TextView) view.findViewById(R.id.tvSloppy2nd);
        tvDoneMenu = (TextView) view.findViewById(R.id.tvDoneMenu);
        tvTermsOf = (TextView) view.findViewById(R.id.tvTermsOf);
        tvPrivacyPo=(TextView) view.findViewById(R.id.tvPrivacyPo);
        tvFrequintly = (TextView) view.findViewById(R.id.tvFrequintly);
        tvContact = (TextView) view.findViewById(R.id.tvContact);
        //=========Set Font====================================================
        fontSet();
        tvDoneMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });
        tvTermsOf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.main="";
                AppConstant.menuType="term";
                TermsOfServiceDialogFragment dialogTerm= new TermsOfServiceDialogFragment();
                dialogTerm.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar);
                dialogTerm.show(getActivity().getFragmentManager(),"");
            }
        });

        tvPrivacyPo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.main="";
                AppConstant.menuType="privacy";
                TermsOfServiceDialogFragment dialogTerm= new TermsOfServiceDialogFragment();
                dialogTerm.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar);
                dialogTerm.show(getActivity().getFragmentManager(),"");

            }
        });
        tvFrequintly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.main="";
                AppConstant.menuType="faq";
                TermsOfServiceDialogFragment dialogTerm= new TermsOfServiceDialogFragment();
                dialogTerm.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar);
                dialogTerm.show(getActivity().getFragmentManager(),"");

            }
        });

        tvContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"hello@sloppy2ndz.com"});
                i.putExtra(Intent.EXTRA_SUBJECT, "Sloppy 2ndz query");
//                i.putExtra(Intent.EXTRA_TEXT   , "I have a question!");
                try {
                    startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(con, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void fontSet() {
        Typeface helveticaNeuRegular = Typeface.createFromAsset(con.getAssets(), "font/helveticaNeuRegular.ttf");
        Typeface helveticaNeueBold = Typeface.createFromAsset(con.getAssets(), "font/helveticaNeueBold.ttf");
        tvSloppy2nd.setTypeface(helveticaNeuRegular);
        tvDoneMenu.setTypeface(helveticaNeueBold);
        tvTermsOf.setTypeface(helveticaNeuRegular);
        tvPrivacyPo.setTypeface(helveticaNeuRegular);
        tvFrequintly.setTypeface(helveticaNeuRegular);
        tvContact.setTypeface(helveticaNeuRegular);
    }
}
