package com.sloppy;


import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.sloppy.aapbdLib.AlertMessage;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.NetInfo;
import com.sloppy.aapbdLib.PersistData;
import com.sloppy.model.AddToBundleResponse;
import com.sloppy.model.BundleListResponse;
import com.sloppy.model.ProductAndUserDetailsInfo;
import com.sloppy.model.ResultList;
import com.sloppy.utils.AAPBDHttpClient;
import com.sloppy.utils.AllURL;
import com.sloppy.utils.AppConstant;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.concurrent.Executors;


/**
 * Created by hp on 4/17/2016.
 */
public class MyBundleFragment extends BaseFragment {
    Context con;
    View view;
    private ListView lVBundle;
    private BundleListResponse myResponse;
    private AddToBundleResponse bundleResponse;
    private MyArrayListAdapter myArrayListAdapter;
    private int myBundlePosition;


    /**
     * ========================On Create================================
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        con = getActivity();
        view = inflater.inflate(R.layout.fragment_my_bundle, container, false);
        initUI();
        return view;
    }

    private void initUI() {
        /**
         * =======================Call 18.1 Bundle List=============================================
         */
                callBundleListAPI();

        /**
         * ========================Initialization===================================================
         */
        ImageView imgMyBundleBack = (ImageView) view.findViewById(R.id.imgMyBundleBack);
        ImageView imgMenuBundle = (ImageView) view.findViewById(R.id.imgMenuBundle);
        lVBundle = (ListView) view.findViewById(R.id.lVBundle);
        imgMenuBundle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.showMenuDiadog(getActivity().getFragmentManager());
            }
        });

        /**
         * ==================On Click===============================================================
         */
        imgMyBundleBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
    }

    /*
    *   -------------------18.1 Bundle List-------------------------------
     */
    protected void callBundleListAPI() {

  /*
   * check internet first
   */
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }
  /*
   * start progress bar
   */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
  /*
   * start Thread
   *
   *
   */
        Executors.newSingleThreadExecutor().submit(new Runnable() {
            String msg = "";
            String response = "";

            @Override
            public void run() {
                try {
                    Log.e("BundleListURL", AllURL.getBundleList());
                    response = AAPBDHttpClient.get(AllURL.getBundleList()).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();
                    Log.e("BundleListResponse", response);
                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        /*
                         * ================Stop progress bar========================================
                         */
                        if (busy != null) {
                            busy.dismis();
                        }

                        //=======Persist Response Using Gson========================================
                        Gson gson = new Gson();
                        myResponse = gson.fromJson(response, BundleListResponse.class);

                        /**
                         * =======================Ui Related Task===================================
                         */
                        if (myResponse.getStatus().equalsIgnoreCase("1")) {
                            if (myResponse.getResult().size() > 0) {
                                //============Adapter Set===========================================
                                myArrayListAdapter = new MyArrayListAdapter(con, myResponse.getResult());
                                lVBundle.setAdapter(myArrayListAdapter);
                                myArrayListAdapter.notifyDataSetChanged();
                            } else {
                                TextView tvDataNotFound = (TextView) view.findViewById(R.id.tvDataNotFound);
                                tvDataNotFound.setVisibility(View.VISIBLE);
                            }

                        } else {
                            msg = myResponse.getMsg();
                            AppConstant.alertDialoag(con, getString(R.string.alert), msg, getString(R.string.ok));
                        }

                    }
                });

            }

        });

    }

    private class MyArrayListAdapter extends ArrayAdapter<ResultList> {
        Context con;
        ImageView  imgUserBundleList,imgProduct1,imgProduct2,imgProduct3,imgProduct4,imgProduct5;
        TextView tvUserNameBundle;
//        HorizontalListView  lvBundleProList;
        MyArrayListAdapter(Context context, List<ResultList> list) {
            super(context, R.layout.row_bundle, list);
            this.con = context;
        }

        @Override
        public View getView(final int position, View v, ViewGroup viewGroup) {

            if (v == null) {
                final LayoutInflater inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = inflater.inflate(R.layout.row_bundle_new, null);
            }
            /**
             * ---------------------Initialization of bundle row field------------------------------
             */
            imgUserBundleList = (ImageView) v.findViewById(R.id.imgUserBundleList);
            imgProduct1 = (ImageView) v.findViewById(R.id.imgProduct1);
            imgProduct2 = (ImageView) v.findViewById(R.id.imgProduct2);
            imgProduct3 = (ImageView) v.findViewById(R.id.imgProduct3);
            imgProduct4 = (ImageView) v.findViewById(R.id.imgProduct4);
            imgProduct5 = (ImageView) v.findViewById(R.id.imgProduct5);

            tvUserNameBundle = (TextView) v.findViewById(R.id.tvUserNameBundle);
//            lvBundleProList=(HorizontalListView ) v.findViewById(R.id.lvBundleProList);
            /**
             * ==========================Set data on the field of Bundle row====================
             */

            //==== Quary To get data from vector/Arrraylist according to there position============
            final ResultList query = myResponse.getResult().get(position);
            if (query.getProducts().size()==1){
                imgProduct1.setVisibility(View.VISIBLE);
                imgProduct2.setVisibility(View.GONE);
                imgProduct3.setVisibility(View.GONE);
                imgProduct4.setVisibility(View.GONE);
                imgProduct5.setVisibility(View.GONE);
                Picasso.with(con).load(query.getProducts().get(0).getPhoto_one())
                        .placeholder(R.drawable.product_bg).error(R.drawable.product_bg).into(imgProduct1);
            } else  if (query.getProducts().size()==2){
                imgProduct1.setVisibility(View.VISIBLE);
                imgProduct2.setVisibility(View.VISIBLE);
                imgProduct3.setVisibility(View.GONE);
                imgProduct4.setVisibility(View.GONE);
                imgProduct5.setVisibility(View.GONE);
                Picasso.with(con).load(query.getProducts().get(0).getPhoto_one())
                        .placeholder(R.drawable.product_bg).error(R.drawable.product_bg).into(imgProduct1);
                Picasso.with(con).load(query.getProducts().get(1).getPhoto_one())
                        .placeholder(R.drawable.product_bg).error(R.drawable.product_bg).into(imgProduct2);
            } else if (query.getProducts().size()==3){
                imgProduct1.setVisibility(View.VISIBLE);
                imgProduct2.setVisibility(View.VISIBLE);
                imgProduct3.setVisibility(View.VISIBLE);
                imgProduct4.setVisibility(View.GONE);
                imgProduct5.setVisibility(View.GONE);
                Picasso.with(con).load(query.getProducts().get(0).getPhoto_one())
                        .placeholder(R.drawable.product_bg).error(R.drawable.product_bg).into(imgProduct1);
                Picasso.with(con).load(query.getProducts().get(1).getPhoto_one())
                        .placeholder(R.drawable.product_bg).error(R.drawable.product_bg).into(imgProduct2);
                Picasso.with(con).load(query.getProducts().get(2).getPhoto_one())
                        .placeholder(R.drawable.product_bg).error(R.drawable.product_bg).into(imgProduct3);
            } else if (query.getProducts().size()==4){
                imgProduct1.setVisibility(View.VISIBLE);
                imgProduct2.setVisibility(View.VISIBLE);
                imgProduct3.setVisibility(View.VISIBLE);
                imgProduct4.setVisibility(View.VISIBLE);
                imgProduct5.setVisibility(View.GONE);
                Picasso.with(con).load(query.getProducts().get(0).getPhoto_one())
                        .placeholder(R.drawable.product_bg).error(R.drawable.product_bg).into(imgProduct1);
                Picasso.with(con).load(query.getProducts().get(1).getPhoto_one())
                        .placeholder(R.drawable.product_bg).error(R.drawable.product_bg).into(imgProduct2);
                Picasso.with(con).load(query.getProducts().get(2).getPhoto_one())
                        .placeholder(R.drawable.product_bg).error(R.drawable.product_bg).into(imgProduct3);
                Picasso.with(con).load(query.getProducts().get(3).getPhoto_one())
                        .placeholder(R.drawable.product_bg).error(R.drawable.product_bg).into(imgProduct4);
            } else if (query.getProducts().size()==5){
                imgProduct1.setVisibility(View.VISIBLE);
                imgProduct2.setVisibility(View.VISIBLE);
                imgProduct3.setVisibility(View.VISIBLE);
                imgProduct4.setVisibility(View.VISIBLE);
                imgProduct5.setVisibility(View.VISIBLE);
                Picasso.with(con).load(query.getProducts().get(0).getPhoto_one())
                        .placeholder(R.drawable.product_bg).error(R.drawable.product_bg).into(imgProduct1);
                Picasso.with(con).load(query.getProducts().get(1).getPhoto_one())
                        .placeholder(R.drawable.product_bg).error(R.drawable.product_bg).into(imgProduct2);
                Picasso.with(con).load(query.getProducts().get(2).getPhoto_one())
                        .placeholder(R.drawable.product_bg).error(R.drawable.product_bg).into(imgProduct3);
                Picasso.with(con).load(query.getProducts().get(3).getPhoto_one())
                        .placeholder(R.drawable.product_bg).error(R.drawable.product_bg).into(imgProduct4);
                Picasso.with(con).load(query.getProducts().get(4).getPhoto_one())
                        .placeholder(R.drawable.product_bg).error(R.drawable.product_bg).into(imgProduct5);
            }

            imgProduct1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppConstant.productIDfeedRow = query.getProducts().get(0).getId();
                    AppConstant.feedRowObject = query.getProducts().get(0);
                    myCommunicator.setContentFragment(new SingleProductFragment(), true);
                }
            });

            imgUserBundleList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AppConstant.otherUserId= query.getId();
                    myCommunicator.setContentFragment(new OtherUserDetailsFragment(), true);
                }
            });

            tvUserNameBundle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AppConstant.otherUserId= query.getId();
                    myCommunicator.setContentFragment(new OtherUserDetailsFragment(), true);
                }
            });


            if (position < myResponse.getResult().size()) {

                try {

                    Picasso.with(con).load(query.getProfile_image())
                            .placeholder(R.drawable.person).error(R.drawable.person).into(imgUserBundleList);
                    tvUserNameBundle.setText(query.getUsername());
//                    MyBundleImageListAdapter  myBundleImageListAdapter = new MyBundleImageListAdapter(con,query.getProducts());
//                    lvBundleProList.setAdapter(myBundleImageListAdapter);
//                    myBundleImageListAdapter.notifyDataSetChanged();
                    v.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final ResultList query = myResponse.getResult().get(position);
                            AppConstant.productIDfeedRow=query.getProducts().get(0).getId();
                            AppConstant.otherUserId=query.getProducts().get(0).getUser_id();
                            AddToBundleFragment fragment = new AddToBundleFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("titleBundle", query.getUsername());
                            fragment.setArguments(bundle);
                            myCommunicator.setContentFragment(fragment, true);
//                            AppConstant.productList=query.getProducts();
//                            myCommunicator.setContentFragment(new BundleListFragment(), true);
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }



                /**
                 * ========================On click=================================================
                 */
//                tvBrandNameBund.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        AppConstant.brandID=query.getProduct_info().getBrand_info().get(0).getId();
//                        AppConstant.brandName=query.getProduct_info().getBrand_info().get(0).getName();
//                        Log.e("BrandID",AppConstant.brandID);
//                        Log.e("BrandName",AppConstant.brandName);
//                        myCommunicator.addContentFragment(new BrandFragment(), true);
//
//                    }
//                });
//                lLCancel.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        myBundlePosition = position;
//                        bundleRemoveDialoag(con, query.getId(), query.getSeller_id());
//                    }
//                });
//
//                v.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        AppConstant.productIDfeedRow = query.getProduct_info().getId();
//                        AppConstant.feedRowObject = query.getProduct_info();
//                        myCommunicator.addContentFragment(new SingleProductFragment(), true);
//                    }
//                });


            }
            return v;

        }

    }

    private class MyBundleImageListAdapter extends ArrayAdapter<ProductAndUserDetailsInfo> {
        Context con;
        ImageView  ivBundlePro;
        List<ProductAndUserDetailsInfo> imageList;
        MyBundleImageListAdapter(Context context, List<ProductAndUserDetailsInfo> list) {
            super(context, R.layout.row_bundle, list);
            this.con = context;
            imageList= list;
        }

        @Override
        public View getView(final int position, View v, ViewGroup viewGroup) {

            if (v == null) {
                final LayoutInflater inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = inflater.inflate(R.layout.row_product_image, null);
            }
            /**
             * ---------------------Initialization of bundle row field------------------------------
             */
            ivBundlePro = (ImageView) v.findViewById(R.id.ivBundlePro);

            /**
             * ==========================Set data on the field of Bundle row====================
             */

            //==== Quary To get data from vector/Arrraylist according to there position============

            if (position < imageList.size()) {

                try {

                    Picasso.with(con).load(imageList.get(position).getPhoto_one())
                            .placeholder(R.drawable.person).error(R.drawable.person).into(ivBundlePro);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                /**
                 * ========================On click=================================================
                 */
//                tvBrandNameBund.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        AppConstant.brandID=query.getProduct_info().getBrand_info().get(0).getId();
//                        AppConstant.brandName=query.getProduct_info().getBrand_info().get(0).getName();
//                        Log.e("BrandID",AppConstant.brandID);
//                        Log.e("BrandName",AppConstant.brandName);
//                        myCommunicator.addContentFragment(new BrandFragment(), true);
//
//                    }
//                });
//                lLCancel.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        myBundlePosition = position;
//                        bundleRemoveDialoag(con, query.getId(), query.getSeller_id());
//                    }
//                });
//
//                v.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        AppConstant.productIDfeedRow = query.getProduct_info().getId();
//                        AppConstant.feedRowObject = query.getProduct_info();
//                        myCommunicator.addContentFragment(new SingleProductFragment(), true);
//                    }
//                });


            }
            return v;

        }

    }

    /**
     * ====================Bundle Remove dialog===========================================
     */
    public void bundleRemoveDialoag(final Context con, final String bundleID, final String sellerID) {

        final Dialog bundleRemovedialog = new Dialog(con);
        bundleRemovedialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        bundleRemovedialog.setContentView(R.layout.dialog_bundle_remov);

        bundleRemovedialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        TextView tvbundleCancel = (TextView) bundleRemovedialog
                .findViewById(R.id.tvbundleCancel);
        TextView tvBundleOk = (TextView) bundleRemovedialog.findViewById(R.id.tvBundleOk);

        tvBundleOk.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                bundleRemovedialog.dismiss();
            }
        });

        tvbundleCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBundleRemoveAPI(bundleID, sellerID);
                bundleRemovedialog.dismiss();
            }
        });

        bundleRemovedialog.show();
    }

    /**
     * -------------------18.2 Bundle remove API------------------
     */
    private void callBundleRemoveAPI(final String bundle_id, final String seller_id) {
      /*
       * ---------------check internet first------------
       */
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }
         /*
          * ----------------Show Busy Dialog -----------------------------------------
          */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
      /*
       *----------------------Start Thread-------------------------------------------
       */
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            String msg = "";
            String response = "";

            @Override
            public void run() {
                // You can performed your task here.

                try {
                    Log.e("AddBundle URL", AllURL.getBundleRemove(bundle_id, seller_id));
                    //-------------Hit Server---------------------
                    response = AAPBDHttpClient.get(AllURL.getBundleRemove(bundle_id, seller_id)).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();

                    //----------------Persist response with the help of Gson----------------
                    Log.e("AddBundle Response", ">>" + response);
                    Gson gson = new Gson();
                    bundleResponse = gson.fromJson(response, AddToBundleResponse.class);

                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //-------Stop Busy Dialog-----
                        if (busy != null) {
                            busy.dismis();
                        }

                        /**
                         * ---------main Ui related work--------------
                         */
                        if (bundleResponse.getStatus().equalsIgnoreCase("1")) {
//                            Toast.makeText(con, "Bundle Remove " + bundleResponse.getMsg(), Toast.LENGTH_LONG).show();

                            Log.e("bundleRowPosition", String.valueOf(myBundlePosition));
                            myResponse.getResult().remove(myBundlePosition);
                            myArrayListAdapter.notifyDataSetChanged();
                        } else {
                            msg = bundleResponse.getMsg();
                            AlertMessage.showMessage(con, "Bunlde", msg);
                        }

                    }
                });

            }

        });

    }

}
