package com.sloppy;

import android.*;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.paypal.android.MEP.CheckoutButton;
import com.paypal.android.MEP.PayPal;
import com.paypal.android.MEP.PayPalActivity;
import com.paypal.android.MEP.PayPalAdvancedPayment;
import com.paypal.android.MEP.PayPalReceiverDetails;
import com.sloppy.aapbdLib.AlertMessage;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.NetInfo;
import com.sloppy.aapbdLib.PersistData;
import com.sloppy.aapbdLib.PersistentUser;
import com.sloppy.model.AddToBundleResponse;
import com.sloppy.model.BundleResultInfo;
import com.sloppy.model.BuyNowResponse;
import com.sloppy.model.FeedRespons;
import com.sloppy.model.LikeUnlikeResponse;
import com.sloppy.model.ProductAndUserDetailsInfo;
import com.sloppy.model.SellerInfo;
import com.sloppy.utils.AAPBDHttpClient;
import com.sloppy.utils.AllURL;
import com.sloppy.utils.AppConstant;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.Executors;

/**
 * Created by hp on 5/5/2016.
 */
public class AddToBundleFragment extends BaseFragment {

    private Context con;
    private View view;
    float totalAmount = 0;
    float netAmount = 0;
    float totaldeliveryCharge=0;
    List<BundleResultInfo> productList;
    private String pictureUrl="";
    float discount = 0;
    private TextView tvBundlefrom, tvCountItem, tvProNameBundle, tvBrandNameBund, tvAddMore, tvViewAll,
            tvOriginalPriceBund, tvlistingPriceBdl, tvTotalAmount, tvDiscountRate, tvNetTotal, tvTotat, tvSellerDis, tvByeBundle;
    private ImageView imgBackBundle, imgBundle, imgLoveBundle;
    private ListView lVBundle;
    private ProductAndUserDetailsInfo productInfo;
    private AddToBundleResponse bundleResponse;
    AddBundleAdapter addBundleAdapter;
    private Typeface helveticaNeuRegular, helveticaNeueBold;
    private LinearLayout lLBuyBundle;
    private String payKey="";
    private float totalPriceBuy=0;
    private String productId="";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        con = getActivity();
        view = inflater.inflate(R.layout.bundle, container, false);
        initUI();

        return view;
    }

    private void initUI() {

        //------------Initialization-----------------
        //====================Text View============================
        lLBuyBundle=(LinearLayout)view.findViewById(R.id.lLBuyBundle);
        tvBundlefrom = (TextView) view.findViewById(R.id.tvBundlefrom);
        tvCountItem = (TextView) view.findViewById(R.id.tvCountItem);
        tvTotalAmount = (TextView) view.findViewById(R.id.tvTotalAmount);
        tvDiscountRate = (TextView) view.findViewById(R.id.tvDiscountRate);
        tvNetTotal = (TextView) view.findViewById(R.id.tvNetTotal);
        tvAddMore = (TextView) view.findViewById(R.id.tvAddMore);
        tvViewAll = (TextView) view.findViewById(R.id.tvViewAll);
        tvTotat = (TextView) view.findViewById(R.id.tvTotat);
        tvSellerDis = (TextView) view.findViewById(R.id.tvSellerDis);
        tvByeBundle = (TextView) view.findViewById(R.id.tvByeBundle);

        //=============Image View===========================
        ImageView imgMenuAdd = (ImageView) view.findViewById(R.id.imgMenuAdd);
        imgBackBundle = (ImageView) view.findViewById(R.id.imgBackBundle);
        //============List view===========================
        lVBundle = (ListView) view.findViewById(R.id.lVBundle);
        //-------------------------------Font set---------------------------------------------------
        helveticaNeuRegular = Typeface.createFromAsset(getActivity().getAssets(), "font/helveticaNeuRegular.ttf");
        helveticaNeueBold = Typeface.createFromAsset(getActivity().getAssets(), "font/helveticaNeueBold.ttf");
        tvTotat.setTypeface(helveticaNeueBold);
        tvNetTotal.setTypeface(helveticaNeueBold);
        tvBundlefrom.setTypeface(helveticaNeuRegular);

        tvAddMore.setTypeface(helveticaNeuRegular);
        tvViewAll.setTypeface(helveticaNeuRegular);
        tvCountItem.setTypeface(helveticaNeuRegular);
        tvSellerDis.setTypeface(helveticaNeuRegular);
        tvTotalAmount.setTypeface(helveticaNeuRegular);
        tvDiscountRate.setTypeface(helveticaNeuRegular);
        tvByeBundle.setTypeface(helveticaNeuRegular);

        tvBundlefrom.setText("Bundle from " + getArguments().getString("titleBundle"));
        tvAddMore.setText("Add to bundle from " +  getArguments().getString("titleBundle"));

        tvAddMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myCommunicator.setContentFragment(new OtherUserDetailsFragment(), true);
            }
        });

        tvViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myCommunicator.setContentFragment(new MyBundleFragment(), true);
            }
        });

        imgMenuAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.showMenuDiadog(getActivity().getFragmentManager());
            }
        });

        PayPal ppObj = PayPal.initWithAppID(getActivity(), "APP-80W284485P519543T", PayPal.ENV_SANDBOX);
        ppObj.setShippingEnabled(true);
        CheckoutButton launchPayPalButton = ppObj.getCheckoutButton(getActivity(), PayPal.BUTTON_194x37, CheckoutButton.TEXT_PAY);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.CENTER_HORIZONTAL);
        launchPayPalButton.setLayoutParams(params);
        launchPayPalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float totalPriceBuy=totalAmount+totaldeliveryCharge;
                float appownerAmonut=(totalPriceBuy*15)/100;
                Log.e("appownerAmonut",""+appownerAmonut);
                float sellerAmount=totalPriceBuy-appownerAmonut;
                PayPalReceiverDetails receiver1=new PayPalReceiverDetails();
                BigDecimal obj_0 = new BigDecimal(appownerAmonut);
                receiver1.setSubtotal(obj_0);
                receiver1.setRecipient("diudeveloper-buyer@gmail.com");
                receiver1.setMerchantName(getString(R.string.app_name));
                PayPalReceiverDetails receiver2=new PayPalReceiverDetails();
                BigDecimal obj_01 = new BigDecimal(sellerAmount);
                receiver2.setSubtotal(obj_01);
                receiver2.setRecipient("diudeveloper-facilitator@gmail.com");
                receiver2.setMerchantName(getString(R.string.app_name));

                PayPalAdvancedPayment advPayment = new PayPalAdvancedPayment();
                advPayment.setCurrencyType("USD");
                advPayment.getReceivers().add(receiver1);
                advPayment.getReceivers().add(receiver2);
                Intent paypalIntent = PayPal.getInstance().checkout(advPayment, getActivity());
                startActivityForResult(paypalIntent, 1);
            }
        });
        lLBuyBundle.addView(launchPayPalButton);

        /**
         * -------------------Call 18. Add To Bundle  API------------------
         */
        callAddtoBundleAPI(AppConstant.productIDfeedRow);

    }

    /**
     * -------------------18. Add To Bundle  API------------------
     */
    private void callAddtoBundleAPI(final String product_id) {
  /*
   * ---------------check internet first------------
   */
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }

         /*
          * ----------------Show Busy Dialog -----------------------------------------
          */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
  /*
   *
   *----------------------Start Thread-------------------------------------------
   *
   */
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            String msg = "";
            String response = "";

            @Override
            public void run() {
                // You can performed your task here.

                try {
                    Log.w("AddBundle URL", AllURL.getAddBundle(product_id));
                    //-------------Hit Server---------------------
                    response = AAPBDHttpClient.get(AllURL.getAddBundle(product_id)).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();

                    //----------------Persist response with the help of Gson----------------
                    Log.w("AddBundle Response", ">>" + response);
                    Gson gson = new Gson();
                    bundleResponse = gson.fromJson(response, AddToBundleResponse.class);
                    Log.e("Product List Size", Integer.toString(bundleResponse.getResult().size()));
                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //-------Stop Busy Dialog-----
                        if (busy != null) {
                            busy.dismis();
                        }
                        /**
                         * ---------main Ui related work--------------
                         */
                        if (bundleResponse.getStatus().equalsIgnoreCase("1")) {
                            //=============amount set=====================
                            amountSet(bundleResponse);

                            /**
                             * ---------------Call Bundle Adapter--------------------
                             */
                            addBundleAdapter = new AddBundleAdapter(con, bundleResponse.getResult());
                            lVBundle.setAdapter(addBundleAdapter);
                            addBundleAdapter.notifyDataSetChanged();

                            imgBackBundle.setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    getActivity().onBackPressed();
                                }
                            });

                        } else {
                            msg = bundleResponse.getMsg();
                            AppConstant.alertDialoag(con,"Alert",msg,"OK");
                        }
                    }
                });
            }
        });
    }

    private void amountSet(AddToBundleResponse bundleResponse) {
        totalAmount = 0;
        discount = 0;
        productId="";
        for (int i = 0; i < bundleResponse.getResult().size(); i++) {
            if (i==0){
                productId=bundleResponse.getResult().get(i).getId()+",";
            }else{
                productId=productId+bundleResponse.getResult().get(i).getId()+",";
            }
            productId=removeLastChanacter(productId,',');
            if (!TextUtils.isEmpty(bundleResponse.getResult().get(i).getProduct_info().getDelivery_charge())){
                totaldeliveryCharge=totaldeliveryCharge+Float.parseFloat(bundleResponse.getResult().get(i).getProduct_info().getDelivery_charge());
            }
            totalAmount = totalAmount + Float.parseFloat(bundleResponse.getResult().get(i).getProduct_info().getListing_price());
            discount = discount + (Float.parseFloat(bundleResponse.getResult().get(i).getProduct_info().getListing_price()) *
                    (Float.parseFloat(bundleResponse.getResult().get(i).getProduct_info().getSeller_info().get(0).getDiscount_percentage()) / 100));
        }

        if (bundleResponse.getResult().size() > 0) {
            SellerInfo sellerInfo = new SellerInfo();
            sellerInfo = bundleResponse.getResult().get(0).getProduct_info().getSeller_info().get(0);

            String totalA = String.format("%.02f", Float.parseFloat(String.valueOf(totalAmount)));
            String discountA = String.format("%.02f", Float.parseFloat(String.valueOf(discount)));
            String net = String.format("%.02f", Float.parseFloat(String.valueOf(totalAmount - discount)));
            tvTotalAmount.setText(AppConstant.getCurrencySymbol(sellerInfo.getCurrency()) + totalA);
            tvDiscountRate.setText(AppConstant.getCurrencySymbol(sellerInfo.getCurrency()) + discountA);
            tvNetTotal.setText(AppConstant.getCurrencySymbol(sellerInfo.getCurrency()) + net);
        } else {
            tvTotalAmount.setText("0.00");
            tvDiscountRate.setText("0.00");
            tvNetTotal.setText("0.00");
        }
        if (bundleResponse.getResult().size() == 1) {
            tvCountItem.setText(String.valueOf(bundleResponse.getResult().size()) + " Item");
        } else {
            tvCountItem.setText(String.valueOf(bundleResponse.getResult().size()) + " Items");
        }

    }

    public String removeLastChanacter(String str,Character characer) {
        if (str != null && str.length() > 0 && str.charAt(str.length()-1)==characer) {
            str = str.substring(0, str.length()-1);
        }
        return str;
    }

    /**
     * -----------------------Add to bundle List Adapter-----------------------
     */
    private class AddBundleAdapter extends ArrayAdapter<BundleResultInfo> {
        Context context;
        ImageView imgComentbdl;


        AddBundleAdapter(Context context, List<BundleResultInfo> temp) {
            super(context, R.layout.row_bundle, temp);
            this.context = context;
            productList = temp;
        }

        @Override
        public View getView(final int position, View bundleView, ViewGroup parent) {
            /**
             * ------------------Set the row layout-----------------------
             */
            if (bundleView == null) {
                final LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                bundleView = vi.inflate(R.layout.row_bundle, null);
            }

            if (position < productList.size()) {
                 productInfo = productList.get(position).getProduct_info();
                /**
                 * ---------------------Initialization of bundle row field------------------------------
                 */
                imgBundle = (ImageView) bundleView.findViewById(R.id.imgBundle);
                imgComentbdl = (ImageView) bundleView.findViewById(R.id.imgComentbdl);
                imgLoveBundle = (ImageView) bundleView.findViewById(R.id.imgLoveBundle);
                LinearLayout lLCancel = (LinearLayout) bundleView.findViewById(R.id.lLCancel);
                tvProNameBundle = (TextView) bundleView.findViewById(R.id.tvProNameBundle);
                tvBrandNameBund = (TextView) bundleView.findViewById(R.id.tvBrandNameBund);
                tvOriginalPriceBund = (TextView) bundleView.findViewById(R.id.tvOriginalPriceBund);
                tvlistingPriceBdl = (TextView) bundleView.findViewById(R.id.tvlistingPriceBdl);
                TextView tvLikeCountBdl = (TextView) bundleView.findViewById(R.id.tvLikeCountBdl);
                TextView imgComentCountBdl = (TextView) bundleView.findViewById(R.id.imgComentCountBdl);
                ImageView imgShare1 = (ImageView) bundleView.findViewById(R.id.imgShare1);
                imgShare1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (PersistentUser.isLogged(context)) {
                            pictureUrl=productList.get(position).getProduct_info().getPhoto_one();
                            if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                    != PackageManager.PERMISSION_GRANTED) {
                                ActivityCompat.requestPermissions(getActivity(),new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 11);
                            }else{
                                Picasso.with(con).load(pictureUrl).memoryPolicy(MemoryPolicy.NO_CACHE).into(target);
                            }

                        } else {
                            AppConstant.loginDialoag(con);
                        }

                    }
                });
                imgComentbdl.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AppConstant.productIDfeedRow = productInfo.getId();
                        FragmentManager manager = ((Activity) con).getFragmentManager();
                        CommantDialogFragment dialogMenu = new CommantDialogFragment();
                        dialogMenu.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar);
                        dialogMenu.show( manager, "");
                    }
                });

                lLCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        bundleRemoveDialoag(productList.get(position).getId(), productList.get(position).getSeller_id(), context);
                    }
                });

                tvBrandNameBund.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AppConstant.brandID = productInfo.getBrand_info().get(0).getId();
                        AppConstant.brandName = productInfo.getBrand_info().get(0).getName();
                        myCommunicator.setContentFragment(new BrandFragment(), true);
                    }
                });

                bundleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AppConstant.productIDfeedRow = productInfo.getId();
                        AppConstant.feedRowObject = productInfo;
                        myCommunicator.addContentFragment(new SingleProductFragment(), true);
                    }
                });

                try {
                    /**
                     * -----------------Set data on the field of Bundle row------------------
                     */
                    Picasso.with(context).load(productInfo.getPhoto_one())
                            .placeholder(R.drawable.product_bg).error(R.drawable.product_bg).into(imgBundle);
                    tvProNameBundle.setText(productInfo.getName());
                    tvBrandNameBund.setText(productInfo.getBrand_info().get(0).getName());
                    tvOriginalPriceBund.setText(AppConstant.getCurrencySymbol(productInfo.getSeller_info().get(0).getCurrency()) + productInfo.getOriginal_price());
                    tvOriginalPriceBund.setPaintFlags(tvOriginalPriceBund.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    tvlistingPriceBdl.setText(AppConstant.getCurrencySymbol(productInfo.getSeller_info().get(0).getCurrency()) + productInfo.getListing_price());
                    tvLikeCountBdl.setText(productInfo.getLike_count());
                    if (productInfo.getIs_liked().equalsIgnoreCase("1")) {
                        imgLoveBundle.setImageResource(R.drawable.love_liked);
                    } else if (productInfo.getIs_liked().equalsIgnoreCase("1")) {
                        imgLoveBundle.setImageResource(R.drawable.love_unliked);
                    }
                    imgLoveBundle.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            callLikeUnlikeAPI(productInfo.getId(),position);
                        }
                    });



                    imgComentCountBdl.setText(productInfo.getComments_count());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return bundleView;
        }
    }

    BusyDialog busyDialog;

    private Target target = new Target() {


        @Override
        public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
            new Thread(new Runnable() {

                @Override
                public void run() {

                    File file = new File(Environment.getExternalStorageDirectory().getPath() + "/picture.jpg");
                    try {
                        file.createNewFile();
                        FileOutputStream ostream = new FileOutputStream(file);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, ostream);
                        busyDialog.dismis();
                        String path = MediaStore.Images.Media.insertImage(con.getContentResolver(), bitmap, "Title", null);
                        AppConstant.defaultShare(con, Uri.parse(path));
                        ostream.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }).start();
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
            busyDialog.dismis();
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
            if (placeHolderDrawable != null) {}
            busyDialog=new BusyDialog(con,false,"");
            busyDialog.show();
        }
    };

    /**
     * -------------------10. Like/Unlike Product API------------------
     */
    public void callLikeUnlikeAPI(final String product_id,  final int position) {
  /*
   * ---------------check internet first------------
   */
        if (!NetInfo.isOnline(con)) {
            AppConstant.alertDialoag(con, con.getString(R.string.status), con.getString(R.string.checkInternet), con.getString(R.string.ok));
            return;
        }

  /*
   * ----------------Show Busy Dialog -----------------------------------------
   */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
        /**
         *----------------------Start Thread-------------------------------------------
         */
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            String msg = "";
            String response = "";
            LikeUnlikeResponse likeUnlikeResponse;

            @Override
            public void run() {
                //--------------- We can performed your task here.-----------------

                try {
                    Log.i("Like/Unlike URL", AllURL.getLikeUnlike(product_id));
                    //-------------Hit Server---------------------
                    response = AAPBDHttpClient.get(AllURL.getLikeUnlike(product_id)).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();

                    //----------------Persist response with the help of Gson----------------
                    Log.i("Like/Unlike Response", ">>" + response);
                    Gson gson = new Gson();
                    likeUnlikeResponse = gson.fromJson(response, LikeUnlikeResponse.class);

                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        /**
                         * -------------Here we do UI related tasks inside run() method of runOnUiThread()-------------------
                         */

                        //-------Stop Busy Dialog-----
                        if (busy != null) {
                            busy.dismis();
                        }
                        /**
                         * ---------main Ui related work--------------
                         */
                        if (likeUnlikeResponse != null) {

                            if (likeUnlikeResponse.getMsg().equalsIgnoreCase("liked")) {
                                productList.get(position).getProduct_info().setLike_count(likeUnlikeResponse.getLikes_count());
                                productList.get(position).getProduct_info().setIs_liked("1");
                                addBundleAdapter.notifyDataSetChanged();

                            } else if (likeUnlikeResponse.getMsg().equalsIgnoreCase("unliked")) {
                                productList.get(position).getProduct_info().setLike_count(likeUnlikeResponse.getLikes_count());
                                productList.get(position).getProduct_info().setIs_liked("0");
                                addBundleAdapter.notifyDataSetChanged();
                            }

                        }

                    }
                });

            }

        });

    }

    /**
     * ====================Bundle Remove dialog===========================================
     */
    public void bundleRemoveDialoag(final String bundleID, final String sellerID, final Context con) {

        final Dialog bundleRemovedialog = new Dialog(con);
        bundleRemovedialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        bundleRemovedialog.setContentView(R.layout.dialog_bundle_remov);

        bundleRemovedialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        TextView tvbundleCancel = (TextView) bundleRemovedialog
                .findViewById(R.id.tvbundleCancel);
        TextView tvBundleOk = (TextView) bundleRemovedialog.findViewById(R.id.tvBundleOk);

        tvBundleOk.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                callBundleRemoveAPI(bundleID, sellerID);
                bundleRemovedialog.dismiss();

            }
        });

        tvbundleCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundleRemovedialog.dismiss();
            }
        });

        bundleRemovedialog.show();
    }

    /**
     * -------------------18.2 Bundle remove API------------------
     */
    private void callBundleRemoveAPI(final String bundle_id, final String seller_id) {
  /*
   * ---------------check internet first------------
   */
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }
         /*
          * ----------------Show Busy Dialog -----------------------------------------
          */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
  /*
   *----------------------Start Thread-------------------------------------------
   */
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            String msg = "";
            String response = "";

            @Override
            public void run() {
                // You can performed your task here.

                try {
                    Log.w("AddBundle URL", AllURL.getBundleRemove(bundle_id, seller_id));
                    //-------------Hit Server---------------------
                    response = AAPBDHttpClient.get(AllURL.getBundleRemove(bundle_id, seller_id)).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();

                    //----------------Persist response with the help of Gson----------------
                    Log.e("AddBundle Response", ">>" + response);
                    Gson gson = new Gson();
                    bundleResponse = gson.fromJson(response, AddToBundleResponse.class);

                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //-------Stop Busy Dialog-----
                        if (busy != null) {
                            busy.dismis();
                        }

                        /**
                         * ---------main Ui related work--------------
                         */
                        if (bundleResponse.getStatus().equalsIgnoreCase("1")) {
                            amountSet(bundleResponse);
//                            Toast.makeText(context, "Bundle Remove " + bundleResponse.getMsg(), Toast.LENGTH_LONG).show();
                            addBundleAdapter = new AddBundleAdapter(con, bundleResponse.getResult());
                            lVBundle.setAdapter(addBundleAdapter);
                            addBundleAdapter.notifyDataSetChanged();
                        } else {

                            msg = bundleResponse.getMsg();
                            AlertMessage.showMessage(con, "Bunlde", msg);

                        }

                    }
                });

            }

        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode==1){
            switch (resultCode) {
                case Activity.RESULT_OK:
                    // The payment succeeded
                    payKey = data.getStringExtra(PayPalActivity.EXTRA_PAY_KEY);
                    buyNowAPI(AllURL.addProductSellURL(productId,""+totalPriceBuy));
                    Log.e("Result Ok: Paykey is:: ",""+payKey);
                    // Tell the user their payment succeeded
                    break;
                case Activity.RESULT_CANCELED:
                    Log.e("Result Cancelled!!!! ",""+payKey);
//                getActivity().finish();
                    // The payment was canceled
                    // Tell the user their payment was canceled
                    break;
                case PayPalActivity.RESULT_FAILURE:
                    // The payment failed -- we get the error from the EXTRA_ERROR_ID
                    // and EXTRA_ERROR_MESSAGE
                    String errorID = data.getStringExtra(PayPalActivity.EXTRA_ERROR_ID);
                    String errorMessage = data.getStringExtra(PayPalActivity.EXTRA_ERROR_MESSAGE);
//                    Log.e("Result Failure:: Error Id!!!! ",""+errorID);
                    System.out.println("Result Failure:: Error Id!!!!"+errorID);
                    System.out.println("Result Failure:: Error Message!!!!"+errorMessage);
//                confirmSubscription.finish();
                    // Tell the user their payment was failed.
                    break;
            }
        }
    }

    public void buyNowAPI(final String url) {
      /*
       * ---------------check internet first------------
       */
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }
         /*
          * ----------------Show Busy Dialog -----------------------------------------
          */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
      /*
       *----------------------Start Thread-------------------------------------------
       */
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            String msg = "";
            String response = "";

            @Override
            public void run() {

                // You can performed your task here.
                try {
                    //-------------Hit Server---------------------
                    response = AAPBDHttpClient.get(url).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();
                    Log.e("List Response", ">>" + response);
                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        TextView tvDataNotFound= (TextView) view.findViewById(R.id.tvDataNotFound);
                        /**
                         * -------------Here we do UI related tasks inside run() method of runOnUiThread()-------------------
                         */
                        //-------Stop Busy Dialog-----
                        if (busy != null) {
                            busy.dismis();
                        }
                        //----------------Persist response with the help of Gson----------------
                        Gson gson = new Gson();
                        BuyNowResponse buyNowResponse = gson.fromJson(response, BuyNowResponse.class);
                        /**
                         * ---------main Ui related work--------------
                         */
                        if (buyNowResponse.getStatus().equalsIgnoreCase("1")) {
                            Toast.makeText(con,"Product Buy successful",Toast.LENGTH_SHORT).show();
                            getActivity().onBackPressed();

                        } else {
                            msg = buyNowResponse.getMsg();
                            AlertMessage.showMessage(con, "ProCommentList", msg);
                        }

                    }
                });

            }

        });

    }

}
