package com.sloppy;

import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sloppy.aapbdLib.AlertMessage;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.NetInfo;
import com.sloppy.aapbdLib.PersistData;
import com.sloppy.adapter.ProductListByTypeAdapter;
import com.sloppy.adapter.RecyclerProductListAdapter;
import com.sloppy.model.ProductAndUserDetailsInfo;
import com.sloppy.model.ProductListByTypeResponse;
import com.sloppy.utils.AAPBDHttpClient;
import com.sloppy.utils.AllURL;
import com.sloppy.utils.AppConstant;

import java.util.concurrent.Executors;

/**
 * Created by hp on 7/21/2016.
 */
public class CommonProductListFragment extends BaseFragment {

    private Context con;
    private View view;
    private RecyclerView rvCommonList;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        con = getActivity();
        view = inflater.inflate(R.layout.fragment_common_list, container, false);
        initUI();
        return view;
    }

    private void initUI() {
        TextView tvDataNotFound= (TextView) view.findViewById(R.id.tvDataNotFound);
        TextView tvTitelCommon = (TextView) view.findViewById(R.id.tvTitelCommon);
        //===============Initialization=================
        rvCommonList = (RecyclerView) view.findViewById(R.id.rvCommonList);
        if (AppConstant.productListTitel.equalsIgnoreCase("Like")){
            tvTitelCommon.setText("My Likes");
        } else {
            tvTitelCommon.setText(AppConstant.productListTitel);
        }
        AppConstant.setFont(con,tvTitelCommon,"regular");
        AppConstant.setFont(con,tvDataNotFound,"regular");
        //=========On Click==============================

        ImageView imgBackCommonList = (ImageView) view.findViewById(R.id.imgBackCommonList);
        ImageView imgMenuCommon = (ImageView) view.findViewById(R.id.imgMenuCommon);
        imgBackCommonList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        imgMenuCommon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.showMenuDiadog(getActivity().getFragmentManager());
            }
        });

        if (AppConstant.productList.size()> 0) {
            tvDataNotFound.setVisibility(View.GONE);
            rvCommonList.setVisibility(View.VISIBLE);
            /**
             * =================Set Adapter===========================================================
             */
            rvCommonList.setLayoutManager(new LinearLayoutManager(con));
            rvCommonList.setAdapter(new RecyclerProductListAdapter(getActivity(), AppConstant.productList));

        } else {
            rvCommonList.setVisibility(View.GONE);
            tvDataNotFound.setVisibility(View.VISIBLE);
        }

    }


}
