package com.sloppy;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.sloppy.aapbdLib.AlertMessage;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.NetInfo;
import com.sloppy.aapbdLib.PersistData;
import com.sloppy.aapbdLib.PersistentUser;
import com.sloppy.model.DiscountUpdateResponse;
import com.sloppy.model.ProfileUpdateResponse;
import com.sloppy.utils.AllURL;
import com.sloppy.utils.AppConstant;

import cz.msebera.android.httpclient.Header;


/**
 * Created by hp on 4/7/2016.
 */
public class SellerDiscountFragment extends Fragment {

    private Context con;
    private View viewsellerDis;
    private String discountPersentage="", minimumItem="", discount_enabled="";
    private TextView tvDis5, tvDis10, tvDis15, tvDis20, tvDis25, tvDis30, tvDisItem2,
            tvDisItem3, tvDisItem4, tvDisItem5, tvDisItem6, tvDisItem7, tvUserWill, tvSDSave;
    private CheckBox checkBox;

    /**
     * ============================ON CREATE========================================
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        con = getActivity();
        viewsellerDis = inflater.inflate(R.layout.fragment_seller_discount, container, false);
        initUI();
        return viewsellerDis;
    }

    @Override
    public void onStart() {
        super.onStart();

        if (PersistData.getStringData(con,AppConstant.discountEnabled).equalsIgnoreCase("yes")){
            checkBox.setChecked(true);
            setCurrencyBackground(PersistData.getStringData(con,AppConstant.sellerPercent));
            setDisItem(PersistData.getStringData(con,AppConstant.sellerItem));
            buttonEnableOrDisable("true");
            tvUserWill.setText("User will get " + PersistData.getStringData(con,AppConstant.sellerPercent) + "% off " + PersistData.getStringData(con,AppConstant.sellerItem) + " or more items");
        }else {
            checkBox.setChecked(false);
            tvUserWill.setText("User will get " + "0" + "% off " + "0" + " or more items");
            buttonEnableOrDisable("false");
        }

//        if (PersistData.getStringData(con,AppConstant.discountEnabled).equalsIgnoreCase("yes")){
//            buttonEnableOrDisable("true");
//        }else {
//            buttonEnableOrDisable("false");
//        }

    }

    private void initUI() {
        /**
         * ----------------Inotialization----------------------------
         */
        ImageView imgSellerDisBack = (ImageView) viewsellerDis.findViewById(R.id.imgSellerDisBack);
        checkBox = (CheckBox) viewsellerDis.findViewById(R.id.checkBox);
        tvUserWill = (TextView) viewsellerDis.findViewById(R.id.tvUserWill);
        tvSDSave = (TextView) viewsellerDis.findViewById(R.id.tvSDSave);
        tvDis5 = (TextView) viewsellerDis.findViewById(R.id.tvDis5);
        tvDis10 = (TextView) viewsellerDis.findViewById(R.id.tvDis10);
        tvDis15 = (TextView) viewsellerDis.findViewById(R.id.tvDis15);
        tvDis20 = (TextView) viewsellerDis.findViewById(R.id.tvDis20);
        tvDis25 = (TextView) viewsellerDis.findViewById(R.id.tvDis25);
        tvDis30 = (TextView) viewsellerDis.findViewById(R.id.tvDis30);

        tvDisItem2 = (TextView) viewsellerDis.findViewById(R.id.tvDisItem2);
        tvDisItem3 = (TextView) viewsellerDis.findViewById(R.id.tvDisItem3);
        tvDisItem4 = (TextView) viewsellerDis.findViewById(R.id.tvDisItem4);
        tvDisItem5 = (TextView) viewsellerDis.findViewById(R.id.tvDisItem5);
        tvDisItem6 = (TextView) viewsellerDis.findViewById(R.id.tvDisItem6);
        tvDisItem7 = (TextView) viewsellerDis.findViewById(R.id.tvDisItem7);

        /**
         * -------------------On Click-------------------------------------
         */

//        if (PersistData.getStringData(con,AppConstant.discountEnabled).equalsIgnoreCase("yes")){
//            checkBox.setChecked(true);
//            setCurrencyBackground(PersistData.getStringData(con,AppConstant.sellerPercent));
//            setDisItem(PersistData.getStringData(con,AppConstant.sellerItem));
////            buttonEnableOrDisable("true");
//            tvUserWill.setText("User will get " + PersistData.getStringData(con,AppConstant.sellerPercent) + "% off " + PersistData.getStringData(con,AppConstant.sellerItem) + " or more items");
//        }else {
//            checkBox.setChecked(false);
//            tvUserWill.setText("User will get " + "0" + "% off " + "0" + " or more items");
////            buttonEnableOrDisable("false");
//        }


        tvSDSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    postDiscountUpdate(AllURL.discountUpdateURL());
            }
        });

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton arg0, boolean arg1) {

                if (arg0.isChecked()) {
                    checkBox.setChecked(true);
                    buttonEnableOrDisable("true");
                    if (PersistData.getStringData(con,AppConstant.discountEnabled).equalsIgnoreCase("yes")){
                        setCurrencyBackground(PersistData.getStringData(con,AppConstant.sellerPercent));
                        setDisItem(PersistData.getStringData(con,AppConstant.sellerItem));
                        minimumItem=PersistData.getStringData(con,AppConstant.sellerItem);
                        discountPersentage=PersistData.getStringData(con,AppConstant.sellerPercent);
                        tvUserWill.setText("User will get " + PersistData.getStringData(con,AppConstant.sellerPercent) + "% off " + PersistData.getStringData(con,AppConstant.sellerItem) + " or more items");
                    }else {
                        minimumItem = "3";
                        discountPersentage = "15";
                        setCurrencyBackground("15");
                        setDisItem("3");
                        tvUserWill.setText("User will get " + "15" + "% off " + "3" + " or more items");
                    }

                    tvDis5.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            discountPersentage = "5";
                            setCurrencyBackground("5");
                        }
                    });
                    tvDis10.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            discountPersentage = "10";
                            setCurrencyBackground("10");
                        }
                    });

                    tvDis15.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            discountPersentage = "15";
                            setCurrencyBackground("15");
                        }
                    });
                    tvDis20.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            discountPersentage = "20";
                            setCurrencyBackground("20");
                        }
                    });
                    tvDis25.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            discountPersentage = "25";
                            setCurrencyBackground("25");
                        }
                    });
                    tvDis30.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            discountPersentage = "30";
                            setCurrencyBackground("30");
                        }
                    });

                    tvDisItem2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            minimumItem = "2";
                            setDisItem("2");
                        }
                    });
                    tvDisItem3.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            minimumItem = "3";
                            setDisItem("3");
                        }
                    });
                    tvDisItem4.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            minimumItem = "4";
                            setDisItem("4");
                        }
                    });
                    tvDisItem5.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            minimumItem = "5";
                            setDisItem("5");
                        }
                    });
                    tvDisItem6.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            minimumItem = "6";
                            setDisItem("6");
                        }
                    });
                    tvDisItem7.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            minimumItem = "7";
                            setDisItem("7");
                        }
                    });
                } else {
                    discountPersentage = "0";
                    minimumItem = "0";
                    buttonEnableOrDisable("false");
                    tvDis5.setBackgroundResource(R.drawable.bg_seller_dcnt);
                    tvDis10.setBackgroundResource(R.drawable.bg_seller_dcnt);
                    tvDis15.setBackgroundResource(R.drawable.bg_seller_dcnt);
                    tvDis20.setBackgroundResource(R.drawable.bg_seller_dcnt);
                    tvDis25.setBackgroundResource(R.drawable.bg_seller_dcnt);
                    tvDis30.setBackgroundResource(R.drawable.bg_seller_dcnt);

                    tvDis5.setTextColor(Color.parseColor("#999999"));
                    tvDis10.setTextColor(Color.parseColor("#999999"));
                    tvDis15.setTextColor(Color.parseColor("#999999"));
                    tvDis20.setTextColor(Color.parseColor("#999999"));
                    tvDis25.setTextColor(Color.parseColor("#999999"));
                    tvDis30.setTextColor(Color.parseColor("#999999"));

                    tvDisItem2.setBackgroundResource(R.drawable.bg_seller_dcnt);
                    tvDisItem3.setBackgroundResource(R.drawable.bg_seller_dcnt);
                    tvDisItem4.setBackgroundResource(R.drawable.bg_seller_dcnt);
                    tvDisItem5.setBackgroundResource(R.drawable.bg_seller_dcnt);
                    tvDisItem6.setBackgroundResource(R.drawable.bg_seller_dcnt);
                    tvDisItem7.setBackgroundResource(R.drawable.bg_seller_dcnt);

                    tvDisItem2.setTextColor(Color.parseColor("#999999"));
                    tvDisItem3.setTextColor(Color.parseColor("#999999"));
                    tvDisItem4.setTextColor(Color.parseColor("#999999"));
                    tvDisItem5.setTextColor(Color.parseColor("#999999"));
                    tvDisItem6.setTextColor(Color.parseColor("#999999"));
                    tvDisItem7.setTextColor(Color.parseColor("#999999"));

                    tvUserWill.setText("User will get " + discountPersentage + "% off " + minimumItem + " or more items");
                }

            }
        });




        imgSellerDisBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().onBackPressed();
            }
        });

    }

    private void buttonEnableOrDisable(String check){

        if (check.equalsIgnoreCase("true")){
            tvDis5.setEnabled(true);
            tvDis10.setEnabled(true);
            tvDis15.setEnabled(true);
            tvDis20.setEnabled(true);
            tvDis25.setEnabled(true);
            tvDis30.setEnabled(true);
            tvDisItem2.setEnabled(true);
            tvDisItem3.setEnabled(true);
            tvDisItem4.setEnabled(true);
            tvDisItem5.setEnabled(true);
            tvDisItem6.setEnabled(true);
            tvDisItem7.setEnabled(true);
        }else{
            tvDis5.setEnabled(false);
            tvDis10.setEnabled(false);
            tvDis15.setEnabled(false);
            tvDis20.setEnabled(false);
            tvDis25.setEnabled(false);
            tvDis30.setEnabled(false);
            tvDisItem2.setEnabled(false);
            tvDisItem3.setEnabled(false);
            tvDisItem4.setEnabled(false);
            tvDisItem5.setEnabled(false);
            tvDisItem6.setEnabled(false);
            tvDisItem7.setEnabled(false);
        }
    }

    /**
     * ------------------Call 36. Discount Info Update API--------------------------------------
     */
    private void postDiscountUpdate(final String url) {
        //--- for net check-----
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }

        final BusyDialog busyNow = new BusyDialog(con, true, false);
        busyNow.show();

        final AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token));
        Log.e("token", PersistData.getStringData(con, AppConstant.token));

        final RequestParams param = new RequestParams();

        try {
            if (checkBox.isChecked()){
                param.put("discount_enabled", "1");
                param.put("discount_percentage", Integer.parseInt(discountPersentage));
                param.put("minimum_items", Integer.parseInt(minimumItem));
            }else{
                param.put("discount_enabled", "0");
                param.put("discount_percentage", "0");
                param.put("minimum_items", "0");
            }


        } catch (final Exception e1) {
            e1.printStackTrace();
        }
        Log.e("DiscountUPdateURL ", ">>" + url);
        client.post(url, param, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers,
                                  byte[] response) {

                if (busyNow != null) {
                    busyNow.dismis();
                }

                Log.e(" OfferResposne ", ">>" + new String(response));

                Gson g = new Gson();
                DiscountUpdateResponse discountUpdateResponse = g.fromJson(new String(response), DiscountUpdateResponse.class);


                if (discountUpdateResponse.getStatus().equalsIgnoreCase("1")) {
                    if (checkBox.isChecked()){
                        PersistData.setStringData(con,AppConstant.discountEnabled,"yes");
                    }else{
                        PersistData.setStringData(con,AppConstant.discountEnabled,"no");
                    }
                    PersistData.setStringData(con,AppConstant.sellerPercent,discountPersentage);
                    PersistData.setStringData(con,AppConstant.sellerItem,minimumItem);
                    Toast.makeText(con, discountUpdateResponse.getMsg() + "", Toast.LENGTH_LONG).show();
                    getActivity().onBackPressed();


                } else {

                    AlertMessage.showMessage(con, "Status",
                            discountUpdateResponse.getMsg() + "");
                    return;
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  byte[] errorResponse, Throwable e) {

                Log.e("errorResponse", new String(errorResponse));

                if (busyNow != null) {
                    busyNow.dismis();
                }
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried

            }
        });

    }

    private void setCurrencyBackground(String disPercentage) {
        switch (disPercentage) {
            case "5":
                tvDis5.setBackgroundResource(R.drawable.grayborder_with_sweetbac);
                tvDis10.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDis15.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDis20.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDis25.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDis30.setBackgroundResource(R.drawable.bg_seller_dcnt);

                tvDis5.setTextColor(Color.parseColor("#FFFFFF"));
                tvDis10.setTextColor(Color.parseColor("#999999"));
                tvDis15.setTextColor(Color.parseColor("#999999"));
                tvDis20.setTextColor(Color.parseColor("#999999"));
                tvDis25.setTextColor(Color.parseColor("#999999"));
                tvDis30.setTextColor(Color.parseColor("#999999"));
                tvUserWill.setText("User will get " + discountPersentage + "% off " + minimumItem + " or more items");

                break;
            case "10":
                tvDis5.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDis10.setBackgroundResource(R.drawable.bg_sweet);
                tvDis15.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDis20.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDis25.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDis30.setBackgroundResource(R.drawable.bg_seller_dcnt);

                tvDis5.setTextColor(Color.parseColor("#999999"));
                tvDis10.setTextColor(Color.parseColor("#FFFFFF"));
                tvDis15.setTextColor(Color.parseColor("#999999"));
                tvDis20.setTextColor(Color.parseColor("#999999"));
                tvDis25.setTextColor(Color.parseColor("#999999"));
                tvDis30.setTextColor(Color.parseColor("#999999"));
                tvUserWill.setText("User will get " + discountPersentage + "% off " + minimumItem + " or more items");
                break;
            case "15":
                tvDis5.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDis10.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDis15.setBackgroundResource(R.drawable.grayborder_with_sweetbac);
                tvDis20.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDis25.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDis30.setBackgroundResource(R.drawable.bg_seller_dcnt);

                tvDis5.setTextColor(Color.parseColor("#999999"));
                tvDis10.setTextColor(Color.parseColor("#999999"));
                tvDis15.setTextColor(Color.parseColor("#FFFFFF"));
                tvDis20.setTextColor(Color.parseColor("#999999"));
                tvDis25.setTextColor(Color.parseColor("#999999"));
                tvDis30.setTextColor(Color.parseColor("#999999"));
                tvUserWill.setText("User will get " + discountPersentage + "% off " + minimumItem + " or more items");
                break;
            case "20":
                tvDis5.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDis10.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDis15.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDis20.setBackgroundResource(R.drawable.grayborder_with_sweetbac);
                tvDis25.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDis30.setBackgroundResource(R.drawable.bg_seller_dcnt);

                tvDis5.setTextColor(Color.parseColor("#999999"));
                tvDis10.setTextColor(Color.parseColor("#999999"));
                tvDis15.setTextColor(Color.parseColor("#999999"));
                tvDis20.setTextColor(Color.parseColor("#FFFFFF"));
                tvDis25.setTextColor(Color.parseColor("#999999"));
                tvDis30.setTextColor(Color.parseColor("#999999"));
                tvUserWill.setText("User will get " + discountPersentage + "% off " + minimumItem + " or more items");
                break;
            case "25":
                tvDis5.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDis10.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDis15.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDis20.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDis25.setBackgroundResource(R.drawable.grayborder_with_sweetbac);
                tvDis30.setBackgroundResource(R.drawable.bg_seller_dcnt);

                tvDis5.setTextColor(Color.parseColor("#999999"));
                tvDis10.setTextColor(Color.parseColor("#999999"));
                tvDis15.setTextColor(Color.parseColor("#999999"));
                tvDis20.setTextColor(Color.parseColor("#999999"));
                tvDis25.setTextColor(Color.parseColor("#FFFFFF"));
                tvDis30.setTextColor(Color.parseColor("#999999"));
                tvUserWill.setText("User will get " + discountPersentage + "% off " + minimumItem + " or more items");
                break;
            case "30":
                tvDis5.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDis10.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDis15.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDis20.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDis25.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDis30.setBackgroundResource(R.drawable.grayborder_with_sweetbac);

                tvDis5.setTextColor(Color.parseColor("#999999"));
                tvDis10.setTextColor(Color.parseColor("#999999"));
                tvDis15.setTextColor(Color.parseColor("#999999"));
                tvDis20.setTextColor(Color.parseColor("#999999"));
                tvDis25.setTextColor(Color.parseColor("#999999"));
                tvDis30.setTextColor(Color.parseColor("#FFFFFF"));
                tvUserWill.setText("User will get " + discountPersentage + "% off " + minimumItem + " or more items");
                break;
        }
    }

    private void setDisItem(String disItem) {
        switch (disItem) {
            case "2":
                tvDisItem2.setBackgroundResource(R.drawable.grayborder_with_sweetbac);
                tvDisItem3.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDisItem4.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDisItem5.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDisItem6.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDisItem7.setBackgroundResource(R.drawable.bg_seller_dcnt);

                tvDisItem2.setTextColor(Color.parseColor("#FFFFFF"));
                tvDisItem3.setTextColor(Color.parseColor("#999999"));
                tvDisItem4.setTextColor(Color.parseColor("#999999"));
                tvDisItem5.setTextColor(Color.parseColor("#999999"));
                tvDisItem6.setTextColor(Color.parseColor("#999999"));
                tvDisItem7.setTextColor(Color.parseColor("#999999"));
                tvUserWill.setText("User will get " + discountPersentage + "% off " + minimumItem + " or more items");
                break;
            case "3":
                tvDisItem2.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDisItem3.setBackgroundResource(R.drawable.grayborder_with_sweetbac);
                tvDisItem4.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDisItem5.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDisItem6.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDisItem7.setBackgroundResource(R.drawable.bg_seller_dcnt);

                tvDisItem2.setTextColor(Color.parseColor("#999999"));
                tvDisItem3.setTextColor(Color.parseColor("#FFFFFF"));
                tvDisItem4.setTextColor(Color.parseColor("#999999"));
                tvDisItem5.setTextColor(Color.parseColor("#999999"));
                tvDisItem6.setTextColor(Color.parseColor("#999999"));
                tvDisItem7.setTextColor(Color.parseColor("#999999"));
                tvUserWill.setText("User will get " + discountPersentage + "% off " + minimumItem + " or more items");
                break;
            case "4":
                tvDisItem2.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDisItem3.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDisItem4.setBackgroundResource(R.drawable.grayborder_with_sweetbac);
                tvDisItem5.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDisItem6.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDisItem7.setBackgroundResource(R.drawable.bg_seller_dcnt);

                tvDisItem2.setTextColor(Color.parseColor("#999999"));
                tvDisItem3.setTextColor(Color.parseColor("#999999"));
                tvDisItem4.setTextColor(Color.parseColor("#FFFFFF"));
                tvDisItem5.setTextColor(Color.parseColor("#999999"));
                tvDisItem6.setTextColor(Color.parseColor("#999999"));
                tvDisItem7.setTextColor(Color.parseColor("#999999"));
                tvUserWill.setText("User will get " + discountPersentage + "% off " + minimumItem + " or more items");
                break;
            case "5":
                tvDisItem2.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDisItem3.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDisItem4.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDisItem5.setBackgroundResource(R.drawable.grayborder_with_sweetbac);
                tvDisItem6.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDisItem7.setBackgroundResource(R.drawable.bg_seller_dcnt);

                tvDisItem2.setTextColor(Color.parseColor("#999999"));
                tvDisItem3.setTextColor(Color.parseColor("#999999"));
                tvDisItem4.setTextColor(Color.parseColor("#999999"));
                tvDisItem5.setTextColor(Color.parseColor("#FFFFFF"));
                tvDisItem6.setTextColor(Color.parseColor("#999999"));
                tvDisItem7.setTextColor(Color.parseColor("#999999"));
                tvUserWill.setText("User will get " + discountPersentage + "% off " + minimumItem + " or more items");
                break;
            case "6":
                tvDisItem2.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDisItem3.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDisItem4.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDisItem5.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDisItem6.setBackgroundResource(R.drawable.grayborder_with_sweetbac);
                tvDisItem7.setBackgroundResource(R.drawable.bg_seller_dcnt);

                tvDisItem2.setTextColor(Color.parseColor("#999999"));
                tvDisItem3.setTextColor(Color.parseColor("#999999"));
                tvDisItem4.setTextColor(Color.parseColor("#999999"));
                tvDisItem5.setTextColor(Color.parseColor("#999999"));
                tvDisItem6.setTextColor(Color.parseColor("#FFFFFF"));
                tvDisItem7.setTextColor(Color.parseColor("#999999"));
                tvUserWill.setText("User will get " + discountPersentage + "% off " + minimumItem + " or more items");
                break;
            case "7":
                tvDisItem2.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDisItem3.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDisItem4.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDisItem5.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDisItem6.setBackgroundResource(R.drawable.bg_seller_dcnt);
                tvDisItem7.setBackgroundResource(R.drawable.grayborder_with_sweetbac);

                tvDisItem2.setTextColor(Color.parseColor("#999999"));
                tvDisItem3.setTextColor(Color.parseColor("#999999"));
                tvDisItem4.setTextColor(Color.parseColor("#999999"));
                tvDisItem5.setTextColor(Color.parseColor("#999999"));
                tvDisItem6.setTextColor(Color.parseColor("#999999"));
                tvDisItem7.setTextColor(Color.parseColor("#FFFFFF"));
                tvUserWill.setText("User will get " + discountPersentage + "% off " + minimumItem + " or more items");
                break;
        }
    }
}
