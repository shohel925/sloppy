package com.sloppy;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sloppy.aapbdLib.AlertMessage;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.NetInfo;
import com.sloppy.aapbdLib.PersistData;
import com.sloppy.aapbdLib.PersistentUser;
import com.sloppy.adapter.DraftRecyclerAdapter;
import com.sloppy.adapter.ProductListByTypeAdapter;
import com.sloppy.adapter.RecyclerProductListAdapter;
import com.sloppy.model.FollowingFollowerResponse;
import com.sloppy.model.ProListBySubResponse;
import com.sloppy.utils.AAPBDHttpClient;
import com.sloppy.utils.AllURL;
import com.sloppy.utils.AppConstant;

import java.util.concurrent.Executors;





/**
 * Created by hp on 6/28/2016.
 */
public class ProductListBySubcategoryFragment extends BaseFragment {
    private Context con;
    private View view;
    private TextView tvTitelCommon;
    private RecyclerView rvCommonList;
    private ProListBySubResponse proListBySubResponse;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        con = getActivity();
        view = inflater.inflate(R.layout.fragment_common_list, container, false);
        initUI();
        return view;
    }

    private void initUI() {
        rvCommonList = (RecyclerView) view.findViewById(R.id.rvCommonList);
        tvTitelCommon = (TextView) view.findViewById(R.id.tvTitelCommon);
        AppConstant.setFont(con, tvTitelCommon, "regular");
        tvTitelCommon.setText(getResources().getString(R.string.search_product));
        if (!PersistentUser.isLogged(con)) {
            callSubCatProListAPI(AppConstant.categoryID, AppConstant.categoryType, getResources().getString(R.string.page_items), "demo", AppConstant.subCategoryID);
        } else {
            callSubCatProListAPI(AppConstant.categoryID, AppConstant.categoryType,getResources().getString(R.string.page_items), "all", AppConstant.subCategoryID);
        }
    }

    /**
     * =======================14. Product List By Subcategory========================================
     */
    protected void callSubCatProListAPI(final String category_id, final String suitable_for, final String page_items, final String user_type, final String sub_category_id) {
        /**
         * =====================Check Internet======================================================
         */
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }
        /**
         * =====================Start progress bar==================================================
         */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
        /**
         * =====================Start Thread========================================================
         */
        Executors.newSingleThreadExecutor().submit(new Runnable() {
            String msg = "";
            String response = "";

            @Override
            public void run() {

                try {
                    Log.e("ProListBy SUB URL", AllURL.getSubCatProList(category_id, suitable_for, page_items, user_type, sub_category_id));

                    response = AAPBDHttpClient.get(AllURL.getSubCatProList(category_id, suitable_for, page_items, user_type, sub_category_id)).//==API call====
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();
                    Gson gson = new Gson();
                    proListBySubResponse = gson.fromJson(response, ProListBySubResponse.class);
                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        TextView tvDataNotFound = (TextView) view.findViewById(R.id.tvDataNotFound);
                        AppConstant.setFont(con,tvDataNotFound,"regular");
                        /**
                         * ======stop progress bar==================================================
                         */
                        if (busy != null) {
                            busy.dismis();
                        }
                        /**
                         * ==============Here make UI Related work==================================
                         */
                        if (proListBySubResponse.getStatus().equalsIgnoreCase("1")) {
                            Log.e("proListBySubResponse", ">>" + response);
//                            Toast.makeText(con, "ProductList By Sub" + proListBySubResponse.getMsg(), Toast.LENGTH_LONG).show();

                            if (proListBySubResponse.getResult().getData().size() > 0) {
                                tvDataNotFound.setVisibility(View.GONE);
                                rvCommonList.setVisibility(View.VISIBLE);
                                /**
                                 * =================Set Adapter===========================================================
                                 */
                                rvCommonList.setLayoutManager(new LinearLayoutManager(con));
                                rvCommonList.setAdapter(new RecyclerProductListAdapter(getActivity(), proListBySubResponse.getResult().getData()));

                            } else {
                                rvCommonList.setVisibility(View.GONE);
                                tvDataNotFound.setVisibility(View.VISIBLE);
                            }
                        } else {

                            msg = proListBySubResponse.getMsg();
                            AlertMessage.showMessage(con, "My follow ", msg);

                        }

                    }
                });

            }

        });

    }
}
