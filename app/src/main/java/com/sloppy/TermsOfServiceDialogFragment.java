package com.sloppy;

import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.sloppy.utils.AppConstant;

/**
 * Created by hp on 7/24/2016.
 */
public class TermsOfServiceDialogFragment extends DialogFragment {
    private Context con;
    private View view;
    private TextView tvTitelTerm,tvDoneTerm;
    private ImageView termsOfSerBack;
    private WebView webview;
    private DialogFragment mMyDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        con = getActivity();
        view = inflater.inflate(R.layout.fragment_term_of_service, container, false);
        initUI();
        return view;
    }

    private void initUI() {
        termsOfSerBack= (ImageView) view.findViewById(R.id.termsOfSerBack);
        webview= (WebView) view.findViewById(R.id.webview);
        tvTitelTerm= (TextView) view.findViewById(R.id.tvTitelTerm);
        tvDoneTerm= (TextView) view.findViewById(R.id.tvDoneTerm);

        AppConstant.setFont(con,tvTitelTerm,"regular");
        AppConstant.setFont(con,tvDoneTerm,"regular");
        if (AppConstant.main.equalsIgnoreCase("yes")){
            termsOfSerBack.setVisibility(View.GONE);
        }

        termsOfSerBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });
        tvDoneTerm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppConstant.dialogMenu!=null) {
                    AppConstant.dialogMenu.dismiss();
                }
                getDialog().dismiss();
            }
        });

        if (AppConstant.menuType.equalsIgnoreCase("term")) {
            tvTitelTerm.setText(getResources().getString(R.string.terms_of));
            webview.loadUrl("file:///android_asset/menu_data/termsofservice.html");
        }else if (AppConstant.menuType.equalsIgnoreCase("faq")){
            tvTitelTerm.setText(getResources().getString(R.string.faq));
            webview.loadUrl("file:///android_asset/menu_data/FAQ.html");
        } else if (AppConstant.menuType.equalsIgnoreCase("privacy")){
            tvTitelTerm.setText(getResources().getString(R.string.privecy_policy));
            webview.loadUrl("file:///android_asset/menu_data/privacypolicy.html");
        }

    }
}
