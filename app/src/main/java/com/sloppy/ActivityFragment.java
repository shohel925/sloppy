package com.sloppy;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;


import com.google.gson.Gson;

import com.sloppy.aapbdLib.AlertMessage;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.NetInfo;
import com.sloppy.aapbdLib.PersistData;
import com.sloppy.aapbdLib.PersistentUser;
import com.sloppy.adapter.NotificationAdapter;
import com.sloppy.adapter.ProductListByTypeAdapter;
import com.sloppy.model.ActivitiesInfo;
import com.sloppy.model.ActivityListResponse;
import com.sloppy.model.ActivityResultInfo;
import com.sloppy.model.ProCommentListResponse;
import com.sloppy.model.ProductListByTypeResponse;
import com.sloppy.model.SingleProductDetailsResponse;
import com.sloppy.utils.AAPBDHttpClient;
import com.sloppy.utils.AllURL;
import com.sloppy.utils.AppConstant;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;

import stickylistheaders.StickyListHeadersAdapter;
import stickylistheaders.StickyListHeadersListView;


public class ActivityFragment extends BaseFragment implements
        StickyListHeadersListView.OnStickyHeaderOffsetChangedListener,
        StickyListHeadersListView.OnStickyHeaderChangedListener {

    private View view;
    private Context con;
    private ActivityListResponse activityListResponse;
    private Typeface helveticaNeuRegular;
    private TextView tvfolllowers, tvFollowing1, tvSloppyActiviy;
    private ImageView imgfolllowers, imgFollowing1;
    private LinearLayout layoutFollower, layoutFollowing, llstickyList;

    List<ActivityResultInfo> data;
    private stickylistheaders.StickyListHeadersListView homeStrickyList;
    private boolean fadeHeader = true;
    private TestBaseAdapter mAdapter;
    private SingleProductDetailsResponse sinProResponse;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        con = getActivity();
        if (!PersistentUser.isLogged(con)) {
            // ------- Log in or not  dialog show----------
            AppConstant.loginDialoag(con);

        } else {
            view = inflater.inflate(R.layout.fragment_activity, container, false);

            initUI(view);
        }
        return view;
    }

    private void initUI(View view) {
        /*
            ----------------Initialization-------------------
         */
        tvfolllowers = (TextView) view.findViewById(R.id.tvfolllowers);
        tvFollowing1 = (TextView) view.findViewById(R.id.tvFollowing1);
        tvSloppyActiviy = (TextView) view.findViewById(R.id.tvSloppyActiviy);

        imgfolllowers = (ImageView) view.findViewById(R.id.imgfolllowers);
        imgFollowing1 = (ImageView) view.findViewById(R.id.imgFollowing1);

        layoutFollower = (LinearLayout) view.findViewById(R.id.layoutFollower);
        layoutFollowing = (LinearLayout) view.findViewById(R.id.layoutFollowing);
        llstickyList = (LinearLayout) view.findViewById(R.id.llstickyList);

/**
 * -------------------Font Set-------------------------------------------------------
 */
        helveticaNeuRegular = Typeface.createFromAsset(getActivity().getAssets(), "font/helveticaNeuRegular.ttf");
        tvSloppyActiviy.setTypeface(helveticaNeuRegular);
        tvfolllowers.setTypeface(helveticaNeuRegular);
        tvfolllowers.setTypeface(helveticaNeuRegular);
        homeStrickyList = (stickylistheaders.StickyListHeadersListView) view.findViewById(R.id.homeStrickyList);

//        homeStrickyList.setOnScrollListener(this);
        homeStrickyList.setOnStickyHeaderChangedListener(this);
        homeStrickyList.setOnStickyHeaderOffsetChangedListener(this);
        if (AppConstant.isFollowing){
            changeBg("following");
            callActivityListAPI("following", "10");
        }else {
            changeBg("follower");
            callActivityListAPI("follower", "10");
        }

        /*
            ----------------On click----------------
         */
        layoutFollower.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.isFollowing=false;
                changeBg("follower");
                callActivityListAPI("follower", "10");
            }

        });

        layoutFollowing.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AppConstant.isFollowing=true;
                changeBg("following");
                callActivityListAPI("following", "10");
            }

        });
    }

    private void changeBg(String gender) {

        if (gender.equalsIgnoreCase("follower")) {
            tvfolllowers.setTextColor(Color.parseColor("#FB9A7F"));
            imgfolllowers.setImageResource(R.drawable.lil_right_arrow_select);

            tvFollowing1.setTextColor(Color.parseColor("#999999"));
            imgFollowing1.setImageResource(R.drawable.rrrrr);
        } else if (gender.equalsIgnoreCase("following")) {
            tvFollowing1.setTextColor(Color.parseColor("#FB9A7F"));
            imgFollowing1.setImageResource(R.drawable.rrrrr_s);

            tvfolllowers.setTextColor(Color.parseColor("#999999"));
            imgfolllowers.setImageResource(R.drawable.lil_right_arrow);

        }
    }

    /**
     * -------------------12. Product Comments List  API------------------
     */
    public void callActivityListAPI(final String type, final String page_items) {
  /*
   * ---------------check internet first------------
   */
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }

         /*
          * ----------------Show Busy Dialog -----------------------------------------
          */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
  /*
   *
   *----------------------Start Thread-------------------------------------------
   *
   */
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            String msg = "";
            String response = "";

            @Override
            public void run() {
                // You can performed your task here.

                try {
                    Log.e("ActivityList URL", AllURL.getActivityListURL(type, page_items));
                    //-------------Hit Server---------------------
                    response = AAPBDHttpClient.get(AllURL.getActivityListURL(type, page_items)).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();

                    //----------------Persist response with the help of Gson----------------
                    Log.e("ActivityList Response", ">>" + response);

                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        /**
                         * -------------Here we do UI related tasks inside run() method of runOnUiThread()-------------------
                         */

                        //-------Stop Busy Dialog-----
                        if (busy != null) {
                            busy.dismis();
                        }
                        Gson gson = new Gson();
                        activityListResponse = gson.fromJson(response, ActivityListResponse.class);
                        TextView tvDataNotFound= (TextView) view.findViewById(R.id.tvDataNotFound);
                        /**
                         * ---------main Ui related work--------------
                         */
                        if (activityListResponse.getStatus().equalsIgnoreCase("1")) {
//                            Toast.makeText(con, "ActivityList " + activityListResponse.getMsg(), Toast.LENGTH_LONG).show();
                            if(activityListResponse.getResult().size() > 0){
                                tvDataNotFound.setVisibility(View.GONE);
                                llstickyList.setVisibility(View.VISIBLE);
                                data = activityListResponse.getResult();
                                //==========Adapter call====================
                                mAdapter = new TestBaseAdapter(con);
                                homeStrickyList.setAdapter(mAdapter);
                                homeStrickyList.setSelection(0);
                                mAdapter.notifyDataSetChanged();

                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {

                                    @Override
                                    public void run() {

                                    }
                                }, 1000);

                            } else {
                                llstickyList.setVisibility(View.GONE);
                                tvDataNotFound.setVisibility(View.VISIBLE);
                            }

                        } else {
                            msg = activityListResponse.getMsg();
                            AlertMessage.showMessage(con, "ProCommentList", msg);
                            Log.e("CommentList size", ">>" + activityListResponse.getResult().size());

                        }

                    }
                });

            }

        });

    }

    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onStickyHeaderOffsetChanged(StickyListHeadersListView l, View header, int offset) {
        if (fadeHeader && Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            header.setAlpha(1 - (offset / (float) header.getMeasuredHeight()));
        }
    }

    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onStickyHeaderChanged(StickyListHeadersListView l, View header, int itemPosition, long headerId) {
        header.setAlpha(1);
    }

    public class TestBaseAdapter extends BaseAdapter implements StickyListHeadersAdapter {

        private final Context mContext;
        private LayoutInflater mInflater;

        public TestBaseAdapter(Context context) {
            mContext = context;
            mInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return data.size();
        }
//
        @Override
        public Object getItem(int position) {
            return data.get(position);
        }

        public long getItemId(int position) {
            return position;
        }


        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
                holder = new ViewHolder();
                convertView = mInflater.inflate(R.layout.view_container, parent, false);
                holder.viewContainers = (LinearLayout) convertView.findViewById(R.id.viewContainers);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.viewContainers.removeAllViews();
            for (int i=0; i<data.get(position).getActivities().size(); i++) {
                final ActivitiesInfo info= data.get(position).getActivities().get(i);
                View newView = ((LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.raw_follow_following, parent, false);
                ImageView imgWatch1 = (ImageView) newView.findViewById(R.id.imgWatch1);
                ImageView imgActivityProduct = (ImageView) newView.findViewById(R.id.imgActivityProduct);
                TextView tvNameFollower = (TextView) newView.findViewById(R.id.tvNameFollower);
                TextView tvActionType = (TextView) newView.findViewById(R.id.tvActionType);
                TextView tvTimeAction = (TextView) newView.findViewById(R.id.tvTimeAction);
                tvNameFollower.setText(info.getUser_info().get(0).getUsername());
                if (!TextUtils.isEmpty(info.getUser_info().get(0).getProfile_image())) {
                    Picasso.with(view.getContext()).load(info.getUser_info().get(0).getProfile_image())
                            .placeholder(R.drawable.person).error(R.drawable.person).into(imgWatch1);
                }

                if (!TextUtils.isEmpty(info.getImage_url())) {
                    imgActivityProduct.setVisibility(View.VISIBLE);
                    Picasso.with(view.getContext()).load(info.getImage_url())
                            .placeholder(R.drawable.product_bg).error(R.drawable.product_bg).into(imgActivityProduct);
                }else{
                    imgActivityProduct.setVisibility(View.GONE);
                }

                if (info.getAction_type().equalsIgnoreCase("feedback")) {
                    tvActionType.setText(getString(R.string.left_new_feedback));
                } else if (info.getAction_type().equalsIgnoreCase("like")) {
                    tvActionType.setText(getString(R.string.liked_your));
                } else if (info.getAction_type().equalsIgnoreCase("follow")) {
                    tvActionType.setText(getString(R.string.nowFollowing));
                } else if (info.getAction_type().equalsIgnoreCase("comments")) {
                    tvActionType.setText(getString(R.string.left_new_comment));
                }else if (info.getAction_type().equalsIgnoreCase("product_sell")) {
                    tvActionType.setText(getString(R.string.purchasedaProduct));
                }else if (info.getAction_type().equalsIgnoreCase("tag")) {
                    tvActionType.setText(getString(R.string.tagSomeOne));
                }else if (info.getAction_type().equalsIgnoreCase("sell")) {
                    tvActionType.setText(getString(R.string.sold_a_pro));
                }
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                SimpleDateFormat dateFormat2 = new SimpleDateFormat("kk:mm");
                try {
                    Date date = dateFormat.parse(info.getCreated_at());
                    String out = dateFormat2.format(date);
                    tvTimeAction.setText(out);
                } catch (ParseException e) {
                }
                holder.viewContainers.addView(newView);
                imgActivityProduct.setTag(info.getProduct_id());
                imgWatch1.setTag(info.getUser_info().get(0).getUser_id());
                tvNameFollower.setTag(info.getUser_info().get(0).getUser_id());
                tvNameFollower.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Object userId =view.getTag();
                        if(userId!=null) {
                            AppConstant.otherUserId= (String) userId;
                            myCommunicator.setContentFragment(new OtherUserDetailsFragment(), true);

                        }
                    }
                });
                imgWatch1.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Object userId =view.getTag();
                        if(userId!=null) {
                            AppConstant.otherUserId= (String) userId;
                            myCommunicator.setContentFragment(new OtherUserDetailsFragment(), true);

                        }
                    }
                });
                imgActivityProduct.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        Object productId =v.getTag();
                        if(productId!=null) {
                            String product= (String) productId;
                            if (!product.equalsIgnoreCase("0")) {
                                callSingProAPI(con, product, "All");
                            }
                        }
                    }
                });
            }
            return convertView;
        }

        class ViewHolder {
            LinearLayout viewContainers;
        }

        class HeaderViewHolder {
            TextView tvActivityDate, tvCount;
        }

        @Override
        public View getHeaderView(final int position, View v, ViewGroup parent) {
            HeaderViewHolder holder;

            if (v == null) {
                holder = new HeaderViewHolder();
                v = mInflater.inflate(R.layout.header_actvity_list, parent, false);
                holder.tvActivityDate = (TextView) v.findViewById(R.id.tvActivityDate);
                holder.tvCount = (TextView) v.findViewById(R.id.tvCount);
                v.setTag(holder);
            } else {
                holder = (HeaderViewHolder) v.getTag();
            }
            ///=================set data================
            holder.tvCount.setText(String.valueOf(data.get(position).getActivities().size()));
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat dateFormat2 = new SimpleDateFormat("MMMM dd, yyyy");
            try {
                Date date = dateFormat.parse(data.get(position).getDay());
                String serverDate = dateFormat.format(date);
                String out = dateFormat2.format(date);

                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                String currentDate = sdf.format(new Date());

                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, -1);
                String yesterday=dateFormat.format(cal.getTime());
                if (serverDate.equalsIgnoreCase(currentDate)) {
                    holder.tvActivityDate.setText(getResources().getString(R.string.today));
                } else if (serverDate.equalsIgnoreCase(yesterday)) {
                    holder.tvActivityDate.setText(getResources().getString(R.string.yesterday));
                } else {
                    holder.tvActivityDate.setText(out);
                }

            } catch (ParseException e) {
            }

            return v;
        }

        /**
         * Remember that these have to be static, postion=1 should always return
         * the same Id that is.
         */
        @Override
        public long getHeaderId(int position) {
            // return the first character of the country as ID because this is what
            // headers are based upon
            return position;
        }


    }

    public void callSingProAPI(final Context con, final String id, final String user_type) {
      /*
       * ---------------check internet first------------
       */
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }
         /*
          * ----------------Show Busy Dialog -----------------------------------------
          */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
      /*
       *----------------------Start Thread-------------------------------------------
       */
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            String msg = "";
            String response = "";

            @Override
            public void run() {

                // You can performed your task here.
                try {
                    Log.e("SinPro URL", AllURL.getSingleProDetaURL(id, user_type));
                    //-------------Hit Server---------------------
                    response = AAPBDHttpClient.get(AllURL.getSingleProDetaURL(id, user_type)).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();
                    Log.e( "SingPro Response", ">>" + response);
                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        TextView tvDataNotFound= (TextView) view.findViewById(R.id.tvDataNotFound);
                        /**
                         * -------------Here we do UI related tasks inside run() method of runOnUiThread()-------------------
                         */
                        //-------Stop Busy Dialog-----
                        if (busy != null) {
                            busy.dismis();
                        }
                        //----------------Persist response with the help of Gson----------------
                        Gson gson = new Gson();
                        sinProResponse = gson.fromJson(response, SingleProductDetailsResponse.class);
                        /**
                         * ---------main Ui related work--------------
                         */
                        if (sinProResponse.getStatus().equalsIgnoreCase("1")) {

                            AppConstant.productIDfeedRow=sinProResponse.getDetails().getId();
                            AppConstant.feedRowObject=sinProResponse.getDetails();
                            myCommunicator.addContentFragment(new SingleProductFragment(), true);

//                            Toast.makeText(con, "Single Pro " + sinProResponse.getMsg(), Toast.LENGTH_LONG).show();
                        } else {
                            msg = sinProResponse.getMsg();
                            AlertMessage.showMessage(con, "ProCommentList", msg);
                        }

                    }
                });

            }

        });

    }


//    private boolean hasMorePage() {
//        if (feedResponse.getPaginator().getPageCount() > feedResponse.getPaginator().getPage()) {
//            return true;
//        }
//
//        return false;
//
//    }


    private boolean listIsAtTop() {
        if (homeStrickyList.getChildCount() == 0) {
            return true;
        }
        return homeStrickyList.getChildAt(0).getTop() == 0;
    }

    private void getDate(String dateString) {


    }


//    @Override
//    public void onScroll(AbsListView lw, int firstVisibleItem, int visibleItemCount, final int totalItemCount) {
//        Log.e("getFirstVisiblePosition", ">>" + homeStrickyList.getFirstVisiblePosition());
//        if (homeStrickyList.getFirstVisiblePosition() == 0) {
//            if (!firstCall) {
//                isFirst = true;
//                Log.e("first", "first");
//                if (!isFirstApiCall) {
//                    index = 1;
//                    callFeedAPI(AllURL.getFeedURL(PersistData.getStringData(con, AppConstant.USERTOKEN),
//                            PersistData.getStringData(con, AppConstant.USERID), "1", "10", PersistData.getStringData(con, AppConstant.GCMID)));
//                }
//            }
//
//        } else {
//            isFirstApiCall = false;
//            if (lvBusy_flag != 1) {
//                final int lastItem = firstVisibleItem + visibleItemCount;
//                if (lastItem == totalItemCount) {
//                    if (preLast != lastItem) {
//                        Log.d("Last", "Last");
//                        preLast = lastItem;
//                        if (hasMorePage()) {
//                            // call api if pageNum>1
//                            Log.e("last", "last");
//                            pageNum = pageNum + 1;
//                            isFirst = false;
//                            index = 1;
//                            callFeedAPI(AllURL.getFeedURL(PersistData.getStringData(con, AppConstant.USERTOKEN),
//                                    PersistData.getStringData(con, AppConstant.USERID), "" + pageNum, "10", PersistData.getStringData(con, AppConstant.GCMID)));
//
//                        }
//                    }
//                }
//            }
//        }
//    }

//    @Override
//    public void onScrollStateChanged(AbsListView view, int scrollState) {
//
//    }


}
