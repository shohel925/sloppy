package com.sloppy;

import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

class MySpinnerAdapter extends ArrayAdapter<String> {



	Typeface helveticaNeuRegular = Typeface.createFromAsset(getContext().getAssets(), "font/helveticaNeuRegular.ttf");
	Typeface helveticaNeueBold = Typeface.createFromAsset(getContext().getAssets(), "font/helveticaNeueBold.ttf");


	MySpinnerAdapter(Context context, int resource, List<String> items) {
		super(context, resource, items);
	}

	// Affects default (closed) state of the spinner
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		TextView view = (TextView) super.getView(position, convertView, parent);
		view.setTypeface(helveticaNeuRegular);
		return view;
	}

	// Affects opened state of the spinner
	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		TextView view = (TextView) super.getDropDownView(position, convertView,
				parent);
		view.setTypeface(helveticaNeuRegular);
		return view;
	}
}
