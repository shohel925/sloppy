package com.sloppy;


import android.app.DialogFragment;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sloppy.aapbdLib.AlertMessage;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.NetInfo;
import com.sloppy.aapbdLib.PersistData;
import com.sloppy.aapbdLib.PersistentUser;
import com.sloppy.model.CategoryInfo;
import com.sloppy.model.GenderInfo;
import com.sloppy.model.SearchProductResponse;
import com.sloppy.model.SearchUserResponse;
import com.sloppy.model.SubcategoryInfo;
import com.sloppy.utils.AAPBDHttpClient;
import com.sloppy.utils.AllURL;
import com.sloppy.utils.AppConstant;

import java.util.ArrayList;
import java.util.concurrent.Executors;

/**
 * Created by hp on 6/25/2016.
 */
public class SubCategoryFragment extends BaseFragment {

    /**
     * ---------------variable-------------------
     */
    Context con;
    private View mainView;
    private int allCount;
    private TextView tvSubWoman, tvSubman, tvSubSearch, tvNumber, tvCatName, tvItem, tvPeople;
    private ImageView imgSubMan, imgSubWoman, imgSubCategoryBac, imgSubCategoryBack, imgMenuSub,imageCros;
    private LinearLayout layoutSubWoMan, layoutSubMan,linearSubLUsearchBefore;
    private RelativeLayout linearSubLUsearchAfter;
    private EditText etSUBProName;
    private Typeface helveticaNeuRegular, helveticaNeueBold;
    private String subType,searchType,genType;
    private ListView listViewSubCategory;


    private SubCategoryAdapter subCategoryAdapter;
    private ArrayList<GenderInfo> subCategoryList;
    private SearchProductResponse searchProductResponse;
    private SearchUserResponse userResponse;
    /**
     * ------------------On Create------------------------------------
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        con = getActivity();
        mainView = inflater.inflate(R.layout.fragment_subcategory, container, false);
        initUI();
        return mainView;
    }

    private void initUI() {
        subCategoryList = new ArrayList<>();
        imageCros=(ImageView)mainView.findViewById(R.id.imageCros);
        linearSubLUsearchBefore=(LinearLayout) mainView.findViewById(R.id.linearSubLUsearchBefore);
        linearSubLUsearchAfter=(RelativeLayout) mainView.findViewById((R.id.linearSubLUsearchAfter));
        tvItem = (TextView) mainView.findViewById(R.id.tvItem);
        tvPeople = (TextView) mainView.findViewById(R.id.tvPeople);
        etSUBProName=(EditText) mainView.findViewById(R.id.etSUBProName);
        helveticaNeuRegular = Typeface.createFromAsset(getActivity().getAssets(), "font/helveticaNeuRegular.ttf");
        helveticaNeueBold = Typeface.createFromAsset(getActivity().getAssets(), "font/helveticaNeueBold.ttf");
        listViewSubCategory = (ListView) mainView.findViewById(R.id.listViewSubCategory);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.list_search_category, listViewSubCategory, false);
        tvNumber = (TextView) header.findViewById(R.id.tvNumber);
        tvItem.setTypeface(helveticaNeuRegular);
        tvPeople.setTypeface(helveticaNeuRegular);
        searchType="item";
        genType = "woman";
        linearSubLUsearchBefore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearSubLUsearchBefore.setVisibility(View.GONE);
                linearSubLUsearchAfter.setVisibility(View.VISIBLE);
                etSUBProName.requestFocus();
                AppConstant.OpenKeyBoard(con);
            }
        });
        etSUBProName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (!TextUtils.isEmpty(etSUBProName.getText().toString())) {
                        AppConstant.keyboardHide(con, mainView);
                        if (searchType.equalsIgnoreCase("item")) {
                            if (PersistentUser.isLogged(con)) {
                                callSearchProductAPI(etSUBProName.getText().toString(), "single", AppConstant.categoryID, genType, getResources().getString(R.string.page_items), "all", "");
                            } else {
                                callSearchProductAPI(etSUBProName.getText().toString(), "single", AppConstant.categoryID, genType, getResources().getString(R.string.page_items), "demo", "");
                            }
                        } else if (searchType.equalsIgnoreCase("people")){
                            if (PersistentUser.isLogged(con)) {
                                callSearchUserAPI(etSUBProName.getText().toString(),"all");
                            } else {
                                callSearchUserAPI(etSUBProName.getText().toString(),"demo");
                            }
                        }
                    }
                    return true;
                }
                return false;
            }
        });
        etSUBProName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(charSequence.toString())){
                    imageCros.setVisibility(View.GONE);
                }else {
                    imageCros.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        imageCros.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etSUBProName.setText("");
            }
        });

        tvItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchType="item";
                tvItem.setBackgroundResource(R.drawable.item_back_sweet);
                tvItem.setTextColor(getResources().getColor(R.color.grayText));
                tvPeople.setBackgroundResource(R.drawable.peaple_back_black);
                tvPeople.setTextColor(getResources().getColor(R.color.sweet));
            }
        });

        tvPeople.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchType = "people";
                tvItem.setBackgroundResource(R.drawable.item_back_black);
                tvItem.setTextColor(getResources().getColor(R.color.sweet));
                tvPeople.setBackgroundResource(R.drawable.peaple_back_sweet);
                tvPeople.setTextColor(getResources().getColor(R.color.grayText));
            }
        });
        listViewSubCategory.addHeaderView(header, null, false);
        listViewSubCategory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                for (int i = 0; i < parent.getChildCount(); i++) {
                    if (i == position) {
                        parent.getChildAt(i).setBackgroundColor(Color.BLUE);
                    } else {
                        parent.getChildAt(i).setBackgroundColor(Color.BLACK);
                    }
                }
            }
        });
        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.keyboardHide(con, mainView);
                AppConstant.subCategoryID = "";
                myCommunicator.addContentFragment(new ProductListBySubcategoryFragment(), true);
            }
        });
        imgMenuSub = (ImageView) mainView.findViewById(R.id.imgMenuSub);
        imgSubMan = (ImageView) mainView.findViewById(R.id.imgSubMan);
        imgSubWoman = (ImageView) mainView.findViewById(R.id.imgSubWoman);
        imgSubCategoryBac = (ImageView) mainView.findViewById(R.id.imgSubCategoryBac);
        imgSubCategoryBack = (ImageView) mainView.findViewById(R.id.imgSubCategoryBack);
        tvSubman = (TextView) mainView.findViewById(R.id.tvSubman);
        tvSubWoman = (TextView) mainView.findViewById(R.id.tvSubWoman);
        tvSubSearch = (TextView) mainView.findViewById(R.id.tvSubSearch);
        tvCatName = (TextView) mainView.findViewById(R.id.tvCatName);
        layoutSubMan = (LinearLayout) mainView.findViewById(R.id.layoutSubMan);
        layoutSubWoMan = (LinearLayout) mainView.findViewById(R.id.layoutSubWoMan);
        tvSubman.setTypeface(helveticaNeuRegular);
        tvSubWoman.setTypeface(helveticaNeuRegular);
        tvSubSearch.setTypeface(helveticaNeuRegular);
        tvSubSearch.setText(AppConstant.categoryName);
        tvCatName.setText(AppConstant.categoryName);
        imgMenuSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.showMenuDiadog(getActivity().getFragmentManager());
//                MenuFragmentDialog dialogMenu = new MenuFragmentDialog();
//                dialogMenu.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar);
//                dialogMenu.show(getActivity().getFragmentManager(), "");
            }
        });
        if (AppConstant.categoryType.equalsIgnoreCase("woman")) {
            genType = "woman";
            changeSubBg("woman");
            subType = "0";
            allCount = 0;
            if (subCategoryAdapter != null) {
                subCategoryAdapter.clear();
            }
                if (AppConstant.categoryInfo.getName().equalsIgnoreCase("Most Popular")){
                    allCount=Integer.parseInt(AppConstant.commonResponse.getMost_popular_count());
                }else{
                    subCategoryList.addAll(AppConstant.categoryInfo.getSubcategory().getWoman());
                        for (int j = 0; j < AppConstant.categoryInfo.getSubcategory().getWoman().size(); j++) {
                            allCount = allCount + Integer.parseInt(AppConstant.categoryInfo.
                                    getSubcategory().getWoman().get(j).getWoman_count());
                        }
                }


            tvNumber.setText(String.valueOf(allCount));
            subCategoryAdapter = new SubCategoryAdapter(con);
            listViewSubCategory.setAdapter(subCategoryAdapter);
            subCategoryAdapter.notifyDataSetChanged();


        } else if ((AppConstant.categoryType.equalsIgnoreCase("man"))) {
            genType = "man";
            changeSubBg("man");
            subType = "1";
            allCount = 0;
            if (subCategoryAdapter != null) {
                subCategoryAdapter.clear();
            }
                if (AppConstant.categoryInfo.getName().equalsIgnoreCase("Most Popular")){
                    allCount=Integer.parseInt(AppConstant.commonResponse.getMost_popular_count());
                }else{
                    subCategoryList.addAll(AppConstant.categoryInfo.getSubcategory().getMan());
                        for (int j = 0; j < AppConstant.categoryInfo.getSubcategory().getMan().size(); j++) {
                            allCount = allCount + Integer.parseInt(AppConstant.categoryInfo.getSubcategory().getMan().get(j).getMan_count());
                        }
                }


            tvNumber.setText(String.valueOf(allCount));
            //---------Call Sub category adapter-----------
            subCategoryAdapter = new SubCategoryAdapter(con);
            listViewSubCategory.setAdapter(subCategoryAdapter);
            subCategoryAdapter.notifyDataSetChanged();
        }


        imgSubCategoryBac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();

            }
        });
        imgSubCategoryBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        layoutSubMan.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AppConstant.categoryType = "man";
                changeSubBg("man");
                subType = "1";
                allCount = 0;
                // ---Adapter call-----
                if (subCategoryAdapter != null) {
                    subCategoryAdapter.clear();
                }
                if (AppConstant.categoryInfo.getName().equalsIgnoreCase("Most Popular")){
                    allCount=Integer.parseInt(AppConstant.commonResponse.getMost_popular_count());
                }else{
                    subCategoryList.addAll(AppConstant.categoryInfo.getSubcategory().getMan());
                    for (int i = 0; i < AppConstant.categoryInfo.getSubcategory().getMan().size(); i++) {
                        allCount = allCount + Integer.parseInt(AppConstant.categoryInfo.getSubcategory().getMan().get(i).getMan_count());

//                    if (AppConstant.commonResponse.getCategory().get(i).getName().equalsIgnoreCase(AppConstant.categoryName)) {
//                        for (int j = 0; j < AppConstant.commonResponse.getCategory().get(i).getSubcategory().size(); j++) {
//                            allCount = allCount + Integer.parseInt(AppConstant.commonResponse.getCategory().get(i).getSubcategory().get(j).getMan_count());
//                            if ((AppConstant.commonResponse.getCategory().get(i).getSubcategory().get(j).getType().equalsIgnoreCase("2")) ||
//                                    (AppConstant.commonResponse.getCategory().get(i).getSubcategory().get(j).getType().equalsIgnoreCase("1"))) {
////                                subCategoryList.add(AppConstant.commonResponse.getCategory().get(i).getSubcategory().get(j));
//                                Log.e("SubCategory", AppConstant.commonResponse.getCategory().get(i).getSubcategory().get(j).getName());
//                            }
//                        }
//                    }
                    }
                }

                tvNumber.setText(String.valueOf(allCount));
                subCategoryAdapter = new SubCategoryAdapter(con);
                listViewSubCategory.setAdapter(subCategoryAdapter);
                subCategoryAdapter.notifyDataSetChanged();
            }

        });

        layoutSubWoMan.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AppConstant.categoryType = "woman";
                changeSubBg("woman");
                subType = "0";
                allCount = 0;
                // ---Adapter call-----
                if (subCategoryAdapter != null) {
                    subCategoryAdapter.clear();
                }
                if (AppConstant.categoryInfo.getName().equalsIgnoreCase("Most Popular")){
                    allCount=Integer.parseInt(AppConstant.commonResponse.getMost_popular_count());
                }else{
                    subCategoryList.addAll(AppConstant.categoryInfo.getSubcategory().getWoman());
                    for (int i = 0; i < AppConstant.categoryInfo.getSubcategory().getWoman().size(); i++) {
                        allCount = allCount + Integer.parseInt(AppConstant.categoryInfo.getSubcategory().getWoman().get(i).getWoman_count());

//                    if (AppConstant.commonResponse.getCategory().get(i).getName().equalsIgnoreCase(AppConstant.categoryName)) {
//                        for (int j = 0; j < AppConstant.commonResponse.getCategory().get(i).getSubcategory().size(); j++) {
//                            allCount = allCount + Integer.parseInt(AppConstant.commonResponse.getCategory().get(i).getSubcategory().get(j).getMan_count());
//                            if ((AppConstant.commonResponse.getCategory().get(i).getSubcategory().get(j).getType().equalsIgnoreCase("2")) ||
//                                    (AppConstant.commonResponse.getCategory().get(i).getSubcategory().get(j).getType().equalsIgnoreCase("1"))) {
////                                subCategoryList.add(AppConstant.commonResponse.getCategory().get(i).getSubcategory().get(j));
//                                Log.e("SubCategory", AppConstant.commonResponse.getCategory().get(i).getSubcategory().get(j).getName());
//                            }
//                        }
//                    }
                    }
                }
                tvNumber.setText(String.valueOf(allCount));
                subCategoryAdapter = new SubCategoryAdapter(con);
                listViewSubCategory.setAdapter(subCategoryAdapter);
                subCategoryAdapter.notifyDataSetChanged();
            }

        });
    }

    protected void callSearchProductAPI(final String search_text, final String search_type, final String category_id, final String suitable_for, final String page_items,final  String user_type,final String subcategory_id) {
        /**
         * =====================Check Internet======================================================
         */
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }
        /**
         * =====================Start progress bar==================================================
         */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
        /**
         * =====================Start Thread========================================================
         */
        Executors.newSingleThreadExecutor().submit(new Runnable() {
            String msg = "";
            String response = "";

            @Override
            public void run() {

                try {
                    Log.e("Search Product URL", AllURL.getSearchProduct(search_text, search_type, category_id, suitable_for, page_items,user_type,subcategory_id));

                    response = AAPBDHttpClient.get(AllURL.getSearchProduct(search_text, search_type, category_id, suitable_for, page_items,user_type,subcategory_id)).//==API call====
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();
                    Gson gson = new Gson();
                    searchProductResponse = gson.fromJson(response, SearchProductResponse.class);
                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        /**
                         * ======stop progress bar==================================================
                         */
                        if (busy != null) {
                            busy.dismis();
                        }
                        /**
                         * ==============Here make UI Related work==================================
                         */
                        if (searchProductResponse.getStatus().equalsIgnoreCase("1")) {
                            Log.e("Search Product Response", ">>" + response);
//                            Toast.makeText(con, "ProductList By Sub" + proListBySubResponse.getMsg(), Toast.LENGTH_LONG).show();
                            AppConstant.productListTitel="Search Product";
                            AppConstant.productList=searchProductResponse.getResult().getData();
                            myCommunicator.addContentFragment(new CommonProductListFragment(), true);

//                            if (searchProductResponse.getResult().getData().size() > 0) {
//                                tvDataNotFound.setVisibility(View.GONE);
//                                rvCommonList.setVisibility(View.VISIBLE);
//                                /**
//                                 * =================Set Adapter===========================================================
//                                 */
//                                rvCommonList.setLayoutManager(new LinearLayoutManager(con));
//                                rvCommonList.setAdapter(new RecyclerProductListAdapter(con, searchProductResponse.getResult().getData()));
//
//                            } else {
//                                rvCommonList.setVisibility(View.GONE);
//                                tvDataNotFound.setVisibility(View.VISIBLE);
//                            }
                        } else {

                            msg = searchProductResponse.getMsg();
                            AlertMessage.showMessage(con, "My follow ", msg);

                        }

                    }
                });

            }

        });

    }

    protected void callSearchUserAPI(final String search_text,final String user_type) {
        /**
         * =====================Check Internet======================================================
         */
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }
        /**
         * =====================Start progress bar==================================================
         */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
        /**
         * =====================Start Thread========================================================
         */
        Executors.newSingleThreadExecutor().submit(new Runnable() {
            String msg = "";
            String response = "";

            @Override
            public void run() {

                try {
                    Log.e("Search User URL", AllURL.getSearchUserList(search_text,user_type));

                    response = AAPBDHttpClient.get(AllURL.getSearchUserList(search_text, user_type)).//==API call====
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();
                    Gson gson = new Gson();
                    userResponse = gson.fromJson(response, SearchUserResponse.class);
                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        /**
                         * ======stop progress bar==================================================
                         */
                        if (busy != null) {
                            busy.dismis();
                        }
                        /**
                         * ==============Here make UI Related work==================================
                         */
                        if (userResponse.getStatus().equalsIgnoreCase("1")) {
                            Log.e("Search User Response", ">>" + response);
                            AppConstant.productListTitel="Search User";
                            AppConstant.userList=userResponse.getResult().getData();
                            myCommunicator.addContentFragment(new SearchUserListFragment(), true);
                        } else {
                            msg = userResponse.getMsg();
                            AlertMessage.showMessage(con, "My follow ", msg);
                        }

                    }
                });

            }

        });

    }


    /*
     * ------------------Subcategory LiAdapter in sub category
	 */
    private class SubCategoryAdapter extends ArrayAdapter<GenderInfo> {
        Context context;

        SubCategoryAdapter(Context context) {
            super(context, R.layout.list_search_category,
                    subCategoryList);
            this.context = context;
        }

        @Override
        public View getView(final int position, View v, ViewGroup parent) {

            if (v == null) {
                final LayoutInflater vi = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.list_search_category, null);
            }

            if (position < subCategoryList.size()) {
                final GenderInfo query = subCategoryList.get(position);

                Log.e("Name", "" + query.toString());
                final TextView tvNumber = (TextView) v.findViewById(R.id.tvNumber);
                final TextView tvCategory = (TextView) v.findViewById(R.id.tvCategory);


                if (subType.equalsIgnoreCase("0")) {
//					if ((query.get(position).getType().equalsIgnoreCase("2"))|| (query.get(position).getType()
//									.equalsIgnoreCase("0"))) {
//
                    tvCategory.setText(query.getName());
                    tvNumber.setText(query.getWoman_count());
                    //}
                } else if (subType.equalsIgnoreCase("1")) {
                    tvCategory.setText(query.getName());
                    tvNumber.setText(query.getMan_count());
                }

                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AppConstant.keyboardHide(con, mainView);
                        AppConstant.subCategoryID = query.getSub_category_id();
                        myCommunicator.addContentFragment(new ProductListBySubcategoryFragment(), true);
                    }
                });
            }

            return v;
        }
    }


    //-----------------For SubCategory-----------------
    private void changeSubBg(String gender) {
        if (gender.equalsIgnoreCase("man")) {
            tvSubman.setTextColor(Color.parseColor("#FB9A7F"));
            imgSubMan.setImageResource(R.drawable.man_select);
            tvSubWoman.setTextColor(Color.parseColor("#000000"));
            imgSubWoman.setImageResource(R.drawable.women);
        } else if (gender.equalsIgnoreCase("woman")) {
            tvSubWoman.setTextColor(Color.parseColor("#FB9A7F"));
            imgSubWoman.setImageResource(R.drawable.women_select);
            tvSubman.setTextColor(Color.parseColor("#000000"));
            imgSubMan.setImageResource(R.drawable.man);
        }
    }

}
