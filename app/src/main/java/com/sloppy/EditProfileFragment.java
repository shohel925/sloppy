package com.sloppy;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.instagram.ApplicationData;
import com.instagram.InstagramApp;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.sloppy.aapbdLib.AlertMessage;
import com.sloppy.aapbdLib.BitmapUtils;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.NetInfo;
import com.sloppy.aapbdLib.PersistData;
import com.sloppy.aapbdLib.PersistentUser;


import com.sloppy.model.CountryResponse;
import com.sloppy.model.MyProfileResponse;
import com.sloppy.model.ProfileUpdateResponse;
import com.sloppy.utils.AllURL;
import com.sloppy.utils.AppConstant;
import com.sloppy.utils.ReadTxtFile;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import cn.jeesoft.widget.pickerview.CharacterPickerWindow;
import cz.msebera.android.httpclient.Header;

public class EditProfileFragment extends BaseFragment{
	Context con;
	//------------Integer Variable-------------------
	private final int galarytakid = 1;
	private final int imagecaptureid = 0;
	//----------------String Variable--------------------
	private String first_name,last_name,Username,email,gender,country,short_bio,password,
			current_password,instagram_id,currency,paypal_email;
	private TextView tveEditPro,tvAddInsragram,tvProImg,tvUpPhoto,tvSaveChanges,tvGender,tvCuntry;
	private ImageView imgProfileImageShow,imgProfileImage;
	private EditText etFirstName,etLastName,etEmail,etUserName,etNewPasword,etShortBio,etPaupalEmail;
	private Spinner genderSpinner,countrySpinner;
	private View view;
	private Dialog dialog;
	private String imageLocal = "";
	private File file,profile_image;
	private Typeface helveticaNeuRegular, helveticaNeueBold;
	private static  File dir = null;
	ProfileUpdateResponse profileUpdateResponse;
	MyProfileResponse newMyProResponse;
	private LinearLayout imgInstagramLayout;
	private InstagramApp mApp;
	private HashMap<String, String> userInfoHashmap = new HashMap<String, String>();
	private EditText etPaswordPrevious;
	private RelativeLayout rlGender,rlCuntry;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		con=getActivity();
		view= inflater.inflate(R.layout.fragment_edit_profile, container,false);
		initUI();
		return  view;
	}

	private Handler handler = new Handler(new Handler.Callback() {

		@Override
		public boolean handleMessage(Message msg) {
			if (msg.what == InstagramApp.WHAT_FINALIZE) {
				userInfoHashmap = mApp.getUserInfo();
				if (userInfoHashmap!=null){
					PersistData.setStringData(con,AppConstant.socialId,userInfoHashmap.get(InstagramApp.TAG_ID));
					String fullName=userInfoHashmap.get(InstagramApp.TAG_FULL_NAME);
					if (!TextUtils.isEmpty(fullName)){
						String lastName = fullName.substring(fullName.lastIndexOf(" ")+1);
						String firstName = fullName.substring(0, fullName.lastIndexOf(' '));
						PersistData.setStringData(con, AppConstant.first_name, firstName);
						PersistData.setStringData(con, AppConstant.last_name, lastName);
					}
					if (!TextUtils.isEmpty(userInfoHashmap.get(InstagramApp.TAG_USERNAME))){
						tvAddInsragram.setText(userInfoHashmap.get(InstagramApp.TAG_USERNAME));
					}
					PersistData.setStringData(con, AppConstant.insagramUserName, userInfoHashmap.get(InstagramApp.TAG_USERNAME));
				}

			} else if (msg.what == InstagramApp.WHAT_FINALIZE) {
				Toast.makeText(getActivity(), "Check your network.",
						Toast.LENGTH_SHORT).show();
			}
			return false;
		}
	});

	private void initUI() {
		tvGender = (TextView) view.findViewById(R.id.tvGender);
		tvCuntry = (TextView) view.findViewById(R.id.tvCuntry);
		rlGender = (RelativeLayout) view.findViewById(R.id.rlGender);
		rlCuntry = (RelativeLayout) view.findViewById(R.id.rlCuntry);
		etPaswordPrevious=(EditText)view.findViewById(R.id.etPaswordPrevious);
		tveEditPro=(TextView) view.findViewById(R.id.tveEditPro);
		tvAddInsragram=(TextView) view.findViewById(R.id.tvAddInsragram);
		tvProImg=(TextView) view.findViewById(R.id.tvProImg);
		tvUpPhoto=(TextView) view.findViewById(R.id.tvUpPhoto);
		tvSaveChanges=(TextView) view.findViewById(R.id.tvSaveChanges);

		ImageView imageviewEditBack=(ImageView) view.findViewById(R.id.imageviewEditBack);
		ImageView imgMenuEdit=(ImageView) view.findViewById(R.id.imgMenuEdit);
		imgProfileImageShow=(ImageView) view.findViewById(R.id.imgProfileImageShow);
		imgProfileImage=(ImageView) view.findViewById(R.id.imgProfileImage);

		 etFirstName= (EditText)view.findViewById(R.id.etFirstName);
		 etLastName= (EditText)view.findViewById(R.id.etLastName);
		 etEmail= (EditText)view.findViewById(R.id.etEmail);
		etPaupalEmail= (EditText)view.findViewById(R.id.etPaupalEmail);
		 etUserName= (EditText)view.findViewById(R.id.etUserName);
		etNewPasword= (EditText)view.findViewById(R.id.etNewPasword);
		 etShortBio= (EditText)view.findViewById(R.id.etShortBio);

		//--------Gender and country spinner------------------
//		genderSpinner=(Spinner) view.findViewById(R.id.genderSpnnier);
//		countrySpinner=(Spinner) view.findViewById(R.id.spinnerCountry);
		imgInstagramLayout=(LinearLayout)view.findViewById(R.id.imgInstagramLayout);
		imgInstagramLayout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				mApp.resetAccessToken();
				CookieSyncManager cookieSyncMngr = CookieSyncManager.createInstance(con);
				CookieManager cookieManager = CookieManager.getInstance();
				cookieManager.removeAllCookie();
				mApp.authorize();
			}
		});


		/**
		 * ----------------------Font set------------------------------------
		 */
		helveticaNeuRegular = Typeface.createFromAsset(getActivity().getAssets(), "font/helveticaNeuRegular.ttf");
		helveticaNeueBold = Typeface.createFromAsset(getActivity().getAssets(), "font/helveticaNeueBold.ttf");
		tveEditPro.setTypeface(helveticaNeuRegular);
		etFirstName.setTypeface(helveticaNeuRegular);
		etLastName.setTypeface(helveticaNeuRegular);
		etEmail.setTypeface(helveticaNeuRegular);
		etUserName.setTypeface(helveticaNeuRegular);
		etNewPasword.setTypeface(helveticaNeuRegular);
		etShortBio.setTypeface(helveticaNeuRegular);
		tvAddInsragram.setTypeface(helveticaNeuRegular);
		tvProImg.setTypeface(helveticaNeuRegular);
		tvUpPhoto.setTypeface(helveticaNeuRegular);
		tvSaveChanges.setTypeface(helveticaNeuRegular);

		final List<String> genderlist = new ArrayList<String>();
		if (genderlist != null) {
			genderlist.clear();
		}
		genderlist.add("Male");
		genderlist.add("Female");

		rlGender.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				AppConstant.keyboardHide(con,rlGender);
				final CharacterPickerWindow windows = new CharacterPickerWindow(con, "Select Gender");
				windows.showAtLocation(v, Gravity.BOTTOM, 0, 0);
//                AppConstant.keyboardHide(con,);
				windows.setPicker(genderlist);
				windows.setWidth(200);
				windows.setHeight(200);
				windows.setOnoptionsSelectListener(new CharacterPickerWindow.OnOptionsSelectListener() {
					@Override
					public void onOptionsSelect(int position, int option2, int options3) {
						tvGender.setText(genderlist.get(position));
						tvGender.setTextColor(getResources().getColor(R.color.black));
					}
				});
			}
		});

		// ----------Country List----------
		final List<String> countrylist = new ArrayList<String>();

		String countryJson = ReadTxtFile.readRawTextFile(con, R.raw.country);
		Gson g = new Gson();
		CountryResponse mCountryResponse = g.fromJson(countryJson, CountryResponse.class);
		for (int i = 0; i < mCountryResponse.getCountries().size(); i++) {
			countrylist.add(mCountryResponse.getCountries().get(i).getName());
		}

		rlCuntry.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				AppConstant.keyboardHide(con,rlCuntry);
				final CharacterPickerWindow windows = new CharacterPickerWindow(con, "Select a Country");
				windows.showAtLocation(v, Gravity.BOTTOM, 0, 0);
				windows.setPicker(countrylist);
				windows.setWidth(200);
				windows.setHeight(200);
				windows.setOnoptionsSelectListener(new CharacterPickerWindow.OnOptionsSelectListener() {
					@Override
					public void onOptionsSelect(int position, int option2, int options3) {
						tvCuntry.setText(countrylist.get(position));
						tvCuntry.setTextColor(getResources().getColor(R.color.black));
					}
				});
			}
		});

		// =======  for Instagram login ==================

		mApp = new InstagramApp(getActivity(), ApplicationData.CLIENT_ID,
				ApplicationData.CLIENT_SECRET, ApplicationData.CALLBACK_URL);
		mApp.setListener(new InstagramApp.OAuthAuthenticationListener() {

			@Override
			public void onSuccess() {
				mApp.fetchUserName(handler);
			}

			@Override
			public void onFail(String error) {
				Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
			}
		});

		imageviewEditBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				getActivity().onBackPressed();
			}
		});

		imgMenuEdit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				AppConstant.showMenuDiadog(getActivity().getFragmentManager());
			}
		});

		//------------------Data get from Persist response-----------------------------------

		newMyProResponse = g.fromJson(PersistData.getStringData(con, AppConstant.myProfileResponceAC), MyProfileResponse.class);
		etFirstName.setText(newMyProResponse.getUser_details().getFirst_name());
		etLastName.setText(newMyProResponse.getUser_details().getLast_name());
		etUserName.setText(newMyProResponse.getUser_details().getUsername());
		etEmail.setText(newMyProResponse.getUser_details().getEmail());
		etPaupalEmail.setText(newMyProResponse.getUser_details().getPaypal_email());
		if (!TextUtils.isEmpty(newMyProResponse.getUser_details().getInstagram_username())){
			tvAddInsragram.setText(newMyProResponse.getUser_details().getInstagram_username());
		}
		if (!TextUtils.isEmpty(newMyProResponse.getUser_details().getGender())) {
			tvGender.setText(newMyProResponse.getUser_details().getGender());
			tvGender.setTextColor(getResources().getColor(R.color.black));
		}
		if (!TextUtils.isEmpty(newMyProResponse.getUser_details().getCountry())) {
			tvCuntry.setText(newMyProResponse.getUser_details().getCountry());
			tvCuntry.setTextColor(getResources().getColor(R.color.black));
		}
//		makeGenderSpinnner(con, genderSpinner);
//		createCountrySpinnner(con, countrySpinner);
		if (newMyProResponse.getUser_details().getShort_bio()!=null) {
			etShortBio.setText(newMyProResponse.getUser_details().getShort_bio());
		}

		if (newMyProResponse.getUser_details().getProfile_image()!= null) {
			imgProfileImage.setVisibility(View.GONE);
			imgProfileImageShow.setVisibility(View.VISIBLE);

			Picasso.with(con)
					.load(newMyProResponse.getUser_details().getProfile_image())
					.placeholder(R.drawable.ic_launcher)
					.error(R.drawable.ic_launcher)
					.into(imgProfileImageShow);
		}


//------------------Image capture-------------------------------------------------
		LinearLayout layoutCamera=(LinearLayout) view.findViewById(R.id.layoutCamera);
		layoutCamera.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				captureImageDialoge();
			}
		});

		tvSaveChanges.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				first_name = etFirstName.getText().toString();
				last_name = etLastName.getText().toString();
				Username = etUserName.getText().toString();
				email = etEmail.getText().toString();
				paypal_email=etPaupalEmail.getText().toString();
				short_bio=etShortBio.getText().toString();
				current_password = etPaswordPrevious.getText().toString();
				password=etNewPasword.getText().toString();

				if (tvGender.getText().toString().equalsIgnoreCase("Gender")){
					AppConstant.alertDialoag(con,getResources().getString(R.string.invalid_info),getResources().getString(R.string.select_gender),getResources().getString(R.string.close));
				}else if (tvCuntry.getText().toString().equalsIgnoreCase("Country")){
					AppConstant.alertDialoag(con,getResources().getString(R.string.invalid_info),getResources().getString(R.string.select_country),getResources().getString(R.string.close));
					return;
				}else {
					country=tvCuntry.getText().toString();
					gender=tvGender.getText().toString();
					profileUpdateAPI(AllURL.profileUpdateURL());
				}


			}
		});

	}

	private void profileUpdateAPI(final String url) {
		//--- for net check-----
		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
			return;
		}

		final BusyDialog busyNow = new BusyDialog(con, true, false);
		busyNow.show();

		final AsyncHttpClient client = new AsyncHttpClient();
		client.addHeader("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token));
		Log.e("token", PersistData.getStringData(con, AppConstant.token));

		final RequestParams param = new RequestParams();


		try {
			Log.e("First name", first_name);
			param.put("first_name", first_name);
			param.put("last_name", last_name);
			param.put("Username", Username);
			param.put("email", email);
			param.put("paypal_email", paypal_email);
			param.put("gender", gender);
			param.put("country", country);
			param.put("short_bio", short_bio);
			if (!TextUtils.isEmpty(userInfoHashmap.get(InstagramApp.TAG_ID))){
				param.put("instagram_id",userInfoHashmap.get(InstagramApp.TAG_ID));
			}
			if (!TextUtils.isEmpty(userInfoHashmap.get(InstagramApp.TAG_USERNAME))){
				param.put("instagram_username",PersistData.getStringData(con,AppConstant.insagramUserName));
			}
			if (password != null) {
				param.put("password", password);
			}
			if (current_password != null) {
				param.put("current_password", current_password);
			}
			if (profile_image != null) {
				param.put("profile_image", profile_image);
			}
			param.put("currency", currency);


		} catch (final Exception e1) {
			e1.printStackTrace();
		}

		client.post(url, param, new AsyncHttpResponseHandler() {

			@Override
			public void onStart() {

			}

			@Override
			public void onSuccess(int statusCode, Header[] headers,
								  byte[] response) {

				if (busyNow != null) {
					busyNow.dismis();
				}

				Log.e("resposne ", ">>" + new String(response));

				Gson g = new Gson();
				profileUpdateResponse = g.fromJson(new String(response), ProfileUpdateResponse.class);


				if (profileUpdateResponse.getStatus().equalsIgnoreCase("1")) {

					PersistentUser.setLogin(con);
					Toast.makeText(con, profileUpdateResponse.getMsg() + "", Toast.LENGTH_LONG).show();


				} else {
					AppConstant.alertDialoag(con,"Alert",profileUpdateResponse.getMsg(),"OK");
//					AlertMessage.showMessage(con, "Status", profileUpdateResponse.getMsg() + "");
					return;
				}

			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
								  byte[] errorResponse, Throwable e) {
				// called when response HTTP status is "4XX" (eg. 401, 403, 404)

				Log.e("errorResponse", new String(errorResponse));

				if (busyNow != null) {
					busyNow.dismis();
				}
			}

			@Override
			public void onRetry(int retryNo) {
				// called when request is retried

			}
		});

	}

	// ---------------------Capture Image dialog show method----------------
	private void captureImageDialoge(){
		dialog = new Dialog(con);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialoge_image_capture);

		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

		LinearLayout llayTakePhoto = (LinearLayout) dialog.findViewById(R.id.llTakePhoto);
		LinearLayout llUploadPhoto = (LinearLayout) dialog.findViewById(R.id.llUploadPhoto);
		ImageView imgdialogeCross = (ImageView) dialog.findViewById(R.id.imgdialogeCross);


		// -------Get picture Using Camera--------

		llayTakePhoto.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				if ( ActivityCompat.checkSelfPermission(con, Manifest.permission.CAMERA)
						!= PackageManager.PERMISSION_GRANTED) {
					ActivityCompat.requestPermissions((Activity) con,
							new String[]{Manifest.permission.CAMERA}, AppConstant.CAMERA_RUNTIME_PERMISSION);
					dialog.dismiss();
				}else{
					AppConstant.isGallery=false;
					if ( ActivityCompat.checkSelfPermission(con, Manifest.permission.WRITE_EXTERNAL_STORAGE)
							!= PackageManager.PERMISSION_GRANTED) {
						ActivityCompat.requestPermissions((Activity) con,
								new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, AppConstant.WRITEEXTERNAL_PERMISSION_RUNTIME);
						dialog.dismiss();
					}else{
						final Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
						startActivityForResult(i, imagecaptureid);
						dialog.dismiss();
					}
				}

			}
		});

		// --------- Get picture from Gallery-----------------
		llUploadPhoto.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				AppConstant.isGallery=true;
				if ( ActivityCompat.checkSelfPermission(con, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
						!= PackageManager.PERMISSION_GRANTED) {
					ActivityCompat.requestPermissions((Activity) con,
							new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, AppConstant.WRITEEXTERNAL_PERMISSION_RUNTIME);
					dialog.dismiss();
				}else{
					final Intent intent = new Intent();
					intent.setType("image/*");
					intent.setAction(Intent.ACTION_GET_CONTENT);
					startActivityForResult(Intent.createChooser(intent,"Select Picture"), galarytakid);
					dialog.dismiss();
				}
			}
		});

		imgdialogeCross.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				dialog.dismiss();
			}
		});

		dialog.show();

	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		if (requestCode == AppConstant.CAMERA_RUNTIME_PERMISSION) {
			if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				// Now user should be able to use camera

				if ( ActivityCompat.checkSelfPermission(con, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
						!= PackageManager.PERMISSION_GRANTED) {
					ActivityCompat.requestPermissions((Activity) con,
							new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, AppConstant.WRITEEXTERNAL_PERMISSION_RUNTIME);
				}else{
					final Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					startActivityForResult(i, imagecaptureid);
				}
			} else {
				// Your app will not have this permission. Turn off all functions
				// that require this permission or it will force close like your
				// original question
			}
		}else if (requestCode==AppConstant.WRITEEXTERNAL_PERMISSION_RUNTIME){
			if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				if (AppConstant.isGallery){
					final Intent intent = new Intent();
					intent.setType("image/*");
					intent.setAction(Intent.ACTION_GET_CONTENT);
					startActivityForResult(Intent.createChooser(intent,"Select Picture"), galarytakid);
				}else {
					final Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					startActivityForResult(i, imagecaptureid);
				}
			}
		}
	}


	//-------------------------Save Picture------------------------------------
	@Override
	public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
		super.onActivityResult(requestCode, resultCode, data);


		if (requestCode == galarytakid && resultCode == Activity.RESULT_OK) {

			// Log.e("In gallelrly", "lllll..........");
			try {

				final Uri selectedImageUri = data.getData();

				final Bitmap bitmap = BitmapFactory.decodeStream(con.getContentResolver().openInputStream(selectedImageUri));

				final String path = setToImageView(bitmap);
				Log.e("Profile Image Path ", ">>>>>" + path);
			//------Convert from path to File-----------------------
				profile_image= new File(path);

				//PersistData.setStringData(con, AppConstant.path, path);

				imgProfileImageShow.setImageBitmap(bitmap);
				//----------for image show on new image view---------------
				imgProfileImage.setVisibility(View.GONE);
				imgProfileImageShow.setVisibility(View.VISIBLE);

			}
			catch (final Exception e) {
				return;
			}

		}
		else if (requestCode == imagecaptureid && resultCode == Activity.RESULT_OK) {

			try {

				final Bundle extras = data.getExtras();
				final Bitmap b = (Bitmap) extras.get("data");

				final String path = setToImageView(b);
				Log.e("Profile Image Path ", ">>>>>" + path);
				//------Convert from path to File-----------------------
				profile_image= new File(path);

				imgProfileImageShow.setImageBitmap(b);

				imgProfileImage.setVisibility(View.GONE);
				imgProfileImageShow.setVisibility(View.VISIBLE);

			}
			catch (final Exception e) {
				return;
			}

		}

	}

	private String setToImageView(Bitmap bitmap) {

		try {
			final Bitmap bit = BitmapUtils.getResizedBitmap(bitmap, 600);
			final double time = System.currentTimeMillis();

			imageLocal = saveBitmapIntoSdcard(bit, "ebuser" + time + ".png");

			Log.e("camera saved URL :  ", " " + imageLocal);

		}
		catch (final IOException e) {
			e.printStackTrace();

			imageLocal = "";
			Log.e("camera saved URL :  ", e.toString());

		}

		return imageLocal;

	}


	private String saveBitmapIntoSdcard(Bitmap bitmap22, String filename) throws IOException {
		/*
		 *
		 * check the path and create if needed
		 */
		createBaseDirctory();

		try {

			new Date();

			OutputStream out = null;
			file = new File(dir, "/" + filename);

			if (file.exists()) {
				file.delete();
			}

			out = new FileOutputStream(file);

			bitmap22.compress(Bitmap.CompressFormat.PNG, 100, out);

			out.flush();
			out.close();
			// Toast.makeText(context, "Saved", Toast.LENGTH_SHORT).show();
			return file.getAbsolutePath();
		}
		catch (final Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public void createBaseDirctory() {
		final String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
		dir = new File(extStorageDirectory + "/sloppy");

		if (dir.mkdir()) {
			System.out.println("Directory created");
		}
		else {
			System.out.println("Directory is not created or exists");
		}
	}


}
