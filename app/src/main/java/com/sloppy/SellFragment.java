package com.sloppy;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.google.gson.Gson;
import com.isseiaoki.simplecropview.CropImageView;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.sloppy.aapbdLib.AlertMessage;
import com.sloppy.aapbdLib.BitmapUtils;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.NetInfo;
import com.sloppy.aapbdLib.PersistData;
import com.sloppy.aapbdLib.PersistentUser;
import com.sloppy.aapbdLib.StartActivity;
import com.sloppy.model.AddProductResponse;
import com.sloppy.model.BrandInfo;
import com.sloppy.model.GenderInfo;
import com.sloppy.model.HashtagInfo;
import com.sloppy.model.ProductAndUserDetailsInfo;
import com.sloppy.utils.AllURL;
import com.sloppy.utils.AppConstant;
import com.squareup.picasso.Picasso;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v13.app.FragmentCompat;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.TextView;

import org.w3c.dom.Text;

import cn.jeesoft.widget.pickerview.CharacterPickerWindow;
import co.lujun.androidtagview.TagContainerLayout;
import co.lujun.androidtagview.TagView;
import cz.msebera.android.httpclient.Header;


public class SellFragment extends Fragment {

    private Context con;
    private View view;

    //------------Integer--------------
    private int categoryPosition, subCategoryPosition, suitablePosition,
            category_id, sub_category_id, brand_id, suitable_for, position, product_id;
    public final int imagegalaryid = 1;
    public final int imagecaptureid = 0;
    //-------------String-------------
    private String capture, name, description, new_tag, delivery_type, brand_name,
            delivery_charge, hashtags, orginal_price, listing_price, size, path, color_code, firstColor;
    private String imageLocal = "";
    //--------------Boolean----------------
    private boolean delivery = false;
    //-------------View-----------
    private TextView tvYes, tvNo, tvSellNow, tvSell2, tvAboutpro, tvProductnew, tvHashtags, tvCategory, tvSubCategory,
            tvSize, tvDelivery, tvNewwith, tvMeetIn, tvSaveDraft, tvColorSell, tvColor2, tvSuitableFor;
    private ImageView imagePlus1, imagePlus2, imagePlus3, imagePlus4, imageCross1, imageCross2, imgSellBack, imgMenu,
            imageCross3, imageCross4, imgCrossOP, imgCrossLP, imgShipping2, imgMeetIn2, imgColor1, imgColorCross, imgSellCross,
            imgCurrencyIcon, imgCurrencyOri, imgCurrencyLis, imgInternationalShipping2;
    private EditText etProName, etProDescription, etOriginalPrice,
            etListingPrice, etPriceforShipping;
    private AutoCompleteTextView autoComTVBrand;
    //----------Layout----------------
    private LinearLayout lLPriceFor, llSave;
    private RelativeLayout relativeMeetIn, relativeShipping, relativeInternational, rlColor, rlSuitableFor, rlSize, rlCategory, rlSubCategory;
    //----------File--------------------
    private File file, photo_one, photo_two, photo_three, photo_four;
    private static File dir = null;
    private ProductAndUserDetailsInfo productDetails;
    //---------------Class-------------------------
    private List<String> catagoryList = new ArrayList<String>();
    private List<String> subCategoryList = new ArrayList<String>();
    private List<String> suitableList = new ArrayList<String>();
    private List<String> brandnameList = new ArrayList<String>();
    private List<String> hashtagList = new ArrayList<String>();
    private List<String> sizesList = new ArrayList<String>();
    private List<String> colorlist;
    DatabaseHandler db;
    private AddProductResponse addProductResponse;
    //-------------Other----------------------
    private Dialog dialog;
    private Spinner categorySpnnier, suitableSpinner, subCategorySpnnier;

    private Typeface helveticaNeuRegular, helveticaNeueBold;
    private AddColorAdapter colorAdapter;
    private EditText edittextHashtag;
    private List<String> selectedHastag = new ArrayList<>();
    private TagContainerLayout addedHashTag;

    /*
     * -------------------On Create Method---------------------
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        con = getActivity();
    /*
     * -----------Dialog show, user is activity_login or not--------------
	 */
        if (!PersistentUser.isLogged(con)) {
            AppConstant.loginDialoag(con);
        } else {
            view = inflater.inflate(R.layout.activity_sell2, container, false);
            initUI(view);
            db = new DatabaseHandler(con);
        }
        return view;
    }

    private void initUI(View v) {
        //--------------Text View--------------
        addedHashTag = (TagContainerLayout) v.findViewById(R.id.addedHashTag);
        addedHashTag.setOnTagClickListener(new TagView.OnTagClickListener() {
            @Override
            public void onTagClick(int position, String text) {
                selectedHastag.remove(position);
                addedHashTag.removeTag(position);
            }

            @Override
            public void onTagLongClick(int position, String text) {

            }
        });


        selectedHastag.clear();
        edittextHashtag = (EditText) v.findViewById(R.id.edittextHashtag);
        tvSell2 = (TextView) v.findViewById(R.id.tvSell2);
        tvCategory = (TextView) v.findViewById(R.id.tvCategory);
        tvMeetIn = (TextView) v.findViewById(R.id.tvMeetIn);
        tvSubCategory = (TextView) v.findViewById(R.id.tvSubCategory);
        tvSaveDraft = (TextView) v.findViewById(R.id.tvSaveDraft);
        tvSuitableFor = (TextView) v.findViewById(R.id.tvSuitableFor);
        tvAboutpro = (TextView) v.findViewById(R.id.tvAboutpro);
        tvProductnew = (TextView) v.findViewById(R.id.tvProductnew);
        tvColorSell = (TextView) v.findViewById(R.id.tvColorSell);
        tvHashtags = (TextView) v.findViewById(R.id.tvHashtags);
        tvDelivery = (TextView) v.findViewById(R.id.tvDelivery);
        tvYes = (TextView) v.findViewById(R.id.tvYes);
        tvSellNow = (TextView) v.findViewById(R.id.tvSellNow);
        tvNo = (TextView) v.findViewById(R.id.tvNo);
        tvNewwith = (TextView) v.findViewById(R.id.tvNewwith);
        tvColor2 = (TextView) v.findViewById(R.id.tvColor2);

        autoComTVBrand = (AutoCompleteTextView) v.findViewById(R.id.autoComTVBrand);

        //--------Edit Text----------------
        etProName = (EditText) v.findViewById(R.id.etProName);
        etProDescription = (EditText) v.findViewById(R.id.etProDescription);
        tvSize = (TextView) v.findViewById(R.id.tvSize);
        etOriginalPrice = (EditText) v.findViewById(R.id.etOriginalPrice);
        etListingPrice = (EditText) v.findViewById(R.id.etListingPrice);
        etPriceforShipping = (EditText) v.findViewById(R.id.etPriceforShipping);

        //-------------Image View---------------
        imgMeetIn2 = (ImageView) v.findViewById(R.id.imgMeetIn2);
        imgColor1 = (ImageView) v.findViewById(R.id.imgColor1);
        imgSellBack = (ImageView) v.findViewById(R.id.imgSellBack);
        imgMenu = (ImageView) v.findViewById(R.id.imgMenu);
        imgShipping2 = (ImageView) v.findViewById(R.id.imgShipping2);
        imgColorCross = (ImageView) v.findViewById(R.id.imgColorCross);
        imgInternationalShipping2 = (ImageView) v.findViewById(R.id.imgInternationalShipping2);
        imgCrossOP = (ImageView) v.findViewById(R.id.imgCrossOP);
        imgCrossLP = (ImageView) v.findViewById(R.id.imgCrossLP);
        imagePlus1 = (ImageView) v.findViewById(R.id.imagePlus1);
        imagePlus2 = (ImageView) v.findViewById(R.id.imagePlus2);
        imagePlus3 = (ImageView) v.findViewById(R.id.imagePlus3);
        imagePlus4 = (ImageView) v.findViewById(R.id.imagePlus4);
        imageCross1 = (ImageView) v.findViewById(R.id.imageCross1);
        imageCross2 = (ImageView) v.findViewById(R.id.imageCross2);
        imageCross3 = (ImageView) v.findViewById(R.id.imageCross3);
        imageCross4 = (ImageView) v.findViewById(R.id.imageCross4);
        imgCurrencyIcon = (ImageView) v.findViewById(R.id.imgCurrencyIcon);
        imgCurrencyOri = (ImageView) v.findViewById(R.id.imgCurrencyOri);
        imgCurrencyLis = (ImageView) v.findViewById(R.id.imgCurrencyLis);
        imgSellCross = (ImageView) view.findViewById(R.id.imgSellCross);
        //--------------Linear Layout---------------
        lLPriceFor = (LinearLayout) v.findViewById(R.id.lLPriceFor);
        //---------------Relative Layout-------------------
        relativeMeetIn = (RelativeLayout) v.findViewById(R.id.relativeMeetIn);
        rlSize = (RelativeLayout) v.findViewById(R.id.rlSize);
        rlCategory = (RelativeLayout) v.findViewById(R.id.rlCategory);
        rlSubCategory = (RelativeLayout) v.findViewById(R.id.rlSubCategory);


        relativeShipping = (RelativeLayout) v.findViewById(R.id.relativeShipping);
        relativeInternational = (RelativeLayout) v.findViewById(R.id.relativeInternational);
        rlSuitableFor = (RelativeLayout) v.findViewById(R.id.rlSuitableFor);
        rlColor = (RelativeLayout) v.findViewById(R.id.rlColor);

        //============Spinner===================================
        suitableSpinner = (Spinner) v.findViewById(R.id.suitableSpinner);
        categorySpnnier = (Spinner) v.findViewById(R.id.categorySpnnier);
        edittextHashtag.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    boolean isExist = false;
                    for (String name : selectedHastag) {
                        if (name.equalsIgnoreCase(edittextHashtag.getText().toString())) {
                            isExist = true;
                            break;
                        } else {
                            isExist = false;
                        }
                    }
                    if (!isExist) {
                        addedHashTag.addTag(edittextHashtag.getText().toString());
                        selectedHastag.add(edittextHashtag.getText().toString());
                    } else {
                        Toast.makeText(con, "This Hashtag already Exist", Toast.LENGTH_SHORT).show();
                    }
                    return true;
                }
                return false;
            }
        });

        edittextHashtag.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(final CharSequence charSequence, int i, int i1, int i2) {

                if (!TextUtils.isEmpty(charSequence.toString())) {
                    Handler handler=new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            hashtagList.clear();
                            for (int j = 0; j < AppConstant.commonResponse.getHashtags().size(); j++) {
                                if (AppConstant.commonResponse.getHashtags().get(j).getName().contains(charSequence.toString())) {
                                    hashtagList.add(AppConstant.commonResponse.getHashtags().get(j).getName());//Add Hash tag list
                                }
                                Log.e("Hashtag Size", ":" + hashtagList.size());
                            }
                            if (hashtagList.size() > 0) {
                                tagDialog(hashtagList);
                            }
                        }
                    },500);

                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        //---------------Font set--------------
        fontSet();

        //=======Data set==================
        AppConstant.path1 = "";
        AppConstant.path2 = "";
        AppConstant.path3 = "";
        AppConstant.path4 = "";
        crossClick();
        if (AppConstant.currencyProfile.equalsIgnoreCase("eur")) {
            imgCurrencyIcon.setImageResource(R.drawable.eur_icon);
            imgCurrencyOri.setImageResource(R.drawable.eur);
            imgCurrencyLis.setImageResource(R.drawable.eur);
        } else if (AppConstant.currencyProfile.equalsIgnoreCase("usd")) {
            imgCurrencyIcon.setImageResource(R.drawable.usd_icon);
            imgCurrencyOri.setImageResource(R.drawable.usd);
            imgCurrencyLis.setImageResource(R.drawable.usd);
        } else if (AppConstant.currencyProfile.equalsIgnoreCase("gbp")) {
            imgCurrencyIcon.setImageResource(R.drawable.gbd_icon);
            imgCurrencyOri.setImageResource(R.drawable.gbd);
            imgCurrencyLis.setImageResource(R.drawable.gbd);
        } else if (AppConstant.currencyProfile.equalsIgnoreCase("rub")) {
            imgCurrencyIcon.setImageResource(R.drawable.rub_icon);
            imgCurrencyOri.setImageResource(R.drawable.rub);
            imgCurrencyLis.setImageResource(R.drawable.rub);
        }
        //===========New Tag==================
        setNewTag("no");//for new tag selected
        deliveryType("men");//for delivery type selected

        //=========On click==============

        onClick();
        forImageCapture();
        forSuitableFor();
        forCategory();
        forSubCategory();
        forBrand();
        forSize();
        forColor();
        forSaveDraft();
        forSell();
        edit();

        imgSellCross.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                removeDataDialoag(con);
            }
        });

    }

    private void removeDataDialoag(final Context con) {
        final Dialog dialogLogin = new Dialog(con);
        dialogLogin.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLogin.setContentView(R.layout.dialog_login);
        dialogLogin.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialogLogin.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView tvTitel2 = (TextView) dialogLogin.findViewById(R.id.tvTitel2);
        TextView tvDescription2 = (TextView) dialogLogin.findViewById(R.id.tvDescription2);
        TextView tvLeftCommund = (TextView) dialogLogin.findViewById(R.id.tvLeftCommund);
        TextView tvRightCommund = (TextView) dialogLogin.findViewById(R.id.tvRightCommund);
        tvTitel2.setText("Confirm");
        tvDescription2.setText("Are you sure to reset all?");
        tvLeftCommund.setText("yes");
        tvRightCommund.setText("No");

        //==================Font set==========================
        Typeface helveticaNeuRegular = Typeface.createFromAsset(con.getAssets(), "font/helveticaNeuRegular.ttf");
        Typeface helveticaNeueBold = Typeface.createFromAsset(con.getAssets(), "font/helveticaNeueBold.ttf");
        tvTitel2.setTypeface(helveticaNeueBold);
        tvLeftCommund.setTypeface(helveticaNeuRegular);
        tvRightCommund.setTypeface(helveticaNeuRegular);
        tvDescription2.setTypeface(helveticaNeuRegular);

        tvLeftCommund.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                removeAlldata();
                dialogLogin.dismiss();

            }
        });

        tvRightCommund.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialogLogin.dismiss();
            }
        });

        dialogLogin.show();
    }

    private void removeAlldata() {
        AppConstant.path1 = "";
        AppConstant.path2 = "";
        AppConstant.path3 = "";
        AppConstant.path4 = "";
        crossClick();
        etProName.setText("");
        etProDescription.setText("");
        tvSuitableFor.setText("Suitable For");
        tvSuitableFor.setTextColor(con.getResources().getColor(R.color.grayText));
        tvCategory.setText("Category");
        category_id = 0;
        sub_category_id = 0;
        suitablePosition = -1;
        tvCategory.setTextColor(con.getResources().getColor(R.color.grayText));
        tvSubCategory.setText("SubCategory");
        tvSubCategory.setTextColor(con.getResources().getColor(R.color.grayText));
        autoComTVBrand.setText("");
        tvSize.setText("Size");
        tvSize.setTextColor(con.getResources().getColor(R.color.grayText));
        setNewTag("no");
//        new_tag = "no";//for new tag selected
        etOriginalPrice.setText("");
        etListingPrice.setText("");
        deliveryType("men");//for delivery type selected
        tvColorSell.setVisibility(View.VISIBLE);
        imgColorCross.setVisibility(View.GONE);
        imgColor1.setVisibility(View.GONE);
        tvColor2.setVisibility(View.GONE);
        color_code = "";
        firstColor = "";
        etPriceforShipping.setText("");
        selectedHastag.clear();
        addedHashTag.removeAllTags();
        edittextHashtag.setText("");

    }


    private void forImageCapture() {
        imagePlus1.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                capture = "1";
                dialogShow();
            }
        });

        imagePlus2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                capture = "2";
                dialogShow();
            }
        });

        rlColor.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                colorDialoag(con);
            }
        });

        imagePlus3.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                capture = "3";
                dialogShow();
            }
        });

        imagePlus4.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                capture = "4";
                dialogShow();
            }
        });
        imageCross1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.path1 = "";
                //--------------To null image file--------------
                photo_one = null;
                //----------set main image---------
                imagePlus1.setImageResource(R.drawable.add_image);
                imageCross1.setVisibility(View.GONE);
                crossClick();
            }
        });
        imageCross2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.path2 = "";
                photo_two = null;
                // ----------set main image---------
                imagePlus2.setImageResource(R.drawable.add_image);
                imageCross2.setVisibility(View.GONE);
                crossClick();
            }
        });

        imageCross3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.path3 = "";
                photo_three = null;
                imagePlus3.setImageResource(R.drawable.add_image);
                imageCross3.setVisibility(View.GONE);
                crossClick();
            }
        });

        imageCross4.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                photo_four = null;
                AppConstant.path4 = "";
                imagePlus4.setImageResource(R.drawable.add_image);
                imageCross4.setVisibility(View.GONE);
                crossClick();
            }
        });
    }

    private void forSuitableFor() {
        if (suitableList != null) {
            suitableList.clear();
        }
        suitableList.add("Man");
        suitableList.add("Woman");
        suitableList.add("Both");

        rlSuitableFor.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.keyboardHide(con, view);
                final CharacterPickerWindow windows = new CharacterPickerWindow(con, "Select Suitable For");
                windows.showAtLocation(v, Gravity.BOTTOM, 0, 0);
                windows.setPicker(suitableList);
                windows.setWidth(200);
                windows.setHeight(200);
                windows.setOnoptionsSelectListener(new CharacterPickerWindow.OnOptionsSelectListener() {
                    @Override
                    public void onOptionsSelect(int position, int option2, int options3) {
                        Log.e("Suitable position", "" + position);
                        tvSuitableFor.setTextColor(con.getResources().getColor(R.color.black));
                        tvCategory.setText("Category");
                        category_id = 0;
                        tvCategory.setTextColor(con.getResources().getColor(R.color.grayText));
                        tvSubCategory.setText("SubCategory");
                        sub_category_id = 0;
                        tvSubCategory.setTextColor(con.getResources().getColor(R.color.grayText));
                        tvSize.setText("Size");
                        tvSize.setTextColor(con.getResources().getColor(R.color.grayText));
                        tvSuitableFor.setText(suitableList.get(position));

                        if (catagoryList != null) {
                            catagoryList.clear();
                        }

                        for (int i = 0; i < AppConstant.commonResponse.getCategory().size(); i++) {
                            if (tvSuitableFor.getText().toString().equalsIgnoreCase("Man")) {
                                if (AppConstant.commonResponse.getCategory().get(i).getType().equalsIgnoreCase("2") || AppConstant.commonResponse.getCategory().get(i).getType().equalsIgnoreCase("1")) {
                                    if (!AppConstant.commonResponse.getCategory().get(i).getName().equalsIgnoreCase("Most Popular")) {
                                        catagoryList.add(AppConstant.commonResponse.getCategory().get(i).getName());
                                    }
                                }
                            } else if (tvSuitableFor.getText().toString().equalsIgnoreCase("Woman")) {
                                if (AppConstant.commonResponse.getCategory().get(i).getType().equalsIgnoreCase("2") || AppConstant.commonResponse.getCategory().get(i).getType().equalsIgnoreCase("0")) {
                                    if (!AppConstant.commonResponse.getCategory().get(i).getName().equalsIgnoreCase("Most Popular")) {
                                        catagoryList.add(AppConstant.commonResponse.getCategory().get(i).getName());
                                    }
                                }
                            } else if (tvSuitableFor.getText().toString().equalsIgnoreCase("Both")) {
                                if (AppConstant.commonResponse.getCategory().get(i).getType().equalsIgnoreCase("2")) {
                                    if (!AppConstant.commonResponse.getCategory().get(i).getName().equalsIgnoreCase("Most Popular")) {
                                        catagoryList.add(AppConstant.commonResponse.getCategory().get(i).getName());
                                    }
                                }
                            }


                        }

                    }
                });
            }
        });
    }

    private void forCategory() {

        rlCategory.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.keyboardHide(con, view);
                if (tvSuitableFor.getText().toString().equalsIgnoreCase("Suitable For")) {
                    Toast.makeText(con, "Select Suitable For First", Toast.LENGTH_LONG).show();
                } else {
                    if (catagoryList.size() > 0) {
                        final CharacterPickerWindow windows = new CharacterPickerWindow(con, "Select Category");
                        windows.showAtLocation(v, Gravity.BOTTOM, 0, 0);
                        windows.setPicker(catagoryList);
                        windows.setWidth(200);
                        windows.setHeight(200);

                        windows.setOnoptionsSelectListener(new CharacterPickerWindow.OnOptionsSelectListener() {
                            @Override
                            public void onOptionsSelect(int position, int option2, int options3) {
                                Log.e("Category position", "" + position);
                                tvCategory.setTextColor(con.getResources().getColor(R.color.black));
                                tvSubCategory.setText("SubCategory");
                                sub_category_id = 0;
                                tvSubCategory.setTextColor(con.getResources().getColor(R.color.grayText));
                                tvSize.setText("Size");
                                tvSize.setTextColor(con.getResources().getColor(R.color.grayText));
                                tvCategory.setText(catagoryList.get(position));

                                categoryPosition = position;
                                if (subCategoryList != null) {
                                    subCategoryList.clear();
                                }
                                for (int i = 0; i < AppConstant.commonResponse.getCategory().size(); i++) {
                                    if (AppConstant.commonResponse.getCategory().get(i).getName().equalsIgnoreCase(catagoryList.get(position))) {
                                        if (suitableList.get(suitablePosition).equalsIgnoreCase("Woman")) {
                                            for (int j = 0; j < AppConstant.commonResponse.getCategory().get(i).getSubcategory().getWoman().size(); j++) {
                                                subCategoryList.add(AppConstant.commonResponse.getCategory().get(i).getSubcategory().getWoman().get(j).getName());
                                            }
                                        } else if (suitableList.get(suitablePosition).equalsIgnoreCase("Man")) {
                                            for (int j = 0; j < AppConstant.commonResponse.getCategory().get(i).getSubcategory().getMan().size(); j++) {
                                                subCategoryList.add(AppConstant.commonResponse.getCategory().get(i).getSubcategory().getMan().get(j).getName());
                                            }
                                        } else {
                                            for (int j = 0; j < AppConstant.commonResponse.getCategory().get(i).getSubcategory().getBoth().size(); j++) {
                                                subCategoryList.add(AppConstant.commonResponse.getCategory().get(i).getSubcategory().getBoth().get(j).getName());
                                            }
                                        }
                                    }
                                }
                            }
                        });
                    } else {
                        Toast.makeText(con, "No category available", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

    }

    private void forSubCategory() {

        rlSubCategory.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.keyboardHide(con, view);
                if (tvCategory.getText().toString().equalsIgnoreCase("Category")) {
                    Toast.makeText(con, "Select A Category First", Toast.LENGTH_LONG).show();
                } else {
                    if (subCategoryList.size() > 0) {
                        final CharacterPickerWindow windows = new CharacterPickerWindow(con, "SubCategory");
                        windows.showAtLocation(v, Gravity.BOTTOM, 0, 0);
                        windows.setPicker(subCategoryList);
                        windows.setWidth(200);
                        windows.setHeight(200);
                        windows.setOnoptionsSelectListener(new CharacterPickerWindow.OnOptionsSelectListener() {
                            @Override
                            public void onOptionsSelect(int position, int option2, int options3) {
                                Log.e("Category position", "" + position);
                                tvSubCategory.setTextColor(con.getResources().getColor(R.color.black));
                                tvSize.setText("Size");
                                tvSize.setTextColor(con.getResources().getColor(R.color.grayText));
                                tvSubCategory.setText(subCategoryList.get(position));
//                                sub_category_id = Integer.parseInt(AppConstant.commonResponse.getCategory().get(categoryPosition).getSubcategory().get(position).getSub_category_id());
                                subCategoryPosition = position;
                                if (sizesList != null) {
                                    sizesList.clear();
                                }

                                for (int k = 0; k < AppConstant.commonResponse.getCategory().size(); k++) {
                                    if (AppConstant.commonResponse.getCategory().get(k).getName().equalsIgnoreCase(tvCategory.getText().toString())) {

                                        if (suitableList.get(suitablePosition).equalsIgnoreCase("Woman")) {
                                            if (AppConstant.commonResponse.getCategory().get(k).getSubcategory().getWoman().get(position).getSizes() != null) {
                                                for (int i = 0; i < AppConstant.commonResponse.getCategory().get(k).getSubcategory().getWoman().get(position).getSizes().size(); i++) {
                                                    sizesList.add(AppConstant.commonResponse.getCategory().get(k).getSubcategory().getWoman().get(position).getSizes().get(i).getSize());
                                                }
                                            }
                                        } else if (suitableList.get(suitablePosition).equalsIgnoreCase("Man")) {
                                            Log.e("CategoryName", "" + AppConstant.commonResponse.getCategory().get(k).getName());
                                            Log.e("Man size", "" + AppConstant.commonResponse.getCategory().get(k).getSubcategory().getMan().size());
                                            if (AppConstant.commonResponse.getCategory().get(k).getSubcategory().getMan().get(position).getSizes() != null) {
                                                for (int i = 0; i < AppConstant.commonResponse.getCategory().get(k).getSubcategory().getMan().get(position).getSizes().size(); i++) {
                                                    sizesList.add(AppConstant.commonResponse.getCategory().get(k).getSubcategory().getMan().get(position).getSizes().get(i).getSize());
                                                }
                                            }

                                        } else {
                                            if (AppConstant.commonResponse.getCategory().get(k).getSubcategory().getBoth().get(position).getSizes() != null) {
                                                for (int i = 0; i < AppConstant.commonResponse.getCategory().get(k).getSubcategory().getBoth().get(position).getSizes().size(); i++) {
                                                    sizesList.add(AppConstant.commonResponse.getCategory().get(k).getSubcategory().getBoth().get(position).getSizes().get(i).getSize());
                                                }
                                            }

                                        }
                                    }
                                }

                            }
                        });
                    } else {
                        Toast.makeText(con, "No SubCategory available", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

    }

    private void forBrand() {
        for (int i = 0; i < AppConstant.commonResponse.getBrand().size(); i++) {
            brandnameList.add(AppConstant.commonResponse.getBrand().get(i).getName());
            Log.e("Brand", ":" + AppConstant.commonResponse.getBrand().get(i).getName());
        }
        ArrayAdapter<String> brandadapter = new ArrayAdapter<String>(con, R.layout.spinner_item, brandnameList);
        autoComTVBrand.setAdapter(brandadapter);

        autoComTVBrand.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View v, int position, long arg3) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    private void forSize() {
        rlSize.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tvCategory.getText().toString().equalsIgnoreCase("Category")) {
                    Toast.makeText(con, "Select A Catagory First", Toast.LENGTH_LONG).show();
                } else {
                    if (tvSubCategory.getText().toString().equalsIgnoreCase("SubCategory")) {
                        Toast.makeText(con, "Select A SubCatagory First", Toast.LENGTH_LONG).show();
                    } else {
                        sizeDialoag(con);
                    }
                }
            }
        });

    }

    private void forColor() {
        colorList();//For color
        imgColorCross.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (tvColor2.getVisibility() == View.VISIBLE) {
                    tvColor2.setVisibility(View.GONE);
                    color_code = firstColor;
                } else {
                    imgColor1.setVisibility(View.GONE);
                    tvColorSell.setVisibility(View.VISIBLE);
                    color_code = "";
                    firstColor = "";
                    imgColorCross.setVisibility(View.GONE);
                }

            }
        });
    }

    private void tagDialog(List<String> tmpList) {
        final Dialog dialogTag = new Dialog(con);
        dialogTag.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogTag.setContentView(R.layout.tag_dialogue);
        dialogTag.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogTag.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        TagContainerLayout tagSearchView = (TagContainerLayout) dialogTag.findViewById(R.id.tagSearchView);
        tagSearchView.setTags(tmpList);
        tagSearchView.setOnTagClickListener(new TagView.OnTagClickListener() {
            @Override
            public void onTagClick(int position, String text) {
                boolean ifExist = false;
                for (String name : selectedHastag) {
                    if (text.equalsIgnoreCase(name)) {
                        ifExist = true;
                        break;
                    } else {
                        ifExist = false;
                    }
                }
                if (!ifExist) {
                    addedHashTag.addTag(text);
                    selectedHastag.add(text);
                    dialogTag.dismiss();
                } else {
                    Toast.makeText(con, "This hashkey already added", Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onTagLongClick(int position, String text) {

            }
        });
        dialogTag.show();
    }

    private String getComaSepateStr(List<String> selectedHastag) {
        String name = "";
        int position = 0;
        for (String seletedName : selectedHastag) {
            if (position == 0) {
                name = seletedName + ",";
                position++;
            } else {
                name = name + seletedName + ",";
            }
        }
        name = removeLastChanacter(name, ',');

        return name;

    }

    private void forSaveDraft() {
        //===============End Edit==================================================================
        tvSaveDraft.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(etProName.getText().toString())) {
                    Toast.makeText(con, getResources().getString(R.string.please_pro), Toast.LENGTH_LONG).show();
                } else {
                    if (tvSuitableFor.getText().toString().equalsIgnoreCase("Woman")) {
                        suitable_for = 0;
                        Log.e("Sutable For", "woman");
                    } else if (tvSuitableFor.getText().toString().equalsIgnoreCase("Man")) {
                        suitable_for = 1;
                        Log.e("Sutable For", "Man");
                    } else if (tvSuitableFor.getText().toString().equalsIgnoreCase("Both")) {
                        suitable_for = 2;
                        Log.e("Sutable For", "Both");
                    }
                    brand_id = AppConstant.getBrandIDByName(autoComTVBrand.getText().toString());
                    category_id = AppConstant.getCategoryIdByName(tvCategory.getText().toString());
                    sub_category_id = AppConstant.getSubCategoryIdByName(tvSuitableFor.getText().toString().trim(), tvCategory.getText().toString(), tvSubCategory.getText().toString());
                    ProductAndUserDetailsInfo pInfo = new ProductAndUserDetailsInfo();
                    pInfo.setId(AppConstant.feedRowObject.getId());
                    pInfo.setName(etProName.getText().toString());
                    pInfo.setDescription(etProDescription.getText().toString());
                    pInfo.setPhoto_one(AppConstant.path1);
                    pInfo.setPhoto_two(AppConstant.path2);
                    pInfo.setPhoto_three(AppConstant.path3);
                    pInfo.setPhoto_four(AppConstant.path4);
                    pInfo.setCategory_id(String.valueOf(category_id));
                    pInfo.setSub_category_id(String.valueOf(sub_category_id));
                    pInfo.setUser_id(PersistData.getStringData(con, AppConstant.user_id));

                    BrandInfo brandInfoList = new BrandInfo();
                    brandInfoList.setName(autoComTVBrand.getText().toString());
                    brandInfoList.setId(String.valueOf(brand_id));
                    pInfo.getBrand_info().add(brandInfoList);

                    pInfo.setSize(tvSize.getText().toString());
                    pInfo.setColor_code(color_code);
                    pInfo.setNew_tag(new_tag);
                    pInfo.setSuitable_for(String.valueOf(suitablePosition));
                    pInfo.setOriginal_price(etOriginalPrice.getText().toString());
                    pInfo.setListing_price(etListingPrice.getText().toString());
                    pInfo.setDelivery_type(delivery_type);
                    pInfo.setDelivery_charge(etPriceforShipping.getText().toString());
                    pInfo.setHashtag_info(getComaSepateStr(selectedHastag));
                    if (AppConstant.editabledraft.equalsIgnoreCase("true")) {
                        boolean isUpdated = db.updateProductInfo(pInfo);
                        if (isUpdated == true) {
                            Toast.makeText(con, "Data Updated", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(con, "Data not Updated", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        boolean insurted = db.insurtProductInfo(pInfo);
                        if (insurted == true) {
                            Toast.makeText(con, "Draft Saved", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(con, "Draft not Saved", Toast.LENGTH_LONG).show();
                        }
                    }
                }

            }
        });

    }

    /**
     * =======================For Edit==============================================================
     */
    private void edit() {

        if (AppConstant.editable.equalsIgnoreCase("editable")) {
            productDetails = AppConstant.feedRowObject;
            imgSellBack = (ImageView) view.findViewById(R.id.imgSellBack);
            imgSellBack.setVisibility(View.VISIBLE);
            llSave = (LinearLayout) view.findViewById(R.id.llSave);
            TextView tvEditPor = (TextView) view.findViewById(R.id.tvEditPor);
            if (AppConstant.editabledraft.equalsIgnoreCase("true")) {
                llSave.setVisibility(View.VISIBLE);
                tvEditPor.setVisibility(View.GONE);
            } else {
                llSave.setVisibility(View.GONE);
                tvEditPor.setVisibility(View.VISIBLE);
            }
            /**
             * =======Set data=================================
             */
            if (AppConstant.editabledraft.equalsIgnoreCase("true")) {
                if (!TextUtils.isEmpty(productDetails.getPhoto_one())) {
                    AppConstant.path1 = productDetails.getPhoto_one();
                    photo_one = new File(AppConstant.path1);
                    imagePlus1.setImageURI(Uri.fromFile(new File(productDetails.getPhoto_one())));
                    crossClick();
                }
            } else {
                if (!TextUtils.isEmpty(productDetails.getPhoto_one())) {
                    AppConstant.path1 = productDetails.getPhoto_one();
                    photo_one = null;
                    Picasso.with(con)
                            .load(productDetails.getPhoto_one())
                            .placeholder(R.drawable.product_bg)
                            .error(R.drawable.product_bg)
                            .into(imagePlus1);
                    crossClick();
                }
            }

            if (AppConstant.editabledraft.equalsIgnoreCase("true")) {
                if (!TextUtils.isEmpty(productDetails.getPhoto_two())) {
                    AppConstant.path2 = productDetails.getPhoto_two();
                    photo_two = new File(AppConstant.path2);
                    imagePlus2.setImageURI(Uri.fromFile(new File(productDetails.getPhoto_two())));
                    crossClick();
                }
            } else {
                if (!TextUtils.isEmpty(productDetails.getPhoto_two())) {
                    AppConstant.path2 = productDetails.getPhoto_two();
                    photo_two = null;
                    Picasso.with(con)
                            .load(productDetails.getPhoto_two())
                            .placeholder(R.drawable.product_bg)
                            .error(R.drawable.product_bg)
                            .into(imagePlus2);
                    crossClick();
                }
            }
            if (AppConstant.editabledraft.equalsIgnoreCase("true")) {
                if (!TextUtils.isEmpty(productDetails.getPhoto_three())) {
                    imagePlus3.setImageURI(Uri.fromFile(new File(productDetails.getPhoto_three())));
                    AppConstant.path3 = productDetails.getPhoto_three();
                    photo_two = new File(AppConstant.path3);
                    crossClick();
                }
            } else {
                if (!TextUtils.isEmpty(productDetails.getPhoto_three())) {
                    AppConstant.path3 = productDetails.getPhoto_three();
                    photo_three = null;
                    Picasso.with(con)
                            .load(productDetails.getPhoto_three())
                            .placeholder(R.drawable.product_bg)
                            .error(R.drawable.product_bg)
                            .into(imagePlus3);
                    crossClick();
                }
            }
            if (AppConstant.editabledraft.equalsIgnoreCase("true")) {
                if (!TextUtils.isEmpty(productDetails.getPhoto_four())) {
                    imagePlus4.setImageURI(Uri.fromFile(new File(productDetails.getPhoto_four())));
                    AppConstant.path4 = productDetails.getPhoto_four();
                    photo_four = new File(AppConstant.path4);
                    crossClick();
                }
            } else {
                if (!TextUtils.isEmpty(productDetails.getPhoto_four())) {
                    AppConstant.path4 = productDetails.getPhoto_four();
                    photo_four = null;
                    Picasso.with(con)
                            .load(productDetails.getPhoto_four())
                            .placeholder(R.drawable.product_bg)
                            .error(R.drawable.product_bg)
                            .into(imagePlus4);
                    crossClick();
                }
            }
            crossClick();
            etProName.setText(productDetails.getName());
            etProDescription.setText(productDetails.getDescription());
            //=======For SuitableFor===========
            if (!productDetails.getSuitable_for().equalsIgnoreCase("SuitableFor")) {
                tvSuitableFor.setTextColor(con.getResources().getColor(R.color.black));
                if (productDetails.getSuitable_for().equalsIgnoreCase("0")) {
                    tvSuitableFor.setText("Woman");
                } else if (productDetails.getSuitable_for().equalsIgnoreCase("1")) {
                    tvSuitableFor.setText("Man");
                } else if (productDetails.getSuitable_for().equalsIgnoreCase("2")) {
                    tvSuitableFor.setText("Both");
                }
            }


            //====================Category==========================
            if (catagoryList != null) {
                catagoryList.clear();
            }
            for (int i = 0; i < AppConstant.commonResponse.getCategory().size(); i++) {
                if (AppConstant.commonResponse.getCategory().get(i).getType().equalsIgnoreCase("2") || AppConstant.commonResponse.getCategory().get(i).getType().equalsIgnoreCase(productDetails.getSuitable_for())) {
                    if (!AppConstant.commonResponse.getCategory().get(i).getName().equalsIgnoreCase("Most Popular")) {
                        catagoryList.add(AppConstant.commonResponse.getCategory().get(i).getName());
                    }
                }
                if (AppConstant.commonResponse.getCategory().get(i).getId().equalsIgnoreCase(productDetails.getCategory_id())) {
                    tvCategory.setText(AppConstant.commonResponse.getCategory().get(i).getName());
                    tvCategory.setTextColor(con.getResources().getColor(R.color.black));
                    category_id = Integer.parseInt(AppConstant.commonResponse.getCategory().get(i).getId());
                    categoryPosition = i;
                }
            }
            //======================subCategory======================
            if (subCategoryList != null) {
                subCategoryList.clear();
            }
            tvSubCategory.setText(AppConstant.getSubCategoryName(productDetails.getCategory_id(), productDetails.getSub_category_id()));
            tvSubCategory.setTextColor(getResources().getColor(R.color.black));
            if (catagoryList.size() > 0) {
                for (int i = 0; i < AppConstant.commonResponse.getCategory().size(); i++) {
                    if (AppConstant.commonResponse.getCategory().get(i).getId().equalsIgnoreCase(productDetails.getCategory_id())) {
                        if (productDetails.getSuitable_for().equalsIgnoreCase("0")) {
                            for (int j = 0; j < AppConstant.commonResponse.getCategory().get(i).getSubcategory().getWoman().size(); j++) {
                                subCategoryList.add(AppConstant.commonResponse.getCategory().get(i).getSubcategory().getWoman().get(j).getName());
                            }
                        } else if (productDetails.getSuitable_for().equalsIgnoreCase("1")) {
                            for (int j = 0; j < AppConstant.commonResponse.getCategory().get(i).getSubcategory().getMan().size(); j++) {
                                subCategoryList.add(AppConstant.commonResponse.getCategory().get(i).getSubcategory().getMan().get(j).getName());

                            }
                        } else {
                            for (int j = 0; j < AppConstant.commonResponse.getCategory().get(i).getSubcategory().getBoth().size(); j++) {
                                subCategoryList.add(AppConstant.commonResponse.getCategory().get(i).getSubcategory().getBoth().get(j).getName());
                            }
                        }
                    }
                }
            }

            //================Size=========================
            if (productDetails.getSize_info() != null) {
                tvSize.setText(productDetails.getSize_info().getSize());
                tvSize.setTextColor(con.getResources().getColor(R.color.black));
            } else {
                tvSize.setText(productDetails.getSize());
                tvSize.setTextColor(con.getResources().getColor(R.color.black));
            }

            if (AppConstant.editabledraft.equalsIgnoreCase("true")) {
                if (!productDetails.getSize().equalsIgnoreCase("Size")) {
                    tvSize.setText(productDetails.getSize());
                    tvSize.setTextColor(con.getResources().getColor(R.color.black));
                } else {
                    tvSize.setText("Size");
                    tvSize.setTextColor(con.getResources().getColor(R.color.grayText));
                }

            }

            if (sizesList != null) {
                sizesList.clear();
            }

            for (int k = 0; k < AppConstant.commonResponse.getCategory().size(); k++) {
                if (AppConstant.commonResponse.getCategory().get(k).getName().equalsIgnoreCase(AppConstant.getCategoryName(productDetails.getCategory_id()))) {

                    if (productDetails.getSuitable_for().equalsIgnoreCase("0")) {
                        for (int j = 0; j < AppConstant.commonResponse.getCategory().get(k).getSubcategory().getWoman().size(); j++) {
                            if (AppConstant.commonResponse.getCategory().get(k).getSubcategory().getWoman().get(j).getSub_category_id().equalsIgnoreCase(productDetails.getSub_category_id())) {
                                for (int i = 0; i < AppConstant.commonResponse.getCategory().get(k).getSubcategory().getWoman().get(j).getSizes().size(); i++) {
                                    sizesList.add(AppConstant.commonResponse.getCategory().get(k).getSubcategory().getWoman().get(j).getSizes().get(i).getSize());
                                }
                            }
                        }
                    } else if (productDetails.getSuitable_for().equalsIgnoreCase("1")) {
                        for (int j = 0; j < AppConstant.commonResponse.getCategory().get(k).getSubcategory().getMan().size(); j++) {
                            if (AppConstant.commonResponse.getCategory().get(k).getSubcategory().getMan().get(j).getSub_category_id().equalsIgnoreCase(productDetails.getSub_category_id())) {
                                for (int i = 0; i < AppConstant.commonResponse.getCategory().get(k).getSubcategory().getMan().get(j).getSizes().size(); i++) {
                                    sizesList.add(AppConstant.commonResponse.getCategory().get(k).getSubcategory().getMan().get(j).getSizes().get(i).getSize());
                                }
                            }
                        }
                    } else {
                        for (int j = 0; j < AppConstant.commonResponse.getCategory().get(k).getSubcategory().getBoth().size(); j++) {
                            if (AppConstant.commonResponse.getCategory().get(k).getSubcategory().getBoth().get(j).getSub_category_id().equalsIgnoreCase(productDetails.getSub_category_id())) {
                                for (int i = 0; i < AppConstant.commonResponse.getCategory().get(k).getSubcategory().getBoth().get(j).getSizes().size(); i++) {
                                    sizesList.add(AppConstant.commonResponse.getCategory().get(k).getSubcategory().getBoth().get(j).getSizes().get(i).getSize());
                                }
                            }
                        }
                    }
                }
            }


            autoComTVBrand.setText(productDetails.getBrand_info().get(0).getName());


            if (!TextUtils.isEmpty(productDetails.getColor_code())) {
                List<String> colorList = Arrays.asList(productDetails.getColor_code().split(","));
                if (colorList.size() > 0) {
                    tvColorSell.setVisibility(View.GONE);
                    imgColorCross.setVisibility(View.VISIBLE);
                    color_code = colorList.get(0);
                    firstColor = colorList.get(0);
                    imgColor1.setVisibility(View.VISIBLE);
                    imgColor1.setBackgroundColor(Color.parseColor("#" + colorList.get(0)));
                    if (colorList.size() == 2) {
                        tvColor2.setVisibility(View.VISIBLE);
                        tvColor2.setBackgroundColor(Color.parseColor("#" + colorList.get(1)));
                        color_code = productDetails.getColor_code();
                    }
                }
            }

            setNewTag(productDetails.getNew_tag());
            etOriginalPrice.setText(productDetails.getOriginal_price());
            etListingPrice.setText(productDetails.getListing_price());
            for (HashtagInfo info : productDetails.getHashtag_detail()) {
                addedHashTag.addTag(info.getName());
            }
            delivery_type=productDetails.getDelivery_type();
            deliveryType(productDetails.getDelivery_type());
            if (productDetails.getDelivery_type().equalsIgnoreCase("shipping")) {
                etPriceforShipping.setText(productDetails.getDelivery_charge());
            }
            imgSellBack.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppConstant.editabledraft = "";
                    AppConstant.editable = "";
                    imgSellBack.setVisibility(View.GONE);
                    getActivity().onBackPressed();
                }
            });

            tvEditPor.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    product_id = Integer.parseInt(productDetails.getId());
                    name = etProName.getText().toString();
                    Log.e("name", "name");
                    description = etProDescription.getText().toString();
                    if (tvSuitableFor.getText().toString().equalsIgnoreCase("Woman")) {
                        suitable_for = 0;
                        Log.e("Sutable For", "woman");
                    } else if (tvSuitableFor.getText().toString().equalsIgnoreCase("Man")) {
                        suitable_for = 1;
                        Log.e("Sutable For", "Man");
                    } else if (tvSuitableFor.getText().toString().equalsIgnoreCase("Both")) {
                        suitable_for = 2;
                        Log.e("Sutable For", "Both");
                    }
                    category_id = AppConstant.getCategoryIdByName(tvCategory.getText().toString());
                    sub_category_id = AppConstant.getSubCategoryIdByName(tvSuitableFor.getText().toString().trim(), tvCategory.getText().toString(), tvSubCategory.getText().toString());
                    brand_name = autoComTVBrand.getText().toString();
                    brand_id = AppConstant.getBrandIDByName(autoComTVBrand.getText().toString());
                    orginal_price = etOriginalPrice.getText().toString();

                    listing_price = etListingPrice.getText().toString();
                    size = tvSize.getText().toString();
                    Log.e("WrittingBrand", brand_name);
                    selectedHastag.clear();
                    for (HashtagInfo info : productDetails.getHashtag_detail()) {
                        selectedHastag.add(info.getName());
                    }
                    delivery_charge = etPriceforShipping.getText().toString();
                    if (TextUtils.isEmpty(AppConstant.path1)) {
                        AppConstant.alertDialoag(con, getString(R.string.status), getString(R.string.provide_photo), getResources().getString(R.string.ok));
                    } else if (TextUtils.isEmpty(etProName.getText().toString())) {
                        AppConstant.alertDialoag(con, getString(R.string.status), getString(R.string.enter_proName), getResources().getString(R.string.ok));
                    } else if (TextUtils.isEmpty(etProDescription.getText().toString())) {
                        AppConstant.alertDialoag(con, getString(R.string.status), getString(R.string.select_suitable), getResources().getString(R.string.ok));
                    } else if (tvSuitableFor.getText().toString().equalsIgnoreCase("Suitable for")) {
                        AppConstant.alertDialoag(con, getString(R.string.status), getString(R.string.select_category), getResources().getString(R.string.ok));
                    } else if (tvCategory.getText().toString().equalsIgnoreCase("Category")) {
                        AppConstant.alertDialoag(con, getString(R.string.status), getString(R.string.enter_ProDescrip), getResources().getString(R.string.ok));
                    } else if (tvSubCategory.getText().toString().equalsIgnoreCase("SubCategory")) {
                        AppConstant.alertDialoag(con, getString(R.string.status), getString(R.string.select_subcategory), getResources().getString(R.string.ok));
                    } else if (TextUtils.isEmpty(autoComTVBrand.getText().toString())) {
                        AppConstant.alertDialoag(con, getString(R.string.status), getString(R.string.select_brand), getResources().getString(R.string.ok));
                    } else if (tvSize.getText().toString().equalsIgnoreCase("Size")) {
                        AppConstant.alertDialoag(con, getString(R.string.status), getString(R.string.enter_size), getResources().getString(R.string.ok));
                    } else if (TextUtils.isEmpty(etOriginalPrice.getText().toString())) {
                        AppConstant.alertDialoag(con, getString(R.string.status), getString(R.string.enter_oriPrice), getResources().getString(R.string.ok));
                    } else if (TextUtils.isEmpty(etListingPrice.getText().toString())) {
                        AppConstant.alertDialoag(con, getString(R.string.status), getString(R.string.enter_listPrice), getResources().getString(R.string.ok));
                    } else if (selectedHastag.size()<1) {
                        AppConstant.alertDialoag(con, getString(R.string.status), getString(R.string.enter_hastags), getResources().getString(R.string.ok));
                    } else if (Float.parseFloat(etOriginalPrice.getText().toString()) < Float.parseFloat(etListingPrice.getText().toString())) {
                        AppConstant.alertDialoag(con, getString(R.string.invalid_info), getString(R.string.invalid_lis), getResources().getString(R.string.close));
                    } else {
                        callUpdateProductAPI(AllURL.postUpdateProductURL());
                    }
                }
            });
        }
    }

    private void forSell() {
        tvSellNow.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                checkSellData();
            }
        });
    }

    public String removeLastChanacter(String str, Character characer) {
        if (str != null && str.length() > 0 && str.charAt(str.length() - 1) == characer) {
            str = str.substring(0, str.length() - 1);
        }
        return str;
    }

    /*
     * ---------------------------------API-34. Update Product ------------------------------
     */
    protected void callUpdateProductAPI(final String url) {
        // --- for net check-----
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }
        Log.e("UpdateProduct URL", url + "");
        final BusyDialog busyNow = new BusyDialog(con, true, false);
        busyNow.show();
        final AsyncHttpClient addclient = new AsyncHttpClient();
        addclient.addHeader("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token));

        final RequestParams param = new RequestParams();

        try {
            param.put("product_id", product_id);
            param.put("name", name);
            param.put("description", description);
            if (photo_one != null) {
                param.put("photo_one", photo_one);
            }
            Log.e(" photo_one", photo_one + "");
            param.put("category_id", category_id);
            param.put("sub_category_id", sub_category_id);
            param.put("suitable_for", "" + suitable_for);
            param.put("new_tag", new_tag);

            if (photo_two != null) {
                param.put("photo_two", photo_two);
                Log.e(" photo_two", photo_two + "");
            }
            if (photo_three != null) {
                param.put("photo_three", photo_three);
                Log.e(" photo_three", photo_three + "");
            }
            if (photo_four != null) {
                param.put("photo_four", photo_four);
                Log.e(" photo_four", photo_four + "");
            }
            param.put("brand_id", brand_id);
            param.put("brand_name", brand_name);
            param.put("size", size);
            if (!TextUtils.isEmpty(color_code)) {
                param.put("color_code", color_code);
            }
            hashtags = getComaSepateStr(selectedHastag);
            param.put("hashtags", hashtags);
            param.put("original_price", orginal_price);
            param.put("listing_price", listing_price);
            param.put("delivery_type", delivery_type);
            if (delivery_type.equalsIgnoreCase("shipping")) {
                param.put("delivery_charge", delivery_charge);
            }

        } catch (final Exception e1) {
            e1.printStackTrace();
        }

        addclient.post(url, param, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {

                if (busyNow != null) {
                    busyNow.dismis();
                }

                Log.e("Updateresposne ", ">>" + new String(response));
                Gson g = new Gson();
                addProductResponse = g.fromJson(new String(response), AddProductResponse.class);

                if (addProductResponse.getStatus().equalsIgnoreCase("1")) {
                    getActivity().onBackPressed();
                    Toast.makeText(con, addProductResponse.getMsg() + "", Toast.LENGTH_LONG).show();
                } else {
                    AlertMessage.showMessage(con, "Status",
                            addProductResponse.getMsg() + "");
                    return;
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  byte[] errorResponse, Throwable e) {

                // Log.e("errorResponse", new String(errorResponse));

                if (busyNow != null) {
                    busyNow.dismis();
                }
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }

    private void setNewTag(String type) {
        if (type.equalsIgnoreCase("yes")) {
            tvYes.setBackgroundResource(R.drawable.no);
            tvNo.setBackgroundResource(R.drawable.yes);
            new_tag = "yes";
        } else if (type.equalsIgnoreCase("no")) {
            tvYes.setBackgroundResource(R.drawable.yes);
            tvNo.setBackgroundResource(R.drawable.no);
            new_tag = "no";
        }
    }


    private class AddColorAdapter extends ArrayAdapter<String> {

        Context context;

        AddColorAdapter(Context context, Dialog dialog) {
            super(context, R.layout.row_color, colorlist);
            this.context = context;
        }

        @Override
        public View getView(final int position, View view,
                            ViewGroup parent) {
            View v = view;
            if (v == null) {
                final LayoutInflater vi = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.row_color, null);
            }

            if (position < colorlist.size()) {
                final TextView tvColor = (TextView) v.findViewById(R.id.tvColor);
                Log.e("Color code", "" + colorlist.get(position));
                tvColor.setBackgroundColor(Color.parseColor("#" + colorlist.get(position)));

                v.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (tvColor2.getVisibility() == View.VISIBLE) {
                            AppConstant.alertDialoag(con, "Max two colors", "You can set at most two colors", "Close");
                        } else {
                            if (TextUtils.isEmpty(firstColor)) {
                                color_code = colorlist.get(position);
                                firstColor = colorlist.get(position);

                            } else {
                                color_code = firstColor + "," + colorlist.get(position);
                                firstColor = colorlist.get(position);
                            }
                            if (imgColor1.getVisibility() == View.VISIBLE) {
                                tvColor2.setVisibility(View.VISIBLE);
                                tvColor2.setBackgroundColor(Color.parseColor("#" + colorlist.get(position)));
                            } else {
                                imgColor1.setVisibility(View.VISIBLE);
                                imgColor1.setBackgroundColor(Color.parseColor("#" + colorlist.get(position)));
                            }

                            imgColorCross.setVisibility(View.VISIBLE);
                            tvColorSell.setVisibility(View.GONE);
                        }

                        dialog.dismiss();

                    }
                });
            }

            return v;
        }
    }


    private void onClick() {

        tvYes.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                tvYes.setBackgroundResource(R.drawable.no);
                tvNo.setBackgroundResource(R.drawable.yes);
                setNewTag("yes");
                Log.e("rebrand", autoComTVBrand.getText().toString());
            }
        });
        tvNo.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                tvNo.setBackgroundResource(R.drawable.no);
                tvYes.setBackgroundResource(R.drawable.yes);
                setNewTag("no");
            }
        });
        imgCrossOP.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                etOriginalPrice.setText("");
            }
        });

        imgCrossLP.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                etListingPrice.setText("");
            }
        });

        relativeMeetIn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                deliveryType("men");
                delivery = true;
            }
        });
        relativeShipping.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                deliveryType("shipping");
                delivery = true;
            }
        });

        relativeInternational.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                deliveryType("international");
                delivery = true;
            }
        });
    }


    private void crossClick() {
        if (TextUtils.isEmpty(AppConstant.path1)) {
            imagePlus1.setEnabled(true);
            imageCross1.setVisibility(View.GONE);
            imagePlus1.setImageResource(R.drawable.add_image_with_bg);
            if (TextUtils.isEmpty(AppConstant.path2)) {
                imageCross2.setVisibility(View.GONE);
                imagePlus2.setImageResource(R.drawable.add_image);
                imagePlus2.setEnabled(false);
            } else {
                imageCross2.setVisibility(View.VISIBLE);
                imagePlus2.setEnabled(true);
            }
            if (TextUtils.isEmpty(AppConstant.path3)) {
                imageCross3.setVisibility(View.GONE);
                imagePlus3.setImageResource(R.drawable.add_image);
                imagePlus3.setEnabled(false);
            } else {
                imageCross3.setVisibility(View.VISIBLE);
                imagePlus3.setEnabled(true);
            }
            if (TextUtils.isEmpty(AppConstant.path4)) {
                imageCross4.setVisibility(View.GONE);
                imagePlus4.setImageResource(R.drawable.add_image);
                imagePlus4.setEnabled(false);
            } else {
                imageCross4.setVisibility(View.VISIBLE);
                imagePlus4.setEnabled(true);
            }

        } else if (TextUtils.isEmpty(AppConstant.path2)) {
            imagePlus1.setEnabled(true);
            imageCross1.setVisibility(View.VISIBLE);
            imagePlus2.setEnabled(true);
            imagePlus2.setImageResource(R.drawable.add_image_with_bg);
            if (TextUtils.isEmpty(AppConstant.path3)) {
                imageCross3.setVisibility(View.GONE);
                imagePlus3.setImageResource(R.drawable.add_image);
                imagePlus3.setEnabled(false);
            } else {
                imageCross3.setVisibility(View.VISIBLE);
                imagePlus3.setEnabled(true);
            }
            if (TextUtils.isEmpty(AppConstant.path4)) {
                imageCross4.setVisibility(View.GONE);
                imagePlus4.setImageResource(R.drawable.add_image);
                imagePlus4.setEnabled(false);
            } else {
                imageCross4.setVisibility(View.VISIBLE);
                imagePlus4.setEnabled(true);
            }
        } else if (TextUtils.isEmpty(AppConstant.path3)) {
            imagePlus1.setEnabled(true);
            imageCross1.setVisibility(View.VISIBLE);
            imageCross2.setVisibility(View.VISIBLE);
            imagePlus2.setEnabled(true);
            imagePlus3.setEnabled(true);
            imagePlus3.setImageResource(R.drawable.add_image_with_bg);
            if (TextUtils.isEmpty(AppConstant.path4)) {
                imageCross4.setVisibility(View.GONE);
                imagePlus4.setImageResource(R.drawable.add_image);
                imagePlus4.setEnabled(false);
            } else {
                imageCross4.setVisibility(View.VISIBLE);
                imagePlus4.setEnabled(true);
            }
        } else if (TextUtils.isEmpty(AppConstant.path4)) {
            imagePlus1.setEnabled(true);
            imagePlus2.setEnabled(true);
            imagePlus3.setEnabled(true);
            imagePlus4.setEnabled(true);
            imageCross1.setVisibility(View.VISIBLE);
            imageCross2.setVisibility(View.VISIBLE);
            imageCross3.setVisibility(View.VISIBLE);
            imagePlus4.setImageResource(R.drawable.add_image_with_bg);
        } else {
            imagePlus1.setEnabled(true);
            imagePlus2.setEnabled(true);
            imagePlus3.setEnabled(true);
            imagePlus4.setEnabled(true);
            imageCross1.setVisibility(View.VISIBLE);
            imageCross2.setVisibility(View.VISIBLE);
            imageCross3.setVisibility(View.VISIBLE);
            imageCross4.setVisibility(View.VISIBLE);
        }
    }


    private void colorDialoag(final Context con) {
        dialog = new Dialog(con);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_color);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView tvCancelSell = (TextView) dialog.findViewById(R.id.tvCancelSell);
        TextView tvChooseColo = (TextView) dialog.findViewById(R.id.tvChooseColo);
        GridView gridViewColor = (GridView) dialog.findViewById(R.id.gridViewColor);

        Typeface helveticaNeuRegular = Typeface.createFromAsset(con.getAssets(), "font/helveticaNeuRegular.ttf");
        Typeface helveticaNeueBold = Typeface.createFromAsset(con.getAssets(), "font/helveticaNeueBold.ttf");

        tvCancelSell.setTypeface(helveticaNeuRegular);
        tvChooseColo.setTypeface(helveticaNeueBold);

        //==================Call Adapter==========================
        colorAdapter = new AddColorAdapter(con, dialog);
        gridViewColor.setAdapter(colorAdapter);
        colorAdapter.notifyDataSetChanged();
        tvCancelSell.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void sizeDialoag(final Context con) {
        final Dialog dialogSize = new Dialog(con);
        dialogSize.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogSize.setContentView(R.layout.dialog_size);
        dialogSize.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialogSize.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView tvSelect = (TextView) dialogSize.findViewById(R.id.tvSelect);
        TextView tvOneSize = (TextView) dialogSize.findViewById(R.id.tvOneSize);
        TextView tvAvailable = (TextView) dialogSize.findViewById(R.id.tvAvailable);
        TextView tvCustomSize = (TextView) dialogSize.findViewById(R.id.tvCustomSize);
        TextView tvCancel = (TextView) dialogSize.findViewById(R.id.tvCancel);

        //==================Font set==========================
        Typeface helveticaNeuRegular = Typeface.createFromAsset(con.getAssets(), "font/helveticaNeuRegular.ttf");

        tvSelect.setTypeface(helveticaNeuRegular);
        tvOneSize.setTypeface(helveticaNeuRegular);
        tvAvailable.setTypeface(helveticaNeuRegular);
        tvCustomSize.setTypeface(helveticaNeuRegular);
        tvCancel.setTypeface(helveticaNeuRegular);

        tvOneSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvSize.setText(con.getString(R.string.os));
                tvSize.setTextColor(con.getResources().getColor(R.color.black));
                dialogSize.dismiss();
            }
        });
        tvAvailable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (sizesList.size() > 0) {
                    Log.e("Available ", "Clicked" + sizesList.size());
                    final CharacterPickerWindow windows = new CharacterPickerWindow(con, "Select Size");
                    windows.showAtLocation(v, Gravity.BOTTOM, 0, 0);
                    windows.setPicker(sizesList);
                    windows.setWidth(300);
                    windows.setHeight(200);
                    windows.setOnoptionsSelectListener(new CharacterPickerWindow.OnOptionsSelectListener() {
                        @Override
                        public void onOptionsSelect(int options1, int option2, int options3) {
                            Log.e("item selected", "options1" + options1);
                            tvSize.setText(sizesList.get(options1));
                            tvSize.setTextColor(con.getResources().getColor(R.color.black));
                            dialogSize.dismiss();
                        }
                    });
                } else {
                    Toast.makeText(con, "No Available size.", Toast.LENGTH_LONG).show();
                }
            }
        });

        tvCustomSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customSizeDialoag(con);
                dialogSize.dismiss();
            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogSize.dismiss();
            }
        });

        dialogSize.show();
    }

    private void customSizeDialoag(final Context con) {
        final Dialog dialog = new Dialog(con);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_custome_size);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView tvInputSz = (TextView) dialog.findViewById(R.id.tvInputSz);
        final TextView etCustomSz = (TextView) dialog.findViewById(R.id.etCustomSz);
        TextView tvDone = (TextView) dialog.findViewById(R.id.tvDone);
        TextView tvCancelSz = (TextView) dialog.findViewById(R.id.tvCancelSz);

        //==================Font set==========================
        Typeface helveticaNeuRegular = Typeface.createFromAsset(con.getAssets(), "font/helveticaNeuRegular.ttf");
        Typeface helveticaNeueBold = Typeface.createFromAsset(con.getAssets(), "font/helveticaNeueBold.ttf");

        tvInputSz.setTypeface(helveticaNeueBold);
        etCustomSz.setTypeface(helveticaNeuRegular);
        tvDone.setTypeface(helveticaNeuRegular);
        tvCancelSz.setTypeface(helveticaNeuRegular);
        etCustomSz.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                }
            }
        });

        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(etCustomSz.getText().toString())) {
//                    dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                    View view = getActivity().getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) con.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                } else {
                    tvSize.setText(etCustomSz.getText().toString());
                    tvSize.setTextColor(con.getResources().getColor(R.color.black));
                    dialog.dismiss();
                }
            }
        });
        tvCancelSz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

	/*
     * ------------Method Sell Data Check-----------------------
	 */

    private void checkSellData() {
        if (TextUtils.isEmpty(AppConstant.path1)) {
            AppConstant.alertDialoag(con, getString(R.string.status), getString(R.string.provide_photo), getResources().getString(R.string.ok));
        } else if (TextUtils.isEmpty(etProName.getText().toString())) {
            AppConstant.alertDialoag(con, getString(R.string.status), getString(R.string.enter_proName), getResources().getString(R.string.ok));
        } else if (TextUtils.isEmpty(etProDescription.getText().toString())) {
            AppConstant.alertDialoag(con, getString(R.string.status), getString(R.string.select_suitable), getResources().getString(R.string.ok));
        } else if (tvSuitableFor.getText().toString().equalsIgnoreCase("Suitable for")) {
            AppConstant.alertDialoag(con, getString(R.string.status), getString(R.string.select_category), getResources().getString(R.string.ok));
        } else if (tvCategory.getText().toString().equalsIgnoreCase("Category")) {
            AppConstant.alertDialoag(con, getString(R.string.status), getString(R.string.enter_ProDescrip), getResources().getString(R.string.ok));
        } else if (tvSubCategory.getText().toString().equalsIgnoreCase("SubCategory")) {
            AppConstant.alertDialoag(con, getString(R.string.status), getString(R.string.select_subcategory), getResources().getString(R.string.ok));
        } else if (TextUtils.isEmpty(autoComTVBrand.getText().toString())) {
            AppConstant.alertDialoag(con, getString(R.string.status), getString(R.string.select_brand), getResources().getString(R.string.ok));
        } else if (tvSize.getText().toString().equalsIgnoreCase("Size")) {
            AppConstant.alertDialoag(con, getString(R.string.status), getString(R.string.enter_size), getResources().getString(R.string.ok));
        } else if (TextUtils.isEmpty(etOriginalPrice.getText().toString())) {
            AppConstant.alertDialoag(con, getString(R.string.status), getString(R.string.enter_oriPrice), getResources().getString(R.string.ok));
        } else if (TextUtils.isEmpty(etListingPrice.getText().toString())) {
            AppConstant.alertDialoag(con, getString(R.string.status), getString(R.string.enter_listPrice), getResources().getString(R.string.ok));
        } else if (TextUtils.isEmpty(edittextHashtag.getText().toString())) {
            AppConstant.alertDialoag(con, getString(R.string.status), getString(R.string.enter_hastags), getResources().getString(R.string.ok));
        } else if (Integer.parseInt(etOriginalPrice.getText().toString()) < Integer.parseInt(etListingPrice.getText().toString())) {
            AppConstant.alertDialoag(con, getString(R.string.invalid_info), getString(R.string.invalid_lis), getResources().getString(R.string.close));
        } else {

            name = etProName.getText().toString();
            Log.e("name", "name");
            description = etProDescription.getText().toString();
            if (tvSuitableFor.getText().toString().equalsIgnoreCase("Woman")) {
                suitable_for = 0;
                Log.e("Sutable For", "woman");
            } else if (tvSuitableFor.getText().toString().equalsIgnoreCase("Man")) {
                suitable_for = 1;
                Log.e("Sutable For", "Man");
            } else if (tvSuitableFor.getText().toString().equalsIgnoreCase("Both")) {
                suitable_for = 2;
                Log.e("Sutable For", "Both");
            }
            category_id = AppConstant.getCategoryIdByName(tvCategory.getText().toString());
            sub_category_id = AppConstant.getSubCategoryIdByName(tvSuitableFor.getText().toString().trim(), tvCategory.getText().toString(), tvSubCategory.getText().toString());
            orginal_price = etOriginalPrice.getText().toString();
            Log.e("orginal_price", ">>" + orginal_price);
            listing_price = etListingPrice.getText().toString();
            size = tvSize.getText().toString();
            brand_name = autoComTVBrand.getText().toString();
            brand_id = AppConstant.getBrandIDByName(autoComTVBrand.getText().toString());
            Log.e("WrittingBrand", brand_name);
            hashtags = getComaSepateStr(selectedHastag);
            Log.e("WrittingHastags", hashtags);
            delivery_charge = etPriceforShipping.getText().toString();
            addProduct(AllURL.addProductURL());
        }
    }

    // ------- for dialog show--------
    public void dialogShow() {
        dialog = new Dialog(con);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialoge_image_capture);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        LinearLayout llTakePhoto = (LinearLayout) dialog.findViewById(R.id.llTakePhoto);
        LinearLayout llUploadPhoto = (LinearLayout) dialog.findViewById(R.id.llUploadPhoto);
        ImageView imgdialogeCross = (ImageView) dialog.findViewById(R.id.imgdialogeCross);

        // ---------To Get picture from Gallery-----------------
        llUploadPhoto.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                AppConstant.isGallery = true;


                dialog.dismiss();


                // old code by shohel

                if (ActivityCompat.checkSelfPermission(con, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions((Activity) con,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, AppConstant.WRITEEXTERNAL_PERMISSION_RUNTIME);
                    dialog.dismiss();
                } else {
                    final Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), imagegalaryid);
                    dialog.dismiss();
                }
            }
        });

        // -------To Get picture Using Camera--------
        llTakePhoto.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                AppConstant.isGallery = false;

                if (ActivityCompat.checkSelfPermission(con, android.Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, AppConstant.CAMERA_RUNTIME_PERMISSION);
                    dialog.dismiss();
                } else {

                    final Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(i, imagecaptureid);
                    dialog.dismiss();

                }
            }
        });

        imgdialogeCross.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == AppConstant.CAMERA_RUNTIME_PERMISSION) {

//            Log.e("Permission length on camera",             grantResults.length + " yes");

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Now user should be able to use camera

                if (ActivityCompat.checkSelfPermission(con, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions((Activity) con,
                            new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, AppConstant.WRITEEXTERNAL_PERMISSION_RUNTIME);
                } else {
                    final Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(i, imagecaptureid);
                }
            } else {
                // Your app will not have this permission. Turn off all functions
                // that require this permission or it will force close like your
                // original question
            }
        } else if (requestCode == AppConstant.WRITEEXTERNAL_PERMISSION_RUNTIME) {

//            Log.e("Permission length on gallery",             grantResults.length + " yes");
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                if (AppConstant.isGallery) {
                    final Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), imagegalaryid);
                } else {
                    final Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(i, imagecaptureid);
                }
            }

        }


    }


    //--------Call onActivityResult After gating image from camera or galary  -----------
    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == imagegalaryid && resultCode == Activity.RESULT_OK) {

            try {
                final Uri selectedImageUri = data.getData();// Create URI from data which onActivityResult give
                final Bitmap bitmap = BitmapFactory.decodeStream(con.getContentResolver().openInputStream(selectedImageUri));//Create bitmap from URI
                String roatedPath = setToImageView(bitmap);
                dialogCropImage(rotatBitmap(roatedPath, bitmap));
            } catch (final Exception e) {
                return;
            }

        } else if (requestCode == imagecaptureid && resultCode == Activity.RESULT_OK) {

            try {
                final Bundle extras = data.getExtras();
                final Bitmap b = (Bitmap) extras.get("data");
                String roatedPath = setToImageView(b);
                dialogCropImage(rotatBitmap(roatedPath, b));


            } catch (final Exception e) {
                return;
            }

        }
    }

    public static Bitmap rotatBitmap(String src, Bitmap bitmap) {
        try {
            int orientation = getExifOrientation(src);

            if (orientation == 1) {
                return bitmap;
            }

            Matrix matrix = new Matrix();
            switch (orientation) {
                case 2:
                    matrix.setScale(-1, 1);
                    break;
                case 3:
                    matrix.setRotate(180);
                    break;
                case 4:
                    matrix.setRotate(180);
                    matrix.postScale(-1, 1);
                    break;
                case 5:
                    matrix.setRotate(90);
                    matrix.postScale(-1, 1);
                    break;
                case 6:
                    matrix.setRotate(90);
                    break;
                case 7:
                    matrix.setRotate(-90);
                    matrix.postScale(-1, 1);
                    break;
                case 8:
                    matrix.setRotate(-90);
                    break;
                default:
                    return bitmap;
            }

            try {
                Bitmap oriented = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                bitmap.recycle();
                return oriented;
            } catch (OutOfMemoryError e) {
                e.printStackTrace();
                return bitmap;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bitmap;
    }

    private static int getExifOrientation(String src) throws IOException {
        int orientation = 1;

        try {
            /**
             * if your are targeting only api level >= 5
             * ExifInterface exif = new ExifInterface(src);
             * orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
             */
            if (Build.VERSION.SDK_INT >= 5) {
                Class<?> exifClass = Class.forName("android.media.ExifInterface");
                Constructor<?> exifConstructor = exifClass.getConstructor(new Class[]{String.class});
                Object exifInstance = exifConstructor.newInstance(new Object[]{src});
                Method getAttributeInt = exifClass.getMethod("getAttributeInt", new Class[]{String.class, int.class});
                Field tagOrientationField = exifClass.getField("TAG_ORIENTATION");
                String tagOrientation = (String) tagOrientationField.get(null);
                orientation = (Integer) getAttributeInt.invoke(exifInstance, new Object[]{tagOrientation, 1});
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (java.lang.InstantiationException e) {
            e.printStackTrace();
        }

        return orientation;
    }

    /**
     * ==========================Dialog for Crop Image===============================================
     */
    private void dialogCropImage(final Bitmap bitmap) {
        final Dialog dialogCropGalary = new Dialog(con);
        dialogCropGalary.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogCropGalary.setContentView(R.layout.dialoge_image_crop);
        dialogCropGalary.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialogCropGalary.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        //============Initialization=================
        final CropImageView cropImageView = (CropImageView) dialogCropGalary.findViewById(R.id.cropImageView);
        ImageView imgCropDone = (ImageView) dialogCropGalary.findViewById(R.id.imgCropDone);
        ImageView imgCropCross = (ImageView) dialogCropGalary.findViewById(R.id.imgCropCross);

        //==========Set bitmap in cropImageView========
        cropImageView.setImageBitmap(bitmap);


        /**
         * =========================On Click========================================================
         */
        imgCropDone.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap bit = cropImageView.getCroppedBitmap();//To Get cropped image from cropImageView, and create bitmap.
                path = setToImageView(bit);//Create path from bitmap, here setToImageView is a custom method
                if (capture.equalsIgnoreCase("1")) {
                    AppConstant.path1 = path;// save path in AppConstant
                    Log.e("path1", ">>>>>" + AppConstant.path1);
                    imagePlus1.setImageBitmap(bit);//Set Image bitmap in imageView
                    imageCross1.setVisibility(View.VISIBLE);
                    photo_one = new File(AppConstant.path1);//convert from path to file to send for API call

                } else if (capture.equalsIgnoreCase("2")) {
                    AppConstant.path2 = path;
                    imagePlus2.setImageBitmap(bit);
                    Log.e("path2", ">>>>>" + AppConstant.path2);
                    photo_two = new File(AppConstant.path2);
                    imageCross2.setVisibility(View.VISIBLE);

                } else if (capture.equalsIgnoreCase("3")) {
                    AppConstant.path3 = path;
                    imagePlus3.setImageBitmap(bit);
                    Log.e("path3", ">>>>>" + AppConstant.path3);
                    photo_three = new File(AppConstant.path3);
                    imageCross3.setVisibility(View.VISIBLE);


                } else if (capture.equalsIgnoreCase("4")) {
                    AppConstant.path4 = path;
                    imagePlus4.setImageBitmap(bit);
                    Log.e("path4", ">>>>>" + AppConstant.path4);
                    photo_four = new File(AppConstant.path4);
                    imageCross4.setVisibility(View.VISIBLE);

                }
                crossClick();
                dialogCropGalary.dismiss();
            }
        });

        imgCropCross.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogCropGalary.dismiss();
            }
        });

        dialogCropGalary.show();

    }

//    private void imageClick() {
//
//        if (TextUtils.isEmpty(AppConstant.path1)) {
//            imagePlus2.setEnabled(false);
//            imagePlus3.setEnabled(false);
//            imagePlus4.setEnabled(false);
//            imageViewPlus1.setBackgroundResource(R.drawable.image_cap_bg);
//            imagePlus2.setBackgroundResource(R.drawable.add_image);
//            imagePlus3.setBackgroundResource(R.drawable.add_image);
//            imagePlus4.setBackgroundResource(R.drawable.add_image);
//        } else {
//            imagePlus2.setEnabled(true);
//            imagePlus3.setEnabled(false);
//            imagePlus4.setEnabled(false);
//        }
//
//        if (TextUtils.isEmpty(AppConstant.path2)) {
//            imagePlus3.setEnabled(false);
//            imagePlus4.setEnabled(false);
//            imageViewPlus1.setBackgroundResource(R.drawable.add_image);
//            imagePlus2.setBackgroundResource(R.drawable.image_cap_bg);
//            imagePlus3.setBackgroundResource(R.drawable.add_image);
//            imagePlus4.setBackgroundResource(R.drawable.add_image);
//        } else {
//            imagePlus3.setBackgroundResource(R.drawable.image_cap_bg);
//            imagePlus3.setEnabled(true);
//            imagePlus4.setEnabled(false);
//        }
//
//        if (TextUtils.isEmpty(AppConstant.path3)) {
//            imagePlus4.setEnabled(false);
//            imageViewPlus1.setBackgroundResource(R.drawable.add_image);
//            imagePlus2.setBackgroundResource(R.drawable.add_image);
//            imagePlus3.setBackgroundResource(R.drawable.image_cap_bg);
//            imagePlus4.setBackgroundResource(R.drawable.add_image);
//        } else {
//            imageViewPlus1.setBackgroundResource(R.drawable.add_image);
//            imagePlus2.setBackgroundResource(R.drawable.add_image);
//            imagePlus3.setBackgroundResource(R.drawable.add_image);
//            imagePlus4.setBackgroundResource(R.drawable.image_cap_bg);
//            imagePlus4.setBackgroundResource(R.drawable.image_cap_bg);
//            imagePlus4.setEnabled(true);
//        }
//    }

    private String setToImageView(Bitmap bitmap) {
        try {
            final Bitmap bit = BitmapUtils.getResizedBitmap(bitmap, 600);
            final double time = System.currentTimeMillis();
            imageLocal = saveBitmapIntoSdcard(bit, "ebuser" + time + ".png");
            Log.e("camera saved URL :  ", " " + imageLocal);

        } catch (final IOException e) {
            e.printStackTrace();
            imageLocal = "";
            Log.e("camera saved URL :  ", e.toString());
        }

        return imageLocal;

    }


    private String saveBitmapIntoSdcard(Bitmap bitmap22, String filename) throws IOException {
        /*
         *
		 * check the path and create if needed
		 */
        createBaseDirctory();
        try {
            new Date();

            OutputStream out = null;
            file = new File(SellFragment.dir, "/" + filename);

            if (file.exists()) {
                file.delete();
            }

            out = new FileOutputStream(file);

            bitmap22.compress(Bitmap.CompressFormat.PNG, 100, out);

            out.flush();
            out.close();
            // Toast.makeText(context, "Saved", Toast.LENGTH_SHORT).show();
            return file.getAbsolutePath();
        } catch (final Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public void createBaseDirctory() {
        final String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        dir = new File(extStorageDirectory + "/sloppy");

        if (SellFragment.dir.mkdir()) {
            System.out.println("Directory created");
        } else {
            System.out.println("Directory is not created or exists");
        }
    }


//    private void deliveryType(String delivery) {
//
//        if (delivery.equalsIgnoreCase("men")) {
//            imgMeetIn2.setImageResource(R.drawable.dott);
//            imgShipping2.setImageResource(R.drawable.dot);
//            imgInternationalShipping2.setImageResource(R.drawable.dot);
//            lLPriceFor.setVisibility(View.GONE);
//
//        } else if (delivery.equalsIgnoreCase("shipping")) {
//            imgMeetIn2.setImageResource(R.drawable.dot);
//            imgShipping2.setImageResource(R.drawable.dott);
//            imgInternationalShipping2.setImageResource(R.drawable.dot);
//            lLPriceFor.setVisibility(View.VISIBLE);
//
//        } else if (delivery.equalsIgnoreCase("international")) {
//            imgMeetIn2.setImageResource(R.drawable.dot);
//            imgShipping2.setImageResource(R.drawable.dot);
//            lLPriceFor.setVisibility(View.GONE);
//            imgInternationalShipping2.setImageResource(R.drawable.dott);
//        }
//    }


    private class BrandNameAdapter extends ArrayAdapter<BrandInfo> {
        Context context;

        BrandNameAdapter(Context context) {
            super(context, R.layout.list_search_category, AppConstant.commonResponse.getBrand());

            this.context = context;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View v = convertView;

            if (v == null) {
                final LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.list_search_category, null);
            }

            if (position < AppConstant.commonResponse.getBrand().size()) {
                final BrandInfo query = AppConstant.commonResponse.getBrand().get(position);
                Log.e("Name", "" + query.toString());
                final TextView tvCategory = (TextView) v.findViewById(R.id.tvCategory);
                tvCategory.setText(query.getName());
            }

            return v;

        }


    }

    /*
     * ---------------------------------API-5. Add Product  call ------------------------------
     */
    protected void addProduct(final String url) {
        // --- for net check-----
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }

        final BusyDialog busyNow = new BusyDialog(con, true, false);
        busyNow.show();
        final AsyncHttpClient addclient = new AsyncHttpClient();
        addclient.addHeader("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token));
        Log.e("token", PersistData.getStringData(con, AppConstant.token));

        final RequestParams param = new RequestParams();

        try {
            param.put("name", name);
            param.put("description", description);
            param.put("photo_one", photo_one);
            Log.e(" photo_one", photo_one + "");
            param.put("category_id", category_id);
            param.put("sub_category_id", sub_category_id);
            param.put("suitable_for", suitable_for);
            param.put("new_tag", new_tag);

            if (photo_two != null) {
                param.put("photo_two", photo_two);
                Log.e(" photo_two", photo_two + "");
            }
            if (photo_three != null) {
                param.put("photo_three", photo_three);
                Log.e(" photo_three", photo_three + "");
            }
            if (photo_four != null) {
                param.put("photo_four", photo_four);
                Log.e(" photo_four", photo_four + "");
            }
            param.put("brand_id", brand_id);
            param.put("brand_name", brand_name);
            param.put("size", size);
            if (!TextUtils.isEmpty(color_code)) {
                param.put("color_code", color_code);
            }
            param.put("hashtags", hashtags);
            param.put("original_price", orginal_price);
            param.put("listing_price", listing_price);
            param.put("delivery_type", delivery_type);
            if (delivery_type.equalsIgnoreCase("shipping")) {
                param.put("delivery_charge", delivery_charge);
            }

        } catch (final Exception e1) {
            e1.printStackTrace();
        }
        Log.e("AddProduct URL", url + "");
        addclient.post(url, param, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {

                if (busyNow != null) {
                    busyNow.dismis();
                }

                Log.e("AddProresposne ", ">>" + new String(response));

                Gson g = new Gson();
                addProductResponse = g.fromJson(new String(response), AddProductResponse.class);

                Log.e("status", "=" + addProductResponse.getStatus());

                if (addProductResponse.getStatus().equalsIgnoreCase("1")) {
                    Toast.makeText(con, addProductResponse.getMsg() + "", Toast.LENGTH_LONG).show();
                    StartActivity.toActivity(con, FeedActivity.class);
                    StartActivity.toActivity(con, MyTabActivity.class);
                    getActivity().finish();

                } else {
                    AlertMessage.showMessage(con, "Status", addProductResponse.getMsg() + "");
                    return;
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  byte[] errorResponse, Throwable e) {

                if (busyNow != null) {
                    busyNow.dismis();
                }
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    private void fontSet() {
        helveticaNeuRegular = Typeface.createFromAsset(getActivity().getAssets(), "font/helveticaNeuRegular.ttf");
        helveticaNeueBold = Typeface.createFromAsset(getActivity().getAssets(), "font/helveticaNeueBold.ttf");
        tvSell2.setTypeface(helveticaNeuRegular);
        tvAboutpro.setTypeface(helveticaNeueBold);
        tvProductnew.setTypeface(helveticaNeueBold);
        tvHashtags.setTypeface(helveticaNeueBold);
        tvDelivery.setTypeface(helveticaNeueBold);
        tvSellNow.setTypeface(helveticaNeueBold);
        tvYes.setTypeface(helveticaNeuRegular);
        tvNo.setTypeface(helveticaNeuRegular);
        etProName.setTypeface(helveticaNeuRegular);
        etProDescription.setTypeface(helveticaNeuRegular);
        tvSize.setTypeface(helveticaNeuRegular);
        etOriginalPrice.setTypeface(helveticaNeuRegular);
        etListingPrice.setTypeface(helveticaNeuRegular);
        etPriceforShipping.setTypeface(helveticaNeuRegular);
        tvNewwith.setTypeface(helveticaNeuRegular);
        autoComTVBrand.setTypeface(helveticaNeuRegular);
        tvMeetIn.setTypeface(helveticaNeuRegular);
        tvSaveDraft.setTypeface(helveticaNeueBold);
    }

    private void colorList() {
        colorlist = new ArrayList<>();
        colorlist.add("EE204D");
        colorlist.add("F52887");
        colorlist.add("FF7538");
        colorlist.add("FCE883");
        colorlist.add("1CAC78");
        colorlist.add("1F75FE");
        colorlist.add("8E35EF");
        colorlist.add("E7C697");
        colorlist.add("CDC5C2");
        colorlist.add("808080");
        colorlist.add("95918C");
        colorlist.add("2F4F4F");
        colorlist.add("DCB66F");
        colorlist.add("B4674D");
        colorlist.add("FAA76C");
        colorlist.add("0000A0");
        colorlist.add("ADD8E6");
        colorlist.add("808000");
        colorlist.add("800000");
        colorlist.add("FAAFBE");
        colorlist.add("008000");
        colorlist.add("FFFF00");
        colorlist.add("FFFFFF");
    }

    private void deliveryType(String delivery) {
        if (delivery.equalsIgnoreCase("men")) {
            delivery_type = "men";
            imgMeetIn2.setImageResource(R.drawable.dott);
            imgShipping2.setImageResource(R.drawable.dot);
            imgInternationalShipping2.setImageResource(R.drawable.dot);
            lLPriceFor.setVisibility(View.GONE);

        } else if (delivery.equalsIgnoreCase("shipping")) {
            delivery_type="shipping";
            imgMeetIn2.setImageResource(R.drawable.dot);
            imgShipping2.setImageResource(R.drawable.dott);
            imgInternationalShipping2.setImageResource(R.drawable.dot);
            lLPriceFor.setVisibility(View.VISIBLE);

        } else if (delivery.equalsIgnoreCase("international")) {
            delivery_type = "international";
            imgMeetIn2.setImageResource(R.drawable.dot);
            imgShipping2.setImageResource(R.drawable.dot);
            lLPriceFor.setVisibility(View.GONE);
            imgInternationalShipping2.setImageResource(R.drawable.dott);
        }
    }

}
