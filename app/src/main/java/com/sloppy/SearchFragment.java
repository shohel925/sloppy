package com.sloppy;


import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.gson.Gson;
import com.sloppy.aapbdLib.AlertMessage;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.NetInfo;
import com.sloppy.aapbdLib.PersistData;
import com.sloppy.aapbdLib.PersistentUser;
import com.sloppy.model.CategoryInfo;
import com.sloppy.model.SearchProductResponse;
import com.sloppy.model.SearchUserResponse;
import com.sloppy.utils.AAPBDHttpClient;
import com.sloppy.utils.AllURL;
import com.sloppy.utils.AppConstant;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.concurrent.Executors;


/**
 * Created by hp on 4/24/2016.
 */
public class SearchFragment extends BaseFragment {

    /**
     * ---------------variable-------------------
     */
    private Context con;
    private View view;
    private LinearLayout layoutWoMan, layoutMan, linearLUsearchBefore, linearLUsearchAfter;
    private ImageView imgWoman, imgMan, imgMenuSearch;
    private TextView tvCategorySearch, tvWoman, tvman, tvJoinFestivals, tvItem, tvPeople;
    private EditText etProName;
    private String genType, searchType;


    // ---Grid View----
    GridView gridViewCategory;
    AddCategoryAdapter CategoryAdapter;
    Typeface helveticaNeuRegular, helveticaNeueBold;
    // ----SubCategoryAdapter-----
    ArrayList<CategoryInfo> categoryList;
    private SearchProductResponse searchProductResponse;
    private SearchUserResponse userResponse;
    private ImageView imageCrosSearch;

    /**
     * ------------------On Create------------------------------------
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_search, container, false);
        con = getActivity();
        initUI();
        return view;
    }

    private void initUI() {

        // ------- ImageView---------
        imageCrosSearch = (ImageView) view.findViewById(R.id.imageCrosSearch);
        imgMan = (ImageView) view.findViewById(R.id.imgMan);
        imgWoman = (ImageView) view.findViewById(R.id.imgWoman);
        imgMenuSearch = (ImageView) view.findViewById(R.id.imgMenuSearch);

        // -----------TextView------------
        tvJoinFestivals = (TextView) view.findViewById(R.id.tvJoinFestivals);
        tvCategorySearch = (TextView) view.findViewById(R.id.tvCategorySearch);
        tvman = (TextView) view.findViewById(R.id.tvman);
        tvWoman = (TextView) view.findViewById(R.id.tvWoman);
        tvItem = (TextView) view.findViewById(R.id.tvItem);
        tvPeople = (TextView) view.findViewById(R.id.tvPeople);
        etProName = (EditText) view.findViewById(R.id.etProName);
        // ----------- LinearLayout------------
        layoutWoMan = (LinearLayout) view.findViewById(R.id.layoutWoMan);
        layoutMan = (LinearLayout) view.findViewById(R.id.layoutMan);

        linearLUsearchBefore = (LinearLayout) view.findViewById(R.id.linearLUsearchBefore);
        linearLUsearchAfter = (LinearLayout) view.findViewById(R.id.linearLUsearchAfter);

        //---------------Grid View-------------------------------
        gridViewCategory = (GridView) view.findViewById(R.id.gridViewCategory);
        imageCrosSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etProName.setText("");
            }
        });

        /**
         * --------------Use Font----------------------
         */
        helveticaNeuRegular = Typeface.createFromAsset(getActivity().getAssets(), "font/helveticaNeuRegular.ttf");
        helveticaNeueBold = Typeface.createFromAsset(getActivity().getAssets(), "font/helveticaNeueBold.ttf");
        tvJoinFestivals.setTypeface(helveticaNeueBold);
        tvJoinFestivals.setTypeface(helveticaNeueBold);
        tvman.setTypeface(helveticaNeuRegular);
        tvWoman.setTypeface(helveticaNeuRegular);
        tvJoinFestivals.setTypeface(helveticaNeuRegular);
        tvCategorySearch.setTypeface(helveticaNeuRegular);
        tvItem.setTypeface(helveticaNeuRegular);
        tvPeople.setTypeface(helveticaNeuRegular);
        searchType = "item";
        etProName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (!TextUtils.isEmpty(etProName.getText().toString())) {
                        AppConstant.keyboardHide(con, view);
                        if (searchType.equalsIgnoreCase("item")) {
                            if (PersistentUser.isLogged(con)) {
                                callSearchProductAPI(etProName.getText().toString(), "all", "", genType, getResources().getString(R.string.page_items), "all", "");
                            } else {
                                callSearchProductAPI(etProName.getText().toString(), "all", "", genType, getResources().getString(R.string.page_items), "demo", "");
                            }
                        } else if (searchType.equalsIgnoreCase("people")) {
                            if (PersistentUser.isLogged(con)) {
                                callSearchUserAPI(etProName.getText().toString(), "all");
                            } else {
                                callSearchUserAPI(etProName.getText().toString(), "demo");
                            }
                        }
                    }
                    return true;
                }
                return false;
            }
        });
        etProName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(charSequence.toString())) {
                    imageCrosSearch.setVisibility(View.GONE);
                } else {
                    imageCrosSearch.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        tvItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchType = "item";
                tvItem.setBackgroundResource(R.drawable.item_back_sweet);
                tvItem.setTextColor(getResources().getColor(R.color.grayText));
                tvPeople.setBackgroundResource(R.drawable.peaple_back_black);
                tvPeople.setTextColor(getResources().getColor(R.color.sweet));
            }
        });

        tvPeople.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchType = "people";
                tvItem.setBackgroundResource(R.drawable.item_back_black);
                tvItem.setTextColor(getResources().getColor(R.color.sweet));
                tvPeople.setBackgroundResource(R.drawable.peaple_back_sweet);
                tvPeople.setTextColor(getResources().getColor(R.color.grayText));
            }
        });


        categoryList = new ArrayList<CategoryInfo>();

        if (categoryList != null) {
            categoryList.clear();
        }
        for (int i = 0; i < AppConstant.commonResponse.getCategory().size(); i++) {
            if ((AppConstant.commonResponse.getCategory().get(i).getType().equalsIgnoreCase("2")) || (AppConstant.commonResponse.getCategory().get(i).getType().equalsIgnoreCase("0"))) {
                categoryList.add(AppConstant.commonResponse.getCategory().get(i));
            }
        }

        //---Category Adapter call-----
        genType = "woman";
        AppConstant.categoryType = "woman";
        CategoryAdapter = new AddCategoryAdapter(con);
        gridViewCategory.setAdapter(CategoryAdapter);
        CategoryAdapter.notifyDataSetChanged();

		/*
         * -------------------- On Click-----------------
		 */
        imgMenuSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.showMenuDiadog(getActivity().getFragmentManager());
            }
        });


        tvJoinFestivals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myCommunicator.setContentFragment(new FirstFestivalFragment(), true);
            }
        });


        linearLUsearchBefore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLUsearchBefore.setVisibility(View.GONE);
                linearLUsearchAfter.setVisibility(View.VISIBLE);
                etProName.requestFocus();
                AppConstant.OpenKeyBoard(con);

            }
        });

        layoutMan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.categoryType = "man";
                if (categoryList != null) {
                    categoryList.clear();
                }
                for (int i = 0; i < AppConstant.commonResponse.getCategory().size(); i++) {
                    if ((AppConstant.commonResponse.getCategory().get(i).getType().equalsIgnoreCase("2")) || (AppConstant.commonResponse.getCategory().get(i).getType().equalsIgnoreCase("1"))) {
                        categoryList.add(AppConstant.commonResponse.getCategory().get(i));
                        Log.e("Category", AppConstant.commonResponse.getCategory().get(i).getName());
                    }
                }
                changeBg("man");
                // ---Adapter call-----
                genType = "man";
                CategoryAdapter = new AddCategoryAdapter(con);
                gridViewCategory.setAdapter(CategoryAdapter);
                CategoryAdapter.notifyDataSetChanged();
            }

        });

        layoutWoMan.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AppConstant.categoryType = "woman";
                if (categoryList != null) {
                    categoryList.clear();
                }
                for (int i = 0; i < AppConstant.commonResponse.getCategory().size(); i++) {
                    if ((AppConstant.commonResponse.getCategory().get(i).getType().equalsIgnoreCase("2")) || (AppConstant.commonResponse.getCategory().get(i).getType().equalsIgnoreCase("0"))) {
                        categoryList.add(AppConstant.commonResponse.getCategory().get(i));
                    }
                }
                changeBg("woman");
                genType = "woman";
                // ---Adapter call-----
                CategoryAdapter = new AddCategoryAdapter(con);
                gridViewCategory.setAdapter(CategoryAdapter);
                CategoryAdapter.notifyDataSetChanged();
            }

        });


    }


    /*
     * --------Change Category Gender Background Method-------
     */
    private void changeBg(String gender) {

        if (gender.equalsIgnoreCase("man")) {
            tvman.setTextColor(Color.parseColor("#FB9A7F"));
            imgMan.setImageResource(R.drawable.man_select);
            tvWoman.setTextColor(Color.parseColor("#000000"));
            imgWoman.setImageResource(R.drawable.women);
        } else if (gender.equalsIgnoreCase("woman")) {
            tvWoman.setTextColor(Color.parseColor("#FB9A7F"));
            imgWoman.setImageResource(R.drawable.women_select);
            tvman.setTextColor(Color.parseColor("#000000"));
            imgMan.setImageResource(R.drawable.man);
        }
    }


    /**
     * --------------------Category Grid View Array Adapter-------------------------------
     */
    private class AddCategoryAdapter extends ArrayAdapter<CategoryInfo> {

        Context context;

        AddCategoryAdapter(Context context) {
            super(context, R.layout.searce_grid_wiew,
                    categoryList);
            this.context = context;
        }

        @Override
        public View getView(final int position, final View convertView,
                            ViewGroup parent) {
            View v = convertView;

            if (v == null) {
                final LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.searce_grid_wiew, null);
            }

            if (position < categoryList.size()) {

                final CategoryInfo query = categoryList.get(position);

                Log.e("Name", ">>" + query.toString());

                // --------View initialization-------
                final ImageView imgMostpo = (ImageView) v.findViewById(R.id.imgMostpo);
                final TextView tvMostpo = (TextView) v.findViewById(R.id.tvMostpo);
                tvMostpo.setTypeface(helveticaNeuRegular);

                if (genType.equalsIgnoreCase("man")) {

                    tvMostpo.setText(query.getName());
                    if (query.getImage_man() == null) {
                        Picasso.with(con).load(R.drawable.ic_launcher).into(imgMostpo);
                    } else {
                        Picasso.with(con).load(query.getImage_man()).into(imgMostpo);
                    }
                } else if (genType.equalsIgnoreCase("woman")) {
                    tvMostpo.setText(query.getName());
                    if (query.getImage_woman() == null) {
                        Picasso.with(con).load(R.drawable.ic_launcher)
                                .into(imgMostpo);
                    } else {
                        Picasso.with(con).load(query.getImage_woman())
                                .into(imgMostpo);
                    }
                }
                /**
                 * --------Click On grid view element----------------
                 */
                v.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        AppConstant.categoryID = query.getId();
                        AppConstant.categoryName = query.getName();
                        AppConstant.categoryInfo = categoryList.get(position);
                        myCommunicator.addContentFragment(new SubCategoryFragment(), true);
                        AppConstant.keyboardHide(con, view);
                    }

                });


            }

            return v;

        }
    }

    protected void callSearchProductAPI(final String search_text, final String search_type, final String category_id, final String suitable_for, final String page_items, final String user_type, final String subcategory_id) {
        /**
         * =====================Check Internet======================================================
         */
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }
        /**
         * =====================Start progress bar==================================================
         */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
        /**
         * =====================Start Thread========================================================
         */
        Executors.newSingleThreadExecutor().submit(new Runnable() {
            String msg = "";
            String response = "";

            @Override
            public void run() {

                try {
                    Log.e("Search Product URL", AllURL.getSearchProduct(search_text, search_type, category_id, suitable_for, page_items, user_type, subcategory_id));

                    response = AAPBDHttpClient.get(AllURL.getSearchProduct(search_text, search_type, category_id, suitable_for, page_items, user_type, subcategory_id)).//==API call====
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();
                    Gson gson = new Gson();
                    searchProductResponse = gson.fromJson(response, SearchProductResponse.class);
                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        /**
                         * ======stop progress bar==================================================
                         */
                        if (busy != null) {
                            busy.dismis();
                        }
                        /**
                         * ==============Here make UI Related work==================================
                         */
                        if (searchProductResponse.getStatus().equalsIgnoreCase("1")) {
                            Log.e("Search Product Response", ">>" + response);
                            AppConstant.productListTitel = "Search Product";
                            AppConstant.productList = searchProductResponse.getResult().getData();

                            myCommunicator.addContentFragment(new CommonProductListFragment(), true);

                        } else {

                            msg = searchProductResponse.getMsg();
                            AlertMessage.showMessage(con, "My follow ", msg);

                        }

                    }
                });

            }

        });

    }

    protected void callSearchUserAPI(final String search_text, final String user_type) {
        /**
         * =====================Check Internet======================================================
         */
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }
        /**
         * =====================Start progress bar==================================================
         */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
        /**
         * =====================Start Thread========================================================
         */
        Executors.newSingleThreadExecutor().submit(new Runnable() {
            String msg = "";
            String response = "";

            @Override
            public void run() {

                try {
                    Log.e("Search User URL", AllURL.getSearchUserList(search_text, user_type));

                    response = AAPBDHttpClient.get(AllURL.getSearchUserList(search_text, user_type)).//==API call====
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();
                    Gson gson = new Gson();
                    userResponse = gson.fromJson(response, SearchUserResponse.class);
                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        /**
                         * ======stop progress bar==================================================
                         */
                        if (busy != null) {
                            busy.dismis();
                        }
                        /**
                         * ==============Here make UI Related work==================================
                         */
                        if (userResponse.getStatus().equalsIgnoreCase("1")) {
                            Log.e("Search User Response", ">>" + response);
                            AppConstant.productListTitel = "Search User";
                            AppConstant.userList = userResponse.getResult().getData();
                            myCommunicator.addContentFragment(new SearchUserListFragment(), true);
                        } else {
                            msg = userResponse.getMsg();
                            AlertMessage.showMessage(con, "My follow ", msg);
                        }

                    }
                });

            }

        });

    }

}
