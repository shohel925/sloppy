package com.sloppy;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.sloppy.model.BrandInfo;
import com.sloppy.model.ProductAndUserDetailsInfo;
import java.util.Vector;

public class DatabaseHandler extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 1;// Database Version

	private static final String DATABASE_NAME = "productInfo";// Database Name

	private static final String TABLE_Product_details = "Product_details";	// Table Name

	/**
	 * ==================Table column Name==========================================================
	 */
	private static final String KEY_id = "ID";
	private static final String KEY_UserId = "UserId";
	private static final String KEY_name = "name";
	private static final String KEY_description = "description";
	private static final String KEY_photo_one = "photo_one";
	private static final String KEY_photo_two = "photo_two";
	private static final String KEY_photo_three = "photo_three";
	private static final String KEY_photo_four = "photo_four";
	private static final String KEY_category_id = "category_id";
	private static final String KEY_sub_category_id = "sub_category_id";
	private static final String KEY_brand_name = "brand_name";
	private static final String KEY_size = "size";
	private static final String KEY_color_code = "color_code";
	private static final String KEY_new_tag= "new_tag";
	private static final String KEY_suitable_for = "suitable_for";
	private static final String KEY_original_price = "original_price";
	private static final String KEY_listing_price = "listing_price";
	private static final String KEY_delivery_type = "delivery_type";
	private static final String KEY_delivery_charge = "delivery_charge";
	private static final String KEY_hashtag_info = "hashtag_info";

	/**
	 *==DatabaseHandler constructor call first when we create an instance of DatabaseHandler in
	 * other activity/fragment to access this class and use the method of this class
     */
	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// ========= Creating a string to crate a table and it pass in onCreate metho ==========================
	final String CREATE_PRODUCT_TABLE = "CREATE TABLE " + TABLE_Product_details
			+ "(" + KEY_id + " INTEGER PRIMARY KEY , "
			+ KEY_name + " TEXT , " + KEY_description + " TEXT , "
			+ KEY_photo_one + " TEXT , " + KEY_photo_two + " TEXT , "
			+ KEY_photo_three + " TEXT , " + KEY_photo_four + " TEXT , "
			+ KEY_category_id + " TEXT , " + KEY_sub_category_id + " TEXT , "
			+ KEY_brand_name + " TEXT , " + KEY_size + " TEXT , "
			+ KEY_color_code + " TEXT , " + KEY_new_tag + " TEXT , "
			+ KEY_suitable_for + " TEXT , " + KEY_original_price + " TEXT , "
			+ KEY_listing_price + " TEXT , " + KEY_delivery_type + " TEXT , "
			+ KEY_delivery_charge + " TEXT , " + KEY_hashtag_info + " TEXT , "+ KEY_UserId + " TEXT "  + ")";

	// onCreate method used to create Table
	@Override
	public void onCreate(SQLiteDatabase db) {

		db.execSQL(CREATE_PRODUCT_TABLE);

	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_Product_details);
		// Create tables again
		onCreate(db);
	}


	/**
	 *==============================Insert Data=====================================================
     */
	public boolean insurtProductInfo(ProductAndUserDetailsInfo pInfo) {

		final SQLiteDatabase db = getWritableDatabase();// Create an instance of SQLiteDatabase

		final ContentValues cv = new ContentValues();// ContentValues is used to put all data in separate column

		cv.put(KEY_name, pInfo.getName());
		cv.put(KEY_description, pInfo.getDescription());
		cv.put(KEY_photo_one, pInfo.getPhoto_one());
		cv.put(KEY_photo_two, pInfo.getPhoto_two());
		cv.put(KEY_photo_three, pInfo.getPhoto_three());
		cv.put(KEY_photo_four, pInfo.getPhoto_four());
		cv.put(KEY_category_id, pInfo.getCategory_id());
		cv.put(KEY_sub_category_id, pInfo.getSub_category_id());
		cv.put(KEY_brand_name, pInfo.getBrand_info().get(0).getName());
		cv.put(KEY_size, pInfo.getSize());
		cv.put(KEY_color_code, pInfo.getColor_code());
		cv.put(KEY_new_tag, pInfo.getNew_tag());
		cv.put(KEY_suitable_for, pInfo.getSuitable_for());
		cv.put(KEY_original_price, pInfo.getOriginal_price());
		cv.put(KEY_listing_price, pInfo.getListing_price());
		cv.put(KEY_delivery_type, pInfo.getDelivery_type());
		cv.put(KEY_delivery_charge, pInfo.getDelivery_charge());
		cv.put(KEY_hashtag_info, pInfo.getHashtag_info());
		cv.put(KEY_UserId, pInfo.getUser_id());

		long result= db.insert(TABLE_Product_details, null, cv);// Inserting Row
		//db.insert it return the row ID of the newly inserted row, or -1 if an error occurred
		db.close();// Closing database connection
		if (result == -1){
			return false;
		} else {
			return true;
		}
	}

	public int getDraftDataSize(String userid){
		final SQLiteDatabase db = getWritableDatabase();
		final String selectQuery = "SELECT * FROM " + TABLE_Product_details+" WHERE "+KEY_UserId+"="+userid;// Query to get all data from the table
		final Cursor cursor = db.rawQuery(selectQuery, null);// Cursor class help to get all data from the separate column
		return cursor.getCount();
	}

	/**
	 *=======To create a vector/List using inserted table data======================================
     */
	public Vector<ProductAndUserDetailsInfo>  getAllProduct(String userid) {//this getAllProduct method returns a vector
		final Vector<ProductAndUserDetailsInfo> myList = new Vector<ProductAndUserDetailsInfo>();
		myList.removeAllElements();//used to remove all data if any data exist

		final SQLiteDatabase db = getWritableDatabase();
		final String selectQuery = "SELECT * FROM " + TABLE_Product_details +" WHERE "+KEY_UserId+"="+userid;
		final Cursor cursor = db.rawQuery(selectQuery, null);// Cursor class help to get all data from the separate column
		// looping through all rows and adding  data to create list
		if (cursor.moveToFirst()) {
			do {
				ProductAndUserDetailsInfo pInfo = new ProductAndUserDetailsInfo();// here we use a model class to put all data
				pInfo.setId(cursor.getString(0));// to get first column data
				Log.e("Inserted ID",">> "+cursor.getString(0));
				pInfo.setName(cursor.getString(1));// to get second column data
				pInfo.setDescription(cursor.getString(2));
				pInfo.setPhoto_one(cursor.getString(3));
				pInfo.setPhoto_two(cursor.getString(4));
				pInfo.setPhoto_three(cursor.getString(5));
				pInfo.setPhoto_four(cursor.getString(6));
				pInfo.setCategory_id(cursor.getString(7));
				pInfo.setSub_category_id(cursor.getString(8));

				BrandInfo brandInfoList= new BrandInfo();
				brandInfoList.setName(cursor.getString(9));
				pInfo.getBrand_info().add(brandInfoList);

				pInfo.setSize(cursor.getString(10));
				pInfo.setColor_code(cursor.getString(11));
				pInfo.setNew_tag(cursor.getString(12));
				pInfo.setSuitable_for(cursor.getString(13));
				pInfo.setOriginal_price(cursor.getString(14));
				pInfo.setListing_price(cursor.getString(15));
				pInfo.setDelivery_type(cursor.getString(16));
				pInfo.setDelivery_charge(cursor.getString(17));
				pInfo.setHashtag_info(cursor.getString(18));
				pInfo.setUser_id(cursor.getString(19));
				myList.addElement(pInfo);

			} while (cursor.moveToNext());
		}
		// return custom list
		return myList;
	}

	/**
	 *==================To Update data of the row according to the ID===============================
     */
	public boolean updateProductInfo(ProductAndUserDetailsInfo pInfo) {

		final SQLiteDatabase db = getWritableDatabase();

		ContentValues cv = new ContentValues();
		cv.put(KEY_name, pInfo.getName()); //These Fields should be your String values of actual column names
		cv.put(KEY_description,pInfo.getDescription());
		cv.put(KEY_photo_one,pInfo.getPhoto_one());
		cv.put(KEY_photo_two,pInfo.getPhoto_two());
		cv.put(KEY_photo_three,pInfo.getPhoto_three());
		cv.put(KEY_photo_four,pInfo.getPhoto_four());
		cv.put(KEY_category_id,pInfo.getCategory_id());
		cv.put(KEY_sub_category_id,pInfo.getSub_category_id());
		cv.put(KEY_brand_name,pInfo.getBrand_info().get(0).getName());
		cv.put(KEY_size,pInfo.getSize());
		cv.put(KEY_color_code,pInfo.getColor_code());
		cv.put(KEY_new_tag,pInfo.getNew_tag());
		cv.put(KEY_suitable_for,pInfo.getSuitable_for());
		cv.put(KEY_original_price,pInfo.getOriginal_price());
		cv.put(KEY_listing_price,pInfo.getListing_price());
		cv.put(KEY_delivery_type,pInfo.getDelivery_type());
		cv.put(KEY_delivery_charge,pInfo.getDelivery_charge());
		cv.put(KEY_hashtag_info,pInfo.getHashtag_info());
		cv.put(KEY_UserId,pInfo.getUser_id());
		Log.e("ID",">> "+pInfo.getId());
		//=== db.update returns an integer value of the number of rows affected
		int result=db.update(TABLE_Product_details, cv," ID= ?", new String[]{ pInfo.getId() });
		db.close();
		Log.e("result",">> "+result);

		if(result==0){
			return false;
		}else{
			return true;
		}

	}

	/**
	 *===========To delete row according to the ID==================================================
     */
	public Integer deteteProductInfo(String id){
		final SQLiteDatabase db = getWritableDatabase();
		return db.delete(TABLE_Product_details," ID = ?", new String[]{ id });
	}

}
