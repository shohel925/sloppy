package com.sloppy;

import android.*;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sloppy.aapbdLib.AlertMessage;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.NetInfo;
import com.sloppy.aapbdLib.PersistData;
import com.sloppy.aapbdLib.PersistentUser;
import com.sloppy.adapter.CommentAdapter;
import com.sloppy.adapter.RecyclerProductListAdapter;
import com.sloppy.adapter.SingleProPagerAdepter;
import com.sloppy.model.BrandDataInfo;
import com.sloppy.model.HashtagInfo;
import com.sloppy.model.ProCommentAddResponse;
import com.sloppy.model.ProCommentListUserDetailsInfo;
import com.sloppy.model.ProductAndUserDetailsInfo;
import com.sloppy.model.LikeUnlikeResponse;
import com.sloppy.model.ProCommentListResponse;
import com.sloppy.model.ProCommentListResultInfo;
//import com.sloppy.model.SigProDetailsInfo;
import com.sloppy.model.ProductOfferResponse;
//import com.sloppy.model.SingleProductDetailsResponse;
import com.sloppy.utils.AAPBDHttpClient;
import com.sloppy.utils.AllURL;
import com.sloppy.utils.AppConstant;
import com.sloppy.utils.CustomScrollView;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;

import android.Manifest;

/**
 * Created by hp on 5/4/2016.
 */
public class SingleProductFragment extends BaseFragment {
    private Context con;
    private View view;
    private TextView tvSPLikeNumber, tvCategorySin, tvSiProSelname, tvSinProName, tvSinProOriginalPrice, tvSPlistingPrice, tvSingProBrand,
            tvSPsize, tvSPDescription, tvSPCommCount, tvSinProBuyNow, tvAddToBundelSing;
    private ImageView imgLVSinglePro, imgLSinglePro, imgSoldStamp, imgBackDescription, imgSinProUser, imgSinComments, imgSingleShare, imgSingleOffer;
    private ProCommentListResponse proCommentListResponse;
    private ProductAndUserDetailsInfo details;
    private List<ProCommentListResultInfo> tempList;
    private LinearLayout hashTagLayout;
    private String pictureUrl = "";
    private RecyclerView rvComment;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        con = getActivity();
        view = inflater.inflate(R.layout.fragment_single_product, container, false);

        tempList = new ArrayList<>();
        if (tempList != null) {
            tempList.clear();
        }
        //--------Call 6. Single product details of a product id.----------------
        if (!PersistentUser.isLogged(con)) {
            callProductCommentListAPI(con, AppConstant.productIDfeedRow, "demo");
        } else {
            callProductCommentListAPI(con, AppConstant.productIDfeedRow, "all");
        }

        initUI();
        return view;
    }

    private void initUI() {
        //--------------------Initialization===================
        rvComment = (RecyclerView) view.findViewById(R.id.rvComment);
        hashTagLayout = (LinearLayout) view.findViewById(R.id.hashTagLayout);
        imgLVSinglePro = (ImageView) view.findViewById(R.id.imgLVSinglePro);
        imgLSinglePro = (ImageView) view.findViewById(R.id.imgLSinglePro);
        imgBackDescription = (ImageView) view.findViewById(R.id.imgBackDescription);
        ImageView imgMenuSngl = (ImageView) view.findViewById(R.id.imgMenuSngl);
        imgSinProUser = (ImageView) view.findViewById(R.id.imgSinProUser);
        imgSinComments = (ImageView) view.findViewById(R.id.imgSinComments);
        imgSoldStamp = (ImageView) view.findViewById(R.id.imgSoldStamp);
        imgSingleShare = (ImageView) view.findViewById(R.id.imgSingleShare);
        imgSingleOffer = (ImageView) view.findViewById(R.id.imgSingleOffer);
        tvCategorySin = (TextView) view.findViewById(R.id.tvCategorySin);
        tvSiProSelname = (TextView) view.findViewById(R.id.tvSiProSelname);
        tvSinProName = (TextView) view.findViewById(R.id.tvSinProName);
        tvSinProOriginalPrice = (TextView) view.findViewById(R.id.tvSinProOriginalPrice);
        tvSPlistingPrice = (TextView) view.findViewById(R.id.tvSPlistingPrice);
        tvSingProBrand = (TextView) view.findViewById(R.id.tvSingProBrand);
        tvSPsize = (TextView) view.findViewById(R.id.tvSPsize);
        tvSPDescription = (TextView) view.findViewById(R.id.tvSPDescription);
        tvSPLikeNumber = (TextView) view.findViewById(R.id.tvSPLikeNumber);
        tvSPCommCount = (TextView) view.findViewById(R.id.tvSPCommCount);
        tvSinProBuyNow = (TextView) view.findViewById(R.id.tvSinProBuyNow);
        TextView tvLeaveComment = (TextView) view.findViewById(R.id.tvLeaveComment);
        tvAddToBundelSing = (TextView) view.findViewById(R.id.tvAddToBundelSing);


        details = AppConstant.feedRowObject;
        setProductDetailsData();

        /**
         * ---------------------------On click-----------------------------------
         */


        imgBackDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        imgMenuSngl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MenuFragmentDialog dialogMenu = new MenuFragmentDialog();
                dialogMenu.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar);
                dialogMenu.show(getActivity().getFragmentManager(), "");
            }
        });

        imgLVSinglePro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PersistentUser.isLogged(con)) {
                    callLikeUnlikeAPI(details.getId());
                } else {
                    AppConstant.loginDialoag(con);
                }

            }
        });

        imgSinComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!PersistentUser.isLogged(con)) {
                    AppConstant.loginDialoag(con);
                } else {
                    AppConstant.productIDfeedRow = details.getId();
                    AppConstant.tagUserList = details.getTaggable_users();
                    CommantDialogFragment dialogMenu = new CommantDialogFragment();
                    dialogMenu.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            tvSPCommCount.setText(AppConstant.commentCount);
                            if (!PersistentUser.isLogged(con)) {
                                callProductCommentListAPI(con, AppConstant.productIDfeedRow, "demo");
                            } else {
                                callProductCommentListAPI(con, AppConstant.productIDfeedRow, "all");
                            }
                        }
                    });
                    dialogMenu.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar);
                    dialogMenu.show(getActivity().getFragmentManager(), "");
                }
            }
        });

        tvLeaveComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!PersistentUser.isLogged(con)) {
                    AppConstant.loginDialoag(con);
                } else {
                    AppConstant.productIDfeedRow = details.getId();
                    AppConstant.tagUserList = details.getTaggable_users();
                    CommantDialogFragment dialogMenu = new CommantDialogFragment();
                    dialogMenu.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            tvSPCommCount.setText(AppConstant.commentCount);
                            if (!PersistentUser.isLogged(con)) {
                                callProductCommentListAPI(con, AppConstant.productIDfeedRow, "demo");
                            } else {
                                callProductCommentListAPI(con, AppConstant.productIDfeedRow, "all");
                            }
                        }
                    });
                    dialogMenu.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar);
                    dialogMenu.show(getActivity().getFragmentManager(), "");
                }
            }
        });

        imgSingleShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (PersistentUser.isLogged(con)) {
                    pictureUrl = details.getPhoto_one();
                    if (ActivityCompat.checkSelfPermission(con, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 11);
                    } else {
                        Picasso.with(con).load(pictureUrl).memoryPolicy(MemoryPolicy.NO_CACHE).into(target);
                    }

                } else {
                    AppConstant.loginDialoag(con);
                }
            }
        });

        tvSingProBrand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.brandID = details.getBrand_info().get(0).getId();
                AppConstant.brandName = details.getBrand_info().get(0).getName();
                myCommunicator.setContentFragment(new BrandFragment(), true);
            }
        });

        tvAddToBundelSing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PersistentUser.isLogged(con)) {
                    AppConstant.productIDfeedRow = details.getId();
                    AppConstant.otherUserId = details.getUser_id();
                    AddToBundleFragment fragment = new AddToBundleFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("titleBundle", details.getSeller_info().get(0).getUsername());
                    fragment.setArguments(bundle);
                    myCommunicator.setContentFragment(fragment, true);
                } else {
                    AppConstant.loginDialoag(con);
                }
            }
        });

        imgSingleOffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!PersistentUser.isLogged(con)) {
                    AppConstant.loginDialoag(con);
                } else {
                    OfferDialogFragment offerDialog = new OfferDialogFragment();
                    offerDialog.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar);
                    offerDialog.show(getActivity().getFragmentManager(), "offerdialog");
                }
            }
        });

        tvSinProBuyNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PersistentUser.isLogged(con)) {
                    myCommunicator.setContentFragment(new BuyNowFragment(), true);
                } else {
                    AppConstant.loginDialoag(con);
                }
            }
        });


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 11) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Picasso.with(con).load(pictureUrl).memoryPolicy(MemoryPolicy.NO_CACHE).into(target);
            }
        }
    }

    BusyDialog busyDialog;

    private Target target = new Target() {


        @Override
        public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
            new Thread(new Runnable() {

                @Override
                public void run() {

                    File file = new File(Environment.getExternalStorageDirectory().getPath() + "/picture.jpg");
                    try {
                        file.createNewFile();
                        FileOutputStream ostream = new FileOutputStream(file);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, ostream);
                        busyDialog.dismis();
                        String path = MediaStore.Images.Media.insertImage(con.getContentResolver(), bitmap, "Title", null);
                        AppConstant.defaultShare(con, Uri.parse(path));
                        ostream.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }).start();
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
            busyDialog.dismis();
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
            if (placeHolderDrawable != null) {
            }
            busyDialog = new BusyDialog(con, false, "");
            busyDialog.show();
        }
    };


    /**
     * -------------------12. Product Comments List  API------------------
     */
    public void callProductCommentListAPI(final Context con, final String product_id, final String userType) {
  /*
   * ---------------check internet first------------
   */
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }
         /*
          * ----------------Show Busy Dialog -----------------------------------------
          */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
  /*
   *
   *----------------------Start Thread-------------------------------------------
   *
   */
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            String msg = "";
            String response = "";

            @Override
            public void run() {
                // You can performed your task here.

                try {
                    Log.e("ProCommentList URL", AllURL.getProCommentList(product_id, userType));
                    //-------------Hit Server---------------------
                    response = AAPBDHttpClient.get(AllURL.getProCommentList(product_id, userType)).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();

                    //----------------Persist response with the help of Gson----------------
                    Log.e("ProCommentList Response", ">>" + response);


                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        /**
                         * -------------Here we do UI related tasks inside run() method of runOnUiThread()-------------------
                         */

                        //-------Stop Busy Dialog-----
                        if (busy != null) {
                            busy.dismis();
                        }
                        Gson gson = new Gson();
                        proCommentListResponse = gson.fromJson(response, ProCommentListResponse.class);
                        /**
                         * ---------main Ui related work--------------
                         */
                        if (proCommentListResponse.getStatus().equalsIgnoreCase("1")) {
                            Log.e("CommentList size", ">>" + proCommentListResponse.getResult().size());
                            if (proCommentListResponse.getResult().size() > 0) {
                                rvComment.setLayoutManager(new LinearLayoutManager(con));
                                rvComment.setAdapter(new CommentAdapter(getActivity(), proCommentListResponse.getResult()));
                            } else {
                                rvComment.setVisibility(View.GONE);
                            }

                        } else {

                            msg = proCommentListResponse.getMsg();
                            AlertMessage.showMessage(con, "ProCommentList", msg);
                        }

                    }
                });

            }

        });

    }

    private void setProductDetailsData() {

        final List<String> singleProImage = new ArrayList<>();

        if (details.getPhoto_one() != null) {
            singleProImage.add(details.getPhoto_one());
        }
        if (details.getPhoto_two() != null) {
            singleProImage.add(details.getPhoto_two());
        }
        if (details.getPhoto_three() != null) {
            singleProImage.add(details.getPhoto_three());
        }
        if (details.getPhoto_four() != null) {
            singleProImage.add(details.getPhoto_four());
        }
        final ViewPager singleProViewPazer = (ViewPager) view.findViewById(R.id.singleProViewPazer);
        singleProViewPazer.setClipToPadding(false);
        singleProViewPazer.setPageMargin(20);
        if (singleProImage.size() == 1) {
            singleProViewPazer.setPadding(0, 0, 0, 0);
        } else {
            singleProViewPazer.setPadding(0, 0, 24, 0);
        }
        SingleProPagerAdepter allSingleProImage = new SingleProPagerAdepter(con, singleProImage);
        singleProViewPazer.setAdapter(allSingleProImage);
        singleProViewPazer.setCurrentItem(0);


        singleProViewPazer.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                position = position % singleProImage.size();
                if (position == 0) {
                    singleProViewPazer.setPadding(0, 0, 24, 0);
                } else if (position == (singleProImage.size() - 1)) {
                    singleProViewPazer.setPadding(40, 0, 0, 0);
                } else {
                    singleProViewPazer.setPadding(24, 0, 24, 0);
                }
            }

            @Override
            public void onPageScrollStateChanged(int position) {

            }
        });


        /**
         * --------------------------Set Data----------------------------
         */
        hashTagLayout.removeAllViews();
        for (int i = 0; i < details.getHashtag_detail().size(); i++) {
            final HashtagInfo info = details.getHashtag_detail().get(i);
            View newView = ((LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.hashtag_row, hashTagLayout, false);
            TextView tvHashtagSP = (TextView) newView.findViewById(R.id.tvHashtagSP);
            tvHashtagSP.setText("#" + info.getName());
            tvHashtagSP.setTag(i);
            tvHashtagSP.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Object position = view.getTag();
                    if (position != null) {
                        AppConstant.hashtagId = details.getHashtag_detail().get((Integer) position).getId();
                        AppConstant.hashtagName = details.getHashtag_detail().get((Integer) position).getName();
                        myCommunicator.setContentFragment(new HashTagFragment(), true);

                    }
                }
            });
            hashTagLayout.addView(newView);
        }
        if (details.getIs_sold().equalsIgnoreCase("1")) {
            imgSoldStamp.setVisibility(View.VISIBLE);
        } else {
            imgSoldStamp.setVisibility(View.GONE);
        }
        if (details.getUser_id().equalsIgnoreCase(PersistData.getStringData(con, AppConstant.user_id)) || details.getIs_sold().equalsIgnoreCase("1")) {
            tvAddToBundelSing.setVisibility(View.GONE);
            imgSingleOffer.setVisibility(View.GONE);
            tvSinProBuyNow.setVisibility(View.GONE);
        } else {
            tvAddToBundelSing.setVisibility(View.VISIBLE);
            imgSingleOffer.setVisibility(View.VISIBLE);
            tvSinProBuyNow.setVisibility(View.VISIBLE);
        }

        imgLSinglePro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (PersistentUser.isLogged(con)) {
                    callLikeUnlikeAPI(details.getId());
                } else {
                    AppConstant.loginDialoag(con);
                }
            }
        });


        if (details.getIs_liked().equalsIgnoreCase("0")) {
            imgLVSinglePro.setImageResource(R.drawable.love_unliked);
            imgLSinglePro.setImageResource(R.drawable.love_unliked);
        } else {
            imgLVSinglePro.setImageResource(R.drawable.love_liked);
            imgLSinglePro.setImageResource(R.drawable.love_liked);
        }

        Picasso.with(con)
                .load(details.getSeller_info().get(0).getProfile_image())
                .placeholder(R.drawable.person)
                .error(R.drawable.person)
                .into(imgSinProUser);
        tvSiProSelname.setText(details.getSeller_info().get(0).getUsername());
        tvSiProSelname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.otherUserId = details.getSeller_info().get(0).getUser_id();
                myCommunicator.setContentFragment(new OtherUserDetailsFragment(), true);
            }
        });

        tvSinProName.setText(details.getName());
        /**
         * -------------Set currency simble-----------------------------------------------------
         */
        tvSPlistingPrice.setText(AppConstant.getCurrencySymbol(details.getSeller_info().get(0).getCurrency()) + details.getOriginal_price());
        tvSinProOriginalPrice.setText(AppConstant.getCurrencySymbol(details.getSeller_info().get(0).getCurrency()) + details.getListing_price());
        tvSPlistingPrice.setPaintFlags(tvSinProOriginalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);


        tvSingProBrand.setText(details.getBrand_info().get(0).getName());
        String subcategoryName = AppConstant.getSubCategoryName(details.getCategory_id(), details.getSub_category_id());
        if (TextUtils.isEmpty(subcategoryName)) {
            tvCategorySin.setText(AppConstant.getCategoryName(details.getCategory_id()));
        } else {
            tvCategorySin.setText(AppConstant.getCategoryName(details.getCategory_id()) + " --> " + subcategoryName);
        }
        if (details.getSize_info() != null) {
            tvSPsize.setText(details.getSize_info().getSize());
        } else {
            tvSPsize.setText(details.getSize());
        }
        tvSPDescription.setText(details.getDescription());
        tvSPLikeNumber.setText(details.getLike_count());
        if (Integer.parseInt(details.getComments_count().toString())>0) {
            tvSPCommCount.setText(details.getComments_count());
        }
    }


    /**
     * -------------------10. Like/Unlike Product API------------------
     */
    private void callLikeUnlikeAPI(final String product_id) {
  /*
   * ---------------check internet first------------
   */
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }
  /*
   * ----------------Show Busy Dialog -----------------------------------------
   */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
  /*
   *
   *----------------------Start Thread-------------------------------------------
   *
   */
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            String msg = "";
            String response = "";
            LikeUnlikeResponse likeUnlikeResponse;

            @Override
            public void run() {
                //--------------- We can performed your task here.-----------------

                try {
                    Log.i("Like/Unlike URL", AllURL.getLikeUnlike(product_id));
                    //-------------Hit Server---------------------
                    response = AAPBDHttpClient.get(AllURL.getLikeUnlike(product_id)).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();

                    //----------------Persist response with the help of Gson----------------
                    Log.i("Like/Unlike Response", ">>" + response);

                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        /**
                         * -------------Here we do UI related tasks inside run() method of runOnUiThread()-------------------
                         */

                        //-------Stop Busy Dialog-----
                        if (busy != null) {
                            busy.dismis();
                        }
                        /**
                         * ---------main Ui related work--------------
                         */
                        Gson gson = new Gson();
                        likeUnlikeResponse = gson.fromJson(response, LikeUnlikeResponse.class);
                        if (likeUnlikeResponse.getMsg().equalsIgnoreCase("liked")) {
                            imgLVSinglePro.setImageResource(R.drawable.love_liked);
                            imgLSinglePro.setImageResource(R.drawable.love_liked);
                            tvSPLikeNumber.setText(likeUnlikeResponse.getLikes_count());
                        } else if (likeUnlikeResponse.getMsg().equalsIgnoreCase("unliked")) {
                            imgLVSinglePro.setImageResource(R.drawable.love_unliked);
                            imgLSinglePro.setImageResource(R.drawable.love_unliked);
                            tvSPLikeNumber.setText(likeUnlikeResponse.getLikes_count());
                        }
                    }
                });
            }
        });
    }
}
