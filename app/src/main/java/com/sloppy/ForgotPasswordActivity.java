package com.sloppy;

import android.app.Activity;
import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.sloppy.aapbdLib.AlertMessage;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.NetInfo;
import com.sloppy.aapbdLib.PersistData;
import com.sloppy.model.ForgotPasswordResponse;
import com.sloppy.utils.AAPBDHttpClient;
import com.sloppy.utils.AllURL;
import com.sloppy.utils.AppConstant;
import java.util.concurrent.Executors;

public class ForgotPasswordActivity extends Activity {
	
//------------------Variable-------------------	
	Context con;
	
	TextView tvRetrivePassword;
	EditText etUserOrEmail;
	ForgotPasswordResponse forgotPasswordResponse;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_forgot_password);
		con = this;
		initUI();
	}

	private void initUI() {
		//-------------Initialization----------------
		tvRetrivePassword = (TextView) findViewById(R.id.tvRetrivePassword);
		etUserOrEmail=(EditText) findViewById(R.id.etUserOrEmail);
		ImageView imgMenuForg= (ImageView) findViewById(R.id.imgMenuForg);
		
		
//-----------------------On Click ---------------------------------------------------		
		imgMenuForg.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				FragmentManager manager= getFragmentManager();
				AppConstant.showMenuDiadog(manager);
			}
		});
		tvRetrivePassword.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (TextUtils.isEmpty(etUserOrEmail.getText().toString())) {
					AppConstant.alertDialoag(con,"Warning!",getString(R.string.email_user_name),"Close");
				} else {
					callForgotPassword(etUserOrEmail.getText().toString());
				}
			}
		});
		
		findViewById(R.id.imgBackRetrivePass).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});

	}

	/**
	 * -------------------30. Forgot Password------------------
	 */
	private void callForgotPassword(final String query_key) {
  /*
   * ---------------check internet first------------
   */
		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
			return;
		}

  /*
   * ----------------Show Busy Dialog -----------------------------------------
   */
		final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
		busy.show();
  /*
   *
   *----------------------Start Thread-------------------------------------------
   *
   */
		Executors.newSingleThreadExecutor().submit(new Runnable() {

			String msg = "";
			String response = "";

			@Override
			public void run() {
				// You can performed your task here.
				try {

					Log.e("ForgotPassword URL", AllURL.forgotPassword(query_key));
					//-------------Hit Server---------------------
					response = AAPBDHttpClient.get(AllURL.forgotPassword(query_key)).
							header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();

					//----------------Persist response with the help of Gson----------------
					Log.e("ForgotPassword Response", ">>" + response);
					Gson gson = new Gson();
					forgotPasswordResponse = gson.fromJson(response, ForgotPasswordResponse.class);

				} catch (Exception e1) {
					e1.printStackTrace();
					msg = e1.getMessage();
				}

				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						/**
						 * -------------Here we do UI related tasks inside run() method of runOnUiThread()-------------------
						 */

						//-------Stop Busy Dialog-----
						if (busy != null) {
							busy.dismis();
						}
						/**
						 * ---------main Ui related work--------------
						 */
						if (forgotPasswordResponse.getStatus().equalsIgnoreCase("1")) {

							alertDialoagForgot(con,getResources().getString(R.string.check_your_email),getResources().getString(R.string.password_recovery),"Ok");
						} else {
							msg = forgotPasswordResponse.getMsg();
							AppConstant.alertDialoag(con,getResources().getString(R.string.alert),msg,getResources().getString(R.string.ok));
						}
					}
				});
			}
		});
	}

	public void alertDialoagForgot(final Context con, final String titel, final String description, final String comand) {

		final Dialog dialogAlert = new Dialog(con);
		dialogAlert.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialogAlert.setContentView(R.layout.dialog_alert);
		dialogAlert.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		dialogAlert.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

		TextView tvTitel = (TextView) dialogAlert.findViewById(R.id.tvTitel);
		TextView tvDescription = (TextView) dialogAlert.findViewById(R.id.tvDescription);
		TextView tvComand = (TextView) dialogAlert.findViewById(R.id.tvComand);

		Typeface helveticaNeuRegular = Typeface.createFromAsset(con.getAssets(), "font/helveticaNeuRegular.ttf");
		Typeface helveticaNeueBold = Typeface.createFromAsset(con.getAssets(), "font/helveticaNeueBold.ttf");
		tvTitel.setTypeface(helveticaNeueBold);
		tvDescription.setTypeface(helveticaNeuRegular);
		tvComand.setTypeface(helveticaNeuRegular);

		tvTitel.setText(titel);
		tvDescription.setText(description);
		tvComand.setText(comand);

		tvComand.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialogAlert.dismiss();
				finish();

			}
		});

		dialogAlert.show();
	}

}
