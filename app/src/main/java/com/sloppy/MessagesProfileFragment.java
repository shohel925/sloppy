package com.sloppy;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.sloppy.aapbdLib.AlertMessage;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.NetInfo;
import com.sloppy.aapbdLib.PersistData;
import com.sloppy.model.MyMessageResultInfo;
import com.sloppy.model.MyMessagesListResponse;
import com.sloppy.utils.AAPBDHttpClient;
import com.sloppy.utils.AllURL;
import com.sloppy.utils.AppConstant;
import com.squareup.picasso.Picasso;
import java.util.List;
import java.util.concurrent.Executors;

/**
 * Created by hp on 3/13/2016.
 */
public class MessagesProfileFragment extends BaseFragment {
    Context con;
    View view;
    private RecyclerView rvMessage;
    private MyMessagesListResponse myMessResponse;
    private  ImageView imgMenuMessage;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {
        con=getActivity();
        view=inflater.inflate(R.layout.fragment_messages, container, false);
        initUI();
        return view;
    }

    private void initUI() {
        rvMessage=(RecyclerView) view.findViewById(R.id.rvMessage);
        rvMessage.setLayoutManager(new LinearLayoutManager(con));
        imgMenuMessage=(ImageView) view.findViewById(R.id.imgMenuMessage);
        imgMenuMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.showMenuDiadog(getActivity().getFragmentManager());
            }
        });
        callMessagesAPI();
        ImageView imgMessageBack=(ImageView) view.findViewById(R.id.imgMessageBack);
        imgMessageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
    }


/*
*   -------------------API-29- My Message------------------------------
 */

    protected void callMessagesAPI() {
          /*
           * check internet first
           */
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }

          /*
           * start progress bar
           */

        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
          /*
           * start Thread
           */
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            String msg = "";
            String response="";

            @Override
            public void run() {
                // You can performed your task here.

                try {
                /**
                 * --------API-29----Get Message--------------------
                 */                  long unixTime = System.currentTimeMillis() / 1000L;
                    String date =String.valueOf(unixTime);
                    Log.e("Date Unix", ">>" + date);
                    response = AAPBDHttpClient.get(AllURL.getMessagesData(date)).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();
                    Gson gson = new Gson();
                    myMessResponse = gson.fromJson(response, MyMessagesListResponse.class);


                } catch (Exception e1) {

                    e1.printStackTrace();

                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                  /*
                   *----------------- stop progress bar--------------------
                   */
                        if (busy != null) {
                            busy.dismis();
                        }


                        if (myMessResponse.getStatus().equalsIgnoreCase("0")) {
                            msg = myMessResponse.getMsg();
                            AlertMessage.showMessage(con, "Sloppy Alert", msg);
                        } else {
                            Log.e("MyMessagesResponse", ">>" + response);
                            Toast.makeText(con, "My Messages " + myMessResponse.getMsg(), Toast.LENGTH_LONG).show();
                            if(myMessResponse.getResult().size() > 0){

                                rvMessage.setAdapter(new MyRecyclerAdapter(con,myMessResponse.getResult()));
                            } else {
                                TextView tvDataNotFound= (TextView) view.findViewById(R.id.tvDataNotFound);
                                tvDataNotFound.setVisibility(View.VISIBLE);
                            }


                        }

                    }
                });

            }

        });

    }

    /**
     * --------------------My Message Adapter--------------
     */
    public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.CustomViewHolder> {
        private List<MyMessageResultInfo> myMessageResultInfos;
        private Context mContext;
        public MyRecyclerAdapter(Context context,List<MyMessageResultInfo> myMessageResultInfos){
            this.mContext=context;
            this.myMessageResultInfos=myMessageResultInfos;
        }
        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_messages_profile,null);
            CustomViewHolder viewHolder=new CustomViewHolder(view);
            return viewHolder;
        }
        public class CustomViewHolder extends RecyclerView.ViewHolder {
            private ImageView imgMessages;
            private TextView tvUName,tvMsgCount;
            private RelativeLayout rlMessage;

            public CustomViewHolder(View view) {
                super(view);
                this.imgMessages = (ImageView) view.findViewById(R.id.imgMessages);
                this.tvUName = (TextView) view.findViewById(R.id.tvUName);
                this.tvMsgCount = (TextView) view.findViewById(R.id.tvMsgCount);
                this.rlMessage = (RelativeLayout) view.findViewById(R.id.rlMessage);
            }
        }

        @Override
        public void onBindViewHolder(CustomViewHolder holder, final int position) {
            final MyMessageResultInfo myResult= myMessageResultInfos.get(position);
            holder.tvUName.setText(myResult.getName());
            holder.tvMsgCount.setText(String.valueOf(myResult.getMessageList().size()));
            Picasso.with(mContext)
                    .load(myResult.getProfile_image())
                    .placeholder(R.drawable.ic_launcher)
                    .error(R.drawable.ic_launcher)
                    .into(holder.imgMessages);
            holder.rlMessage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppConstant.chatList= myResult;
                    myCommunicator.setContentFragment(new MessageChatFragment(),true);
                }
            });

        }

        @Override
        public int getItemCount() {
            return (null != myMessageResultInfos ? myMessageResultInfos.size() : 0);
        }


    }
}
