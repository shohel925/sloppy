package com.sloppy;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sloppy.aapbdLib.AlertMessage;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.NetInfo;
import com.sloppy.aapbdLib.PersistData;
import com.sloppy.aapbdLib.PersistentUser;
import com.sloppy.model.FeedbackResultInfo;
import com.sloppy.model.FollowUnfollowResponse;
import com.sloppy.model.LikeUnlikeResponse;
import com.sloppy.model.MyProfileResponse;
import com.sloppy.model.OtherUserDetailsResponse;
import com.sloppy.model.ProductAndUserDetailsInfo;
import com.sloppy.model.SendMessageResponse;
import com.sloppy.model.SubmitFeedbackResponse;
import com.sloppy.model.UserFeedbacksResponse;
import com.sloppy.utils.AAPBDHttpClient;
import com.sloppy.utils.AllURL;
import com.sloppy.utils.AppConstant;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;


/**
 * Created by hp on 4/27/2016.
 */
public class OtherUserDetailsFragment extends BaseFragment {
    /**
     * ----------------------Variable------------------------
     */
    private Context con;
    private View view;
    private int ratingCount = 0;
    private String typeFollow;
    private GridView gvUserShopsItem;
    private TextView tvFollowUser, tvOtrUsrName, tvfolllogfdwers, tvFeedBa, tvMyRatingo, tvFollowin, tvSendme, tvNicknamee, tvOUFeedbackCount, tvNicknameShop, tvfolllowersCount,
            tvOUMRCount, tvShopCount, tvfolllowingCount, tvRatingCount, tvFeedBackText, tvFBUserName, feedbackTime, tvBio, tnNoFeedback;
    private ImageView imgLoveShop, ivOterUser, imgMenuOther, ivFeedBack, imgFeedbackUser, ivRating;
    private EditText etMessagingPerso, etWriteFeedback;
    private LinearLayout lLSendMessage, llFollowerOther, llFollowingOther;
    private RatingBar rb;
    private OtherUserDetailsResponse otrUDResponse, otherUserDetailsResponse;
    private Typeface helveticaNeuRegular, helveticaNeueBold;
    private ShopeItemAdapter shopeItemAdapter;
    FeedbacksListAdapter feedbacksListAdapter;
    private UserFeedbacksResponse feedbacksResponse;
    private SubmitFeedbackResponse submitFeedbackResponse;
    String rattingNue;
    private LinearLayout layoutFol;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        con = getActivity();
        view = inflater.inflate(R.layout.fragment_other_user_details, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initUI();
    }

    private void initUI() {
        if (!PersistentUser.isLogged(con)) {
            getUserDetailsOtherAPI(AppConstant.otherUserId, "demo");
        } else {
            getUserDetailsOtherAPI(AppConstant.otherUserId, "all");
        }
        layoutFol=(LinearLayout)view.findViewById(R.id.layoutFol);
        otrUDResponse = AppConstant.otherUserDetailsResponse;
        otherUserDetailsResponse = AppConstant.otherUserDetailsResponse;

        ImageView imgOtherUserBack = (ImageView) view.findViewById(R.id.imgOtherUserBack);
        imgOtherUserBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        layoutFol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               sentInstagramProfile(otherUserDetailsResponse.getUser_details().getInstagram_username());
            }
        });

        //------------Initialization-----------------
        llFollowerOther = (LinearLayout) view.findViewById(R.id.llFollowerOther);
        llFollowingOther = (LinearLayout) view.findViewById(R.id.llFollowingOther);
        gvUserShopsItem = (GridView) view.findViewById(R.id.gvUserShopsItem);
        ivOterUser = (ImageView) view.findViewById(R.id.ivOterUser);
        imgMenuOther = (ImageView) view.findViewById(R.id.imgMenuOther);
        ivFeedBack = (ImageView) view.findViewById(R.id.ivFeedBack);
        ivRating = (ImageView) view.findViewById(R.id.ivRating);
        tvOtrUsrName = (TextView) view.findViewById(R.id.tvOtrUsrName);
        tvFeedBa = (TextView) view.findViewById(R.id.tvFeedBa);
        tvBio = (TextView) view.findViewById(R.id.tvBio);
        tvMyRatingo = (TextView) view.findViewById(R.id.tvMyRatingo);
        tvfolllogfdwers = (TextView) view.findViewById(R.id.tvfolllogfdwers);
        tvFollowin = (TextView) view.findViewById(R.id.tvFollowin);
        tvSendme = (TextView) view.findViewById(R.id.tvSendme);
        tvNicknamee = (TextView) view.findViewById(R.id.tvNicknamee);
        tvOUFeedbackCount = (TextView) view.findViewById(R.id.tvOUFeedbackCount);
        tvNicknameShop = (TextView) view.findViewById(R.id.tvNicknameShop);
        tvOUMRCount = (TextView) view.findViewById(R.id.tvOUMRCount);
        tvfolllowersCount = (TextView) view.findViewById(R.id.tvfolllowersCount);
        tvShopCount = (TextView) view.findViewById(R.id.tvShopCount);
        tvfolllowingCount = (TextView) view.findViewById(R.id.tvfolllowingCount);

        lLSendMessage = (LinearLayout) view.findViewById(R.id.lLSendMessage);
        tvFollowUser = (TextView) view.findViewById(R.id.tvFollowUser);
        //-------------------------------Font set---------------------------------------------------
        helveticaNeuRegular = Typeface.createFromAsset(getActivity().getAssets(), "font/helveticaNeuRegular.ttf");
        helveticaNeueBold = Typeface.createFromAsset(getActivity().getAssets(), "font/helveticaNeueBold.ttf");
        tvOtrUsrName.setTypeface(helveticaNeuRegular);
        tvOUFeedbackCount.setTypeface(helveticaNeuRegular);
        tvOUMRCount.setTypeface(helveticaNeuRegular);
        tvFeedBa.setTypeface(helveticaNeuRegular);
        tvMyRatingo.setTypeface(helveticaNeuRegular);
        tvFollowUser.setTypeface(helveticaNeueBold);
        tvfolllowersCount.setTypeface(helveticaNeuRegular);
        tvfolllogfdwers.setTypeface(helveticaNeuRegular);
        tvfolllowingCount.setTypeface(helveticaNeuRegular);
        tvFollowin.setTypeface(helveticaNeuRegular);
        tvSendme.setTypeface(helveticaNeuRegular);
        tvNicknamee.setTypeface(helveticaNeuRegular);
        tvNicknameShop.setTypeface(helveticaNeueBold);
        tvShopCount.setTypeface(helveticaNeuRegular);

        ivFeedBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callFeedbackAPI(otherUserDetailsResponse.getUser_details().getId());
            }
        });

        ivRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callFeedbackAPI(otherUserDetailsResponse.getUser_details().getId());
            }
        });

        imgMenuOther.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.showMenuDiadog(getActivity().getFragmentManager());
            }
        });
        gvUserShopsItem.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_BUTTON_PRESS:
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;

                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });

    }

    /**
     * -------------------16. User Feedbacks List API------------------
     */
    private void callFeedbackAPI(final String user_id) {
  /*
   * ---------------check internet first------------
   */
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }

  /*
   * ----------------Show Busy Dialog -----------------------------------------
   */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
  /*
   *
   *----------------------Start Thread-------------------------------------------
   *
   */
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            String msg = "";
            String response = "";

            @Override
            public void run() {

                try {
                    Log.e("UserFeedbackList URL", AllURL.getFeedbacks(user_id));
                    //-------------Hit Server---------------------
                    response = AAPBDHttpClient.get(AllURL.getFeedbacks(user_id)).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();
                    //----------------Persist response with the help of Gson----------------
                    Log.e("UserFeedbackList Resp", ">>" + response);
                    Gson gson = new Gson();
                    feedbacksResponse = gson.fromJson(response, UserFeedbacksResponse.class);
                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //-------Stop Busy Dialog-----
                        if (busy != null) {
                            busy.dismis();
                        }
                        /**
                         * ---------main Ui related work--------------
                         */
                        if (feedbacksResponse.getStatus().equalsIgnoreCase("1")) {
                            /**
                             * ----------------Feedback Dialog Show---------------------
                             */
                            feedBacksDialog();
                        } else {
                            msg = feedbacksResponse.getMsg();
                            AlertMessage.showMessage(con, "UserFeedbackList", msg);
                        }

                    }
                });

            }

        });

    }

    /**
     * --------------------Feed Back dialog------------------------------------------
     */
    private void feedBacksDialog() {

        final Dialog dialogFeedbacks = new Dialog(con);
        dialogFeedbacks.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogFeedbacks.setContentView(R.layout.dialog_feedbacks);
        dialogFeedbacks.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogFeedbacks.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        /**
         * ---------------Initialization----------------------
         */
        TextView tvPostFeedbck = (TextView) dialogFeedbacks.findViewById(R.id.tvPostFeedbck);
        tvRatingCount = (TextView) dialogFeedbacks.findViewById(R.id.tvRatingCount);
        tnNoFeedback = (TextView) dialogFeedbacks.findViewById(R.id.tnNoFeedback);
        ImageView imgFeedbackCross = (ImageView) dialogFeedbacks.findViewById(R.id.imgFeedbackCross);
        LinearLayout llWriteComment=(LinearLayout) dialogFeedbacks.findViewById(R.id.llWriteComment);
        View vLine= (View) dialogFeedbacks.findViewById(R.id.vLine);
        rb = (RatingBar) dialogFeedbacks.findViewById(R.id.ratingBar1);
        ListView listViewFeedback = (ListView) dialogFeedbacks.findViewById(R.id.listViewFeedback);

        etWriteFeedback = (EditText) dialogFeedbacks.findViewById(R.id.etWriteFeedback);
        /**
         * ---------------Set Adapter----------------
         */
        if (PersistData.getStringData(con,AppConstant.user_id).equalsIgnoreCase(otherUserDetailsResponse.getUser_details().getId())){
            etWriteFeedback.setVisibility(View.GONE);
            vLine.setVisibility(View.GONE);
            rb.setEnabled(false);
            tvPostFeedbck.setVisibility(View.GONE);
        }

        if (feedbacksResponse.getResult().size() < 1) {
            listViewFeedback.setVisibility(View.GONE);
            tnNoFeedback.setVisibility(View.VISIBLE);
            tnNoFeedback.setTypeface(helveticaNeueBold);
        } else {
            tnNoFeedback.setVisibility(View.GONE);
            listViewFeedback.setVisibility(View.VISIBLE);
            feedbacksListAdapter = new FeedbacksListAdapter(con);
            listViewFeedback.setAdapter(feedbacksListAdapter);
            feedbacksListAdapter.notifyDataSetChanged();
        }
        /**
         * --------------On Click------------------------------
         */
        tvPostFeedbck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(etWriteFeedback.getText().toString()) && tvRatingCount.getText().toString().equalsIgnoreCase("0.0")) {
                    AppConstant.alertDialoag(con, getResources().getString(R.string.warning), getResources().getString(R.string.you_have_to_put), getResources().getString(R.string.ok));
                } else {
                    /**
                     * -------------------15. Submit Feedback API------------------
                     */
                    callSubmitFeedbackAPI(otherUserDetailsResponse.getUser_details().getId(), etWriteFeedback.getText().toString(), ratingCount);
                }
            }
        });
        imgFeedbackCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogFeedbacks.dismiss();
            }
        });
        if (!TextUtils.isEmpty(otherUserDetailsResponse.getUser_details().getRating_value())) {
            String test = String.format("%.01f", Float.parseFloat(otherUserDetailsResponse.getUser_details().getRating_value()));
            tvRatingCount.setText(test);
        }

        if(!TextUtils.isEmpty(otherUserDetailsResponse.getUser_details().getRating_value())){
            rb.setRating(Float.parseFloat(otherUserDetailsResponse.getUser_details().getRating_value()));
        }


        rb.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating,
                                        boolean fromUser) {
                ratingCount = Math.round(rating);
                tvRatingCount.setText(Float.toString(rating));
            }

        });

        dialogFeedbacks.show();
    }
    private void sentInstagramProfile(String insgramId){

        String url="http://instagram.com/_u/"+insgramId;
        Uri uri = Uri.parse(url);
        Intent insta = new Intent(Intent.ACTION_VIEW, uri);
        insta.setPackage("com.instagram.android");

        if (isIntentAvailable(con, insta)){
            startActivity(insta);
        } else{
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        }
    }

    private boolean isIntentAvailable(Context ctx, Intent intent) {
        final PackageManager packageManager = ctx.getPackageManager();
        List<ResolveInfo> list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }

    public class FeedbacksListAdapter extends ArrayAdapter<FeedbackResultInfo> {
        Context context;

        FeedbacksListAdapter(Context context) {
            super(context, R.layout.row_feedback_list, feedbacksResponse.getResult());
            this.context = context;
        }

        @Override
        public View getView(final int feedBackListPosition, View commentView, ViewGroup parent) {
            /**
             * ------------------Set the row layout-----------------------
             */
            if (commentView == null) {
                final LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                commentView = vi.inflate(R.layout.row_feedback_list, null);

            }
            Log.e("Product List Size", Integer.toString(feedbacksResponse.getResult().size()));


            if (feedBackListPosition < feedbacksResponse.getResult().size()) {

                // TO get data from vector/Arrraylist according to there position
                final FeedbackResultInfo feedBackQuery = feedbacksResponse.getResult().get(feedBackListPosition);

                /**
                 * ---------------------Initialization of Feed Back row field------------------------------
                 */
                imgFeedbackUser = (ImageView) commentView.findViewById(R.id.imgFeedbackUser);
                tvFeedBackText = (TextView) commentView.findViewById(R.id.tvFeedBackText);
                tvFBUserName = (TextView) commentView.findViewById(R.id.tvFBUserName);
                feedbackTime = (TextView) commentView.findViewById(R.id.feedbackTime);

                try {
                    /**
                     * -----------------Set data on field of Comment row------------------
                     */
                    Picasso.with(context).load(feedBackQuery.getUser_details().getProfile_image())
                            .placeholder(R.drawable.person).error(R.drawable.person).into(imgFeedbackUser);
                    tvFBUserName.setText(feedBackQuery.getUser_details().getName());
                    tvFeedBackText.setText(feedBackQuery.getComment());
                    String inputPattern = "yyyy-MM-dd HH:mm:ss";
                    String outputPattern = "HH:mm dd/MM/yy";
                    SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
                    SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

                    Date date = null;
                    String str = null;

                    try {
                        date = inputFormat.parse(feedBackQuery.getCreated_at());
                        str = outputFormat.format(date);
                        feedbackTime.setText(str);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return commentView;
        }
    }

    /**
     * -------------------15. Submit Feedback API------------------
     */
    private void callSubmitFeedbackAPI(final String user_id, final String feedback_text, final int rating_value) {
  /*
   * ---------------check internet first------------
   */
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }

  /*
   * ----------------Show Busy Dialog -----------------------------------------
   */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
  /*
   *
   *----------------------Start Thread-------------------------------------------
   *
   */
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            String msg = "";
            String response = "";

            @Override
            public void run() {

                try {
                    Log.e("SubmitFeedback URL", AllURL.getSubmitFeedback(user_id, feedback_text, rating_value));
                    //-------------Hit Server---------------------
                    response = AAPBDHttpClient.get(AllURL.getSubmitFeedback(user_id, feedback_text, rating_value)).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();

                    //----------------Persist response with the help of Gson----------------
                    Log.e("SubmitFeedback Response", ">>" + response);
                    Gson gson = new Gson();
                    submitFeedbackResponse = gson.fromJson(response, SubmitFeedbackResponse.class);

                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //-------Stop Busy Dialog-----
                        if (busy != null) {
                            busy.dismis();
                        }
                        /**
                         * ---------main Ui related work--------------
                         */
                        if (submitFeedbackResponse.getStatus().equalsIgnoreCase("1")) {
                            /**
                             * -------Success message Show---------------------
                             */
                            etWriteFeedback.setText("");
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            String currentDateandTime = sdf.format(new Date());
                            /**
                             * --------Set data locally in the List View---------------------
                             */
                            //-------Create an object same as array list---------------
                            FeedbackResultInfo newFeedback = new FeedbackResultInfo();
                            //---------set data to creat new row of array list
                            Gson gsonProfile = new Gson();
                            MyProfileResponse myProfileResponse = gsonProfile.fromJson(PersistData.getStringData(con, AppConstant.myProfileResponceAC), MyProfileResponse.class);

                            newFeedback.setComment(feedback_text);
                            newFeedback.getUser_details().setName(myProfileResponse.getUser_details().getFirst_name() + " " + myProfileResponse.getUser_details().getLast_name());
                            newFeedback.getUser_details().setProfile_image(myProfileResponse.getUser_details().getProfile_image());
                            newFeedback.setCreated_at(currentDateandTime);
//							//-------add row data in the List----------
                            feedbacksResponse.getResult().add(newFeedback);
                            //--------refresh adapter-------------
                            feedbacksListAdapter.notifyDataSetChanged();

                        } else {
                            msg = submitFeedbackResponse.getMsg();
                            Toast.makeText(con, msg, Toast.LENGTH_LONG).show();
                        }

                    }
                });

            }

        });

    }

    /**
     * -------------------17. User Details (Other profile)------------------
     */
    private void getUserDetailsOtherAPI(final String user_id, final String type) {
  /*
   * ---------------check internet first------------
   */
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }

         /*
          * ----------------Show Busy Dialog -----------------------------------------
          */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
  /*
   *
   *----------------------Start Thread-------------------------------------------
   *
   */
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            String msg = "";
            String response = "";

            @Override
            public void run() {
                // You can performed your task here.

                try {
                    Log.e("OtherUserDetails URL", AllURL.getUserDetailURL(user_id, type));
                    //-------------Hit Server---------------------
                    response = AAPBDHttpClient.get(AllURL.getUserDetailURL(user_id, type)).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();

                    //----------------Persist response with the help of Gson----------------
                    Log.e("AddBundle Response", ">>" + response);

                    Gson gson = new Gson();
                    otherUserDetailsResponse = gson.fromJson(response, OtherUserDetailsResponse.class);

                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //-------Stop Busy Dialog-----
                        if (busy != null) {
                            busy.dismis();
                        }
                        /**
                         * ---------main Ui related work--------------
                         */
                        if (otherUserDetailsResponse.getStatus().equalsIgnoreCase("1")) {
                            /**
                             * --------------Set data--------------------
                             */
                            if (PersistData.getStringData(con,AppConstant.user_id).equalsIgnoreCase(otherUserDetailsResponse.getUser_details().getId())){
//                                tvFollowUser.setVisibility(View.GONE);
                                tvFollowUser.setText(otherUserDetailsResponse.getUser_details().getUsername());
                                tvFollowUser.setEnabled(false);
                            }else{
                                tvFollowUser.setEnabled(true);
//                                tvFollowUser.setVisibility(View.VISIBLE);
                                if (otherUserDetailsResponse.getUser_details().getIs_followed() == "0") {
                                    typeFollow="1";
                                    tvFollowUser.setText("Follow " + otherUserDetailsResponse.getUser_details().getUsername());
                                } else {
                                    typeFollow="0";
                                    tvFollowUser.setText("Unfollow " + otherUserDetailsResponse.getUser_details().getUsername());
                                }
                            }
                            if (TextUtils.isEmpty(otherUserDetailsResponse.getUser_details().getInstagram_username())){
                                layoutFol.setVisibility(View.GONE);
                            }else{
                                layoutFol.setVisibility(View.VISIBLE);
                                tvNicknamee.setText(otherUserDetailsResponse.getUser_details().getInstagram_username());
                            }

                            tvShopCount.setText(String.valueOf(otherUserDetailsResponse.getUser_details().getUser_shop().size()));
                            tvOtrUsrName.setText(otherUserDetailsResponse.getUser_details().getUsername());
                            tvNicknameShop.setText(otherUserDetailsResponse.getUser_details().getUsername() + "'s shop");
                            tvBio.setText(otherUserDetailsResponse.getUser_details().getShort_bio());


                            if (!TextUtils.isEmpty(otherUserDetailsResponse.getUser_details().getFeedback_count())) {
                                tvOUFeedbackCount.setText(otherUserDetailsResponse.getUser_details().getFeedback_count());
                            } else {
                                tvOUFeedbackCount.setText("0");
                            }

                            if (!TextUtils.isEmpty(otherUserDetailsResponse.getUser_details().getRating_value())) {
                                String test = String.format("%.01f", Float.parseFloat(otherUserDetailsResponse.getUser_details().getRating_value()));
                                tvOUMRCount.setText(test);
                            } else {
                                tvOUMRCount.setText(R.string.doble_zero);
                            }

                            if (otherUserDetailsResponse.getUser_details().getFollower_count().equalsIgnoreCase("1")){
                                tvfolllogfdwers.setText("Follower");
                            }else {
                                tvfolllogfdwers.setText("Followers");
                            }


                            tvfolllowersCount.setText(otherUserDetailsResponse.getUser_details().getFollower_count());
                            tvfolllowingCount.setText(otherUserDetailsResponse.getUser_details().getFollowing_count());

                            Picasso.with(con).load(otherUserDetailsResponse.getUser_details().getProfile_image())
                                    .placeholder(R.drawable.person).error(R.drawable.person).into(ivOterUser);


                            /**
                             * ---------------Call Shope List Adapter--------------------
                             */
                            shopeItemAdapter = new ShopeItemAdapter(con);
                            gvUserShopsItem.setAdapter(shopeItemAdapter);
                            shopeItemAdapter.notifyDataSetChanged();
                            //        setGridViewHeightBasedOnChildren( gvUserShopsItem , 2 );
                            gvUserShopsItem.setOnTouchListener(new View.OnTouchListener() {
                                // Setting on Touch Listener for handling the touch inside ScrollView
                                @Override
                                public boolean onTouch(View v, MotionEvent event) {
                                    // Disallow the touch request for parent scroll on touch of child view
                                    v.getParent().requestDisallowInterceptTouchEvent(true);
                                    return false;
                                }
                            });

                            /**
                             * ----------------ON Click------------------------------------------------
                             */
                            llFollowerOther.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    AppConstant.followingFollower = "follower";
                                    AppConstant.otherUserIdFoll = otherUserDetailsResponse.getUser_details().getId();
                                    FollowersFollowingFragment fragment = new FollowersFollowingFragment();
                                    Bundle bundle = new Bundle();
                                    bundle.putString("title", otherUserDetailsResponse.getUser_details().getUsername());
                                    fragment.setArguments(bundle);
                                    myCommunicator.setContentFragment(fragment, true);
                                }
                            });

                            llFollowingOther.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    AppConstant.followingFollower = "following";
                                    AppConstant.otherUserIdFoll = otherUserDetailsResponse.getUser_details().getId();
                                    FollowersFollowingFragment fragment = new FollowersFollowingFragment();
                                    Bundle bundle = new Bundle();
                                    bundle.putString("title", otherUserDetailsResponse.getUser_details().getUsername());
                                    fragment.setArguments(bundle);
                                    myCommunicator.setContentFragment(fragment, true);
                                }
                            });

                            tvFollowUser.setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {

                                    if (!PersistentUser.isLogged(con)) {
                                        // ------- Log in or not  dialog show----------
                                        AppConstant.loginDialoag(con);
                                    } else {
                                        callFollowUnfollowAPI(otherUserDetailsResponse.getUser_details().getUser_shop().get(0).getUser_id());
                                    }
                                }
                            });
                            lLSendMessage.setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
//                                    if (!PersistentUser.isLogged(con)) {
//                                        // ------- Log in or not  dialog show----------
//                                        AppConstant.loginDialoag(con);
//
//                                    } else {
//                                        writeMessageDialog(otherUserDetailsResponse);
//                                    }

                                }
                            });
                        } else {

                            msg = otherUserDetailsResponse.getMsg();
                            AlertMessage.showMessage(con, "Bunlde", msg);

                        }

                    }
                });

            }

        });

    }

//    public void setGridViewHeightBasedOnChildren(GridView gridView, int columns) {
//        ListAdapter listAdapter = gridView.getAdapter();
//        if (listAdapter == null) {
//            // pre-condition
//            return;
//        }
//
//        int totalHeight = 0;
//        int items = listAdapter.getCount();
//        int rows = 0;
//
//        View listItem = listAdapter.getView(0, null, gridView);
//        listItem.measure(0, 0);
//        totalHeight = listItem.getMeasuredHeight();
//
//        float x = 1;
//        if( items > columns ){
//            x = items/columns;
//            rows = (int) (x + 1);
//            totalHeight *= rows;
//        }
//
//        ViewGroup.LayoutParams params = gridView.getLayoutParams();
//        params.height = totalHeight;
//        gridView.setLayoutParams(params);
//
//    }

//    public static void setListViewHeightBasedOnChildren(GridView listView) {
//        ListAdapter listAdapter = listView.getAdapter();
//        if (listAdapter == null)
//            return;
//
//        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
//        int totalHeight = 0;
//        View view = null;
//        for (int i = 0; i < listAdapter.getCount(); i++) {
//            view = listAdapter.getView(i, view, listView);
//            if (i == 0)
//                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));
//
//            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
//            totalHeight += view.getMeasuredHeight();
//        }
//        ViewGroup.LayoutParams params = listView.getLayoutParams();
//        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
//        listView.setLayoutParams(params);
//    }

    /**
     * -----------------------Other User shop items Adapter-----------------------
     */
    private class ShopeItemAdapter extends ArrayAdapter<ProductAndUserDetailsInfo> {

        Context context;

        ShopeItemAdapter(Context context) {
            super(context, R.layout.shope_item_of_gridview, otherUserDetailsResponse.getUser_details().getUser_shop());
            this.context = context;
        }

        @Override
        public View getView(final int position, View shopeItemView, ViewGroup parent) {
            /**
             * ------------------Set the row layout-----------------------
             */
            if (shopeItemView == null) {
                final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                shopeItemView = inflater.inflate(R.layout.shope_item_of_gridview, null);
            }

            if (position < otherUserDetailsResponse.getUser_details().getUser_shop().size()) {

                // TO get data from vector/Arrraylist according to there position
                final ProductAndUserDetailsInfo query = otherUserDetailsResponse.getUser_details().getUser_shop().get(position);

                /**
                 * ---------------------Initialization of bundle row field------------------------------
                 */
                imgLoveShop = (ImageView) shopeItemView.findViewById(R.id.imgLoveShop);
                ImageView imgOtherSItems = (ImageView) shopeItemView.findViewById(R.id.imgOtherSItems);
                ImageView imgSoldStam = (ImageView) shopeItemView.findViewById(R.id.imgSoldStam);
                TextView tvOSItemName = (TextView) shopeItemView.findViewById(R.id.tvOSItemName);
                //----------font set-------------
                tvOSItemName.setTypeface(helveticaNeuRegular);

                if (query.getIs_sold().equalsIgnoreCase("1")){
                    imgSoldStam.setVisibility(View.VISIBLE);
                }else {
                    imgSoldStam.setVisibility(View.GONE);
                }

                if (query.getIs_liked().equalsIgnoreCase("0")) {

                    imgLoveShop.setImageResource(R.drawable.love_unliked);
                } else {
                    imgLoveShop.setImageResource(R.drawable.love_liked);
                }
                if (query.getPhoto_one() != null) {
                    Picasso.with(context).load(query.getPhoto_one())
                            .placeholder(R.drawable.product_bg).error(R.drawable.product_bg).into(imgOtherSItems);
                }
                tvOSItemName.setText(query.getName());
                imgLoveShop.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!PersistentUser.isLogged(con)) {
                            // ------- Log in or not  dialog show----------
                            AppConstant.loginDialoag(con);
                        } else {
                            callLikeUnlikeAPI(query.getId(), position);
                        }
                    }
                });

                shopeItemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AppConstant.feedRowObject = query;
                        AppConstant.productIDfeedRow = query.getId();
                        myCommunicator.addContentFragment(new SingleProductFragment(), true);
                    }
                });


            }

            return shopeItemView;

        }

    }


    /**
     * --------------------------Write Message Dialog-----------------------------
     */
    private void writeMessageDialog(final OtherUserDetailsResponse otUDResponse) {

        final Dialog dialogWriteMessage = new Dialog(con);
        dialogWriteMessage.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogWriteMessage.setContentView(R.layout.dialog_write_message);
        dialogWriteMessage.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogWriteMessage.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        //------------Initialization-----------------
        TextView tvSendMessage = (TextView) dialogWriteMessage.findViewById(R.id.tvSendMessage);
        etMessagingPerso = (EditText) dialogWriteMessage.findViewById(R.id.etMessagingPerso);
        ImageView imgMessageCross = (ImageView) dialogWriteMessage.findViewById(R.id.imgMessageCross);
        ImageView imgMessagingPerson = (ImageView) dialogWriteMessage.findViewById(R.id.imgMessagingPerson);
        tvSendMessage.setTypeface(helveticaNeueBold);
        etMessagingPerso.setTypeface(helveticaNeuRegular);

        /**
         * --------------Set data--------------------
         */

        Picasso.with(con).load(otUDResponse.getUser_details().getProfile_image())
                .placeholder(R.drawable.person).error(R.drawable.person).into(imgMessagingPerson);

        /**
         * ----------------ON Click------------------------------------------------
         */


        tvSendMessage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(etMessagingPerso.getText().toString())) {
                    AlertMessage.showMessage(con, getString(R.string.status),
                            "Write message first");
                } else {
                    callSendMessageAPI(otUDResponse.getUser_details().getUser_shop().get(0).getUser_id(), etMessagingPerso.getText().toString());
                }
            }
        });


        imgMessageCross.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                dialogWriteMessage.dismiss();
            }
        });

        dialogWriteMessage.show();
    }

    /**
     * -------------------22. Send message to a user------------------
     */
    private void callSendMessageAPI(final String user_id, final String msg_text) {
  /*
   * ---------------check internet first------------
   */
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }

         /*
          * ----------------Show Busy Dialog -----------------------------------------
          */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
  /*
   *
   *----------------------Start Thread-------------------------------------------
   *
   */
        Executors.newSingleThreadExecutor().submit(new Runnable() {
            SendMessageResponse sendMessageResponse;
            String msg = "";
            String response = "";

            @Override
            public void run() {
                // You can performed your task here.
                try {
                    Log.w("22. Send message URL", AllURL.getSendMessage(user_id, msg_text));
                    //-------------Hit Server---------------------
                    response = AAPBDHttpClient.get(AllURL.getSendMessage(user_id, msg_text)).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();

                    //----------------Persist response with the help of Gson----------------
                    Log.w("22Send messageResponse", ">>" + response);
                    Gson gson = new Gson();
                    sendMessageResponse = gson.fromJson(response, SendMessageResponse.class);

                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        /**
                         * -------------Here we do UI related tasks inside run() method of runOnUiThread()-------------------
                         */
                        //-------Stop Busy Dialog-----
                        if (busy != null) {
                            busy.dismis();
                        }

                        /**
                         * ---------main Ui related work--------------
                         */
                        if (sendMessageResponse.getStatus().equalsIgnoreCase("1")) {
                            Toast.makeText(con, "Message send" + sendMessageResponse.getMsg(), Toast.LENGTH_LONG).show();
                            etMessagingPerso.setText("");
                        } else {
                            msg = sendMessageResponse.getMsg();
                            AlertMessage.showMessage(con, "22.Send message to a user", msg);
                        }
                    }
                });
            }
        });
    }

    /**
     * -------------------10. Like/Unlike Product API------------------
     */
    private void callLikeUnlikeAPI(final String product_id, final int position) {
  /*
   * ---------------check internet first------------
   */
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }

  /*
   * ----------------Show Busy Dialog -----------------------------------------
   */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
  /*
   *----------------------Start Thread--------------------------------------------------------------
   */
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            String msg = "";
            String response = "";
            LikeUnlikeResponse likeUnlikeResponse;

            @Override
            public void run() {
                //--------------- We can performed your task here.-----------------

                try {
                    Log.i("Like/Unlike URL", AllURL.getLikeUnlike(product_id));
                    //-------------Hit Server---------------------
                    response = AAPBDHttpClient.get(AllURL.getLikeUnlike(product_id)).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();

                    //----------------Persist response with the help of Gson----------------
                    Log.i("Like/Unlike Response", ">>" + response);
                    Gson gson = new Gson();
                    likeUnlikeResponse = gson.fromJson(response, LikeUnlikeResponse.class);

                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        /**
                         * -------------Here we do UI related tasks inside run() method of runOnUiThread()-------------------
                         */

                        //-------Stop Busy Dialog-----
                        if (busy != null) {
                            busy.dismis();
                        }
                        /**
                         * ---------main Ui related work--------------
                         */
                        if (likeUnlikeResponse != null) {

                            if (likeUnlikeResponse.getMsg().equalsIgnoreCase("liked")) {
                                otherUserDetailsResponse.getUser_details().getUser_shop().get(position).setIs_liked("1");
                                shopeItemAdapter.notifyDataSetChanged();
                                Toast.makeText(con, likeUnlikeResponse.getMsg(), Toast.LENGTH_LONG).show();

                            } else if (likeUnlikeResponse.getMsg().equalsIgnoreCase("unliked")) {
                                otherUserDetailsResponse.getUser_details().getUser_shop().get(position).setIs_liked("0");
                                shopeItemAdapter.notifyDataSetChanged();
                                Toast.makeText(con, likeUnlikeResponse.getMsg(), Toast.LENGTH_LONG).show();
                            }

                        }

                    }
                });

            }

        });

    }

    /**
     * -------------------20. Follow/Unfollow a user------------------
     */
    private void callFollowUnfollowAPI(final String user_id) {
  /*
   * ---------------check internet first------------
   */
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }

         /*
          * ----------------Show Busy Dialog -----------------------------------------
          */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
  /*
   *
   *----------------------Start Thread-------------------------------------------
   *
   */
        Executors.newSingleThreadExecutor().submit(new Runnable() {
            FollowUnfollowResponse followUnfollowResponse;
            String msg = "";
            String response = "";

            @Override
            public void run() {
                // You can performed your task here.

                try {
                    Log.e("20.Follow/Unfollow URL", AllURL.getFollowUnfollowUrl(user_id));
                    //-------------Hit Server---------------------
                    response = AAPBDHttpClient.get(AllURL.getFollowUnfollowUrl(user_id)).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();

                    //----------------Persist response with the help of Gson----------------
                    Log.e("20.Follow/UnfolResponse", ">>" + response);
                    Gson gson = new Gson();
                    followUnfollowResponse = gson.fromJson(response, FollowUnfollowResponse.class);

                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        /**
                         * -------------Here we do UI related tasks inside run() method of runOnUiThread()-------------------
                         */
                        //-------Stop Busy Dialog-----
                        if (busy != null) {
                            busy.dismis();
                        }

                        /**
                         * ---------main Ui related work--------------
                         */
                        if (followUnfollowResponse.getStatus().equalsIgnoreCase("1")) {
                            Toast.makeText(con, followUnfollowResponse.getMsg(), Toast.LENGTH_LONG).show();
                            if (typeFollow=="1") {
                                typeFollow="0";
                                tvFollowUser.setText("Unfollow " + otherUserDetailsResponse.getUser_details().getUsername());
                            } else {
                                typeFollow="1";
                                tvFollowUser.setText("Follow " + otherUserDetailsResponse.getUser_details().getUsername());
                            }
                        } else {

                            msg = followUnfollowResponse.getMsg();
                            AlertMessage.showMessage(con, "Follow/Unfollow", msg);

                        }

                    }
                });

            }

        });

    }
}