package com.sloppy;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.Gson;
import com.instagram.ApplicationData;
import com.instagram.InstagramApp;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.sloppy.aapbdLib.AlertMessage;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.NetInfo;
import com.sloppy.aapbdLib.PersistData;
import com.sloppy.aapbdLib.PersistentUser;
import com.sloppy.aapbdLib.StartActivity;
import com.sloppy.model.LoginResponse;
import com.sloppy.utils.AllURL;
import com.sloppy.utils.AppConstant;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.HashMap;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends Activity {
	/**
	 * ---------------------Variable--------------------------------------------
	 */

	Context con;
	private LinearLayout signUpWithEmail,linLayoutLogin,imgInstagram;
	private TextView tvSignUpLater;
	GoogleCloudMessaging gcm;
	private Typeface helveticaNeuRegular,helveticaNeueBold;
	private String SENDER_ID = "809492412066";
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	public static  MainActivity instance;
	private InstagramApp mApp;
	private HashMap<String, String> userInfoHashmap = new HashMap<String, String>();
	private LoginResponse logInResponse;

	/**
	 *--------------------On Create method--------------------------------------
	 */

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		con = this;
		instance=this;
		/**
		 * * --------Check Login or not-------------------
		 */

			initUI();
		if (checkPlayServices()) {
			gcm = GoogleCloudMessaging.getInstance(this);
			Log.e("GcmId",
					":" + PersistData.getStringData(con, AppConstant.GCMID));
			if (PersistData.getStringData(con, AppConstant.GCMID).length() == 0) {
				new RegisterBackground().execute();
			}

		}
		// =======  for Instagram login ==================

		mApp = new InstagramApp(this, ApplicationData.CLIENT_ID,
				ApplicationData.CLIENT_SECRET, ApplicationData.CALLBACK_URL);
		mApp.setListener(new InstagramApp.OAuthAuthenticationListener() {

			@Override
			public void onSuccess() {
				mApp.fetchUserName(handler);
			}

			@Override
			public void onFail(String error) {
				Toast.makeText(MainActivity.this, error, Toast.LENGTH_SHORT)
						.show();
			}
		});

	}

	private Handler handler = new Handler(new Handler.Callback() {

		@Override
		public boolean handleMessage(Message msg) {
			if (msg.what == InstagramApp.WHAT_FINALIZE) {
				userInfoHashmap = mApp.getUserInfo();
				if (userInfoHashmap!=null){
					PersistData.setStringData(con,AppConstant.socialId,userInfoHashmap.get(InstagramApp.TAG_ID));
					String fullName=userInfoHashmap.get(InstagramApp.TAG_FULL_NAME);
					if (!TextUtils.isEmpty(fullName)){
						String lastName = fullName.substring(fullName.lastIndexOf(" ")+1);
						String firstName = fullName.substring(0, fullName.lastIndexOf(' '));
						PersistData.setStringData(con, AppConstant.first_name, firstName);
						PersistData.setStringData(con, AppConstant.last_name, lastName);
					}
					PersistData.setStringData(con, AppConstant.insagramUserName, userInfoHashmap.get(InstagramApp.TAG_USERNAME));
					socialLogin(AllURL.socialSignUpURL());
				}

			} else if (msg.what == InstagramApp.WHAT_FINALIZE) {
				Toast.makeText(MainActivity.this, "Check your network.",
						Toast.LENGTH_SHORT).show();
			}
			return false;
		}
	});

	/**
	 * ----------------Initialization and On Click-------------------------------
	 */
	private void initUI() {
		float size = new TextView(this).getTextSize();

		TextView tvTermAndSer = (TextView) findViewById(R.id.tvTermAndSer);
		TextView tvPrivacy = (TextView) findViewById(R.id.tvPrivacy);


		//--------------Text View----------------------
		tvSignUpLater=(TextView) findViewById(R.id.tvSignUpLater);
		imgInstagram=(LinearLayout)findViewById(R.id.imgInstagram);

		//--------------Linear Layout----------------------
		signUpWithEmail=(LinearLayout) findViewById(R.id.linLayoutSignUpWithEmail);
		linLayoutLogin=(LinearLayout) findViewById(R.id.linLayoutLogin);
		/**
		 * -------------------Font Set------------------------------
		 */
		helveticaNeuRegular = Typeface.createFromAsset(getAssets(), "font/helveticaNeuRegular.ttf");
		helveticaNeueBold = Typeface.createFromAsset(getAssets(), "font/helveticaNeueBold.ttf");

		tvSignUpLater.setTypeface(helveticaNeuRegular);
		tvTermAndSer.setTypeface(helveticaNeuRegular);
		tvPrivacy.setTypeface(helveticaNeuRegular);
		((TextView)findViewById(R.id.tvLoremIpsum)).setTypeface(helveticaNeuRegular);
		((TextView)findViewById(R.id.tvSignUp)).setTypeface(helveticaNeuRegular);
		((TextView)findViewById(R.id.tvInstagram)).setTypeface(helveticaNeuRegular);
		((TextView)findViewById(R.id.tvLogin)).setTypeface(helveticaNeuRegular);

		SpannableString content = new SpannableString(getResources().getString(R.string.terms_of));
		content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
		tvTermAndSer.setText(content);
		SpannableString privecyPolicy = new SpannableString(getResources().getString(R.string.privecy_policy));
		privecyPolicy.setSpan(new UnderlineSpan(), 0, privecyPolicy.length(), 0);
		tvPrivacy.setText(privecyPolicy);
		tvTermAndSer.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AppConstant.menuType="term";
				FragmentManager manager= getFragmentManager();
				TermsOfServiceDialogFragment dialogTerm= new TermsOfServiceDialogFragment();
				dialogTerm.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar);
				dialogTerm.show(manager,"");
			}
		});

		tvPrivacy.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AppConstant.menuType="privacy";
				FragmentManager manager= getFragmentManager();
				TermsOfServiceDialogFragment dialogTerm= new TermsOfServiceDialogFragment();
				dialogTerm.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar);
				dialogTerm.show(manager,"");
			}
		});
		imgInstagram.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				mApp.resetAccessToken();
				CookieSyncManager cookieSyncMngr = CookieSyncManager.createInstance(con);
				CookieManager cookieManager = CookieManager.getInstance();
				cookieManager.removeAllCookie();
				mApp.authorize();
//				if(!PersistData.getStringData(con,AppConstant.socialId).equalsIgnoreCase("")){
//					socialLogin(AllURL.socialSignUpURL());
//				}else{
//					mApp.authorize();
//				}
			}
		});

		/**
		 * -------Sign Up Click----------------------
		 */
		signUpWithEmail.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				StartActivity.toActivity(con, SignUpActivity.class);
			}
		});
		//-------------Login Click--------------------
		linLayoutLogin.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AppConstant.islogin=true;
				StartActivity.toActivity(con, LoginActivity.class);

			}
		});
		//-------------Sign Up Later click---------------------
		tvSignUpLater.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				StartActivity.toActivity(con, MyTabActivity.class);
				finish();
			}
		});
	}

	// ---------For GCM ID-----------------

	private boolean checkPlayServices() {
		final int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(getApplicationContext());
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, this,
						MainActivity.PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				Log.i(LoginActivity.TAG, "This device is not supported.");
				finish();
			}
			return false;
		}
		return true;
	}

	class RegisterBackground extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... arg0) {

			String msg = "";
			try {
				if (gcm == null) {
					gcm = GoogleCloudMessaging.getInstance(con);
				}
				String regid = gcm.register(SENDER_ID);
				PersistData.setStringData(con, AppConstant.GCMID, regid);

				msg = "Dvice registered, registration ID=" + regid;
				Log.e("Google Registration ID", "---------" + msg);
				// Persist the regID - no need to
			} catch (final IOException ex) {
				msg = "Error :" + ex.getMessage();
			}
			return msg;
		}

		@Override
		protected void onPostExecute(String msg) {

		}
	}

	protected void socialLogin(final String url) {
		//--- for net check-----
		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
			return;
		}
		final BusyDialog busyNow = new BusyDialog(con, true, false);
		busyNow.show();
		final AsyncHttpClient client = new AsyncHttpClient();
		final RequestParams param = new RequestParams();

		try {
			param.put("social_id", PersistData.getStringData(con,AppConstant.socialId));
			param.put("first_name", PersistData.getStringData(con,AppConstant.first_name));
			param.put("last_name", PersistData.getStringData(con,AppConstant.last_name));
			param.put("registrationtype", "instagram");
			param.put("instagram_username",PersistData.getStringData(con,AppConstant.insagramUserName));
			param.put("device_type", "android");
			param.put("push_id", PersistData.getStringData(con, AppConstant.GCMID));
			param.put("currency", "usd");

		} catch (final Exception e1) {
			e1.printStackTrace();
		}

		client.post(url, param, new AsyncHttpResponseHandler() {

			@Override
			public void onStart() {

			}

			@Override
			public void onSuccess(int statusCode, Header[] headers,
								  byte[] response) {

				if (busyNow != null) {
					busyNow.dismis();
				}

				Log.e("SignUp resposne ", ">>" + new String(response));

				Gson g = new Gson();
				logInResponse = g.fromJson(new String(response), LoginResponse.class);

				if (logInResponse.getStatus().equalsIgnoreCase("1")) {

					PersistentUser.setLogin(con);
					PersistData.setStringData(con, AppConstant.user_id, logInResponse.getResults().getId());
					PersistData.setStringData(con, AppConstant.first_name, logInResponse.getResults().getFirst_name());
					PersistData.setStringData(con, AppConstant.last_name, logInResponse.getResults().getLast_name());
					PersistData.setStringData(con, AppConstant.email, logInResponse.getResults().getEmail());
					PersistData.setStringData(con, AppConstant.username, logInResponse.getResults().getUsername());
					PersistData.setStringData(con, AppConstant.loginResponse, new String(response));
					PersistData.setStringData(con, AppConstant.role, logInResponse.getResults().getRole());
					PersistData.setStringData(con, AppConstant.registrationtype, logInResponse.getResults().getRegistrationtype());
					PersistData.setStringData(con, AppConstant.profile_image, logInResponse.getResults().getProfile_image());
					PersistData.setStringData(con, AppConstant.device_type, logInResponse.getResults().getDevice_type());
					PersistData.setStringData(con, AppConstant.push_id, logInResponse.getResults().getPush_id());
					PersistData.setStringData(con, AppConstant.token, logInResponse.getToken());

					Log.e("token", "=" + PersistData.getStringData(con, AppConstant.token));

					//---------Go Tab Activity-----------------------
					MainActivity.instance.finish();
					if (MyTabActivity.getMyTabActivity()!=null){
						MyTabActivity.getMyTabActivity().finish();
					}
					StartActivity.toActivity(con, MyTabActivity.class);
					finish();

				} else {
					AppConstant.alertDialoag(con,"Status",logInResponse.getMsg(),"Ok");
					return;
				}

			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
								  byte[] errorResponse, Throwable e) {
				Log.e("errorResponse", new String(errorResponse));
				if (busyNow != null) {
					busyNow.dismis();
				}
			}

			@Override
			public void onRetry(int retryNo) {
				// called when request is retried
			}
		});

	}

}
