package com.sloppy;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.sloppy.aapbdLib.AlertMessage;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.NetInfo;
import com.sloppy.aapbdLib.PersistData;
import com.sloppy.aapbdLib.PersistentUser;
import com.sloppy.aapbdLib.StartActivity;
import com.sloppy.aapbdLib.ValidateEmail;
import com.sloppy.model.CountryResponse;
import com.sloppy.model.LoginResponse;
import com.sloppy.utils.AllURL;
import com.sloppy.utils.AppConstant;
import com.sloppy.utils.BitmapUtils;
import com.sloppy.utils.ReadTxtFile;
import android.app.Activity;
import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.jeesoft.widget.pickerview.CharacterPickerWindow;
import cz.msebera.android.httpclient.Header;
import android.Manifest;

public class SignUpActivity extends Activity {
    // --------variable-----
    private Context con;
    private ImageView imgBack,imgMenuSignUp;
    private EditText etFirstName, etLastName, etEmail, etUserName, etPasword, etPaypal;
    private String firstName, lastName, email, username, password, gender, country, paypal_email;
    private RelativeLayout rlGender, rlCuntry;
    private TextView tvSignUp, tvSignUp1, tvGender, tvCuntry;
    // ---for Camera------
    LinearLayout layoutCamera;
    Dialog dialog;
    private final int galarytakid = 1;
    private final int imagecaptureid = 0;
    ImageView imgProfileImage, imgProfileImageShow;
    String imageLocal = "";
    private File file;
    private static File dir = null;
    private Typeface helveticaNeuRegular, helveticaNeueBold;
    private FrameLayout view;
    private String path="";
    private LoginResponse logInResponse;


    // -------------------On Create method--------------------------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_sign_up);
        con = this;
        initUI();
    }

    private void initUI() {
        // ---------------Initialization----------
        // ----TextView---
        tvSignUp1 = (TextView) findViewById(R.id.tvSignUp1);
        tvSignUp = (TextView) findViewById(R.id.tvSignUp);
        tvGender = (TextView) findViewById(R.id.tvGender);
        tvCuntry = (TextView) findViewById(R.id.tvCuntry);
        // --- ImageView---
        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgMenuSignUp = (ImageView) findViewById(R.id.imgMenuSignUp);
        imgProfileImage = (ImageView) findViewById(R.id.imgProfileImage);
        imgProfileImageShow = (ImageView) findViewById(R.id.imgProfileImageShow);

        // ---EditText---
        etFirstName = (EditText) findViewById(R.id.etFirstName);
        etPaypal = (EditText) findViewById(R.id.etPaypal);
        etLastName = (EditText) findViewById(R.id.etLastName);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etUserName = (EditText) findViewById(R.id.etUserName);
        etPasword = (EditText) findViewById(R.id.etPasword);
        // ----layout-----
        layoutCamera = (LinearLayout) findViewById(R.id.layoutCamera);
        rlGender = (RelativeLayout) findViewById(R.id.rlGender);
        rlCuntry = (RelativeLayout) findViewById(R.id.rlCuntry);

        view = (FrameLayout) findViewById(R.id.flFullLayput);


        /**
         * ------------------------Font set-----------------------------------------------------------------
         */
        helveticaNeuRegular = Typeface.createFromAsset(getAssets(), "font/helveticaNeuRegular.ttf");
        helveticaNeueBold = Typeface.createFromAsset(getAssets(), "font/helveticaNeueBold.ttf");

        tvSignUp1.setTypeface(helveticaNeuRegular);
        etFirstName.setTypeface(helveticaNeuRegular);
        etLastName.setTypeface(helveticaNeuRegular);
        etEmail.setTypeface(helveticaNeuRegular);
        etUserName.setTypeface(helveticaNeuRegular);
        etPasword.setTypeface(helveticaNeuRegular);
        tvSignUp.setTypeface(helveticaNeuRegular);
        ((TextView) findViewById(R.id.tvUpPhoto)).setTypeface(helveticaNeuRegular);
        ((TextView) findViewById(R.id.tvProImg)).setTypeface(helveticaNeuRegular);

        // -----------------------On Click--------------------
        imgBack.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });
        imgMenuSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fra= getFragmentManager();
                MenuFragmentDialog dialogMenu= new MenuFragmentDialog();
                dialogMenu.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar);
                dialogMenu.show(fra,"");
            }
        });
        tvSignUp.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                checkCreateAccountData();
            }
        });
        final List<String> genderlist = new ArrayList<String>();
        if (genderlist != null) {
            genderlist.clear();
        }
        genderlist.add("Male");
        genderlist.add("Female");

        rlGender.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.keyboardHide(con,rlGender);
                final CharacterPickerWindow windows = new CharacterPickerWindow(con, "Select Gender");
                windows.showAtLocation(v, Gravity.BOTTOM, 0, 0);
//                AppConstant.keyboardHide(con,);
                windows.setPicker(genderlist);
                windows.setWidth(200);
                windows.setHeight(200);
                windows.setOnoptionsSelectListener(new CharacterPickerWindow.OnOptionsSelectListener() {
                    @Override
                    public void onOptionsSelect(int position, int option2, int options3) {
                        tvGender.setText(genderlist.get(position));
                        tvGender.setTextColor(getResources().getColor(R.color.black));
                    }
                });
            }
        });

        // ----------Country List----------
        final List<String> countrylist = new ArrayList<String>();

        String countryJson = ReadTxtFile.readRawTextFile(con, R.raw.country);
        Gson g = new Gson();
        CountryResponse mCountryResponse = g.fromJson(countryJson, CountryResponse.class);
        for (int i = 0; i < mCountryResponse.getCountries().size(); i++) {
            countrylist.add(mCountryResponse.getCountries().get(i).getName());
        }

        rlCuntry.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.keyboardHide(con,rlCuntry);
                final CharacterPickerWindow windows = new CharacterPickerWindow(con, "Select a Country");
                windows.showAtLocation(v, Gravity.BOTTOM, 0, 0);
                windows.setPicker(countrylist);
                windows.setWidth(200);
                windows.setHeight(200);
                windows.setOnoptionsSelectListener(new CharacterPickerWindow.OnOptionsSelectListener() {
                    @Override
                    public void onOptionsSelect(int position, int option2, int options3) {
                        tvCuntry.setText(countrylist.get(position));
                        tvCuntry.setTextColor(getResources().getColor(R.color.black));
                    }
                });
            }
        });


// --------------- Picture capture and Save ---------------------

        layoutCamera.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog = new Dialog(con);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_image_capture);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

                TextView tvUseCamera = (TextView) dialog.findViewById(R.id.tvUseCamera);
                TextView tvChooseFrom = (TextView) dialog.findViewById(R.id.tvChooseFrom);
                TextView tvPhotoCancel = (TextView) dialog.findViewById(R.id.tvPhotoCancel);

                // -------Get picture Using Camera--------

                tvUseCamera.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if ( ActivityCompat.checkSelfPermission(con, Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions((Activity) con,
                                    new String[]{Manifest.permission.CAMERA}, AppConstant.CAMERA_RUNTIME_PERMISSION);
                            dialog.dismiss();
                        }else{
                            AppConstant.isGallery=false;
                            if ( ActivityCompat.checkSelfPermission(con, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                    != PackageManager.PERMISSION_GRANTED) {
                                ActivityCompat.requestPermissions((Activity) con,
                                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, AppConstant.WRITEEXTERNAL_PERMISSION_RUNTIME);
                                dialog.dismiss();
                            }else{
                                final Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(i, imagecaptureid);
                                dialog.dismiss();
                            }
                        }
                    }
                });

                // --------- Get picture from Gallery-----------------
                tvChooseFrom.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        AppConstant.isGallery=true;
                        if ( ActivityCompat.checkSelfPermission(con, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions((Activity) con,
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, AppConstant.WRITEEXTERNAL_PERMISSION_RUNTIME);
                            dialog.dismiss();
                        }else{
                            final Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent,"Select Picture"), galarytakid);
                            dialog.dismiss();
                        }
                    }
                });

                tvPhotoCancel.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();

            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == AppConstant.CAMERA_RUNTIME_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Now user should be able to use camera

                if ( ActivityCompat.checkSelfPermission(con, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions((Activity) con,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, AppConstant.WRITEEXTERNAL_PERMISSION_RUNTIME);
                }else{
                    final Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(i, imagecaptureid);
                }
            } else {
                // Your app will not have this permission. Turn off all functions
                // that require this permission or it will force close like your
                // original question
            }
        }else if (requestCode==AppConstant.WRITEEXTERNAL_PERMISSION_RUNTIME){
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (AppConstant.isGallery){
                    final Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent,"Select Picture"), galarytakid);
                }else {
                    final Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(i, imagecaptureid);
                }
            }
        }
    }

    //--------Save Picture-----------
    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == galarytakid && resultCode == Activity.RESULT_OK) {

            try {

                final Uri selectedImageUri = data.getData();

                final Bitmap bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImageUri));

                path = setToImageView(bitmap);
                File file=new File(path);
                Uri tempUri=Uri.fromFile(file);
                BitmapUtils.rotateAndSaveImg(con,file,tempUri);
                imgProfileImageShow.setImageBitmap(bitmap);
                //----------for image show on new image view---------------
                imgProfileImage.setVisibility(View.GONE);
                imgProfileImageShow.setVisibility(View.VISIBLE);
            } catch (final Exception e) {
                return;
            }

        } else if (requestCode == imagecaptureid && resultCode == Activity.RESULT_OK) {

            try {
                final Bundle extras = data.getExtras();
                final Bitmap b = (Bitmap) extras.get("data");
                path = setToImageView(b);
                File file=new File(path);
                Uri tempUri=Uri.fromFile(file);
                BitmapUtils.rotateAndSaveImg(con,file,tempUri);
                imgProfileImageShow.setImageBitmap(b);
                imgProfileImage.setVisibility(View.GONE);
                imgProfileImageShow.setVisibility(View.VISIBLE);

            } catch (final Exception e) {
                return;
            }

        }

    }

    private String setToImageView(Bitmap bitmap) {

        try {
            final Bitmap bit = BitmapUtils.getResizedBitmap(bitmap, 600);
            final double time = System.currentTimeMillis();
            imageLocal = saveBitmapIntoSdcard(bit, "ebuser" + time + ".png");
            Log.e("camera saved URL :  ", " " + imageLocal);

        } catch (final IOException e) {
            e.printStackTrace();
            imageLocal = "";
            Log.e("camera saved URL :  ", e.toString());
        }

        return imageLocal;

    }


    private String saveBitmapIntoSdcard(Bitmap bitmap22, String filename) throws IOException {
        /*
         *
		 * check the path and create if needed
		 */
        createBaseDirctory();

        try {

            new Date();

            OutputStream out = null;
            file = new File(SignUpActivity.dir, "/" + filename);

            if (file.exists()) {
                file.delete();
            }

            out = new FileOutputStream(file);

            bitmap22.compress(Bitmap.CompressFormat.PNG, 100, out);

            out.flush();
            out.close();
            // Toast.makeText(context, "Saved", Toast.LENGTH_SHORT).show();
            return file.getAbsolutePath();
        } catch (final Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public void createBaseDirctory() {
        final String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        dir = new File(extStorageDirectory + "/sloppy");

        if (SignUpActivity.dir.mkdir()) {
            System.out.println("Directory created");
        } else {
            System.out.println("Directory is not created or exists");
        }
    }

    // ------------- Check CreateAccountData method------------
    private void checkCreateAccountData() {
        if (TextUtils.isEmpty(etFirstName.getText().toString())) {
            AppConstant.alertDialoag(con, "Alert", getString(R.string.enter_first_name), "Close");
        } else if (TextUtils.isEmpty(etLastName.getText().toString())) {
            AppConstant.alertDialoag(con, "Alert", getString(R.string.enter_last_name), "Close");
        } else if (TextUtils.isEmpty(etEmail.getText().toString())) {
            AppConstant.alertDialoag(con, "Alert", getString(R.string.enter_email), "Close");
        } else if (!ValidateEmail.validateEmail(etEmail.getText().toString())) {
            AppConstant.alertDialoag(con, "Alert", getString(R.string.enter_valid), "Close");
        } else if (TextUtils.isEmpty(etUserName.getText().toString())) {
            AppConstant.alertDialoag(con, "Alert", getString(R.string.enter_user_name), "Close");
        } else if (etUserName.getText().toString().length() < 6) {
            AppConstant.alertDialoag(con, "Alert", getString(R.string.user_name_should), "Close");
        } else if (TextUtils.isEmpty(etPasword.getText().toString())) {
            AppConstant.alertDialoag(con, "Alert", getString(R.string.enter_password), "Close");
        } else if (etPasword.getText().toString().length() < 6) {
            AppConstant.alertDialoag(con, "Alert", getString(R.string.password_should), "Close");
        } else if (tvGender.getText().toString().equalsIgnoreCase("Gender")) {
            AppConstant.alertDialoag(con, "Alert", getString(R.string.select_gender), "Close");
        } else if (tvCuntry.getText().toString().equalsIgnoreCase("Country")) {
            AppConstant.alertDialoag(con, "Alert", getString(R.string.select_country), "Close");
        } else if (TextUtils.isEmpty(path)){
            AppConstant.alertDialoag(con, "Alert", getString(R.string.please_provie_picture), "Close");
        }
        else{

            firstName = etFirstName.getText().toString();
            lastName = etLastName.getText().toString();
            email = etEmail.getText().toString();
            if (!TextUtils.isEmpty(etPaypal.getText().toString())) {
                paypal_email = etPaypal.getText().toString();
            }
            password = etPasword.getText().toString();
            username = etUserName.getText().toString();
            gender = tvGender.getText().toString();
            country = tvCuntry.getText().toString();
            normalUserSignUp(AllURL.signUp());
        }
    }

    protected void normalUserSignUp(final String url) {
        //--- for net check-----
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }
        final BusyDialog busyNow = new BusyDialog(con, true, false);
        busyNow.show();
        final AsyncHttpClient client = new AsyncHttpClient();
        final RequestParams param = new RequestParams();

        try {
            param.put("first_name", firstName);
            param.put("last_name", lastName);
            param.put("email", email);
            param.put("paypal_email", paypal_email);
            param.put("username", username);
            param.put("password", password);
            param.put("gender", gender);
            param.put("country", country);
            if (!TextUtils.isEmpty(path)) {
                param.put("profile_image", new File(path));
            }
            param.put("registrationtype", "normal");
            param.put("device_type", "android");
            param.put("push_id", PersistData.getStringData(con, AppConstant.GCMID));
            param.put("currency", "usd");

        } catch (final Exception e1) {
            e1.printStackTrace();
        }

        client.post(url, param, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers,
                                  byte[] response) {

                if (busyNow != null) {
                    busyNow.dismis();
                }

                Log.e("SignUp resposne ", ">>" + new String(response));

                Gson g = new Gson();
                logInResponse = g.fromJson(new String(response), LoginResponse.class);

                if (logInResponse.getStatus().equalsIgnoreCase("1")) {

                    PersistentUser.setLogin(con);
                    PersistData.setStringData(con, AppConstant.user_id, logInResponse.getResults().getId());
                    PersistData.setStringData(con, AppConstant.first_name, logInResponse.getResults().getFirst_name());
                    PersistData.setStringData(con, AppConstant.last_name, logInResponse.getResults().getLast_name());
                    PersistData.setStringData(con, AppConstant.email, logInResponse.getResults().getEmail());
                    PersistData.setStringData(con, AppConstant.username, logInResponse.getResults().getUsername());
                    PersistData.setStringData(con, AppConstant.loginResponse, new String(response));
                    PersistData.setStringData(con, AppConstant.role, logInResponse.getResults().getRole());
                    PersistData.setStringData(con, AppConstant.registrationtype, logInResponse.getResults().getRegistrationtype());
                    PersistData.setStringData(con, AppConstant.profile_image, logInResponse.getResults().getProfile_image());
                    PersistData.setStringData(con, AppConstant.device_type, logInResponse.getResults().getDevice_type());
                    PersistData.setStringData(con, AppConstant.push_id, logInResponse.getResults().getPush_id());
                    PersistData.setStringData(con, AppConstant.token, logInResponse.getToken());

                    Log.e("token", "=" + PersistData.getStringData(con, AppConstant.token));

                    //---------Go Tab Activity-----------------------
                    MainActivity.instance.finish();
                    if (MyTabActivity.getMyTabActivity()!=null){
                        MyTabActivity.getMyTabActivity().finish();
                    }
                    StartActivity.toActivity(con, MyTabActivity.class);
                    finish();

                } else {
                    AppConstant.alertDialoag(con,"Status",logInResponse.getMsg(),"Ok");
//					AlertMessage.showMessage(con, "Status", logInResponse.getMsg() + "");
                    return;
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  byte[] errorResponse, Throwable e) {
//                Log.e("errorResponse", new String(errorResponse));
                if (busyNow != null) {
                    busyNow.dismis();
                }
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }

}
