package com.sloppy;

import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sloppy.utils.AppConstant;

/**
 * Created by hp on 8/8/2016.
 */
public class TagDialogFragment extends DialogFragment {
    private Context con;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        con = getActivity();
        view = inflater.inflate(R.layout.tag_dialog_fragment, container, false);
        initUI();
        return view;
    }

    private void initUI() {
        TextView tvSelectA= (TextView) view.findViewById(R.id.tvSelectA);

        AppConstant.setFont(con,tvSelectA,"regular");
    }

}
