package com.sloppy;

import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.sloppy.utils.AppConstant;

/**
 * Created by hp on 4/24/2016.
 */
public class FirstFestivalFragment extends BaseFragment {
    /**
     * ----------------------Variable------------------------
     */
    Context con;
    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        con=getActivity();
        view=inflater.inflate(R.layout.fragment_1festivals, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initUI();
    }

    private void initUI() {

        ImageView imgFst1Back=(ImageView) view.findViewById(R.id.imgFst1Back);
        ImageView imgMenuFirst=(ImageView) view.findViewById(R.id.imgMenuFirst);
        imgFst1Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        imgMenuFirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.showMenuDiadog(getActivity().getFragmentManager());
            }
        });
    }

}
