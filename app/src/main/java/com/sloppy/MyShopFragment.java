package com.sloppy;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sloppy.BaseFragment;
import com.sloppy.BrandFragment;
import com.sloppy.MenuFragmentDialog;
import com.sloppy.OtherUserDetailsFragment;
import com.sloppy.R;
//import com.sloppy.SellFragment;
import com.sloppy.SellFragment;
import com.sloppy.SingleProductFragment;
import com.sloppy.aapbdLib.AlertMessage;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.NetInfo;
import com.sloppy.aapbdLib.PersistData;
import com.sloppy.adapter.DraftRecyclerAdapter;
import com.sloppy.adapter.MyShopAdapter;
import com.sloppy.model.AddToBundleResponse;
import com.sloppy.model.DeleteProductResponse;
import com.sloppy.model.MyShopResponse;
import com.sloppy.model.ProductAndUserDetailsInfo;
import com.sloppy.model.ResultList;
import com.sloppy.utils.AAPBDHttpClient;
import com.sloppy.utils.AllURL;
import com.sloppy.utils.AppConstant;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.concurrent.Executors;


/**
 * Created by hp on 3/7/2016.
 */
public class MyShopFragment extends BaseFragment {
    private Context con;
    private View view;
    private MyShopResponse myShopResponse;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        con = getActivity();
        view = inflater.inflate(R.layout.fragment_common_list, container, false);
        initUI();
        return view;
    }

    private void initUI() {
        ImageView imgBackCommonList = (ImageView) view.findViewById(R.id.imgBackCommonList);
        ImageView imgMenuCommon = (ImageView) view.findViewById(R.id.imgMenuCommon);
        TextView tvTitelCommon = (TextView) view.findViewById(R.id.tvTitelCommon);
        tvTitelCommon.setText(getResources().getString(R.string.my_shop));
        AppConstant.setFont(con, tvTitelCommon, "regular");
        /**
         * ============Call 32. Get User Product List  by Type======================================
         */
        callMyShopAPI();

        /**
         * ============On Click=====================================================================
         */
        imgBackCommonList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.editable = "";
                getActivity().onBackPressed();
            }
        });

        imgMenuCommon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MenuFragmentDialog dialogMenu = new MenuFragmentDialog();
                dialogMenu.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar);
                dialogMenu.show(getActivity().getFragmentManager(), "");
            }
        });
    }
    /**
     * -------------------My Shop API call-------------------------------
     */
    protected void callMyShopAPI() {

        /**
         * check internet first
         */
        if (!NetInfo.isOnline(con)) {
            AppConstant.alertDialoag(con, getResources().getString(R.string.alert), getResources().getString(R.string.checkInternet), getResources().getString(R.string.ok));
            return;
        }
        /**
         * start progress bar
         */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
        /**
         * start Thread
         */
        Executors.newSingleThreadExecutor().submit(new Runnable() {
            String msg = "";
            String response = "";
            TextView tvDataNotFound = (TextView) view.findViewById(R.id.tvDataNotFound);
            RecyclerView rvCommonList=(RecyclerView) view.findViewById(R.id.rvCommonList);
            @Override
            public void run() {
                // You can performed your task here.

                try {
/**
 * --------API-32----Get User Product List  by Type------My Shop-----------------
 */
                    Log.e("MyshopListURL", AllURL.getMyShopData("myshop", "20"));
                    response = AAPBDHttpClient.get(AllURL.getMyShopData("myshop", "20")).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();
                    Log.e("MyshopListResponse", response);
                    Gson gson = new Gson();
                    myShopResponse = gson.fromJson(response, MyShopResponse.class);



                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                      /*
                       * ================Stop progress bar==============================
                       */
                        if (busy != null) {
                            busy.dismis();
                        }
                        if (myShopResponse.getStatus().equalsIgnoreCase("1")) {
//                            Toast.makeText(con, "My Shop " + myShopResponse.getMsg(), Toast.LENGTH_LONG).show();
                            if (myShopResponse.getResult().size() > 0) {
                                rvCommonList.setVisibility(View.VISIBLE);
                                tvDataNotFound.setVisibility(View.GONE);
                                rvCommonList.setLayoutManager(new LinearLayoutManager(con));
                                rvCommonList.setAdapter(new MyShopAdapter(getActivity(), myShopResponse.getResult()));
                            } else {
                                rvCommonList.setVisibility(View.GONE);
                                tvDataNotFound.setVisibility(View.VISIBLE);
                            }

                        } else {
                            msg = myShopResponse.getMsg();
                            AlertMessage.showMessage(con, "Sloppy Alert", msg);
                        }

                    }
                });

            }

        });

    }

}
