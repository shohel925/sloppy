package com.sloppy;

import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sloppy.aapbdLib.AlertMessage;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.NetInfo;
import com.sloppy.aapbdLib.PersistData;
import com.sloppy.aapbdLib.PersistentUser;
import com.sloppy.adapter.BrandListAdapter;
import com.sloppy.model.ProductListByBrandResponse;
import com.sloppy.utils.AAPBDHttpClient;
import com.sloppy.utils.AllURL;
import com.sloppy.utils.AppConstant;

import java.util.concurrent.Executors;


/**
 * Created by hp on 5/3/2016.
 */
public class BrandFragment extends BaseFragment {

    private Context con;
    private View view;
    private ImageView imgBranBack;
    private TextView tvBrandNam;
    private RecyclerView rvBrand;
    private ProductListByBrandResponse myBrandResponse;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        con = getActivity();

        view = inflater.inflate(R.layout.fragment_brand, container, false);
        initUI();
        return view;
    }

    private void initUI() {
        //-------------------Initialization---------------------
        imgBranBack = (ImageView) view.findViewById(R.id.imgBranBack);
        tvBrandNam = (TextView) view.findViewById(R.id.tvBrandNam);
        imgBranBack = (ImageView) view.findViewById(R.id.imgBranBack);
        rvBrand = (RecyclerView) view.findViewById(R.id.rvBrand);
        rvBrand.setLayoutManager(new LinearLayoutManager(con));
        AppConstant.setFont(con, tvBrandNam, "regular");
        //--------------------Data set---------------------------
        tvBrandNam.setText(AppConstant.brandName);
        if (PersistentUser.isLogged(con)) {
            callBrandListAPI("all");
        } else {
            callBrandListAPI("demo");
        }


        ImageView imgMenuBrand = (ImageView) view.findViewById(R.id.imgMenuBrand);
        imgMenuBrand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.showMenuDiadog(getActivity().getFragmentManager());
            }
        });
        imgBranBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

    }


    /**
     * -------------------35. Product List By Brand-------------------------------
     */
    protected void callBrandListAPI(final String type) {
              /*
               * check internet first
               */
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }
              /*
               * start progress bar
               */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
          /*
           * start Thread
           */
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            String msg = "";
            String response = "";

            @Override
            public void run() {
                // You can performed your task here.
                try {
                    response = AAPBDHttpClient.get(AllURL.getbrandList(AppConstant.brandID, type)).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();
                    Log.e("BrandListResponse", ">>" + response);

                } catch (Exception e1) {

                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                      /*
                       * stop progress bar
                       */
                        if (busy != null) {
                            busy.dismis();
                        }
                        Gson gson = new Gson();
                        myBrandResponse = gson.fromJson(response, ProductListByBrandResponse.class);

                        if (myBrandResponse.getStatus().equalsIgnoreCase("1")) {
                            if (myBrandResponse.getResult().getData().size() > 0) {
                                /*
                                 *  =================Call brand Adapter==========================
                                 */
                                rvBrand.setAdapter(new BrandListAdapter(getActivity(), myBrandResponse.getResult().getData()));
                            } else {
                                TextView tvDataNotFound = (TextView) view.findViewById(R.id.tvDataNotFound);
                                AppConstant.setFont(con, tvDataNotFound, "regular");
                                tvDataNotFound.setVisibility(View.VISIBLE);
                            }

                        } else {
                            msg = myBrandResponse.getMsg();
                            AppConstant.alertDialoag(con, "Alert", msg, "OK");
                        }

                    }
                });

            }

        });

    }


}
