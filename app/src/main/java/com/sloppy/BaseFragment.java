package com.sloppy;

import android.app.Activity;
import android.support.v4.app.Fragment;


public abstract class BaseFragment extends Fragment {
    public CommunicatorFragmentInterface myCommunicator;
    private boolean openMenuOnBackPress=false;

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);

        try {
            myCommunicator = (CommunicatorFragmentInterface) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        myCommunicator = null;
    }

    public boolean openMenuOnBackPress() {
        return openMenuOnBackPress;
    }

    public void setOpenMenuOnBackPress(boolean openMenuOnBackPress) {
        this.openMenuOnBackPress = openMenuOnBackPress;
    }

}
