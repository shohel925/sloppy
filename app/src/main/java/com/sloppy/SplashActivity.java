package com.sloppy;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.sloppy.aapbdLib.PersistentUser;

public class SplashActivity extends Activity {

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.splash);
        context = this;
        initUI();
    }

    private void initUI() {

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (PersistentUser.isLogged(context)) {
                    startActivity(new Intent(context, MyTabActivity.class));
                } else {
                    startActivity(new Intent(context, MainActivity.class));
                }
                finish();
            }
        }, 1000);
    }
}
