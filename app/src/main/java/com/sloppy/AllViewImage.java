package com.sloppy;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;


public class AllViewImage extends PagerAdapter {

    private LayoutInflater inflater;
    public Context con;
    List<String> imageRSC = new ArrayList<String>();
    public int LOOPS_COUNT = 50;
    private int i=0;
    private int viewPos=0;

    // constructor
    public AllViewImage(Context con, List<String> imgsSrc,int viewPos) {
        this.con = con;
        this.imageRSC = imgsSrc;
        this.viewPos=viewPos;
    }

    @Override
    public int getCount() {
        if (this.imageRSC.size()>1){
            return this.imageRSC.size()*LOOPS_COUNT;
        }else{
            return  this.imageRSC.size();
        }
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (RelativeLayout) object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView detailsImageView;
        TextView slideTitle, slideDes;
        position = position % imageRSC.size();
        inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View viewLayout = inflater.inflate(R.layout.pagerlayout,
                container, false);

        detailsImageView = (ImageView) viewLayout.findViewById(R.id.detailsImageView);
        detailsImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                i++;
                Handler handler = new Handler();
                Runnable r = new Runnable() {

                    @Override
                    public void run() {
                        i = 0;
                    }
                };

                if (i == 1) {
                    //Single click
                    handler.postDelayed(r, 500);
                } else if (i == 2) {
                    //Double click
                    i = 0;
                    Log.e("viewPos",""+viewPos);

                    FeedFragment.instance.callLikeUnlikeAPI(FeedFragment.instance.feedResponse.getResult().get(viewPos).getId(),"click",viewPos);
                }
            }
        });

//		slideTitle = (TextView) viewLayout.findViewById(R.id.slideTitle);
//		slideDes = (TextView) viewLayout.findViewById(R.id.slideDes);


//				slideTitle.setTypeface(tf);					
//				slideDes.setTypeface(tf2);

        try {

            if (imageRSC.size() == 0) {
                Picasso.with(con).load(R.drawable.ic_launcher).into(detailsImageView);
            } else {
                Picasso.with(con).load(imageRSC.get(position)).into(detailsImageView);
            }


//			detailsImageView.setImageResource(imageRSC[position]);
//			slideTitle.setText(titleArray[position]);
//			slideDes.setText(desArray[position]);
//			Picasso.with(con)
//					.load(imageRSC[position])
//					.error(R.drawable.ic_launcher).into(detailsImageView);
        } catch (final Exception e) {
            e.printStackTrace();
        }

        ((ViewPager) container).addView(viewLayout);

        return viewLayout;
    }

    @Override
    public String toString() {
        return "AllViewImage [inflater=" + inflater + ", con=" + con
                + ", imageRSC=" + imageRSC + "]";
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);

    }
}