package com.sloppy;

import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sloppy.aapbdLib.PersistData;
import com.sloppy.adapter.DraftRecyclerAdapter;
import com.sloppy.utils.AppConstant;

/**
 * Created by hp on 4/17/2016.
 */
public class DraftsFragment extends BaseFragment {

    Context con;
    View view;
    RecyclerView rvMyShop;
    DatabaseHandler dbholder;
    private TextView tvDataNotFound;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        con = getActivity();
        view = inflater.inflate(R.layout.fragment_draft, container, false);
        initUI();
        return view;
    }

    private void initUI() {
        dbholder = new DatabaseHandler(con);
        rvMyShop = (RecyclerView) view.findViewById(R.id.rvMyShop);
        rvMyShop.setLayoutManager(new LinearLayoutManager(con));
        tvDataNotFound=(TextView) view.findViewById(R.id.tvDataNotFound);
        ImageView imgDraftsBack = (ImageView) view.findViewById(R.id.imgDraftsBack);
        ImageView imgMenuDraft = (ImageView) view.findViewById(R.id.imgMenuDraft);
        imgMenuDraft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.showMenuDiadog(getActivity().getFragmentManager());
            }
        });
        imgDraftsBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        //==============Set Adapter================================================
        if (dbholder.getDraftDataSize(PersistData.getStringData(con,AppConstant.user_id))>0) {
            rvMyShop.setVisibility(View.VISIBLE);
            tvDataNotFound.setVisibility(View.GONE);
            rvMyShop.setAdapter(new DraftRecyclerAdapter(con, dbholder.getAllProduct(PersistData.getStringData(con, AppConstant.user_id))));
        } else{
            rvMyShop.setVisibility(View.GONE);
            tvDataNotFound.setVisibility(View.VISIBLE);
        }
    }

}
