package com.sloppy;

import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sloppy.aapbdLib.AlertMessage;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.NetInfo;
import com.sloppy.aapbdLib.PersistData;
import com.sloppy.adapter.ProductListByTypeAdapter;
import com.sloppy.model.ProductListByTypeResponse;
import com.sloppy.utils.AAPBDHttpClient;
import com.sloppy.utils.AllURL;
import com.sloppy.utils.AppConstant;

import java.util.concurrent.Executors;

/**
 * Created by hp on 6/23/2016.
 */
public class CommonListFragment extends BaseFragment {

    private Context con;
    private View view;
    private ProductListByTypeResponse productListByTypeResponse;
    private RecyclerView rvCommonList;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        con = getActivity();
        view = inflater.inflate(R.layout.fragment_common_list, container, false);
        initUI();
        return view;
    }

    private void initUI() {
        callProductListByLikeAPI(con, AppConstant.productListType, "10");
        TextView tvTitelCommon = (TextView) view.findViewById(R.id.tvTitelCommon);
        //===============Initialization=================
        rvCommonList = (RecyclerView) view.findViewById(R.id.rvCommonList);
        tvTitelCommon.setText(AppConstant.productListTitel);
        //=========On Click==============================

        ImageView imgBackCommonList = (ImageView) view.findViewById(R.id.imgBackCommonList);
        ImageView imgMenuCommon = (ImageView) view.findViewById(R.id.imgMenuCommon);
        imgBackCommonList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        imgMenuCommon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.showMenuDiadog(getActivity().getFragmentManager());
            }
        });
    }

    /**
     * -------------------32. Get User Product List  by Type------------------
     */
    public void callProductListByLikeAPI(final Context con, final String type, final String page_items) {
      /*
       * ---------------check internet first------------
       */
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }
         /*
          * ----------------Show Busy Dialog -----------------------------------------
          */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
      /*
       *----------------------Start Thread-------------------------------------------
       */
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            String msg = "";
            String response = "";

            @Override
            public void run() {

                // You can performed your task here.
                try {
                    Log.e(type + "List URL", AllURL.getMyShopData(type, page_items));
                    //-------------Hit Server---------------------
                    response = AAPBDHttpClient.get(AllURL.getMyShopData(type, page_items)).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();
                    Log.e(type + "List Response", ">>" + response);
                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        TextView tvDataNotFound= (TextView) view.findViewById(R.id.tvDataNotFound);
                        /**
                         * -------------Here we do UI related tasks inside run() method of runOnUiThread()-------------------
                         */
                        //-------Stop Busy Dialog-----
                        if (busy != null) {
                            busy.dismis();
                        }
                        //----------------Persist response with the help of Gson----------------
                        Gson gson = new Gson();
                        productListByTypeResponse = gson.fromJson(response, ProductListByTypeResponse.class);
                        /**
                         * ---------main Ui related work--------------
                         */
                        if (productListByTypeResponse.getStatus().equalsIgnoreCase("1")) {
                            Log.e("LikeProList size", ">>" + productListByTypeResponse.getResult().size());
//                            Toast.makeText(con, "LikeProList " + productListByTypeResponse.getMsg(), Toast.LENGTH_LONG).show();
                            if (productListByTypeResponse.getResult().size()> 0) {
                                tvDataNotFound.setVisibility(View.GONE);
                                rvCommonList.setVisibility(View.VISIBLE);
                                /**
                                 * =================Set Adapter===========================================================
                                 */
                                rvCommonList.setLayoutManager(new LinearLayoutManager(con));
                                rvCommonList.setAdapter(new ProductListByTypeAdapter(getActivity(), productListByTypeResponse.getResult()));

                            } else {
                                rvCommonList.setVisibility(View.GONE);
                                tvDataNotFound.setVisibility(View.VISIBLE);
                            }

                        } else {
                            msg = productListByTypeResponse.getMsg();
                            AlertMessage.showMessage(con, "ProCommentList", msg);
                        }

                    }
                });

            }

        });

    }

}
