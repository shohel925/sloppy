package com.sloppy;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.sloppy.aapbdLib.AlertMessage;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.NetInfo;
import com.sloppy.aapbdLib.PersistData;
import com.sloppy.model.MessageListInfo;
import com.sloppy.model.MyMessageResultInfo;
import com.sloppy.model.ProCommentAddResponse;
import com.sloppy.model.SendMessageResponse;
import com.sloppy.utils.AAPBDHttpClient;
import com.sloppy.utils.AllURL;
import com.sloppy.utils.AppConstant;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;

/**
 * Created by hp on 5/25/2016.
 */
public class MessageChatFragment extends BaseFragment {
    private Context con;
    private View v;
    private ImageView imgMsgBack;
    private TextView tvLogin1, etWriteComment, tvSendComment;
    private ListView listViewProComment;
    MyArrayListAdapter myArrayListAdapter;
    MyMessageResultInfo myResponse;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        con = getActivity();
        v = inflater.inflate(R.layout.dialog_comments, container, false);
        initUI();
        return v;
    }

    private void initUI() {
        myResponse = AppConstant.chatList;
        /**
         * ==================Initilization==========================================================
         */
        imgMsgBack = (ImageView) v.findViewById(R.id.imgMsgBack);
        imgMsgBack.setVisibility(View.VISIBLE);
        tvLogin1 = (TextView) v.findViewById(R.id.tvLogin1);
        etWriteComment = (TextView) v.findViewById(R.id.etWriteComment);
        tvSendComment = (TextView) v.findViewById(R.id.tvSendComment);
        listViewProComment = (ListView) v.findViewById(R.id.listViewProComment);


        /**
         * =================Data set================================================================
         */
        tvLogin1.setText(getResources().getString(R.string.Chat));
        etWriteComment.setText(getResources().getString(R.string.tap_to));
        tvSendComment.setText(getResources().getString(R.string.send_message));

        /**
         * ====================On click=============================================================
         */
        imgMsgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        tvSendComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(etWriteComment.getText().toString())) {
                    AlertMessage.showMessage(con, getString(R.string.status),
                            getString(R.string.write_comment));
                } else {
                    /**
                     * -------------------Call 22. Send message to a user-----------------
                     */
                    callSendMessageAPI(myResponse.getUser_id(), etWriteComment.getText().toString());
                }
            }
        });

        myArrayListAdapter = new MyArrayListAdapter(con, myResponse.getMessageList());
        listViewProComment.setAdapter(myArrayListAdapter);
        myArrayListAdapter.notifyDataSetChanged();
    }

    /**
     * -------------------22. Send message to a user------------------
     */
    private void callSendMessageAPI( final String user_id, final String msg_text) {
  /*
   * ---------------check internet first------------
   */
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }

         /*
          * ----------------Show Busy Dialog -----------------------------------------
          */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
      /*
       *----------------------Start Thread-------------------------------------------
       */
        Executors.newSingleThreadExecutor().submit(new Runnable() {
            SendMessageResponse sendMessageResponse;
            String msg = "";
            String response = "";

            @Override
            public void run() {
                // You can performed your task here.
                try {
                    Log.w("22. Send message URL", AllURL.getSendMessage(user_id,msg_text));
                    //-------------Hit Server---------------------
                    response = AAPBDHttpClient.get(AllURL.getSendMessage(user_id,msg_text)).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();

                    //----------------Persist response with the help of Gson----------------
                    Log.w("22Send messageResponse", ">>" + response);
                    Gson gson = new Gson();
                    sendMessageResponse = gson.fromJson(response, SendMessageResponse.class);

                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        /**
                         * -------------Here we do UI related tasks inside run() method of runOnUiThread()-------------------
                         */
                        //-------Stop Busy Dialog-----
                        if (busy != null) {
                            busy.dismis();
                        }

                        /**
                         * ---------main Ui related work--------------
                         */
                        if (sendMessageResponse.getStatus().equalsIgnoreCase("1")) {
                            Toast.makeText(con, "Message send"+ sendMessageResponse.getMsg(), Toast.LENGTH_LONG).show();
                            MessageListInfo newObject= new MessageListInfo();
                            newObject.setUser_id(PersistData.getStringData(con,AppConstant.user_id));
                            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm a dd/MM/yy");
                            String currentDateandTime = sdf.format(new Date());
                            newObject.setCreated_at(currentDateandTime);
                            newObject.setMessage_text(etWriteComment.getText().toString());
                            myResponse.getMessageList().add(newObject);
                            myArrayListAdapter.notifyDataSetChanged();
                        } else {
                            msg = sendMessageResponse.getMsg();
                            AlertMessage.showMessage(con, "22.Send message to a user", msg);
                        }

                    }
                });

            }

        });

    }


    /**
     * ============================Message List Adapter==========================================
     */
    private class MyArrayListAdapter extends ArrayAdapter<MessageListInfo> {
        Context con;
        private ImageView imgOtherUser, imgChatUser;
        private TextView tvOtherUsRName, tvOtrChatTime, tvOtrUserText, tvUserChatText, tvUserNameChat, tvUserChatTime;

        MyArrayListAdapter(Context context, List<MessageListInfo> list) {
            super(context, R.layout.row_chat_user, list);
            this.con = context;
        }

        @Override
        public View getView(final int position, View v, ViewGroup viewGroup) {

                final LayoutInflater inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                if (PersistData.getStringData(con, AppConstant.user_id).equalsIgnoreCase(myResponse.getMessageList().get(position).getUser_id())) {
                   //======================Row xml set/ Inflate row=================================
                    v = inflater.inflate(R.layout.row_chat_user, null);
                    //==================Initialization==============================================
                    imgChatUser = (ImageView) v.findViewById(R.id.imgChatUser);
                    tvUserNameChat = (TextView) v.findViewById(R.id.tvUserNameChat);
                    tvUserChatTime = (TextView) v.findViewById(R.id.tvUserChatTime);
                    tvUserChatText = (TextView) v.findViewById(R.id.tvUserChatText);

                    //========================Data Set==============================================
                    final MessageListInfo query = myResponse.getMessageList().get(position);
                    Picasso.with(con)
                            .load(PersistData.getStringData(con,AppConstant.userImage))
                            .placeholder(R.drawable.ic_launcher)
                            .error(R.drawable.ic_launcher)
                            .into(imgChatUser);
                    SimpleDateFormat getFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    SimpleDateFormat newFormat = new SimpleDateFormat("hh:mm a dd/MM/yy");
                    Date date = null;
                    try {
                        date = getFormat.parse(query.getCreated_at());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    String out = newFormat.format(date);
                    Log.e("Time", out);
                    tvUserChatTime.setText(out);
                    tvUserChatText.setText(query.getMessage_text());

                } else {

                    v = inflater.inflate(R.layout.row_chat, null);
                    //==================Initialization==============================================
                    imgOtherUser = (ImageView) v.findViewById(R.id.imgOtherUser);
                    tvOtherUsRName = (TextView) v.findViewById(R.id.tvOtherUsRName);
                    tvOtrChatTime = (TextView) v.findViewById(R.id.tvOtrChatTime);
                    tvOtrUserText = (TextView) v.findViewById(R.id.tvOtrUserText);

                    //========================Data Set==============================================
                    final MessageListInfo query = myResponse.getMessageList().get(position);
                    Picasso.with(con)
                            .load(myResponse.getProfile_image())
                            .placeholder(R.drawable.ic_launcher)
                            .error(R.drawable.ic_launcher)
                            .into(imgOtherUser);
                    tvOtherUsRName.setText(myResponse.getName());
                    SimpleDateFormat getFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    SimpleDateFormat newFormat = new SimpleDateFormat("hh:mm a dd/MM/yy");
                    Date date = null;
                    try {
                        date = getFormat.parse(query.getCreated_at());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    String out = newFormat.format(date);
                    Log.e("Time", out);
                    tvOtrChatTime.setText(out);
                    tvOtrUserText.setText(query.getMessage_text());
                }

            return v;
        }
    }
}
