package com.sloppy;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class WebFragment extends BaseFragment implements View.OnClickListener{
    private View view;
    Context con;
    WebView webview;
    ProgressBar progress;

    private ImageView termsOfSerBack,imgTermOfMenu;
    TextView tvprivacypolicytitle;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_term_of_service, container, false);
        con = getActivity();
        initUi();

        return view;
    }
    private void initUi() {
        // TODO Auto-generated method stub
        termsOfSerBack = (ImageView) view.findViewById(R.id.termsOfSerBack);
//        tvprivacypolicytitle=(TextView)view.findViewById(R.id.tvprivacypolicytitle);
//        tvprivacypolicytitle.setText("Website");
//        progress = (ProgressBar) view.findViewById(R.id.progress);
//        webBack.setOnClickListener(this);
//        getWebView(AppConstant.webUrl);
        termsOfSerBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
    }

    public void getWebView(String url) {

        final CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.setAcceptCookie(true);
        CookieSyncManager.createInstance(getActivity());
        webview = (WebView) view.findViewById(R.id.webview);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);
        webview.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
        webview.getSettings().setDomStorageEnabled(true);

        webview.getSettings().setLoadWithOverviewMode(true);
        webview.getSettings().setUseWideViewPort(true);

//        if (AppConstant.webUrl != "") {
//            webview.loadUrl(AppConstant.webUrl);
//        }

        webview.setWebViewClient(new WebViewClient() {

            @Override
            public void onReceivedError(WebView view, int errorCode,
                                        String description, String failingUrl) {
                // Toast.makeText(activity, "Oh no! " + description,
                // Toast.LENGTH_LONG).show();
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                progress.setVisibility(View.VISIBLE);

                // Toast.makeText(con, "page start URL : "+url, 2000).show();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                progress.setVisibility(View.INVISIBLE);

//                AppConstant.webUrl = url;

                Log.d("Finish URL  ", ": " + url);

            }

            /*
             * (non-Javadoc)
             *
             * @see
             * android.webkit.WebViewClient#shouldOverrideUrlLoading(android
             * .webkit.WebView, java.lang.String)
             */
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                CookieSyncManager.getInstance().sync();

//                AppConstant.webUrl = url;

                Log.d("WebApp", "URL is:" + url);
                // Toast.makeText(con, "URL : "+url, 2000).show();
                // homebtn.setBackgroundResource(R.drawable.backbtn);

                if (url.startsWith("tel:")) {

                    Log.d("Where : ", "Tel");

                    final Intent intent = new Intent(Intent.ACTION_DIAL, Uri
                            .parse(url));
                    startActivity(intent);
                    return true;
                }

                view.loadUrl(url);
                return true;
            }

        });

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub

//        if (v.getId() == R.id.ibPrivacyPolicyBack) {
//            getActivity().onBackPressed();
//        }
    }

/*    private int getScale(){

        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int width = metrics.widthPixels;

        Double val = new Double(width)/new Double(PIC_WIDTH);
        val = val * 100d;
        return val.intValue();
    }*/

}
