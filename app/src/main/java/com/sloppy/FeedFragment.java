package com.sloppy;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.sloppy.aapbdLib.AlertMessage;
import com.sloppy.aapbdLib.BusyDialog;
import com.sloppy.aapbdLib.NetInfo;
import com.sloppy.aapbdLib.PersistData;
import com.sloppy.aapbdLib.PersistentUser;
import com.sloppy.model.CommonInformationResponse;
import com.sloppy.model.ProductAndUserDetailsInfo;
import com.sloppy.model.FeedRespons;
import com.sloppy.model.LikeUnlikeResponse;
import com.sloppy.utils.AAPBDHttpClient;
import com.sloppy.utils.AllURL;
import com.sloppy.utils.AppConstant;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import android.Manifest;

import cn.jeesoft.widget.pickerview.CharacterPickerWindow;
import cz.msebera.android.httpclient.Header;

/**
 * Created by hp on 4/26/2016.
 */
public class FeedFragment extends BaseFragment {

    /**
     * ------------Variable ----------------------------------
     */
    private Context con;
    private View view;

    private int feedRowPosition = 0;
    private String selectedProductID;
    private TextView tvSloppy2ndFeed, tvFeedLikeNumber, tvNodataFeed;
    private ImageView imgLike, imgComMessages, imgLVSinglePro, imgLSinglePro, imgReload;
    private ListView listFeed;
    private LinearLayout buyView, llProductDetails;
    private BusyDialog busyNow;
    private Dialog dialog;
    public ImageView imgLikeview;
    //------------Response------------------------
    public FeedRespons feedResponse;

    //---------------Adapter-------------------
    private CustomFeedListAadpter customFeedListAadpter;
    private String pictureUrl = "";
    private int DOUBLE_CLICK_TIME_INTERVAL = 500;
    private int i = 0;
    public static FeedFragment instance;
    private String loginType = "";
    private String flag_type="",flag_id="",flag_action="";

    /**
     * ------------------On Create------------------------------------
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        instance = this;
        con = getActivity();
        view = inflater.inflate(R.layout.fragment_feed, container, false);
        initUI();
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //--------Check log in or not-------------------
        if (!PersistentUser.isLogged(con)) {
            /**
             * --------------Call API8. Demo user (For only Home Feed)
             */
            gethomeFeedData(AllURL.getFeeddataDemo());
              /*
		 * -------------Call API-9. Common Information---------
		 */
            getCommonInformationFromServer(AllURL.commonInformationUrl("demo"));
            loginType = "demo";

        } else {
            loginType = "all";
            gethomeFeedData(AllURL.getFeeddata("10"));
            getCommonInformationFromServer(AllURL.commonInformationUrl("all"));
        }

    }

    /**
     * ------------------Initialization--------------------------------------------------
     */
    private void initUI() {
        tvSloppy2ndFeed = (TextView) view.findViewById(R.id.tvSloppy2ndFeed);

        //-------------Image View---------------------------
        imgLSinglePro = (ImageView) view.findViewById(R.id.imgLSinglePro);
        imgLVSinglePro = (ImageView) view.findViewById(R.id.imgLVSinglePro);
        imgReload = (ImageView) view.findViewById(R.id.imgReload);

        //------Linear Layout-------------------
        buyView = (LinearLayout) view.findViewById(R.id.buyView);
        listFeed = (ListView) view.findViewById(R.id.listFeed);

        //------------Feed layout Visible as first layout-------------

        imgReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!PersistentUser.isLogged(con)) {
                    /**
                     * --------------Call API8. Demo user (For only Home Feed)
                     */
                    gethomeFeedData(AllURL.getFeeddataDemo());
                } else {
                    gethomeFeedData(AllURL.getFeeddata("10"));
                }
            }
        });
    }

    long lastTouchUpTime = 0;
    boolean isDoubleClick = false;

    private void performDoubleClick() {
        long currentTime = System.currentTimeMillis();
        if (!isDoubleClick && currentTime - lastTouchUpTime < DOUBLE_CLICK_TIME_INTERVAL) {
            isDoubleClick = true;
            lastTouchUpTime = currentTime;
            Toast.makeText(con, "double click", Toast.LENGTH_SHORT).show();
        } else {
            lastTouchUpTime = currentTime;
            isDoubleClick = false;
        }
    }

    private void reportDialoge(final Context con,final int pos) {
        final Dialog dialogSize = new Dialog(con);
        dialogSize.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogSize.setContentView(R.layout.dialog_report);
        dialogSize.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialogSize.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView reportItem = (TextView) dialogSize.findViewById(R.id.reportItem);
        TextView blockItem = (TextView) dialogSize.findViewById(R.id.blockItem);
        TextView reportSeller = (TextView) dialogSize.findViewById(R.id.reportSeller);
        TextView blockSeller = (TextView) dialogSize.findViewById(R.id.blockSeller);
        TextView tvCancel = (TextView) dialogSize.findViewById(R.id.tvCancel);

        //==================Font set==========================
        Typeface helveticaNeuRegular = Typeface.createFromAsset(con.getAssets(), "font/helveticaNeuRegular.ttf");

        reportItem.setTypeface(helveticaNeuRegular);
        blockItem.setTypeface(helveticaNeuRegular);
        reportSeller.setTypeface(helveticaNeuRegular);
        blockSeller.setTypeface(helveticaNeuRegular);
        tvCancel.setTypeface(helveticaNeuRegular);

        reportItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flag_type="product";
                flag_id=feedResponse.getResult().get(pos).getId();
                flag_action="report";
                reportSubmitDialoge(con);
                dialogSize.dismiss();
            }
        });
        blockItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flag_type="product";
                flag_id=feedResponse.getResult().get(pos).getId();
                flag_action="block";
                reportSubmitDialoge(con);
                dialogSize.dismiss();
            }
        });

        reportSeller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flag_type="user";
                flag_id=feedResponse.getResult().get(pos).getUser_id();
                flag_action="report";
                reportSubmitDialoge(con);
                dialogSize.dismiss();
            }
        });
        blockSeller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flag_type="user";
                flag_id=feedResponse.getResult().get(pos).getUser_id();
                flag_action="block";
                reportSubmitDialoge(con);
                dialogSize.dismiss();
            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogSize.dismiss();
            }
        });

        dialogSize.show();
    }

    private void reportSubmitDialoge(final Context con) {
        final Dialog dialogSize = new Dialog(con);
        dialogSize.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogSize.setContentView(R.layout.dialog_repost_input);
        dialogSize.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialogSize.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final EditText etCustomSz = (EditText) dialogSize.findViewById(R.id.etCustomSz);
        TextView tvSubmit = (TextView) dialogSize.findViewById(R.id.tvSubmit);
        TextView tvCancelSz = (TextView) dialogSize.findViewById(R.id.tvCancelSz);

        //==================Font set==========================
        Typeface helveticaNeuRegular = Typeface.createFromAsset(con.getAssets(), "font/helveticaNeuRegular.ttf");

        etCustomSz.setTypeface(helveticaNeuRegular);
        tvSubmit.setTypeface(helveticaNeuRegular);
        tvCancelSz.setTypeface(helveticaNeuRegular);

        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(etCustomSz.getText().toString())){
                    AppConstant.alertDialoag(con,getString(R.string.alert),"Please Enter report/Block Reason","");
                }else{
                    sentReportAPI(AllURL.sentReport(),etCustomSz.getText().toString());
                    dialogSize.dismiss();
                }

            }
        });
        tvCancelSz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogSize.dismiss();
            }
        });

        dialogSize.show();
    }

    /**
     * -----------------Array Adapter of Product list---------------------------------------------------
     */
    private class CustomFeedListAadpter extends ArrayAdapter<ProductAndUserDetailsInfo> {
        Context context;

        //-------Constructor--------------
        public CustomFeedListAadpter(Context context) {
            super(context, R.layout.raw_feed, feedResponse.getResult());
            this.context = context;
        }

        @SuppressWarnings("unchecked")
        @Override
        public View getView(final int position, View feedItemView, ViewGroup parent) {
            if (feedItemView == null) {
                final LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                feedItemView = vi.inflate(R.layout.raw_feed, null);
            }

            if (position < feedResponse.getResult().size()) {
                //------------Add photo in a list-------------
                final ProductAndUserDetailsInfo query = feedResponse.getResult().get(position);
                final List<String> imgsSrc = new ArrayList<>();
                imgsSrc.clear();
                if (query.getPhoto_one() != null) {
                    imgsSrc.add(query.getPhoto_one());
                }
                if (query.getPhoto_two() != null) {
                    imgsSrc.add(query.getPhoto_two());
                }
                if (query.getPhoto_three() != null) {
                    imgsSrc.add(query.getPhoto_three());
                }
                if (query.getPhoto_four() != null) {
                    imgsSrc.add(query.getPhoto_four());
                }

                Log.e("imgsSrc", "" + imgsSrc.size());
                ImageView imageviewDot = (ImageView) feedItemView.findViewById(R.id.imageviewDot);
                imageviewDot.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        reportDialoge(con,position);

                    }
                });
                if (PersistData.getStringData(context, AppConstant.user_id).equalsIgnoreCase(query.getUser_id())) {
                    imageviewDot.setVisibility(View.GONE);
                } else {
                    imageviewDot.setVisibility(View.VISIBLE);
                }
                final LinearLayout indicator = (LinearLayout) feedItemView.findViewById(R.id.dotIndicator);
                llProductDetails = (LinearLayout) feedItemView.findViewById(R.id.llProductDetails);
                final ViewPager fullpageimage = (ViewPager) feedItemView.findViewById(R.id.fullpageimage);
                AllViewImage allViewImage = new AllViewImage(con, imgsSrc, position);
                fullpageimage.setAdapter(allViewImage);
                fullpageimage.setCurrentItem(0);
                final ImageView imgman = (ImageView) feedItemView.findViewById(R.id.imgman);
                final ImageView imgSoldStamp = (ImageView) feedItemView.findViewById(R.id.imgSoldStamp);
                final TextView tvFeedTime = (TextView) feedItemView.findViewById(R.id.tvFeedTime);
                final TextView tvBrandsign = (TextView) feedItemView.findViewById(R.id.tvBrandsign);
                final TextView tvFeedProductSi = (TextView) feedItemView.findViewById(R.id.tvFeedProductSi);
                final TextView tvFeedProductSize = (TextView) feedItemView.findViewById(R.id.tvFeedProductSize);
                final ImageView tvBuyNow = (ImageView) feedItemView.findViewById(R.id.tvBuyNow);
                final TextView tvOriginalPricefeed = (TextView) feedItemView.findViewById(R.id.tvOriginalPricefeed);
                final TextView tvlistingPrice = (TextView) feedItemView.findViewById(R.id.tvlistingPrice);
                final TextView tvFeedUname = (TextView) feedItemView.findViewById(R.id.tvFeedUname);
                final TextView tvFeedProductTitle = (TextView) feedItemView.findViewById(R.id.tvFeedProductTitle);
                final TextView tvFeedBrandName = (TextView) feedItemView.findViewById(R.id.tvFeedBrandName);
                tvFeedLikeNumber = (TextView) feedItemView.findViewById(R.id.tvFeedLikeNumber);
                final TextView tvFeedMailNumber = (TextView) feedItemView.findViewById(R.id.tvFeedMailNumber);
                final TextView tvAddToBundela = (TextView) feedItemView.findViewById(R.id.tvAddToBundela);

                imgLikeview = (ImageView) feedItemView.findViewById(R.id.imgLikeview);
                imgLike = (ImageView) feedItemView.findViewById(R.id.imgLike);
                final ImageView imgShare = (ImageView) feedItemView.findViewById(R.id.imgShare);

                final ImageView imgComments = (ImageView) feedItemView.findViewById(R.id.imgComments);
                final ImageView imgOffer = (ImageView) feedItemView.findViewById(R.id.imgOffer);


                try {

                    indicator.removeAllViews(); // simple linear layout

                    final LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(

                            ViewGroup.LayoutParams.WRAP_CONTENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT);

                    layoutParams.setMargins(10, 7, 10, 2);

                    final ImageView[] b = new ImageView[imgsSrc.size()];

                    for (int i1 = 0; i1 < imgsSrc.size(); i1++) {

                        b[i1] = new ImageView(con);

                        b[i1].setId(1000 + i1);
                        Log.e("Control i1", " " + i1);

                        Log.e("pageno", " " + 0);

                        if (0 == i1) {
                            b[i1].setBackgroundResource(R.drawable.slide_dott_select);
                        } else {
                            b[i1].setBackgroundResource(R.drawable.slide_dott);
                        }

                        b[i1].setLayoutParams(layoutParams);
                        indicator.addView(b[i1]);
                    }

                } catch (final Exception e) {

                }

                /// ========== Change control dot =============
                fullpageimage.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                    }

                    @Override
                    public void onPageSelected(int position) {
                        if (imgsSrc.size() > 1) {
                            position = position % imgsSrc.size();
                        }

                        try {
                            indicator.removeAllViews(); // simple linear layout
                            final LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                                    ViewGroup.LayoutParams.WRAP_CONTENT);

                            layoutParams.setMargins(10, 7, 10, 2);

                            final ImageView[] b = new ImageView[imgsSrc.size()];

                            for (int i1 = 0; i1 < imgsSrc.size(); i1++) {

                                b[i1] = new ImageView(con);

                                b[i1].setId(1000 + i1);
                                Log.e("Control i1", " " + i1);

                                Log.e("pageno", " " + position);

                                if (position == i1) {
                                    b[i1].setBackgroundResource(R.drawable.slide_dott_select);
                                } else {
                                    b[i1].setBackgroundResource(R.drawable.slide_dott);
                                }

                                b[i1].setLayoutParams(layoutParams);
                                indicator.addView(b[i1]);
                            }

                        } catch (final Exception e) {

                        }
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }
                });

                //--------------------Font Set---------------------------------
                Typeface helveticaNeuRegular = Typeface.createFromAsset(getContext().getAssets(), "font/helveticaNeuRegular.ttf");
                Typeface helveticaNeueBold = Typeface.createFromAsset(getContext().getAssets(), "font/helveticaNeueBold.ttf");

                tvlistingPrice.setTypeface(helveticaNeueBold);
                tvFeedUname.setTypeface(helveticaNeuRegular);
                tvFeedProductTitle.setTypeface(helveticaNeuRegular);
                tvFeedTime.setTypeface(helveticaNeuRegular);
                tvFeedProductSize.setTypeface(helveticaNeuRegular);

                tvOriginalPricefeed.setTypeface(helveticaNeuRegular);
                tvFeedBrandName.setTypeface(helveticaNeuRegular);
                tvFeedLikeNumber.setTypeface(helveticaNeuRegular);
                tvAddToBundela.setTypeface(helveticaNeuRegular);
                tvSloppy2ndFeed.setTypeface(helveticaNeuRegular);

                Picasso.with(context)
                        .load(query.getSeller_info().get(0).getProfile_image())
                        .placeholder(R.drawable.person)
                        .error(R.drawable.person)
                        .into(imgman);
                /**
                 * -------------Set currency simble-----------------------------------------------------
                 */

                tvOriginalPricefeed.setText(AppConstant.getCurrencySymbol(query.getSeller_info().get(0).getCurrency()) + query.getOriginal_price());
                tvlistingPrice.setText(AppConstant.getCurrencySymbol(query.getSeller_info().get(0).getCurrency()) + query.getListing_price());
                tvOriginalPricefeed.setPaintFlags(tvOriginalPricefeed.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                tvFeedUname.setText(query.getSeller_info().get(0).getUsername());
                tvFeedProductTitle.setText(query.getName());
                tvFeedBrandName.setText(query.getBrand_info().get(0).getName());
                tvFeedLikeNumber.setText(query.getLike_count());
                tvFeedMailNumber.setText(query.getComments_count());
                LinearLayout llSize = (LinearLayout) feedItemView.findViewById(R.id.llSize);
                if (query.getSize_info() != null) {
                    llSize.setVisibility(View.VISIBLE);
                    tvFeedProductSize.setText(query.getSize_info().getSize());
                } else {
                    llSize.setVisibility(View.VISIBLE);
                    tvFeedProductSize.setText(query.getSize());
//                    llSize.setVisibility(View.GONE);
                }

                /**
                 * -----------Make how time ago product is created------------------------
                 */

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String createdDate = query.getCreated_at();
                Date createdate = null;
                try {
                    createdate = sdf.parse(createdDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                tvFeedTime.setText(getDisplayableTime(createdate.getTime()));
                ;
                Log.e("createdDate", ">>>" + createdDate);
                if (query.getIs_sold().equalsIgnoreCase("1")) {
                    imgSoldStamp.setVisibility(View.VISIBLE);
                } else {
                    imgSoldStamp.setVisibility(View.GONE);
                }
                if (query.getUser_id().equalsIgnoreCase(PersistData.getStringData(context, AppConstant.user_id)) || query.getIs_sold().equalsIgnoreCase("1")) {
                    tvAddToBundela.setVisibility(View.GONE);
                    imgOffer.setVisibility(View.GONE);
                    tvBuyNow.setVisibility(View.GONE);
                } else {
                    tvAddToBundela.setVisibility(View.VISIBLE);
                    imgOffer.setVisibility(View.VISIBLE);
                    tvBuyNow.setVisibility(View.VISIBLE);
                }
                /**
                 * ---------Check Like or not and set image
                 */
                if (query.getIs_liked().equalsIgnoreCase("0")) {
                    imgLikeview.setImageResource(R.drawable.love_unliked);
                    imgLike.setImageResource(R.drawable.love_unliked);
                } else {
                    imgLikeview.setImageResource(R.drawable.love_liked);
                    imgLike.setImageResource(R.drawable.love_liked);
                }

                /**
                 * -------------Click on Feed Array List Field----------------
                 */
                imgComments.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (!PersistentUser.isLogged(con)) {
                            AppConstant.loginDialoag(context);
                        } else {
                            final ProductAndUserDetailsInfo query = feedResponse.getResult().get(position);
                            AppConstant.productIDfeedRow = query.getId();
                            AppConstant.tagUserList = query.getTaggable_users();
                            FragmentManager manager = getActivity().getFragmentManager();
                            CommantDialogFragment dialogMenu = new CommantDialogFragment();
                            dialogMenu.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    tvFeedMailNumber.setText(AppConstant.commentCount);
                                }
                            });
                            dialogMenu.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar);
                            dialogMenu.show(manager, "");
                        }

                    }
                });

                tvFeedBrandName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        AppConstant.brandID = query.getBrand_info().get(0).getId();
                        AppConstant.brandName = query.getBrand_info().get(0).getName();
                        AppConstant.feedRowPosition = position;
                        myCommunicator.setContentFragment(new BrandFragment(), true);

                    }
                });
                imgman.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AppConstant.feedRowPosition = position;
                        feedRowPosition = position;
                        /**
                         * --------------Call API8. Demo user (For only Home Feed)
                         */
                        AppConstant.otherUserId = query.getUser_id();
                        myCommunicator.setContentFragment(new OtherUserDetailsFragment(), true);


                    }
                });

                tvFeedUname.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AppConstant.feedRowPosition = position;
                        feedRowPosition = position;
                        /**
                         * --------------Call API8. Demo user (For only Home Feed)
                         */
                        AppConstant.otherUserId = query.getUser_id();
                        myCommunicator.setContentFragment(new OtherUserDetailsFragment(), true);


                    }
                });
                imgLikeview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!PersistentUser.isLogged(con)) {
                            AppConstant.loginDialoag(context);
                        } else {
                            feedRowPosition = position;
                            selectedProductID = query.getId();
                            callLikeUnlikeAPI(selectedProductID, "click", position);
                        }

                    }
                });

                imgLike.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!PersistentUser.isLogged(con)) {
                            AppConstant.loginDialoag(context);
                        } else {
                            feedRowPosition = position;
                            selectedProductID = query.getId();
                            callLikeUnlikeAPI(selectedProductID, "click", position);
                        }

                    }
                });


                tvAddToBundela.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AppConstant.feedRowPosition = position;
                        if (!PersistentUser.isLogged(con)) {
                            AppConstant.loginDialoag(context);
                        } else {
                            final ProductAndUserDetailsInfo query = feedResponse.getResult().get(position);
                            AppConstant.productIDfeedRow = query.getId();
                            AppConstant.otherUserId = query.getUser_id();
                            AddToBundleFragment fragment = new AddToBundleFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("titleBundle", query.getSeller_info().get(0).getUsername());
                            fragment.setArguments(bundle);
                            myCommunicator.setContentFragment(fragment, true);
//                            myCommunicator.setContentFragment(new AddToBundleFragment(), true);
                        }

                    }
                });

                tvBuyNow.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        AppConstant.feedRowPosition = position;
                        if (!PersistentUser.isLogged(con)) {
                            AppConstant.loginDialoag(context);
                        } else {
                            AppConstant.feedRowObject = feedResponse.getResult().get(position);
                            myCommunicator.setContentFragment(new BuyNowFragment(), true);
                        }
                    }
                });

                imgShare.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (PersistentUser.isLogged(context)) {
                            pictureUrl = feedResponse.getResult().get(position).getPhoto_one();
                            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                    != PackageManager.PERMISSION_GRANTED) {
                                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 11);
                            } else {
                                Picasso.with(con).load(pictureUrl).memoryPolicy(MemoryPolicy.NO_CACHE).into(target);
                            }

                        } else {
                            AppConstant.loginDialoag(con);
                        }
                    }
                });

                feedItemView.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        AppConstant.feedRowPosition = position;
                        AppConstant.feedRowObject = feedResponse.getResult().get(position);
                        AppConstant.productIDfeedRow = query.getId();
                        myCommunicator.setContentFragment(new SingleProductFragment(), true);
                    }
                });

                llProductDetails.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        AppConstant.feedRowPosition = position;
                        AppConstant.feedRowObject = feedResponse.getResult().get(position);
                        AppConstant.productIDfeedRow = query.getId();
                        myCommunicator.setContentFragment(new SingleProductFragment(), true);
                    }
                });

                imgOffer.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (!PersistentUser.isLogged(con)) {
                            AppConstant.loginDialoag(context);
                        } else {
                            AppConstant.feedRowObject = feedResponse.getResult().get(position);
                            OfferDialogFragment offerDialog = new OfferDialogFragment();
                            offerDialog.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar);
                            offerDialog.show(getActivity().getFragmentManager(), "offerdialog");
                        }
                    }
                });

            }
            return feedItemView;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 11) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Picasso.with(con).load(pictureUrl).memoryPolicy(MemoryPolicy.NO_CACHE).into(target);
            }
        }
    }

    BusyDialog busyDialog;

    private Target target = new Target() {


        @Override
        public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
            new Thread(new Runnable() {

                @Override
                public void run() {

                    File file = new File(Environment.getExternalStorageDirectory().getPath() + "/picture.jpg");
                    try {
                        file.createNewFile();
                        FileOutputStream ostream = new FileOutputStream(file);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, ostream);
                        busyDialog.dismis();
                        String path = MediaStore.Images.Media.insertImage(con.getContentResolver(), bitmap, "Title", null);
                        AppConstant.defaultShare(con, Uri.parse(path));
                        ostream.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }).start();
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
            busyDialog.dismis();
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
            if (placeHolderDrawable != null) {
            }
            busyDialog = new BusyDialog(con, false, "");
            busyDialog.show();
        }
    };

    public static String getDisplayableTime(long delta) {
        long difference = 0;
        Long mDate = java.lang.System.currentTimeMillis();

        if (mDate > delta) {
            difference = mDate - delta;
            final long seconds = difference / 1000;
            final long minutes = seconds / 60;
            final long hours = minutes / 60;
            final long days = hours / 24;
            final long weeks = days / 7;
            final long months = days / 31;
            final long years = days / 365;

            if (seconds < 0) {
                return "not yet";
            } else if (seconds < 60) {
                return seconds == 1 ? "one second ago" : seconds + " seconds ago";
            } else if (seconds < 120) {
                return "a minute ago";
            } else if (seconds < 2700) // 45 * 60
            {
                return minutes + " minutes ago";
            } else if (seconds < 5400) // 90 * 60
            {
                return "an hour ago";
            } else if (seconds < 86400) // 24 * 60 * 60
            {
                return hours + " hours ago";
            } else if (seconds < 172800) // 48 * 60 * 60
            {
                return "yesterday";
            } else if (seconds < 604800) // 7 * 24 * 60 * 60
            {
                return days + " days ago";
            } else if (seconds < 2592000) // 30 * 24 * 60 * 60
            {
                return weeks <= 1 ? "1 week ago" : weeks + " weeks ago";
            } else if (seconds < 31104000) // 12 * 30 * 24 * 60 * 60
            {

                return months <= 1 ? "1 month ago" : months + " months ago";
            } else {

                return years <= 1 ? "one year ago" : years + " years ago";
            }
        }
        return null;
    }


    /**
     * -------------------10. Like/Unlike Product API------------------
     */
    public void callLikeUnlikeAPI(final String product_id, final String feedProLike, final int feedRowPosition) {
  /*
   * ---------------check internet first------------
   */
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }

  /*
   * ----------------Show Busy Dialog -----------------------------------------
   */
        final BusyDialog busy = new BusyDialog(con, false, "Please wait.....", false);
        busy.show();
  /*
   *
   *----------------------Start Thread-------------------------------------------
   *
   */
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            String msg = "";
            String response = "";
            LikeUnlikeResponse likeUnlikeResponse;

            @Override
            public void run() {
                //--------------- We can performed your task here.-----------------

                try {
                    Log.i("Like/Unlike URL", AllURL.getLikeUnlike(product_id));
                    //-------------Hit Server---------------------
                    response = AAPBDHttpClient.get(AllURL.getLikeUnlike(product_id)).
                            header("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token)).body();

                    //----------------Persist response with the help of Gson----------------
                    Log.i("Like/Unlike Response", ">>" + response);
                    Gson gson = new Gson();
                    likeUnlikeResponse = gson.fromJson(response, LikeUnlikeResponse.class);

                } catch (Exception e1) {
                    e1.printStackTrace();
                    msg = e1.getMessage();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        /**
                         * -------------Here we do UI related tasks inside run() method of runOnUiThread()-------------------
                         */

                        //-------Stop Busy Dialog-----
                        if (busy != null) {
                            busy.dismis();
                        }
                        /**
                         * ---------main Ui related work--------------
                         */
                        if (likeUnlikeResponse.getStatus().equalsIgnoreCase("1")) {
                            Toast.makeText(con, likeUnlikeResponse.getMsg(), Toast.LENGTH_LONG).show();
                        }
                        if (likeUnlikeResponse != null) {
                            if (feedProLike.equalsIgnoreCase("click")) {
                                if (likeUnlikeResponse.getMsg().equalsIgnoreCase("liked")) {
                                    feedResponse.getResult().get(feedRowPosition).setLike_count(likeUnlikeResponse.getLikes_count());
                                    feedResponse.getResult().get(feedRowPosition).setIs_liked("1");
//                                    customFeedListAadpter.notifyDataSetChanged();
                                } else if (likeUnlikeResponse.getMsg().equalsIgnoreCase("unliked")) {
                                    feedResponse.getResult().get(feedRowPosition).setLike_count(likeUnlikeResponse.getLikes_count());
                                    feedResponse.getResult().get(feedRowPosition).setIs_liked("0");
//                                    customFeedListAadpter.notifyDataSetChanged();
                                }
                            } else {
                                if (likeUnlikeResponse.getMsg().equalsIgnoreCase("liked")) {
                                    imgLVSinglePro.setImageResource(R.drawable.love_liked);
                                    imgLSinglePro.setImageResource(R.drawable.love_liked);
                                    feedResponse.getResult().get(feedRowPosition).setLike_count(likeUnlikeResponse.getLikes_count());
                                    feedResponse.getResult().get(feedRowPosition).setIs_liked("1");
//                                    customFeedListAadpter.notifyDataSetChanged();
                                } else if (likeUnlikeResponse.getMsg().equalsIgnoreCase("unliked")) {
                                    imgLVSinglePro.setImageResource(R.drawable.love_unliked);
                                    imgLSinglePro.setImageResource(R.drawable.love_unliked);
                                    feedResponse.getResult().get(feedRowPosition).setLike_count(likeUnlikeResponse.getLikes_count());
                                    feedResponse.getResult().get(feedRowPosition).setIs_liked("0");
//                                    customFeedListAadpter.notifyDataSetChanged();
                                }
                            }
                        }
                        customFeedListAadpter.notifyDataSetChanged();
//                        listFeed.setSelection(feedRowPosition);


                    }
                });

            }

        });

    }


    private void gethomeFeedData(final String url) {

        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status),
                    getString(R.string.checkInternet));
            return;
        }

        busyNow = new BusyDialog(con, true, false);
        busyNow.show();

//------Show URL-------------------
        Log.e("token", PersistData.getStringData(con, AppConstant.token));
        final AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token));
        client.get(url, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (busyNow != null) {
                    busyNow.dismis();
                }

                Log.e("Feed Response", new String(responseBody));
                Gson g = new Gson();
                feedResponse = g.fromJson(new String(responseBody), FeedRespons.class);

                Log.e("status", "" + feedResponse.getStatus());
                PersistData.setStringData(con, AppConstant.feedsResponse, new String(responseBody));

                if (feedResponse.getStatus().equalsIgnoreCase("1")) {
                    if (feedResponse.getResult().size() > 0) {
                        tvNodataFeed = (TextView) view.findViewById(R.id.tvNodataFeed);
                        tvNodataFeed.setVisibility(View.GONE);
                        listFeed.setVisibility(View.VISIBLE);
                        customFeedListAadpter = new CustomFeedListAadpter(con);
                        listFeed.setAdapter(customFeedListAadpter);
                        customFeedListAadpter.notifyDataSetChanged();
                        listFeed.setSelection(AppConstant.feedRowPosition);
                        Log.e("MyPosition", Integer.toString(AppConstant.feedRowPosition));
                    } else {
                        tvNodataFeed = (TextView) view.findViewById(R.id.tvNodataFeed);
                        listFeed.setVisibility(View.GONE);
                        tvNodataFeed.setVisibility(View.VISIBLE);
                    }


                } else {

                    AlertMessage.showMessage(con, "Status",
                            feedResponse.getMsg() + "");
                    return;
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                if (busyNow != null) {
                    busyNow.dismis();
                }
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried

            }

        });

    }

    /*
     * ------------------Common Information API---------------
     */
    private void getCommonInformationFromServer(final String commonurl) {

        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status),
                    getString(R.string.checkInternet));
            return;
        }

        final AsyncHttpClient client = new AsyncHttpClient();
        Log.e("token", PersistData.getStringData(con, AppConstant.token));
        client.addHeader("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token));

        // ------Show URL-------------------
        Log.e("Common URL", commonurl + "");
        client.get(commonurl, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (busyNow != null) {
                    busyNow.dismis();
                }

                Log.e("Response", new String(responseBody));

                Gson g = new Gson();
                CommonInformationResponse coderesponse = g.fromJson(new String(
                        responseBody), CommonInformationResponse.class);

                Log.e("status", "" + coderesponse.getStatus());

                if (coderesponse.getStatus().equalsIgnoreCase("1")) {

                    AppConstant.commonResponse = coderesponse;

//                    Toast.makeText(con, "Common Information"+coderesponse.getMsg() + "",
//                            Toast.LENGTH_LONG).show();

                } else {

                    AlertMessage.showMessage(con, "Status",
                            coderesponse.getMsg() + "");
                    return;
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                if (busyNow != null) {
                    busyNow.dismis();
                }
            }


            @Override
            public void onRetry(int retryNo) {
                // called when request is retried

            }

        });

    }

    private void sentReportAPI(final String url, final String message) {
        //--- for net check-----
        if (!NetInfo.isOnline(con)) {
            AlertMessage.showMessage(con, getString(R.string.status), getString(R.string.checkInternet));
            return;
        }

        final BusyDialog busyNow = new BusyDialog(con, true, false);
        busyNow.show();

        final AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("Authorization", "Bearer " + PersistData.getStringData(con, AppConstant.token));
        Log.e("token", PersistData.getStringData(con, AppConstant.token));

        final RequestParams param = new RequestParams();
        param.put("user_type", loginType);
        param.put("flag_id", flag_id);
        param.put("flag_type", flag_type);
        param.put("flag_action", flag_action);
        param.put("title", flag_action +""+flag_type);
        param.put("message", message);

        client.post(url, param, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers,
                                  byte[] response) {

                if (busyNow != null) {
                    busyNow.dismis();
                }

                Log.e("resposne ", ">>" + new String(response));
                Gson g = new Gson();
                LikeUnlikeResponse likeUnlikeResponse = g.fromJson(new String(response), LikeUnlikeResponse.class);

                if (likeUnlikeResponse.getStatus().equalsIgnoreCase("1")) {
                    Toast.makeText(con, likeUnlikeResponse.getMsg() + "", Toast.LENGTH_LONG).show();


                } else {
                    AppConstant.alertDialoag(con, "Alert", likeUnlikeResponse.getMsg(), "OK");
                    return;
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)

                Log.e("errorResponse", new String(errorResponse));

                if (busyNow != null) {
                    busyNow.dismis();
                }
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried

            }
        });

    }
}
