package com.sloppy;

import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Spinner;

import com.sloppy.utils.AppConstant;

/**
 * Created by hp on 4/6/2016.
 */
public class BillingInformationFragment extends Fragment {

    private Context con;
    private View viewBilling;
    private Spinner spnrBillingCountry;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        con=getActivity();
        viewBilling= inflater.inflate(R.layout.fragment_billing_information, container,false);
        initUI();
        return  viewBilling;
    }

    private void initUI() {
        /**
         * ----------------Inotialization----------------------------
         */
        ImageView imgMenuBilling=(ImageView) viewBilling.findViewById(R.id.imgMenuBilling);
        spnrBillingCountry=(Spinner) viewBilling.findViewById(R.id.spnrBillingCountry);
        ImageView imgBillingBack= (ImageView) viewBilling.findViewById(R.id.imgBillingBack);

        /**
         * ----------------Set data-----------------------------------------
         */
        imgMenuBilling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.showMenuDiadog(getActivity().getFragmentManager());
            }
        });

        imgBillingBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
    }
}
