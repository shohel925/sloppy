package com.sloppy;



import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;


/**
 * User: tobiasbuchholz
 * Date: 18.09.14 | Time: 11:02
 */
public class MemeViewPagerItemFragment extends Fragment {

    private static Context con;
    private static String imageUR;
    public static MemeViewPagerItemFragment instantiateWithArgs(final Context context, final String imageURL) {
        final MemeViewPagerItemFragment fragment = (MemeViewPagerItemFragment) instantiate(context, MemeViewPagerItemFragment.class.getName());
        final Bundle args = new Bundle();
        con=context;
        imageUR=imageURL;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArguments();
    }

    private void initArguments() {
        final Bundle arguments = getArguments();
        if(arguments != null) {

        }
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.pagerlayout, container, false);
        final ImageView backgroundImage = (ImageView) view.findViewById(R.id.detailsImageView);
        Picasso.with(con)
                .load(imageUR)
                .placeholder(R.drawable.ic_launcher)
                .error(R.drawable.ic_launcher)
                .into(backgroundImage);
        return view;
    }


}
